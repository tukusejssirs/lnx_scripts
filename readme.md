# Linux scripts

Scripts used on Linux by me. Some of them I created, others I found on the Internet. Most of them are BASH scripts.

[![Liberapay](https://flat.badgen.net/liberapay/patrons/tukusejssirs?icon=https://liberapay.com/assets/liberapay/icon-v2_white-on-yellow.svg)](https://liberapay.com/tukusejssirs/donate)