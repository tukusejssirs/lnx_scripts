# GNU Nano configuration file



# Don't automatically hard-wrap the current line when it becomes overlong
unset breaklonglines

# Spread overlong lines over multiple screen lines
set softwrap

# When soft line wrapping is enabled, make it wrap lines at blanks (tabs and spaces) instead of always at the edge of the screen
set atblanks

# Don't automatically add a final newline when a file does not end with one
set nonewlines

# Make the 'nextword' function (^Right) stop at word beginnings
unset afterends

# Don't convert typed tabs to spaces
unset tabstospaces

# Make the Home key smarter, i.e. when Home is pressed anywhere but at the very beginning of non-whitespace characters on a line, the cursor will jump to that beginning (either forwards or backwards). If the cursor is already at that position, it will jump to the true beginning of the line
set smarthome

# Automatically indent a newly created line to the same number of tabs and/or spaces as the preceding line or as the next line if the preceding line is the beginning of a paragraph
set autoindent

# Back up files to the current filename plus a tilde
set backup

# Don't do case-sensitive searches by default
unset casesensitive

# Convert files from DOS/Mac format
unset noconvert

# Save a file by default in Unix format
# Note: This overrides nano's default behavior of saving a file in the format that it had
# Note: This option is overriden by `set noconvert`
set unix

# Display the helpful shortcut lists at the bottom of the screen
unset nohelp

# Constantly display the cursor position in the status bar
# Note: This overrides "quickblank"
set constantshow

# Use the line below the title bar
unset emptyline

# Use this tab size instead of the default
# Note: It must be greater than 0
set tabsize 2

# Display line numbers to the left of the text
set linenumbers

# Scroll the buffer contents per line
unset jumpyscrolling

# Enable mouse support, if available for your system. When enabled, mouse clicks can be used to place the cursor, set the mark (with a double click), and execute shortcuts. The mouse will work in the X Window System, and on the console when gpm is running
set mouse

# Make ^Backspace delete the word to the left of the cursor
# Note: On some terminals the <Ctrl+Backspace> keystroke produces ^H, which is
# the ASCII character for backspace, so it is bound by default to the
# backspace function, therefore this might not work on all terminals
bind ^H chopwordleft main

# Custom keybinding for commenting out current line (or the selected lines)
bind ^/ comment main

# Include syntax definitions
include "/usr/share/nano/*.nanorc"

# The opening and closing brackets that can be found by bracket searches
# Note: They cannot contain blank characters
# Note: The former set must come before the latter set, and both must be in the same order
set matchbrackets "(<[{`)>]}`"

# The characters treated as closing brackets when justifying paragraphs
# Note: This may not include any blank characters
# Note: Only closing punctuation, optionally followed by these closing brackets, can end sentences
set brackets ""')>]}`"