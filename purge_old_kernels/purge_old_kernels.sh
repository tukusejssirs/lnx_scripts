#!/bin/bash

# This script purges old kernels on Debian-like systems.
# It purges kernel only if the number of currently installed kernels is more than number kernels to keep.

# author:  Tukusej's Sirs; original authors of original script listed below

#    purge-old-kernels - remove old kernel packages
#    Copyright (C) 2012 Dustin Kirkland <kirkland@ubuntu.com>
#
#    Authors: Dustin Kirkland <kirkland@ubuntu.com>
#             Kees Cook <kees@ubuntu.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Configurable variables
kernels_to_keep=2
# log_file=  # path + machine name + script name + date + time + .log; e.g.: /etc/zutom/logs/$HOSTNAME_sys_update_%Y%m%d_%H%M.log

kernel_current="$(uname -r | sed 's/-[^\-]*$//' -)"
kernels_installed=$(/bin/ls -f /boot/vmlinuz-* | grep -v '.efi.signed\|rescue' | sed -e 's/[^\-]*-//;s/-[^\-]*$//' | sort -rh)
kernel_latest="$(/bin/echo "$kernels_installed" | head -1)"
purged_kernels=0

if [ "$kernel_latest" = "$kernel_current" ]; then
	# Remove first n kernels (n = $kernels_to_keep)
	kernels_to_remove=$(/bin/echo "$kernels_installed" | sed "1,${kernels_to_keep}d")
else
	# Reboot and run this script again
	/bin/echo "We need to reboot in order to use the latest kernel."
	# /bin/echo "We need to reboot in order to use the latest kernel." >> $log_file
	/bin/echo "Rebooting ..."
	reboot
fi

for v in $kernels_to_remove; do
	for k in linux-image linux-image-extra linux-headers linux-signed-image linux-image-virtual; do
		for f in generic lowlatency; do
			dpkg-query -s "$k-$v-$f" >/dev/null 2>&1 && kernels_to_purge="$kernels_to_purge $k-$v-$f" && let purged_kernels++
		done
	done
done

if [ -z "$kernels_to_purge" ]; then
	/bin/echo "No kernels are eligible for removal."
	# /bin/echo "No kernels are eligible for removal" >> $log_file
	exit 0
fi

# Use apt-get, rather than apt, for compatibility with precise/trusty
if ! apt-get -y install --fix-broken --dry-run >/dev/null 2>&1; then
	/bin/echo "APT is broken, trying with dpkg ..."
	# /bin/echo "APT is broken, trying with dpkg" >> $log_file
	dpkg --purge $kernels_to_purge
	apt-get -y install --fix-broken
fi
apt-get -y remove --purge $kernels_to_purge
apt-get -y autoremove --purge
if [ $purged_kernels -gt 1 ]; then
	/bin/echo "Purged $purged_kernels kernel(s)."
	# /bin/echo "Purged $purged_kernels kernel(s)." >> $log_file
else
	/bin/echo "Purged $purged_kernels kernels."
	# /bin/echo "Purged $purged_kernels kernels." >> $log_file
fi











# Create service to start the script after reboot
/bin/echo -e "[Unit]\nDescription=\"Purge old kernels after reboot\"\n\n[Service]\nExecStart=$(realpath $(dirname $0))/$(basename $0)\nType=oneshot\nRemainAfterExit=yes\n\n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/purge_old_kernels.service
systemctl daemon-reload
systemctl enable purge_old_kernels &>/dev/null


########################################
# test service
########################################
# Create service to start the script after reboot
/bin/echo -e "[Unit]\nDescription=\"Purge old kernels after reboot\"\n\n[Service]\nExecStart=/home/ts/test.sh\nExecStart=/home/ts/serv_rem.sh\nType=oneshot\nRemainAfterExit=yes\n\n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/purge_old_kernels.service
systemctl daemon-reload
systemctl enable purge_old_kernels &>/dev/null




######
# remove service
systemctl stop purge_old_kernels
systemctl disable purge_old_kernels
/bin/rm /etc/systemd/system/purge_old_kernels.service
systemctl daemon-reload
systemctl reset-failed

##############

[Unit]
Description=\"Purge old kernels after reboot\"

[Service]
ExecStart=/home/ts/test.sh
ExecStart=/home/ts/serv_rem.sh
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target


zimbra
plesk
+vpn
+itop