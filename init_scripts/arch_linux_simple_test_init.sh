#!/bin/bash

# Arch Linux installation script

# TODO
# - [$NW] test gpt + lvm + uefi;
# - [   ] test gpt + uefi;


# Variables
TMP='/dev/shm'
KBLO='uk'
ENABLE_NTP='true'
TIMEZONE='Europe/Bratislava'
LOCALE='en_GB.UTF-8'
DISK='/dev/sda'
PARTITION_TABLE='gpt'  # gpt | msdos; Note that `msdos` is not tested at all
PART_BOOT_SIZE='1.5GiB'  # Use `GiB` or `MiB` (or similar) for unit
LVM_LV_SWAP_SIZE='2G'  # Use `G` or `M` (or similar) for unit
NOT_ROOT_USER_NAME='ts'
LVM_VG_NAME='vg_arch'

# Change the keyboard layout to English (UK)
loadkeys "$KBLO"

# Update the system clock
timedatectl set-ntp "$ENABLE_NTP"

# Change the timezone
timedatectl set-timezone "$TIMEZONE"

# Make sure no LVM VG is active
vgchange -an

# Create new partition table
parted -s "$DISK" mklabel gpt

# Create /boot partition
# FIXME: THIS IS NOT BOOTABLE !!!
parted -s "$DISK" mkpart primary fat32 1MiB "$PART_BOOT_SIZE"

# Set the boot flag on this (first) partition
# parted -s "$DISK" set 1 boot on
parted -s "$DISK" set 1 esp on

# Use rest in LVM2 formatted as ext4
parted -s "$DISK" mkpart primary ext4 "$PART_BOOT_SIZE" 100%

# Set the LVM flag on this (second) partition
parted -s "$DISK" set 2 lvm on

# Create a physical volume
# pvcreate -ff "${DISK}2"
pvcreate "${DISK}2"

# Create a volume group
vgcreate "$LVM_VG_NAME" "${DISK}2"

# Activate a volume group
vgchange -ay "$LVM_VG_NAME"

# Create a logical volume for the swap partition
lvcreate -L "$LVM_LV_SWAP_SIZE" "$LVM_VG_NAME" -n swap

# Create a logical volume for the root partition
lvcreate -l +100%FREE "$LVM_VG_NAME" -n root

# Format the boot partition
mkfs.fat -F32 "${DISK}1"

# Label the boot partition
fatlabel "${DISK}1" boot

# Format the root partition
mkfs.ext4 "/dev/$LVM_VG_NAME/root"

# Label the root partition
e2label "/dev/$LVM_VG_NAME/root" root

# Format the swap partition
mkswap -L swap "/dev/$LVM_VG_NAME/swap"

# Mount root at /mnt
mkdir /mnt
mount "/dev/$LVM_VG_NAME/root" /mnt

# Mount boot at /mnt/boot
mkdir /mnt/boot
mount "${DISK}1" /mnt/boot

# Enable swap
swapon "/dev/$LVM_VG_NAME/swap"

# Install the essential packages
# base            # Arch base package
# linux           # Linux kernel and modules
# linux-firmware  # Firmware for common hardware
pacstrap /mnt base linux linux-firmware

# Generate `fstab`
genfstab -U /mnt >> /mnt/etc/fstab

# Change the keyboard layout to English (UK)
cat << EOF > /mnt/root/conf.sh
loadkeys "$KBLO"
EOF

# Update the system clock
cat << EOF > /mnt/root/conf.sh
timedatectl set-ntp "$ENABLE_NTP"
EOF

# Change the timezone
cat << EOF > /mnt/root/conf.sh
timedatectl set-timezone "$TIMEZONE"
EOF

# Set locale
# FIXME: This does not work yet
cat << EOF > /mnt/root/conf.sh
sed -i "s/#$LOCALE/$LOCALE/" /etc/locale.gen
locale-gen
localectl set-locale LANG=$LOCALE
localectl set-locale $LOCALE
EOF

# Upgrade packages
cat << EOF > /mnt/root/conf.sh
pacman -Syyu --noconfirm
EOF

# Install `reflector` and `dmidecode`
cat << EOF >> /mnt/root/conf.sh
pacman -S --noconfirm reflector dmidecode
EOF

# Get a list of mirrors
# Use only mirrors in SK, CZ, HU and A
# Of them, use (at most) 20 fastest mirrors
# Use https protocol
# Sort the mirrors by rate
cat << EOF >> /mnt/root/conf.sh
reflector --country Slovakia,Czechia,Hungary,Austria -p https -f 20 --sort rate --save /etc/pacman.d/mirrorlist
# reflector -p https -l 200 -f 20 --sort rate --save /etc/pacman.d/mirrorlist
EOF

# Configure reflector timer service
cat << EOFILE >> /mnt/root/conf.sh
cat << EOF > /etc/xdg/reflector/reflector.conf
# Use only mirrors in SK, CZ, HU and A
--country Slovakia,Czechia,Hungary,Austria

# Use https protocol
-p https

# Of them, use (at most) 20 fastest mirrors
-f 20

# Sort the mirrors by rate
--sort rate

# Save the output to the pacman configuration folder
--save /etc/pacman.d/mirrorlist
EOF
EOFILE

# Enable the `reflector` and `reflector.timer` services
cat << EOF >> /mnt/root/conf.sh
systemctl enable --now reflector reflector.timer
EOF

# Create a folder for `mirrorupgrade.hook`
cat << EOF >> /mnt/root/conf.sh
mkdir -p /etc/pacman.d/hooks
EOF

# Create `mirrorupgrade.hook`
cat << EOFILE >> /mnt/root/conf.sh
cat << EOF > /etc/pacman.d/hooks/mirrorupgrade.hook
[Trigger]
Operation = Upgrade
Type = Package
Target = pacman-mirrorlist

[Action]
Description = Updating pacman-mirrorlist with reflector and removing pacnew...
When = PostTransaction
Depends = reflector
Exec = /bin/sh -c 'systemctl start reflector; if [ -f /etc/pacman.d/mirrorlist.pacnew ]; then rm /etc/pacman.d/mirrorlist.pacnew; fi'
EOF
EOFILE

# Configure the clock
# FIXME: Decide what is the best approach: set hardware clock to UTC or local time? Set it up from the OS or from BIOS?
# FIXME: Check if we are running in a VM (then don't configure the hardware clock at all)
# src: https://wiki.archlinux.org/index.php/Installation_guide#Time_zone
# cat << EOF >> /mnt/root/conf.sh
# hwclock --systohc
# EOF

# Install more packages
# TODO: Add packages for:
# - firmware/modules/drivers:
# 	- wifi: https://wiki.archlinux.org/index.php/Broadcom_wireless
# - opt_dep for links/xlinks: libx11 libtiff libpng librsvg;
# - tar;
# - ??? bzip2 cabextract unace [archivers];
# - opt_dep for openssh: xorg-xauth (X11 forwarding);
# -  cmake{,3} meson ninja (the dev tools not in `base-devel` group);
# - possibly missing firmware for modules: wd719x aic94xx xhci_pci;
# - ??? chrony;
# - ??? cpulimit;
# - fontawesome-free-fonts ### sudo dnf -y copr enable vishalvvr/fontawesome-fonts;
# - ??? gourmet;
# - ??? libffi-devel;
# - ??? libhandy;
# - ??? libyaml;
# - megasync;
# - node.js:
# 	- https://wiki.archlinux.org/index.php/Node.js
# 	- https://github.com/nodesource/distributions#installation-instructions-1
# - ??? selinux + policycoreutils policycoreutils-gui setools-console (see https://wiki.archlinux.org/index.php/SELinux):
# 	- https://wiki.archlinux.org/index.php/SELinux
# 	- https://wiki.archlinux.org/index.php/Security
# - ??? pygobject2;
# - ??? ruby-devel;
# - ??? rust;
# - scantailor (from src);
# - ??? playerctl;
# - yay -S wlogout
# alien
# bison
# ocrmypdf
# patch
# pdf-stapler
# sqlite-devel
# yaru-theme
# FIXME: `broadcom-wl` and `broadcom-wl-dkms` are is conflict with each other. Test which one I need.
cat << EOF >> /mnt/root/conf.sh
pacman -Syy --noconfirm --needed autoconf automake base-devel bash bash-completion bc broadcom-wl-dkms chrome-gnome-shell dhclient diffutils dkms dnsmasq dosfstools efibootmgr eog epiphany espeak evince exfatprogs file-roller gcc gdm gedit git gnome-calculator gnome-characters gnome-clocks gnome-color-manager gnome-contacts gnome-control-center gnome-disk-utility gnome-font-viewer gnome-keyring gnome-logs gnome-screenshot gnome-session gnome-settings-daemon gnome-shell gnome-terminal gnome-themes-extra gnome-tweaks gparted htop intel-ucode libtool linux-headers lshw lvm2 make man-db man-pages meson mkinitcpio mkinitcpio-nfs-utils mlocate mtools mutter nano nautilus networkmanager nfs-utils nodejs noto-fonts noto-fonts-emoji npm openssh openssl p7zip parted pavucontrol perl php python python-html2text readline reflector rsync sshpass sudo sway swaylock syslinux systemd-resolvconf tcpdump usb_modeswitch usbutils waybar wayland wireless-regdb wireless_tools wl-clipboard wpa_supplicant xclip xdg-user-dirs-gtk zlib xorg xorg-server xsane
EOF
# TODO: Consider adding:
# - org.freedesktop.secrets (op_dep for libsecret)

# Variables that depend on PC_MODEL
# TODO: Make this more portable
cat << EOF >> /mnt/root/conf.sh
PC_MODEL="\$(dmidecode -t system | grep -Po 'Version: [ThinkPad Edge]* \K.*$' | tr '[:upper:]' '[:lower:]')"
HOST_NAME="\$PC_MODEL.domcek.lan"
EOF

# Set up hostname
cat << EOF >> /mnt/root/conf.sh
hostnamectl set-hostname "\$HOST_NAME"
EOF

# Set swappiness to 10 %
# Default: 60
cat << EOF >> /mnt/root/conf.sh
sysctl vm.swappiness=10 > /dev/null
EOF

# # Disable handling of the power button by SystemD
# # Note: I use `wlogout` for interactive power button behaviour and merely disabling this makes it working as I want it.
# # TODO: Use the script instead of command.
# cat << EOF >> /mnt/root/conf.sh
# # $XDG_GIT_DIR/lnx_scripts/systemd/disable_power_button_handling.sh
# sudo sed -i 's/^HandlePowerKey=.*$/HandlePowerKey=ignore/' /etc/systemd/logind.conf
# EOF

# # Disable suspending when the lid is closed
# cat << EOF >> /mnt/root/conf.sh
# sed -i 's/^#*\(HandleLidSwitch\)=.*$/\1=ignore/;
# 	s/^#*\(HandleLidSwitchExternalPower\)=.*$/\1=ignore/;
# 	s/^#*\(HandleLidSwitchDocked\)=.*$/\1=ignore/;
# 	s/^#*\(IdleAction\)=.*$/\1=ignore/' /etc/systemd/logind.conf
# EOF

# Create the non-root user and set their preferences and permissions
# TODO: CHANGE THE `PW` DEFINITION !!!
# TODO: Initialise Git
# TODO: Update `bash_init` to initialise Bash in `/etc/skel` before we create this user
# TODO: Uncomment `setfacl` command
cat << EOF >> /mnt/root/conf.sh
# PW="\$(node "$INSTALL_PATH/build/bw.js" list items --search "$NOT_ROOT_USER_NAME@\$PC_MODEL" | jq -r .[].login.password)"
PW='2e5bf2li'
PW_HASH="\$(python -c "import crypt, getpass; pw = '$PW'; print(crypt.crypt(pw))")"
# useradd "$NOT_ROOT_USER_NAME" -rmp "\$PW_HASH"
useradd -rmUG wheel "$NOT_ROOT_USER_NAME" -p "\$PW_HASH"
# usermod -aG wheel "$NOT_ROOT_USER_NAME"
# setfacl -Rm u:$NOT_ROOT_USER_NAME:rwx /bzr /opt /svn "\$XDG_GIT_DIR"

# "\$XDG_GIT_DIR/lnx_scripts/init_scripts/git_init.sh"
EOF

# Create additional sudoers configuration file
cat << EOFILE >> /mnt/root/conf.sh
cat << EOF > "/etc/sudoers.d/$NOT_ROOT_USER_NAME"
Defaults editor=/usr/bin/nano
$NOT_ROOT_USER_NAME ALL = (ALL) NOPASSWD: ALL
EOF
EOFILE

# Install `yay` as AUR helper
# TODO: Run this after `bash_init.sh`
# FIXME: `yay` cannot be made in `/root` nor by `root`
# FIXME: while building, it asks for the non-root user password
# FIXME: `makepkg` still asks for the non-root user password, even with `sudo -S`
cat << EOF >> /mnt/root/conf.sh
sudo -u "$NOT_ROOT_USER_NAME" git clone -q https://aur.archlinux.org/yay-git.git "/home/$NOT_ROOT_USER_NAME/yay"
sudo -u "$NOT_ROOT_USER_NAME" bash -c "cd '/home/$NOT_ROOT_USER_NAME/yay' && makepkg -si --noconfirm"
EOF

# Update AUR packages
cat << EOF >> /mnt/root/conf.sh
sudo -u "$NOT_ROOT_USER_NAME" bash -c 'yay -Syyu'
EOF

# Install packages from AUR
cat << EOF >> /mnt/root/conf.sh
sudo -u "$NOT_ROOT_USER_NAME" bash -c 'yay -Sy --noconfirm wlogout etherwake'
EOF

# Create `root` password
# TODO: CHANGE THE `PW` DEFINITION !!!
cat << EOF >> /mnt/root/conf.sh
# PW="\$(node "$INSTALL_PATH/build/bw.js" list items --search "root@\$PC_MODEL" | jq -r .[].login.password)"
PW='2e5bf2li'
echo "root:\$PW" | chpasswd
EOF

# Create additional sudoers configuration file
cat << EOF >> /mnt/root/conf.sh
sed -i 's/^$NOT_ROOT_USER_NAME ALL/# &/' "/etc/sudoers.d/$NOT_ROOT_USER_NAME"
EOF

# Update `/var/lib/mlocate/mlocate.db` (else `locate` won't work)
cat << EOF >> /mnt/root/conf.sh
updatedb
EOF

# Change root into the new system
chmod a+x /mnt/root/conf.sh
arch-chroot /mnt /root/conf.sh