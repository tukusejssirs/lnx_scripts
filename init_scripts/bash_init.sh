#!/bin/bash

# Bash files initialisation script


# Define XDG_GIT_DIR if not defined
if [ ! "$XDG_GIT_DIR" ]; then
	source "$HOME/.config/user-dirs.dirs" &> /dev/null

	if [ ! "$XDG_GIT_DIR" ]; then
		XDG_GIT_DIR='/git'
		echo "[WARNING] XDG_GIT_DIR is not defined. Using $XDG_GIT_DIR." 1>&2
	fi
fi

# Remove old files
rm -rf "$HOME/.bash_profile" "$HOME/.bashrc" "$HOME/.bash_logout"

# Create symlinks to the new files
ln -s "$XDG_GIT_DIR/lnx_scripts/bash/bashrc/bashrc_stripped.sh" "$HOME/.bashrc"
ln -s "$HOME/.bashrc" "$HOME/.bash_profile"
ln -s "$XDG_GIT_DIR/lnx_scripts/bash_logout/.bash_logout" "$HOME/.bash_logout"