# Download `lnx_scripts` repository
curl -o ~/Desktop/lnx_scripts.zip https://gitlab.com/tukusejssirs/lnx_scripts/-/archive/master/lnx_scripts-master.zip

# Extract `lnx_scripts` repository
mkdir ~/git
Expand-Archive -Path ~/Desktop/lnx_scripts.zip -DestinationPath ~/Desktop
rm ~/Desktop/lnx_scripts.zip

# Set the Execution Policy to Unrestricted
type ../windows/settings/set_execution_policy_unrestricted.ps1 | powershell -noprofile -

# Enable system-wide dark theme
../windows/settings/dark_theme_system-wide.ps1

# Disable UAC
../windows/settings/disable_uac.ps1

# Enable RDP
../windows/settings/enable_rdp.ps1

# Set path environment variable
../windows/settings/set_path_variable.ps1

# Set up timezone
../windows/settings/set_timezone.ps1

# Enable WSL
../windows/installers/wsl.ps1

# Install Chocolatey
../windows/installers/chocolatey.ps1

# Install packages via Chocolatey
# Note: `vcredist140` is Microsoft Visual C++ Redistributable for Visual Studio.
choco install -y 7zip git megasync vcredist140 vlc set_timezone choco-upgrade-all-at-startup curl jq yq grep sed

# TODO: Configure `choco-upgrade-all-at-startup` (delete desktop shorcuts, default user profile is `ts`).

# TODO: If `Lenovo Thinkpad P15 Gen 1`, install drivers from here → https://pcsupport.lenovo.com/rs/en/products/laptops-and-netbooks/thinkpad-p-series-laptops/thinkpad-p15-type-20st-20su/downloads/driver-list/

# Set the hostname
$hostname = ((Get-ComputerInfo).CsSystemFamily | sed 's/Thinkpad//i;s/gen [0-9]\+/i;s/^ \+//;s/ \+$//;s/ \+/-/g;s/$/-win/').tolower()
Rename-Computer -NewName "$hostname"

# TODO: Manually:
# - configure display arrangement (monitors);