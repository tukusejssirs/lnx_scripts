#!/bin/bash

# Arch Linux installation script

# Note: Use an empty HDD/SSD/vDisk disk

# Dependencies:
# TODO

# TODO
# - change the console font to Ubuntu Mono;
# - install wifi, bluetooth and fingerprint drivers for e531;
# - connect to wifi network: https://wiki.archlinux.org/index.php/Iwd#iwctl;
# 	- consider using `nmcli`;


# TODO: remove me
START_DATE="$(date)"

# Variables
TMP='/dev/shm'
KBLO='uk'
ENABLE_NTP='true'
TIMEZONE='Europe/Bratislava'
LOCALE='en_GB.UTF-8'
DISK='/dev/sda'
PARTITION_TABLE='gpt'  # gpt | msdos; Note that `msdos` is not tested at all
PART_BOOT_SIZE='1.5GiB'  # Use `GiB` or `MiB` (or similar) for unit
LVM_LV_SWAP_SIZE='4G'  # Use `G` or `M` (or similar) for unit
NOT_ROOT_USER_NAME='ts'
LVM_VG_NAME='vg_arch'

# Change the keyboard layout to English (UK)
loadkeys "$KBLO"

# Update the system clock
timedatectl set-ntp "$ENABLE_NTP"

# Change the timezone
timedatectl set-timezone "$TIMEZONE"

# Make sure no LVM VG is active
vgchange -an

# Create new partition table
parted -s "$DISK" mklabel gpt

# Create /boot partition
# FIXME: THIS IS NOT BOOTABLE !!!
parted -s "$DISK" mkpart primary fat32 1MiB "$PART_BOOT_SIZE"

# Set the boot flag on this (first) partition
parted -s "$DISK" set 1 boot on

# Use rest in LVM2 formatted as ext4
parted -s "$DISK" mkpart primary ext4 "$PART_BOOT_SIZE" 100%

# Set the LVM flag on this (second) partition
parted -s "$DISK" set 2 lvm on

# Create a physical volume
# pvcreate -ff "${DISK}2"
pvcreate "${DISK}2"

# Create a volume group
vgcreate "$LVM_VG_NAME" "${DISK}2"

# Activate a volume group
vgchange -ay "$LVM_VG_NAME"

# Create a logical volume for the swap partition
lvcreate -L "$LVM_LV_SWAP_SIZE" "$LVM_VG_NAME" -n swap

# Create a logical volume for the root partition
lvcreate -l +100%FREE "$LVM_VG_NAME" -n root

# Format the boot partition
mkfs.fat -F32 "${DISK}1"

# Label the boot partition
fatlabel "${DISK}1" boot

# Format the root partition
mkfs.ext4 "/dev/$LVM_VG_NAME/root"

# Label the root partition
e2label "/dev/$LVM_VG_NAME/root" root

# Format the swap partition
mkswap -L swap "/dev/$LVM_VG_NAME/swap"

# Mount root at /mnt
mkdir /mnt
mount "/dev/$LVM_VG_NAME/root" /mnt

# Mount boot at /mnt/boot
mkdir /mnt/boot
mount "${DISK}1" /mnt/boot

# Enable swap
swapon "/dev/$LVM_VG_NAME/swap"

# Install the essential packages
# base            # Arch base package
# linux           # Linux kernel and modules
# linux-firmware  # Firmware for common hardware
pacstrap /mnt base linux linux-firmware

# Generate `fstab`
genfstab -U /mnt >> /mnt/etc/fstab

# Change the keyboard layout to English (UK)
cat << EOF > /mnt/root/conf.sh
loadkeys "$KBLO"
EOF

# Update the system clock
cat << EOF > /mnt/root/conf.sh
timedatectl set-ntp "$ENABLE_NTP"
EOF

# Change the timezone
cat << EOF > /mnt/root/conf.sh
timedatectl set-timezone "$TIMEZONE"
EOF

# Set locale
# FIXME: This does not work yet
cat << EOF > /mnt/root/conf.sh
sed -i "s/#$LOCALE/$LOCALE/" /etc/locale.gen
locale-gen
# localectl set-locale LANG=$LOCALE
# localectl set-locale $LOCALE
EOF

# Upgrade packages
cat << EOF > /mnt/root/conf.sh
pacman -Syyu --noconfirm
EOF

# Install `reflector` and `dmidecode`
cat << EOF >> /mnt/root/conf.sh
pacman -S --noconfirm reflector dmidecode
EOF

# Get a list of mirrors
# Use only mirrors in SK, CZ, HU and A
# Of them, use (at most) 20 fastest mirrors
# Use https protocol
# Sort the mirrors by rate
cat << EOF >> /mnt/root/conf.sh
pacman -Sy --noconfirm reflector
reflector --country Slovakia,Czechia,Hungary,Austria --latest 25 --age 24 --protocol https --completion-percent 100 --sort rate --save /etc/pacman.d/mirrorlist
EOF

# Configure reflector timer service
cat << EOFILE >> /mnt/root/conf.sh
cat << EOF > /etc/xdg/reflector/reflector.conf
# Use only mirrors in SK, CZ, HU and A
--country Slovakia,Czechia,Hungary,Austria

# Use https protocol
-p https

# Of them, use (at most) 20 fastest mirrors
-f 20

# Sort the mirrors by rate
--sort rate

# Save the output to the pacman configuration folder
--save /etc/pacman.d/mirrorlist
EOF
EOFILE

# Enable the `reflector` and `reflector.timer` services
cat << EOF >> /mnt/root/conf.sh
systemctl enable --now reflector reflector.timer
EOF

# Create a folder for `mirrorupgrade.hook`
cat << EOF >> /mnt/root/conf.sh
mkdir -p /etc/pacman.d/hooks
EOF

# Create `mirrorupgrade.hook`
cat << EOFILE >> /mnt/root/conf.sh
cat << EOF > /etc/pacman.d/hooks/mirrorupgrade.hook
[Trigger]
Operation = Upgrade
Type = Package
Target = pacman-mirrorlist

[Action]
Description = Updating pacman-mirrorlist with reflector and removing pacnew...
When = PostTransaction
Depends = reflector
Exec = /bin/sh -c 'systemctl start reflector; if [ -f /etc/pacman.d/mirrorlist.pacnew ]; then rm /etc/pacman.d/mirrorlist.pacnew; fi'
EOF
EOFILE

# Configure the clock
# FIXME: Decide what is the best approach: set hardware clock to UTC or local time? Set it up from the OS or from BIOS?
# FIXME: Check if we are running in a VM (then don't configure the hardware clock at all)
# src: https://wiki.archlinux.org/index.php/Installation_guide#Time_zone
# cat << EOF >> /mnt/root/conf.sh
# hwclock --systohc
# EOF


# Variables that depend on PC_MODEL
# TODO: Make this more portable
cat << EOF >> /mnt/root/conf.sh
PC_MODEL="\$(dmidecode -t system | grep -Po 'Version: [ThinkPad Edge]* \K.*$' | tr '[:upper:]' '[:lower:]')"
HOST_NAME="\$PC_MODEL.domcek.lan"
EOF

# Set up hostname
cat << EOF >> /mnt/root/conf.sh
hostnamectl set-hostname "\$HOST_NAME"
EOF

# Set swappiness to 10 %
# Default: 60
cat << EOF >> /mnt/root/conf.sh
sysctl vm.swappiness=10 > /dev/null
EOF

# Disable handling of the power button by SystemD
# Note: I use `wlogout` for interactive power button behaviour and merely disabling this makes it working as I want it.
# TODO: Use the script instead of command.
cat << EOF >> /mnt/root/conf.sh
# $XDG_GIT_DIR/lnx_scripts/systemd/disable_power_button_handling.sh
sudo sed -i 's/^HandlePowerKey=.*$/HandlePowerKey=ignore/' /etc/systemd/logind.conf
EOF

# Disable suspending when the lid is closed
cat << EOF >> /mnt/root/conf.sh
sed -i 's/^#*\(HandleLidSwitch\)=.*$/\1=ignore/;
	s/^#*\(HandleLidSwitchExternalPower\)=.*$/\1=ignore/;
	s/^#*\(HandleLidSwitchDocked\)=.*$/\1=ignore/;
	s/^#*\(IdleAction\)=.*$/\1=ignore/' /etc/systemd/logind.conf
EOF

# LD config paths
# TODO: Create a script for this
cat << EOF >> /mnt/root/conf.sh
sudo bash -c "echo '/usr/lib' > /etc/ld.so.conf.d/usr_lib.conf"
sudo bash -c "echo '/usr/lib64' > /etc/ld.so.conf.d/usr_lib64.conf"
sudo bash -c "echo '/usr/local/lib' > /etc/ld.so.conf.d/usr_local_lib.conf"
sudo bash -c "echo '/usr/local/lib64' > /etc/ld.so.conf.d/usr_local_lib64.conf"
EOF

# Remove htop.desktop (I don't want it in the Gnome overview)
cat << EOF >> /mnt/root/conf.sh
rm -rf /usr/share/applications/htop.desktop
EOF

# # Clone `lnx_scripts` repo
# cat << EOF >> /mnt/root/conf.sh
# git clone https://gitlab.com/tukusejssirs/lnx_scripts.git "$TMP/lnx_scripts"
# EOF

# # Change user folder names
# cat << EOF >> /mnt/root/conf.sh
# mkdir -p "$HOME/.config"
# if [ "$(uname -r | grep -o 'Microsoft$')" != 'Microsoft' ]; then
# 	if [ -f "$HOME/.config/user-dirs.dirs" ]; then
# 		mv "$HOME/.config/user-dirs.dirs{,.bak}"
# 	fi

# 	if [ -f "$HOME/.config/user-dirs.locale" ]; then
# 		mv "$HOME/.config/user-dirs.locale{,.bak}"
# 	fi

# 	for n in $(cat "$TMP/lnx_scripts/user-dirs/user-dirs.dirs_${USER}" | grep -oP "^XDG[^=]*=\"\K[^\"]*" | tr '\n' ' ' | sed "s#\$HOME#$HOME#g" -); do
# 		mkdir -p "$n"
# 	done

# 	cp "$TMP/lnx_scripts/user-dirs/user-dirs.dirs_${USER}" "$HOME/.config/user-dirs.dirs"
# 	cp "$TMP/lnx_scripts/user-dirs/user-dirs.locale_${USER}" "$HOME/.config/user-dirs.locale"
# fi

# mkdir -p "$(grep -Po '^XDG.*="\K[^"]*' "$HOME/.config/user-dirs.dirs" | sed "/^\/git$/d;s|\$HOME|$HOME|g" | tr '\n' ' ')"

# source "$HOME/.config/user-dirs.dirs"
# EOF

# # Create folders for `git`, `bzr` and `svn`
# # TODO: Define XDG_SVN_DIR and XDG_BZR_DIR
# cat << EOF >> /mnt/root/conf.sh
# mkdir -p /svn /bzr "$XDG_GIT_DIR"
# EOF

# # Move `lnx_scripts` repo to XDG_GIT_DIR
# cat << EOF >> /mnt/root/conf.sh
# mv "$TMP/lnx_scripts" "$XDG_GIT_DIR"
# EOF

# # Deal with the original user folders
# # TODO: Move this into `xdg_init.sh` script
# cat << EOF >> /mnt/root/conf.sh
# LIST_XDG_DIRS="$(grep -o '^\s*[^#][^=]*' "$HOME/.config/user-dirs.dirs")"

# for n in $LIST_XDG_DIRS; do
# 	case "${!n}" in
# 		XDG_DESKTOP_DIR)
# 			PARENT_DIR="$(dirname "$XDG_DESKTOP_DIR")"
# 			BASE_DIR="$(basename "$XDG_DESKTOP_DIR")"

# 			if [ "$BASE_DIR" = 'Desktop' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Desktop" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Desktop" "$XDG_DESKTOP_DIR"
# 			fi
# 		;;
# 		XDG_DOCUMENTS_DIR)
# 			PARENT_DIR="$(dirname "$XDG_DOCUMENTS_DIR")"
# 			BASE_DIR="$(basename "$XDG_DOCUMENTS_DIR")"

# 			if [ "$BASE_DIR" = 'Documents' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Documents" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Documents" "$XDG_DOCUMENTS_DIR"
# 			fi
# 		;;
# 		XDG_DOWNLOAD_DIR)
# 			PARENT_DIR="$(dirname "$XDG_DOWNLOAD_DIR")"
# 			BASE_DIR="$(basename "$XDG_DOWNLOAD_DIR")"

# 			if [ "$BASE_DIR" = 'Downloads' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Downloads" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Downloads" "$XDG_DOWNLOAD_DIR"
# 			fi
# 		;;
# 		XDG_MUSIC_DIR)
# 			PARENT_DIR="$(dirname "$XDG_MUSIC_DIR")"
# 			BASE_DIR="$(basename "$XDG_MUSIC_DIR")"

# 			if [ "$BASE_DIR" = 'Music' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Music" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Music" "$XDG_MUSIC_DIR"
# 			fi
# 		;;
# 		XDG_PICTURES_DIR)
# 			PARENT_DIR="$(dirname "$XDG_PICTURES_DIR")"
# 			BASE_DIR="$(basename "$XDG_PICTURES_DIR")"

# 			if [ "$BASE_DIR" = 'Pictures' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Pictures" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Pictures" "$XDG_PICTURES_DIR"
# 			fi
# 		;;
# 		XDG_TEMPLATES_DIR)
# 			PARENT_DIR="$(dirname "$XDG_TEMPLATES_DIR")"
# 			BASE_DIR="$(basename "$XDG_TEMPLATES_DIR")"

# 			if [ "$BASE_DIR" = 'Templates' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Templates" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Templates" "$XDG_TEMPLATES_DIR"
# 			fi
# 		;;
# 		XDG_VIDEOS_DIR)
# 			PARENT_DIR="$(dirname "$XDG_VIDEOS_DIR")"
# 			BASE_DIR="$(basename "$XDG_VIDEOS_DIR")"

# 			if [ "$BASE_DIR" = 'Videos' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Videos" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Videos" "$XDG_VIDEOS_DIR"
# 			fi
# 		;;
# 		*)
# 			mkdir -p "${!n}" 2> /dev/null
# 		;;
# 	esac
# done
# EOF

# # Bash initialisation
# cat << EOF >> /mnt/root/conf.sh
# TEST_BASH="$(whereis bash | grep -Po 'bash: \K.*$')"

# if [ "$TEST_BASH" ]; then
# 	"$XDG_GIT_DIR/lnx_scripts/init_scripts/bash_init.sh" "$XDG_GIT_DIR"
# fi
# EOF

# # Build and install Git from source
# # TODO: Do I really want to build Git from source or will the `pacman` version be enough?
# # cat << EOF >> /mnt/root/conf.sh
# # "$XDG_GIT_DIR/lnx_scripts/bash_progs/git_src.sh"
# # EOF

# # Initialise Git
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/init_scripts/git_init.sh"
# EOF

# # Change `lnx_scripts` remote url (https to git protocol transition)
# cat << EOF >> /mnt/root/conf.sh
# cd "$XDG_GIT_DIR/lnx_scripts"
# git remote rm origin
# git remote add origin git@gitlab.com:tukusejssirs/lnx_scripts.git &> /dev/null
# git branch --set-upstream-to=origin/master master &> /dev/null
# git push --set-upstream origin master &> /dev/null
# git remote set-head origin -a &> /dev/null
# EOF

# # Install `bw`
# cat << EOF >> /mnt/root/conf.sh
# "$XDG_GIT_DIR/lnx_scripts/bash_progs/bitwarden_cli.sh"
# EOF

# # Login into Bitwarden
# # TODO: Make it more scripted (i.e. login automatically), while keeping it secure (try not to use plain-text password in a variable nor in the command itself)
# cat << EOF >> /mnt/root/conf.sh
# echo "Sign into Bitwarden:"
# BW_SESSION=$(bw login | tail -1 | awk '{print $6}')
# echo $BW_SESSION > "$HOME/.bw/BW_SESSION"
# export BW_SESSION
# EOF

# Create the non-root user and set their preferences and permissions
# TODO: CHANGE THE `PW` DEFINITION !!!
# TODO: Initialise Git
# TODO: Update `bash_init` to initialise Bash in `/etc/skel` before we create this user
# TODO: Uncomment `setfacl` command
cat << EOF >> /mnt/root/conf.sh
# PW="\$(node "$INSTALL_PATH/build/bw.js" list items --search "$NOT_ROOT_USER_NAME@\$PC_MODEL" | jq -r .[].login.password)"
PW='2e5bf2li'
PW_HASH="\$(python -c "import crypt, getpass; pw = '$PW'; print(crypt.crypt(pw))")"
# useradd "$NOT_ROOT_USER_NAME" -rmp "\$PW_HASH"
useradd -rmUG wheel "$NOT_ROOT_USER_NAME" -p "\$PW_HASH"
# usermod -aG wheel "$NOT_ROOT_USER_NAME"
# setfacl -Rm u:$NOT_ROOT_USER_NAME:rwx /bzr /opt /svn "\$XDG_GIT_DIR"

# "\$XDG_GIT_DIR/lnx_scripts/init_scripts/git_init.sh"
EOF

# Create additional sudoers configuration file
cat << EOF >> /mnt/root/conf.sh
cat << END > "/etc/sudoers.d/$NOT_ROOT_USER_NAME"
Defaults editor=/usr/bin/nano
$NOT_ROOT_USER_NAME ALL = (ALL) NOPASSWD: ALL
END
EOF

# Install `yay` as AUR helper
# TODO: Run this after `bash_init.sh`
# FIXME: `yay` cannot be made in `/root` nor by `root`
# FIXME: while building, it asks for the non-root user password
# FIXME: `makepkg` still asks for the non-root user password, even with `sudo -S`
cat << EOF >> /mnt/root/conf.sh
# git clone -q https://aur.archlinux.org/yay-git.git "$XDG_GIT_DIR/others/yay"
# cd "$XDG_GIT_DIR/others/yay" && makepkg -si --noconfirm
sudo -u "$NOT_ROOT_USER_NAME" git clone -q https://aur.archlinux.org/yay-git.git "/home/$NOT_ROOT_USER_NAME/yay"
# cd ""/home/$NOT_ROOT_USER_NAME/yay"" && echo $PW | sudo -Su "$NOT_ROOT_USER_NAME" makepkg -si --noconfirm
EOF

# Update AUR packages
cat << EOF >> /mnt/root/conf.sh
yay -Syyu
EOF

# Install packages from AUR
cat << EOF >> /mnt/root/conf.sh
yay -S --noconfirm wlogout etherwake
EOF

# Create `root` password
# TODO: CHANGE THE `PW` DEFINITION !!!
cat << EOF >> /mnt/root/conf.sh
# PW="\$(node "$INSTALL_PATH/build/bw.js" list items --search "root@\$PC_MODEL" | jq -r .[].login.password)"
PW='2e5bf2li'
echo "root:\$PW" | chpasswd
EOF

# Create additional sudoers configuration file
cat << EOF >> /mnt/root/conf.sh
sed -i 's/^$NOT_ROOT_USER_NAME ALL/# &/' "/etc/sudoers.d/$NOT_ROOT_USER_NAME"
EOF

# Update `/var/lib/mlocate/mlocate.db` (else `locate` won't work)
cat << EOF >> /mnt/root/conf.sh
updatedb
EOF

# # If Gnome is installed, set some Gnome-specific settings
# GSTNGS_TEST="$(whereis gsettings | grep -Po "gsettings: \K.*$")"

# if [ "$GSTNGS_TEST" ]; then
# 	"${XDG_GIT_DIR}/lnx_scripts/init_scripts/gnome_init.sh"
# fi


# Clone `.mozilla` repo
# TODO

# Clone `.config` repo
# Note that `~/.config` folder contains some files that are not in the `.config` repo

# Install WiFi driver
# Note: it should be done by installing `broadcom-wl-dkms`
# TODO: Update this script for Arch
# cat << EOF >> /mnt/root/conf.sh
# "$XDG_GIT_DIR/lnx_scripts/bash_progs/wifi_driver_e531_bcm43142.sh"
# EOF

# # Install Firefox Nightly
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/bash_progs/ff_nightly.sh"
# EOF

# # Install some fonts
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/bash_progs/fonts.sh" -a
# EOF

# # Install MailSpring
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/mailspring.sh"
# # EOF

# # Install Winbox
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/bash_progs/winbox.sh"
# EOF

# # Install Remmina
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/remmina.sh"
# # EOF

# # Install Sublime Text 3
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/subl.sh"
# # EOF

# # TODO: Install dependencies for ST3 plugins
# # pacman -S shellcheck perl php rubygem npm nodejs python pip  # python & pip of version 3.x
# # npm install -g csslint eslint
# # gem install sqlint

# # Install Telegram
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/telegram.sh"
# # EOF

# # Install TeamViewer
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/teamviewer.sh"
# # EOF

# # Install pirate-get
# # TODO: Fix the script as it relies on repo taken down by Microsoft
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/bash_progs/pirate-get.sh"
# EOF

# Install Session (Matrix fork) client
# Install Cawbird (Twitter client)
# Install WhatsApp client: https://www.fossmint.com/whatsapp-desktop-client-for-linux/
# Install Caprine (Facebook Messenger client)
# Install Signal client: https://getsession.org/linux
# Install Viber client
# Install Skype client
# Install Discord client
# Install Rocket Chat client
# Install Riot.im client
# Install Jitsi client
# Install Jitsi Meet client
# Install MegaSync
# Install LibreOffice from source    ### DO I REALLY WANT THIS?
# Install Borg and BorgMatic         ### DO I REALLY WANT THIS? If yes, update the script for Arch
#
# [go through `bash_progs` scripts and check if I want to install some of those programs]
# TODO

# # Start Transmission service
# cat << EOF >> /mnt/root/conf.sh
# transmission-daemon
# EOF

# Change root into the new system
chmod a+x /mnt/root/conf.sh
arch-chroot /mnt /root/conf.sh

# TODO: Remove this
echo "start date : $START_DATE"
echo "end date   : $(date)"