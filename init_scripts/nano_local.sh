#!/bin/bash

# Local nano configuration



# Variables
PATH_REPO_ROOT="$(dirname "$(dirname "$(realpath "$BASH_SOURCE")")")"

# Back up the file if exists
if [ -f /home/$USER/.nanorc ]; then
	mv /home/$USER/.nanorc{,.bak}
fi

# Create a symlink
if [ $(man 5 nanorc | grep -c breaklonglines) -gt 0 ]; then
	ln -s "$PATH_REPO_ROOT/nanorc/nanorc" /home/$USER/.nanorc
else
	ln -s "$PATH_REPO_ROOT/nanorc/nanorc_old" /home/$USER/.nanorc
fi