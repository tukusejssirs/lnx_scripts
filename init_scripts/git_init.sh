#!/bin/bash

# This script initialises Git

# Dependencies: git openssh bw gpg2/gnupg

# TODO
# - Purism GitLab;
# - add GPG keys via APIs to:
#   - GitHub;
#   - Purism GitLab;

# FIXME
# - GPG key generation does not work on Arch Linux (at least in `arch-chroot`);

# User-dependant, global variables
temp='/dev/shm'
lmagenta='\e[35m'
fdefault='\e[39m'
dot_ssh_path="$HOME/.ssh"
real_name="Tukusej’s Sirs"
user_email='tukusejssirs@protonmail.com'
real_name_itens='Otto Bolyós'
user_email_itens='otto@itens.sk'
git_editor='nano'
git_push_default='simple'  # [ simple | current | matching | upstream | nothing ]; src: http://git-scm.com/docs/git-config
global_gitignore="$HOME/.gitignore"
local_user_host="$(whoami)@$HOSTNAME"
ssh_key_remote_name="$local_user_host"

# Get the PATH_FN path
test_git_repo_repo="$(git -C "$(dirname "$BASH_SOURCE")" rev-parse --show-toplevel 2> /dev/null)"
test_git_repo_repo_retval="$?"

if [ $test_git_repo_repo_retval = 0 ] && [ -d "$test_git_repo_repo/bash/functions" ]; then
	path_fn="$test_git_repo_repo/bash/functions"
else
	path_fn="$(dirname "$(dirname "$(realpath "$BASH_SOURCE")")")/bash/functions"
fi

# Source some functions
source "$path_fn/add_to_path.sh"
source "$path_fn/ssh_key_init.sh"
source "$path_fn/add_ssh_keys.sh"

# Git global configuration
git config --global user.name "$real_name"
git config --global user.email "$user_email"
git config --global push.default "$git_push_default"
git config --global pull.rebase false
git config --global core.editor "$git_editor"
git config --global color.ui auto
git config --global gpg.program gpg2
git config --global commit.gpgsign true
git config --global core.excludesfile "$global_gitignore"
git config --global push.followTags true

# Define XDG_GIT_DIR if not defined
if [ ! "$XDG_GIT_DIR" ]; then
	source "$HOME/.config/user-dirs.dirs" &> /dev/null

	if [ ! "$XDG_GIT_DIR" ]; then
		XDG_GIT_DIR='/git'
	fi
fi

# Git Itens configuration
mkdir -p "$XDG_GIT_DIR/itens"
echo -e "[user]\n\tname = $real_name_itens\n\temail = $user_email_itens" > "$XDG_GIT_DIR/itens/.gitconfig"
echo -e "[includeIf \"gitdir:$XDG_GIT_DIR/itens/\"]\n\tpath = $XDG_GIT_DIR/itens/.gitconfig" >> "$HOME/.gitconfig"

# Get BW_SESSION
if [ ! "$BW_SESSION" ] && [ -f "$HOME/.bw/BW_SESSION" ]; then
	BW_SESSION="$(< "$HOME/.bw/BW_SESSION")"
else
	# TODO: What if the variable is not set nor the file exists?
	# TODO: Output an error and exit
	:
fi

# Make sure `gpg-agent` and `pinentry` is running
killall gpg-agent &> /dev/null
gpg-agent --daemon --pinentry-program /usr/local/bin/pinentry &> /dev/null

# It might be also required to run this
# src: https://stackoverflow.com/a/55993078/3408342
GPG_TTY="$(tty)" && export GPG_TTY

# Add the servers to SSH known hosts
# TODO: First check if these hosts are already in `known_hosts`:
# 	- if [ ! -f "$HOME/.ssh/known_hosts" ] || ! grep -q "$(grep -o '[^ ]*$' <<< "$SSH_GITHUB")" "$HOME/.ssh/known_hosts"; then
mkdir -p "$HOME/.ssh"

{
	ssh-keyscan -H gitlab.com 2> /dev/null
	ssh-keyscan -p 5222 -H git.itens.sk 2> /dev/null
	ssh-keyscan -H github.com 2> /dev/null
	ssh-keyscan -H bitbucket.org 2> /dev/null
	ssh-keyscan -H gitlab.gnome.org 2> /dev/null
	ssh-keyscan -H notabug.org 2> /dev/null
} >> "$HOME/.ssh/known_hosts"

# Use the authentication identity files specified on the command line or the configured in the ssh_config file
if [ -f "$dot_ssh_path/config" ]; then
	content="$(<"$dot_ssh_path/config")"
	cat << EOF > "$dot_ssh_path/config"
Host *
     IdentitiesOnly=yes

$content
EOF
else
	mkdir -p "$dot_ssh_path"
	cat << EOF > "$dot_ssh_path/config"
Host *
     IdentitiesOnly=yes
EOF
fi

# Generate SSH key for GitLab
echo -e "${lmagenta}[INFO] Generating SSH key for GitLab ...${fdefault}"
server_name='gitlab'
server_url='gitlab.com'
ssh_key_name="${server_name}_$local_user_host"
token="$("$(command -v bw)" get item 71a20424-7c68-4506-8ee1-aaf900d4d4d0 --session "$BW_SESSION" | jq -r .notes)"

if ! ssh_key_init -qan "$server_url" -k "$ssh_key_name" "$server_url"; then
	echo 'ERROR: SSH key generation failed.' 1>&2
fi

# Upload the public SSH key
echo -e "${lmagenta}[INFO] Uploading SSH key for GitLab ...${fdefault}"
key="$(<"$dot_ssh_path/$ssh_key_name.pub")"

if ! curl -s --data-urlencode "key=$key" --data-urlencode "title=$ssh_key_remote_name" "https://$server_url/api/v4/user/keys?private_token=$token" > /dev/null; then
	echo 'ERROR: SSH key upload failed.' 1>&2
fi

# Generate GPG key for GitLab
echo -e "${lmagenta}[INFO] Generating GPG key for GitLab ...${fdefault}"

# Note: This does not work with gnupg2 v2.0.22, but works with v2.2.9
# Note: Manual generation using gnupg2 v2.0.22 on Forpsi VPS, has not enough entropy. Run `dd if=/dev/sda of=/dev/zero` in background before the generation and kill the process after generation is complete
echo -e "Key-Type: 1\nKey-Length: 4096\nSubkey-Type: 1\nSubkey-Length: 4096\nName-Real: $real_name\nName-Email: $user_email\nExpire-Date: 0" > "$temp/gpg_key_data.txt"

if ! gpg2 --batch --gen-key "$temp/gpg_key_data.txt" > /dev/null; then
	echo 'ERROR: GPG key generation failed.' 1>&2
fi

rm "$temp/gpg_key_data.txt"
gpg_key_id="$(gpg2 --list-public-keys --keyid-format LONG "$user_email" | grep -Po "^pub\s*[^/]*/\K[^\s]*"  | tail -1)"
gpg_key="$(gpg2 --armor --export "$gpg_key_id")"

echo -e "${lmagenta}[INFO] Uploading GPG key for GitLab ...${fdefault}"

if ! curl -s --data-urlencode "key=$gpg_key" "https://gitlab.com/api/v4/user/gpg_keys?private_token=$token" > /dev/null; then
	echo 'ERROR: GPG key upload failed.' 1>&2
fi

git config --global user.signingkey "$gpg_key_id"

# Generate SSH key for Itens GitLab
echo -e "${lmagenta}[INFO] Generating SSH key for Itens GitLab ...${fdefault}"
server_name='itens_gitlab'
server_url='git.itens.sk'
PORT='5222'
ssh_key_name="${server_name}_$local_user_host"
token="$("$(command -v bw)" get item 1d62f78b-fab0-4cef-87e8-aafd01559445 --session "$BW_SESSION" | jq -r .notes)"

if ! ssh_key_init -qan "$server_url" -k "$ssh_key_name" "$server_url:$PORT"; then
	echo 'ERROR: SSH key generation failed.' 1>&2
fi

# Upload the public SSH key
echo -e "${lmagenta}[INFO] Uploading SSH key for Itens GitLab ...${fdefault}"
key="$(<"$dot_ssh_path/$ssh_key_name.pub")"

if ! curl -s --data-urlencode "key=$key" --data-urlencode "title=$ssh_key_remote_name" "https://$server_url/api/v4/user/keys?private_token=$token" > /dev/null; then
	echo 'ERROR: SSH key upload failed.' 1>&2
fi

# Generate GPG key for Itens GitLab
echo -e "${lmagenta}[INFO] Generating GPG key for Itens GitLab ...${fdefault}"
echo -e "Key-Type: 1\nKey-Length: 4096\nSubkey-Type: 1\nSubkey-Length: 4096\nName-Real: $real_name_itens\nName-Email: $user_email_itens\n%no-protection\nExpire-Date: 0" > "$temp/gpg_key_data.txt"

if ! gpg2 --batch --gen-key "$temp/gpg_key_data.txt" &> /dev/null; then
	echo 'ERROR: GPG key generation failed.' 1>&2
fi

rm "$temp/gpg_key_data.txt"
gpg_key_id="$(gpg2 --list-public-keys --keyid-format LONG "$user_email_itens" | grep -Po "^pub\s*[^/]*/\K[^\s]*"  | tail -1)"
gpg_key="$(gpg2 --armor --export "$gpg_key_id")"

echo -e "${lmagenta}[INFO] Uploading GPG key for Itens GitLab ...${fdefault}"

if ! curl -s --data-urlencode "key=$gpg_key" "https://git.itens.sk/api/v4/user/gpg_keys?private_token=$token" > /dev/null; then
	echo 'ERROR: GPG key upload failed.' 1>&2
fi

if [ -f "$XDG_GIT_DIR/itens/.gitconfig" ]; then
	sed -i '/signingkey/d' "$XDG_GIT_DIR/itens/.gitconfig"
fi

echo -e "\tsigningkey = $gpg_key_id" >> "$XDG_GIT_DIR/itens/.gitconfig"

# Generate SSH key for GitHub
echo -e "${lmagenta}[INFO] Generating SSH key for GitHub ...${fdefault}"
server_name='github'
server_url='github.com'
ssh_key_name="${server_name}_$local_user_host"
token="$("$(command -v bw)" get item 89b5923d-b1e1-4d5a-95f5-aaf900d49efc --session "$BW_SESSION" | jq -r .notes)"

if ! ssh_key_init -qan "$server_url" -k "$ssh_key_name" "$server_url"; then
	echo 'ERROR: SSH key generation failed.' 1>&2
fi

# Upload the public SSH key
echo -e "${lmagenta}[INFO] Uploading SSH key for GitHub ...${fdefault}"
key="$(<"$dot_ssh_path/$ssh_key_name.pub")"
json="$(printf '{"title": "%s", "key": "%s"}' "$ssh_key_remote_name" "$key")"

if ! curl -s -H "Authorization: token $token" -s -d "$json" "https://api.$server_url/user/keys" > /dev/null; then
	echo 'ERROR: SSH key upload failed.' 1>&2
fi

# Generate SSH key for Bitbucket
echo -e "${lmagenta}[INFO] Generating SSH key for Bitbucket ...${fdefault}"
server_name='bitbucket'
server_url='bitbucket.org'
ssh_key_name="${server_name}_$local_user_host"

if ! ssh_key_init -qan "$server_url" -k "$ssh_key_name" "$server_url"; then
	echo 'ERROR: SSH key generation failed.' 1>&2
fi

# Upload the public SSH key
echo -e "${lmagenta}[INFO] Uploading SSH key for Bitbucket ...${fdefault}"
bitbucket_data="$("$(command -v bw)" get item 4ab42cdb-840d-4a3c-be71-aa72007ef132 --session "$BW_SESSION")"
bitbucket_uuid="$(jq -r '.fields[] | select(.name == "uuid").value' <<< "$bitbucket_data" | sed 's/{/%7b/;s/}/%7d/')"
bitbucket_user="$(jq -r .login.username <<< "$bitbucket_data" | sed 's/{/%7b/;s/}/%7d/')"
bitbucket_pass="$(jq -r .login.password <<< "$bitbucket_data" | sed 's/{/%7b/;s/}/%7d/')"
key="$(<"$dot_ssh_path/$ssh_key_name.pub")"

if ! curl -s --user "$bitbucket_user:$bitbucket_pass" -X POST -H "Content-Type: application/json" -d "{\"key\": \"$key\", \"label\": \"$ssh_key_remote_name\"}" "https://api.$server_url/2.0/users/$bitbucket_uuid/ssh-keys" &> /dev/null; then
	echo
	echo 'ERROR: SSH key upload failed.' 1>&2
fi

# Generate SSH key for NotABug
echo -e "${lmagenta}[INFO] Generating SSH key for NotABug ...${fdefault}"
server_name='notabug'
server_url='notabug.org'
ssh_key_name="${server_name}_$local_user_host"

if ! ssh_key_init -qan "$server_url" -k "$ssh_key_name" "$server_url"; then
	echo 'ERROR: SSH key generation failed.' 1>&2
fi

# Generate SSH key for GNOME GitLab
echo -e "${lmagenta}[INFO] Generating SSH key for GNOME GitLab ...${fdefault}"
server_name='gnome_gitlab'
server_url='gitlab.gnome.org'
ssh_key_name="${server_name}_$local_user_host"
token="$("$(command -v bw)" get item 1d852b54-9cef-42c8-8712-aaf90101f56d --session "$BW_SESSION" | jq -r .notes)"

if ! ssh_key_init -qan "$server_url" -k "$ssh_key_name" "$server_url"; then
	echo 'ERROR: SSH key generation failed.' 1>&2
fi

# Upload the public SSH key
echo -e "${lmagenta}[INFO] Uploading SSH key for GNOME GitLab ...${fdefault}"
key="$(<"$dot_ssh_path/$ssh_key_name.pub")"

if ! curl -s --data-urlencode "key=$key" --data-urlencode "title=$ssh_key_remote_name" "https://$server_url/api/v4/user/keys?private_token=$token" > /dev/null; then
	echo 'ERROR: SSH key generation failed.' 1>&2
fi

# Generate GPG key for Gnome GitLab
# TODO: HOW DO I SEPARATE THE GNOME REPOS FROM OTHERS? USING ${XDG_GIT_LOCAL_DIR}/{C10N,OTHERS}/GNOME?
# echo -e "Key-Type: 1\nKey-Length: 4096\nSubkey-Type: 1\nSubkey-Length: 4096\nName-Real: $real_name\nName-Email: $user_email\n%no-protection\nExpire-Date: 0" > "$temp/gpg_key_data.txt"
# gpg2 --batch --gen-key "$temp/gpg_key_data.txt"
# echo "Gnome GitLab GPG key created."
# rm "$temp/gpg_key_data.txt"
# gpg_key_id=$(gpg2 --list-public-keys --keyid-format LONG $user_email | grep -Po "^pub\s*[^/]*/\K[^\s]*"  | tail -1)
# gpg_key=$(gpg2 --armor --export $gpg_key_id)
# curl --data-urlencode "key=$gpg_key" https://gitlab.gnome.com/api/v4/user/gpg_keys?private_token=$token &>/dev/null
# if [ "${XDG_GIT_DIR}/gnome/.gitconfig" ]; then
# 	sed -i '/signingkey/d' "${my_user_git_local}/gnome/.gitconfig"
# fi
# echo "\tsigningkey = $gpg_key_id" >> "${XDG_GIT_LOCAL_DIR}/gnome/.gitconfig"
# echo "Gnome GitLab GPG key added to Gnome GitLab."

cat << EOF > "$HOME/ks-todo_init.sh"
#!/bin/bash
cat ${HOME}/.ssh/notabug_$(whoami)@${HOSTNAME}.pub | wl-copy || xclip -r -selection c ${HOME}/.ssh/notabug_$(whoami)@${HOSTNAME}.pub
firefox https://notabug.org/user/settings/ssh &>/dev/null&
echo "NotABug SSH key copied."
read
EOF

# `chmod` ~/.ssh/config
chmod 600 "$HOME/.ssh/config"

# Reload `gpg-agent` to make the new GPG keys accessible
# TODO: Do this only when not in `arch-chroot` [error: killall: /prooc lacks process entries (not mounted ?)]
killall gpg-agent &> /dev/null
gpg-agent --daemon --pinentry-program /usr/local/bin/pinentry &> /dev/null

# It might be also required to run this
# src: https://stackoverflow.com/a/55993078/3408342
GPG_TTY="$(tty)" && export GPG_TTY

# Add SSH keys to the ssh-agent
add_ssh_keys