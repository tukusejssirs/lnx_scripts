#!/bin/bash

# new, not working


# Arch Linux installation using ALIF

# TODO
# - setup WiFi for the usual networks using `bw`:
#   - blocker: how to insert _securely_ the master password?
# - check if ALIF:
#   - makes the system is bootable;
#   - makes the GNOME working;
#   - makes the SwayWM working;
#   - asks for any additional input;
#   - installs `reflector` service;


# Load keyboard layout
# TODO: Remove this as it is not necessary
loadkeys uk

# Download ALIS script
curl -sL https://raw.githubusercontent.com/picodotdev/alis/master/download.sh | bash

# Use standard mirror list on Live OS
# Note: Otherwise it would fail to download `core.db`
sed -i '/Server = $PACMAN_MIRROR/d' alis.sh

# Use custom LVM VG name
sed -i "s/^\(LVM_VOLUME_GROUP=\)\"vg\"$/\1'vg_arch'/" alis.sh

# Configure ALIF
cat << EOF > alis.conf
KEYS='uk'
LOG='true'
DEVICE='/dev/sda'
DEVICE_TRIM='true'
LVM='true'
LUKS_PASSWORD=''
LUKS_PASSWORD_RETYPE=''
FILE_SYSTEM_TYPE='ext4'
SWAP_SIZE='4096'
PARTITION_MODE='auto'
PING_HOSTNAME='mirrors.kernel.org'
REFLECTOR='true'
REFLECTOR_COUNTRIES=('Slovakia' 'Czechia' 'Hungary' 'Austria')
KERNELS_COMPRESSION='zstd'
KERNELS_PARAMETERS='ipv6.disable=1'
DISPLAY_DRIVER='intel !amdgpu !ati !nvidia !nvidia-lts !nvidia-dkms !nvidia-390xx !nvidia-390xx-lts !nvidia-390xx-dkms !nouveau' # (single)
KMS='true'
FASTBOOT='true'
FRAMEBUFFER_COMPRESSION='true'
DISPLAY_DRIVER_DDX='false'
VULKAN='true'
DISPLAY_DRIVER_HARDWARE_ACCELERATION='true'
DISPLAY_DRIVER_HARDWARE_ACCELERATION_INTEL='intel-media-driver !libva-intel-driver' # (single)
TIMEZONE='/usr/share/zoneinfo/Europe/Bratislava'
LOCALES=('en_GB.UTF-8 UTF-8')
LOCALE_CONF=('LANG=en_GB.UTF-8' 'LANGUAGE=en_GB:en')
KEYMAP="KEYMAP=\$KEYS"
KEYLAYOUT='gb'  # Note: this is Xorg/X11 KBLO, not one for loadkeys
KEYMODEL=''
KEYVARIANT=''
KEYOPTIONS=''
FONT=''
FONT_MAP=''
HOSTNAME='archlinux'         # FIXME
ROOT_PASSWORD='test'         # FIXME
ROOT_PASSWORD_RETYPE='test'  # FIXME
USER_NAME='ts'
USER_PASSWORD='test'
USER_PASSWORD_RETYPE='test'
SYSTEMD_HOMED='false'
HOOKS='base !udev !usr !resume !systemd !btrfs keyboard autodetect modconf block !net !dmraid !mdadm !mdadm_udev !keymap !consolefont !sd-vconsole !encrypt !lvm2 !sd-encrypt !sd-lvm2 fsck filesystems'
BOOTLOADER='!grub !refind systemd' # (single)
CUSTOM_SHELL='bash' # (single)
DESKTOP_ENVIRONMENT=''
PACKAGES_MULTILIB='true'
PACKAGES_INSTALL='true'
VAGRANT='false'
SYSTEMD_UNITS="+chronyd +ufw -iptables"
REBOOT='false'
EOF

# [Optional] Edit configuration and change variables values with your preferences (packages to install)
cat << EOF > alis-packages.conf
PACKAGES_PACMAN_INSTALL='true'
PACKAGES_FLATPAK_INSTALL='false'  # TODO: evaluate
PACKAGES_SDKMAN_INSTALL='false'   # TODO: evaluate
PACKAGES_AUR_INSTALL='true'
PACKAGES_AUR_COMMAND='yay'
PACKAGES_PACMAN='android-tools arp-scan autoconf automake base-devel bash bash-completion bc bind-tools breezy broadcom-wl-dkms calibre cheese chrome-gnome-shell chrony cifs-utils colordiff curl dhclient dialog diffutils dkms dmidecode dnsmasq dosfstools easytag efibootmgr eog epiphany espeak etckeeper ethtool evince exfatprogs expect ffmpeg file-roller folks fping gcc gdm gedit gimp gimp-help-en_gb gimp-nufraw gimp-plugin-gmic git gnome-calculator gnome-calendar gnome-characters gnome-clocks gnome-color-manager gnome-contacts gnome-control-center gnome-disk-utility gnome-font-viewer gnome-keyring gnome-logs gnome-screenshot gnome-session gnome-settings-daemon gnome-shell gnome-terminal gnome-themes-extra gnome-tweaks gnome-user-share gparted gthumb gvfs gvfs-afc gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb htop intel-ucode jq lftp libtool links linux-headers lshw lvm2 lynx make man-db man-pages meson mkinitcpio mkinitcpio-nfs-utils mlocate mtools mutter nano nautilus networkmanager nfs-utils nodejs noto-fonts noto-fonts-emoji npm ntfs-3g openssh openssl p7zip parted pavucontrol perl php pv python python-html2text qpdf readline reflector rsync rubygems rygel samba shellcheck smartmontools sshpass subversion sudo sway swaylock syslinux systemd-resolvconf tcpdump tesseract tesseract-data-ces tesseract-data-deu tesseract-data-heb tesseract-data-hun tesseract-data-lat tesseract-data-slk testdisk tidy transmission-gtk unrar usb_modeswitch usbutils vlc w3m waybar wayland wf-recorder wget wireless-regdb wireless_tools wl-clipboard wpa_supplicant xclip xdg-user-dirs-gtk xorg xorg-server xsane xsane-gimp zenity zlib'
PACKAGES_AUR='wlogout etherwake'
EOF

# Start installation
echo y | ./alis.sh

# Start packages installation
./alis-packages.sh

# Make sure that users in the `wheel` group must you password
# Note: This is an ALIS issue.
arch-chroot /mnt sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL$/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Set up global variables
TMP='/dev/shm'

# Update the system clock
# FIXME: Run this after installing and enabling `chrony`.
# TODO: Test if this is working out-of-box after installing and enabling `chrony` via ALIS
# arch-chroot /mnt timedatectl set-ntp true

# Allow IPv4 addresses only to use in chrony
# FIXME: This does not work yet
cat << EOF >> /etc/chrony.conf

# Allow IPv4 addresses only to use in chrony
allow 0/0
deny ::/0
EOF

# Get a list of mirrors
# Use only mirrors in SK, CZ, HU and A
# Of them, use (at most) 20 fastest mirrors
# Use https protocol
# Sort the mirrors by rate
arch-chroot /mnt pacman -Sy --noconfirm reflector
arch-chroot /mnt reflector --country Slovakia,Czechia,Hungary,Austria --latest 25 --age 24 --protocol https --completion-percent 100 --sort rate --save /etc/pacman.d/mirrorlist

# Configure reflector timer service
arch-chroot /mnt 'cat << EOF > /etc/xdg/reflector/reflector.conf
# Use only mirrors in SK, CZ, HU and A
--country Slovakia,Czechia,Hungary,Austria

# Use https protocol
-p https

# Of them, use (at most) 20 fastest mirrors
-f 20

# Sort the mirrors by rate
--sort rate

# Save the output to the pacman configuration folder
--save /etc/pacman.d/mirrorlist
EOF'

# Enable the `reflector` and `reflector.timer` services
arch-chroot /mnt systemctl enable --now reflector reflector.timer

# Create a folder for `mirrorupgrade.hook`
arch-chroot /mnt mkdir -p /etc/pacman.d/hooks

# Create `mirrorupgrade.hook`
arch-chroot /mnt "cat << EOF > /etc/pacman.d/hooks/mirrorupgrade.hook
[Trigger]
Type = Package
Operation = Upgrade
Target = pacman-mirrorlist

[Action]
Description = Updating pacman-mirrorlist with reflector and removing pacnew...
When = PostTransaction
Depends = reflector
Exec = /bin/sh -c 'systemctl start reflector; if [ -f /etc/pacman.d/mirrorlist.pacnew ]; then rm /etc/pacman.d/mirrorlist.pacnew; fi'
EOF"

# Set up hostname
# TODO: Make this more portable
arch-chroot /mnt bash -c "
	PC_MODEL=\"\$(dmidecode -t system | grep -Po 'Version: [ThinkPad Edge]* \K.*$' | tr '[:upper:]' '[:lower:]')\"
	HOST_NAME=\"\$PC_MODEL.domcek.lan\"
	hostnamectl set-hostname \"\$HOST_NAME\"
"

# Disable handling of the power button by SystemD
# Note: I use `wlogout` for interactive power button behaviour and merely disabling this makes it working as I want it.
# TODO: Use the script instead of command.
# $XDG_GIT_DIR/lnx_scripts/systemd/disable_power_button_handling.sh
arch-chroot /mnt sed -i 's/^HandlePowerKey=.*$/HandlePowerKey=ignore/' /etc/systemd/logind.conf

# Disable suspending when the lid is closed
arch-chroot /mnt bash -c "sed -i 's/^#*\(HandleLidSwitch\)=.*$/\1=ignore/;
	s/^#*\(HandleLidSwitchExternalPower\)=.*$/\1=ignore/;
	s/^#*\(HandleLidSwitchDocked\)=.*$/\1=ignore/;
	s/^#*\(IdleAction\)=.*$/\1=ignore/' /etc/systemd/logind.conf"

# LD config paths
# TODO: Create a script for this
arch-chroot /mnt bash -c "
	echo '/usr/lib' > /etc/ld.so.conf.d/usr_lib.conf
	echo '/usr/lib64' > /etc/ld.so.conf.d/usr_lib64.conf
	echo '/usr/local/lib' > /etc/ld.so.conf.d/usr_local_lib.conf
	echo '/usr/local/lib64' > /etc/ld.so.conf.d/usr_local_lib64.conf
"

# Remove htop.desktop (I don't want it in the Gnome overview)
arch-chroot /mnt rm -rf /usr/share/applications/htop.desktop

# # Clone `lnx_scripts` repo
# arch-chroot /mnt git clone https://gitlab.com/tukusejssirs/lnx_scripts.git "$TMP/lnx_scripts"

# # Change user folder names
# FIXME: Run this for both root user and non-root user
# cat << EOF >> /mnt/root/conf.sh
# mkdir -p "$HOME/.config"
# if [ "$(uname -r | grep -o 'Microsoft$')" != 'Microsoft' ]; then
# 	if [ -f "$HOME/.config/user-dirs.dirs" ]; then
# 		mv "$HOME/.config/user-dirs.dirs{,.bak}"
# 	fi

# 	if [ -f "$HOME/.config/user-dirs.locale" ]; then
# 		mv "$HOME/.config/user-dirs.locale{,.bak}"
# 	fi

# 	for n in $(cat "$TMP/lnx_scripts/user-dirs/user-dirs.dirs_${USER}" | grep -oP "^XDG[^=]*=\"\K[^\"]*" | tr '\n' ' ' | sed "s#\$HOME#$HOME#g" -); do
# 		mkdir -p "$n"
# 	done

# 	cp "$TMP/lnx_scripts/user-dirs/user-dirs.dirs_${USER}" "$HOME/.config/user-dirs.dirs"
# 	cp "$TMP/lnx_scripts/user-dirs/user-dirs.locale_${USER}" "$HOME/.config/user-dirs.locale"
# fi

# mkdir -p "$(grep -Po '^XDG.*="\K[^"]*' "$HOME/.config/user-dirs.dirs" | sed "/^\/git$/d;s|\$HOME|$HOME|g" | tr '\n' ' ')"

# source "$HOME/.config/user-dirs.dirs"
# EOF

# # Create folders for `git`, `bzr` and `svn`
# # TODO: Define XDG_SVN_DIR and XDG_BZR_DIR
# cat << EOF >> /mnt/root/conf.sh
# mkdir -p /svn /bzr "$XDG_GIT_DIR"
# EOF

# # Move `lnx_scripts` repo to XDG_GIT_DIR
# cat << EOF >> /mnt/root/conf.sh
# mv "$TMP/lnx_scripts" "$XDG_GIT_DIR"
# EOF

# # Deal with the original user folders
# # TODO: Move this into `xdg_init.sh` script
# cat << EOF >> /mnt/root/conf.sh
# LIST_XDG_DIRS="$(grep -o '^\s*[^#][^=]*' "$HOME/.config/user-dirs.dirs")"

# for n in $LIST_XDG_DIRS; do
# 	case "${!n}" in
# 		XDG_DESKTOP_DIR)
# 			PARENT_DIR="$(dirname "$XDG_DESKTOP_DIR")"
# 			BASE_DIR="$(basename "$XDG_DESKTOP_DIR")"

# 			if [ "$BASE_DIR" = 'Desktop' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Desktop" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Desktop" "$XDG_DESKTOP_DIR"
# 			fi
# 		;;
# 		XDG_DOCUMENTS_DIR)
# 			PARENT_DIR="$(dirname "$XDG_DOCUMENTS_DIR")"
# 			BASE_DIR="$(basename "$XDG_DOCUMENTS_DIR")"

# 			if [ "$BASE_DIR" = 'Documents' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Documents" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Documents" "$XDG_DOCUMENTS_DIR"
# 			fi
# 		;;
# 		XDG_DOWNLOAD_DIR)
# 			PARENT_DIR="$(dirname "$XDG_DOWNLOAD_DIR")"
# 			BASE_DIR="$(basename "$XDG_DOWNLOAD_DIR")"

# 			if [ "$BASE_DIR" = 'Downloads' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Downloads" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Downloads" "$XDG_DOWNLOAD_DIR"
# 			fi
# 		;;
# 		XDG_MUSIC_DIR)
# 			PARENT_DIR="$(dirname "$XDG_MUSIC_DIR")"
# 			BASE_DIR="$(basename "$XDG_MUSIC_DIR")"

# 			if [ "$BASE_DIR" = 'Music' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Music" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Music" "$XDG_MUSIC_DIR"
# 			fi
# 		;;
# 		XDG_PICTURES_DIR)
# 			PARENT_DIR="$(dirname "$XDG_PICTURES_DIR")"
# 			BASE_DIR="$(basename "$XDG_PICTURES_DIR")"

# 			if [ "$BASE_DIR" = 'Pictures' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Pictures" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Pictures" "$XDG_PICTURES_DIR"
# 			fi
# 		;;
# 		XDG_TEMPLATES_DIR)
# 			PARENT_DIR="$(dirname "$XDG_TEMPLATES_DIR")"
# 			BASE_DIR="$(basename "$XDG_TEMPLATES_DIR")"

# 			if [ "$BASE_DIR" = 'Templates' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Templates" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Templates" "$XDG_TEMPLATES_DIR"
# 			fi
# 		;;
# 		XDG_VIDEOS_DIR)
# 			PARENT_DIR="$(dirname "$XDG_VIDEOS_DIR")"
# 			BASE_DIR="$(basename "$XDG_VIDEOS_DIR")"

# 			if [ "$BASE_DIR" = 'Videos' ]; then
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Videos" "$PARENT_DIR"
# 			else
# 				mkdir -p "$PARENT_DIR" 2> /dev/null
# 				mv "$HOME/Videos" "$XDG_VIDEOS_DIR"
# 			fi
# 		;;
# 		*)
# 			mkdir -p "${!n}" 2> /dev/null
# 		;;
# 	esac
# done
# EOF

# # Bash initialisation
# cat << EOF >> /mnt/root/conf.sh
# TEST_BASH="$(whereis bash | grep -Po 'bash: \K.*$')"

# if [ "$TEST_BASH" ]; then
# 	"$XDG_GIT_DIR/lnx_scripts/init_scripts/bash_init.sh" "$XDG_GIT_DIR"
# fi
# EOF

# # Build and install Git from source
# # TODO: Do I really want to build Git from source or will the `pacman` version be enough?
# # cat << EOF >> /mnt/root/conf.sh
# # "$XDG_GIT_DIR/lnx_scripts/bash_progs/git_src.sh"
# # EOF

# # Initialise Git
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/init_scripts/git_init.sh"
# EOF

# # Change `lnx_scripts` remote url (https to git protocol transition)
# cat << EOF >> /mnt/root/conf.sh
# cd "$XDG_GIT_DIR/lnx_scripts"
# git remote rm origin
# git remote add origin git@gitlab.com:tukusejssirs/lnx_scripts.git &> /dev/null
# git branch --set-upstream-to=origin/master master &> /dev/null
# git push --set-upstream origin master &> /dev/null
# git remote set-head origin -a &> /dev/null
# EOF

# # Install `bw`
# cat << EOF >> /mnt/root/conf.sh
# "$XDG_GIT_DIR/lnx_scripts/bash_progs/bitwarden_cli.sh"
# EOF

# # Login into Bitwarden
# # TODO: Make it more scripted (i.e. login automatically), while keeping it secure (try not to use plain-text password in a variable nor in the command itself)
# cat << EOF >> /mnt/root/conf.sh
# echo "Sign into Bitwarden:"
# BW_SESSION=$(bw login | tail -1 | awk '{print $6}')
# echo $BW_SESSION > "$HOME/.bw/BW_SESSION"
# export BW_SESSION
# EOF

# # Create the non-root user and set their preferences and permissions
# # TODO: CHANGE THE `PW` DEFINITION !!!
# # TODO: Initialise Git
# # TODO: Update `bash_init` to initialise Bash in `/etc/skel` before we create this user
# # TODO: Uncomment `setfacl` command
# cat << EOF >> /mnt/root/conf.sh
# # PW="\$(node "$INSTALL_PATH/build/bw.js" list items --search "$NOT_ROOT_USER_NAME@\$PC_MODEL" | jq -r .[].login.password)"
# PW='2e5bf2li'
# PW_HASH="\$(python -c "import crypt, getpass; pw = '$PW'; print(crypt.crypt(pw))")"
# # useradd "$NOT_ROOT_USER_NAME" -rmp "\$PW_HASH"
# useradd -rmUG wheel "$NOT_ROOT_USER_NAME" -p "\$PW_HASH"
# # usermod -aG wheel "$NOT_ROOT_USER_NAME"
# # setfacl -Rm u:$NOT_ROOT_USER_NAME:rwx /bzr /opt /svn "\$XDG_GIT_DIR"

# # "\$XDG_GIT_DIR/lnx_scripts/init_scripts/git_init.sh"
# EOF

# # Create additional sudoers configuration file
# cat << EOF >> /mnt/root/conf.sh
# cat << END > "/etc/sudoers.d/$NOT_ROOT_USER_NAME"
# Defaults editor=/usr/bin/nano
# $NOT_ROOT_USER_NAME ALL = (ALL) NOPASSWD: ALL
# END
# EOF

# # Create `root` password
# # TODO: CHANGE THE `PW` DEFINITION !!!
# cat << EOF >> /mnt/root/conf.sh
# # PW="\$(node "$INSTALL_PATH/build/bw.js" list items --search "root@\$PC_MODEL" | jq -r .[].login.password)"
# PW='2e5bf2li'
# echo "root:\$PW" | chpasswd
# EOF

# # Create additional sudoers configuration file
# cat << EOF >> /mnt/root/conf.sh
# sed -i 's/^$NOT_ROOT_USER_NAME ALL/# &/' "/etc/sudoers.d/$NOT_ROOT_USER_NAME"
# EOF

# Update `/var/lib/mlocate/mlocate.db` (else `locate` won't work)
cat << EOF >> /mnt/root/conf.sh
updatedb
EOF

# # If Gnome is installed, set some Gnome-specific settings
# GSTNGS_TEST="$(whereis gsettings | grep -Po "gsettings: \K.*$")"

# if [ "$GSTNGS_TEST" ]; then
# 	"${XDG_GIT_DIR}/lnx_scripts/init_scripts/gnome_init.sh"
# fi


# Clone `.mozilla` repo
# TODO

# Clone `.config` repo
# Note that `~/.config` folder contains some files that are not in the `.config` repo

# Install WiFi driver
# Note: it should be done by installing `broadcom-wl-dkms`
# TODO: Update this script for Arch
# cat << EOF >> /mnt/root/conf.sh
# "$XDG_GIT_DIR/lnx_scripts/bash_progs/wifi_driver_e531_bcm43142.sh"
# EOF

# # Install Firefox Nightly
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/bash_progs/ff_nightly.sh"
# EOF

# # Install some fonts
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/bash_progs/fonts.sh" -a
# EOF

# # Install MailSpring
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/mailspring.sh"
# # EOF

# # Install Winbox
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/bash_progs/winbox.sh"
# EOF

# # Install Remmina
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/remmina.sh"
# # EOF

# # Install Sublime Text 3
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/subl.sh"
# # EOF

# # TODO: Install dependencies for ST3 plugins
# # pacman -S shellcheck perl php rubygem npm nodejs python pip  # python & pip of version 3.x
# # npm install -g csslint eslint
# # gem install sqlint

# # Install Telegram
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/telegram.sh"
# # EOF

# # Install TeamViewer
# # TODO: Update this script for Arch
# # cat << EOF >> /mnt/root/conf.sh
# # "${XDG_GIT_DIR}/lnx_scripts/bash_progs/teamviewer.sh"
# # EOF

# # Install pirate-get
# # TODO: Fix the script as it relies on repo taken down by Microsoft
# cat << EOF >> /mnt/root/conf.sh
# "${XDG_GIT_DIR}/lnx_scripts/bash_progs/pirate-get.sh"
# EOF

# Install Session (Matrix fork) client
# Install Cawbird (Twitter client)
# Install WhatsApp client: https://www.fossmint.com/whatsapp-desktop-client-for-linux/
# Install Caprine (Facebook Messenger client)
# Install Signal client: https://getsession.org/linux
# Install Viber client
# Install Skype client
# Install Discord client
# Install Rocket Chat client
# Install Riot.im client
# Install Jitsi client
# Install Jitsi Meet client
# Install MegaSync
# Install LibreOffice from source    ### DO I REALLY WANT THIS?
# Install Borg and BorgMatic         ### DO I REALLY WANT THIS? If yes, update the script for Arch
#
# [go through `bash_progs` scripts and check if I want to install some of those programs]
# TODO

# # Start Transmission service
# cat << EOF >> /mnt/root/conf.sh
# transmission-daemon
# EOF







# Reboot the system
./alis-reboot.sh