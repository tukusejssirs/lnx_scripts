#!/bin/bash

# Arch Linux installation using ALIF

# TODO
# - setup WiFi for the usual networks using `bw`:
#   - blocker: how to insert _securely_ the master password?
# - check if ALIF:
#   - makes the system is bootable;
#   - makes the GNOME working;
#   - makes the SwayWM working;
#   - asks for any additional input;
#   - installs `reflector` service;


# Loady keyboard layout
loadkeys uk

# Download ALIS script
curl -sL https://raw.githubusercontent.com/picodotdev/alis/master/download.sh | bash

# Make ALIF don't ask for confirmation that it will delete all partitions on the DEVICE
# Note: This prohibits (somehow) ALIF from installing Arch Linux
# sed -i '/Do you want to continue/d' alis.sh

# Configure ALIF
cat << EOF > alis.conf
KEYS='uk'
LOG='true'
DEVICE='/dev/sda'
DEVICE_TRIM='true'
LVM='true'
LUKS_PASSWORD=''
LUKS_PASSWORD_RETYPE=''
FILE_SYSTEM_TYPE='ext4'
SWAP_SIZE='4096'
PARTITION_MODE='auto'
PING_HOSTNAME='mirrors.kernel.org'
REFLECTOR='true'
REFLECTOR_COUNTRIES=('Slovakia' 'Czechia' 'Hungary' 'Austria')
# PACMAN_MIRROR="https://mirrors.kernel.org/archlinux/\$repo/os/\$arch"
# PACMAN_MIRROR="https://mirrors.edge.kernel.org/archlinux/\$repo/os/\$arch"
PACMAN_MIRROR="https://ftp.cvut.cz/arch/\$repo/os/\$arch"
KERNELS_COMPRESSION='zstd'
KERNELS_PARAMETERS='ipv6.disable=1'
DISPLAY_DRIVER='intel !amdgpu !ati !nvidia !nvidia-lts !nvidia-dkms !nvidia-390xx !nvidia-390xx-lts !nvidia-390xx-dkms !nouveau' # (single)
KMS='true'
FASTBOOT='true'
FRAMEBUFFER_COMPRESSION='true'
DISPLAY_DRIVER_DDX='false'
VULKAN='true'
DISPLAY_DRIVER_HARDWARE_ACCELERATION='true'
DISPLAY_DRIVER_HARDWARE_ACCELERATION_INTEL='intel-media-driver !libva-intel-driver' # (single)
TIMEZONE='/usr/share/zoneinfo/Europe/Bratislava'
LOCALES=('en_GB.UTF-8 UTF-8')
LOCALE_CONF=('LANG=en_GB.UTF-8' 'LANGUAGE=en_GB:en')
KEYMAP="KEYMAP=\$KEYS"
KEYLAYOUT="\$KEYS"
KEYMODEL=''
KEYVARIANT=''
KEYOPTIONS=''
FONT=''
FONT_MAP=''
HOSTNAME="archlinux"             # FIXME
ROOT_PASSWORD='2e5bf2li'         # FIXME
ROOT_PASSWORD_RETYPE='2e5bf2li'  # FIXME
USER_NAME='ts'
USER_PASSWORD='2e5bf2li'
USER_PASSWORD_RETYPE='2e5bf2li'
SYSTEMD_HOMED='false'
HOOKS='base !udev !usr !resume !systemd !btrfs keyboard autodetect modconf block !net !dmraid !mdadm !mdadm_udev !keymap !consolefont !sd-vconsole !encrypt !lvm2 !sd-encrypt !sd-lvm2 fsck filesystems'
BOOTLOADER='!grub !refind systemd' # (single)
CUSTOM_SHELL='bash' # (single)
DESKTOP_ENVIRONMENT='gnome' # (single)
PACKAGES_MULTILIB='true'
PACKAGES_INSTALL='true'
VAGRANT='false'
SYSTEMD_UNITS="!ufw.service !docker.service"
REBOOT='false'
EOF

# [Optional] Edit configuration and change variables values with your preferences (packages to install)
cat << EOF > alis-packages.conf
PACKAGES_PACMAN_INSTALL='true'
PACKAGES_FLATPAK_INSTALL='false'  # TODO: evaluate
PACKAGES_SDKMAN_INSTALL='false'   # TODO: evaluate
PACKAGES_AUR_INSTALL='true'
PACKAGES_AUR_COMMAND='yay'
PACKAGES_PACMAN='autoconf automake base-devel bash bash-completion bc broadcom-wl-dkms chrome-gnome-shell curl dhclient diffutils dkms dnsmasq dosfstools efibootmgr eog epiphany espeak evince exfatprogs file-roller gcc gedit git gnome-calculator gnome-characters gnome-clocks gnome-color-manager gnome-contacts gnome-control-center gnome-disk-utility gnome-font-viewer gnome-keyring gnome-logs gnome-screenshot gnome-terminal gnome-themes-extra gnome-tweaks gparted gthumb htop intel-ucode libtool linux-headers lshw lvm2 make man-db man-pages meson mkinitcpio mkinitcpio-nfs-utils mlocate mtools mutter nano nautilus networkmanager nfs-utils nodejs noto-fonts noto-fonts-emoji npm openssh openssl p7zip parted pavucontrol perl php python python-html2text readline  rsync sshpass sudo sway swaylock syslinux systemd-resolvconf tcpdump usb_modeswitch usbutils waybar wayland wget wireless-regdb wireless_tools wl-clipboard wpa_supplicant xclip xdg-user-dirs-gtk zlib xorg xorg-server xsane'
PACKAGES_AUR='wlogout etherwake'
EOF

# Start installation
echo y | ./alis.sh

# Start packages installation
./alis-packages.sh

# Reboot the system
./alis-reboot.sh