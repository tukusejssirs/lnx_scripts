#!/bin/bash

# Create an upgrade script in a Mikrotik device and schedule its run



# Variables
BW_DATA="$(bw get item 8627734f-d159-4038-8eb1-aa72007ef131)"
IP="$(jq -r .login.uris[].uri <<< "$BW_DATA" | grep -Po '^https*://\K[0-9.]*')"
USER="$(jq -r .login.username <<< "$BW_DATA")"
PASS="$(jq -r .login.password <<< "$BW_DATA")"
PORT="$(jq -r '.fields[] | select(.name=="ssh_port") | .value' <<< "$BW_DATA")"
INTERVAL='1w'
SCRIPT_NAME='routeros_upgrade'
SCHEDULE_NAME='ROUTEROS_UPGRADE'
FILE_PATH="$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh"

# Create a script
add name="$SCRIPT_NAME" source="$(sed -z 's/\n/\\n/g' "$FILE_PATH")"
add name="test" source="$(sed -z 's/\n/\\n/g' "$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh")"

# Create a scheduler event
/system scheduler add name="$SCHEDULE_NAME" interval=$INTERVAL start-date=aug/15/2020 start-time=04:00:00 on-event="/system script run routeros_upgrade"



################
################
################


# Variables
BW_DATA="$(bw get item 8627734f-d159-4038-8eb1-aa72007ef131)"
IP="$(jq -r .login.uris[].uri <<< "$BW_DATA" | grep -Po '^https*://\K[0-9.]*')"
USER="$(jq -r .login.username <<< "$BW_DATA")"
PASS="$(jq -r .login.password <<< "$BW_DATA")"
PORT="$(jq -r '.fields[] | select(.name=="ssh_port") | .value' <<< "$BW_DATA")"
URL="$(curl -s https://mikrotik.com/download | grep -o 'https://download.mikrotik.com/routeros/[0-9.]*/routeros-arm-[0-9.]*.npk' | sort | tail -1)"
FILENAME="$(grep -o '[^/]*$' <<< "$URL")"
PATH_DOWNLOAD="$XDG_DOWNLOAD_DIR"

# Check the current firmware version
VER_CURRENT="$(sshpass -p "$PASS" ssh "$USER@$IP" -p $PORT '/system routerboard; :put [get current-firmware]')"
VER_UPGRADE="$(sshpass -p "$PASS" ssh "$USER@$IP" -p $PORT '/system routerboard; :put [get upgrade-firmware]')"
VER_LATEST="$(grep -Po '^routeros-arm-\K[0-9.]*(?=.npk)' <<< "$FILENAME")"

# Upgrade RouterOS if not the latest
if [ "$(echo -e "$VER_LATEST\n$VER_CURRENT" | sort -V | tail -1)" = "$VER_LATEST" ]; then
	# Check if we have already upgraded RouterOS, but we did not reboot the device
	if [ "$VER_UPGRADE" != "$VER_LATEST" ]; then
		# Download the file
		curl -so "$PATH_DOWNLOAD/$FILENAME" "$URL"

		# Copy the file onto the device via SSH
		# sshpass -p "$PASS" ssh "$USER@$IP" -p "$PORT"
		# sshpass -p "$PASS" scp "$PATH_DOWNLOAD/$FILENAME" -P "$PORT" "$USER@$IP"
		sshpass -p "$PASS" scp -P $PORT "$PATH_DOWNLOAD/$FILENAME" "$USER@$IP:/"
	fi

	# Reboot the device
	sshpass -p "$PASS" ssh "$USER@$IP" -p "$PORT" ':execute {/system reboot;}'
fi

##################
##################
##################

sed -z 's/\n/\\n/g;s/"/\"/g' routeros_upgrade.sh | sed 's/\\/\\\\/g'

sshpass -p "$PASS" ssh "$USER@$IP" -p "$PORT" "add name=\"test\" source=\"$(sed -z 's/\n/\\n/g;s/"/\"/g' "$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh" | sed 's/\\/\\\\/g')\""

add name="test" source="# Upgrade Mikrotik RouterOS and the firmware\n\n# author:  Tukusej's Sirs\n# date:    13 August 2020\n# version: 1.0\n\n# src: https://mivilisnet.wordpress.com/2020/03/31/updating-mikrotik-router-from-the-command-line/\n\n\n# Enter the specified location\n/system package update\n\n# Set the channel\nset channel=stable\n\n# Check for updates\ncheck-for-updates\n\n# Upgrade if current version is not the latest\n:if (installed-version != latest-version) do={\n  # Download the latest version\n  download\n\n  # Create a scheduler event that\n  # - upgrades RouterOS;\n  # - reboots the system;\n  # - upgrades the firmware;\n  # - reboots the system.\n  /system scheduler add name=REBOOT interval=5s on-event="/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\"; :execute {/system reboot;}"\n}"


add name="test" source="# Upgrade Mikrotik RouterOS and the firmware\n\n# author:  Tukusej's Sirs\n# date:    13 August 2020\n# version: 1.0\n\n# src: https://mivilisnet.wordpress.com/2020/03/31/updating-mikrotik-router-from-the-command-line/\n\n\n# Enter the specified location\n/system package update\n\n# Set the channel\nset channel=stable\n\n# Check for updates\ncheck-for-updates\n\n# Upgrade if current version is not the latest\n:if (installed-version != latest-version) do={\n  # Download the latest version\n  download\n\n  # Create a scheduler event that\n  # - upgrades RouterOS;\n  # - reboots the system;\n  # - upgrades the firmware;\n  # - reboots the system.\n  /system scheduler add name=REBOOT interval=5s on-event="/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\"; :execute {/system reboot;}"\n}


/system scheduler add name="test" source="# Enter the specified location\n /system package update\n\n# Set the channel\nset channel=stable\n\n# Check for updates\ncheck-for-updates\n\n# Upgrade if current version is not the latest\n:if (installed-version != latest-version) do={\n  # Download the latest version\n  download\n\n  # Create a scheduler event that\n  # - upgrades RouterOS;\n  # - reboots the system;\n  # - upgrades the firmware;\n  # - reboots the system.\n  /system scheduler add name=REBOOT interval=5s on-event="/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\"; :execute {/system reboot;}"\n}"


# Upgrade Mikrotik RouterOS and the firmware\n\n# author:  Tukusej's Sirs\n# date:    13 August 2020\n# version: 1.0\n\n# src: https://mivilisnet.wordpress.com/2020/03/31/updating-mikrotik-router-from-the-command-line/\n\n\n# Enter the specified location\n/system package update\n\n# Set the channel\nset channel=stable\n\n# Check for updates\ncheck-for-updates\n\n# Upgrade if current version is not the latest\n:if (installed-version != latest-version) do={\n  # Download the latest version\n  download\n\n  # Create a scheduler event that\n  # - upgrades RouterOS;\n  # - reboots the system;\n  # - upgrades the firmware;\n  # - reboots the system.\n  /system scheduler add name=REBOOT interval=5s on-event="/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\"; :execute {/system reboot;}"\n}

# this works, but I want to set it up via SSH
# /system script add name="test" source="# Upgrade Mikrotik RouterOS and the firmware\n\n# author:  Tukusej's Sirs\n# date:    13 August 2020\n# version: 1.0\n\n# src: https://mivilisnet.wordpress.com/2020/03/31/updating-mikrotik-router-from-the-command-line/\n\n\n# Enter the specified location\n/system package update\n\n# Set the channel\nset channel=stable\n\n# Check for updates\ncheck-for-updates\n\n# Upgrade if current version is not the latest\n:if (installed-version != latest-version) do={\n  # Download the latest version\n  download\n\n  # Create a scheduler event that\n  # - upgrades RouterOS;\n  # - reboots the system;\n  # - upgrades the firmware;\n  # - reboots the system.\n  /system scheduler add name=REBOOT interval=5s on-event=\"/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\\\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\\\"; :execute {/system reboot;}\"\n}"

# sed -z 's/\n/\\n/g' "$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh" | sed 's/\\"/\\\\"/g;s/"/\\"/g'
add name="test" source="$(sed -z 's/\n/\\n/g' "$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh" | sed 's/\\"/\\\\"/g;s/"/\\"/g')"

sshpass -p "$PASS" ssh "$USER@$IP" -p "$PORT" "$(echo -e "\n\n\n"; echo "add name=\"test\" source=\"$(sed -z 's/\n/\\n/g' "$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh" | sed 's/\\"/\\\\"/g;s/"/\\"/g')\"" | sed 's/"/\\"/g')"

echo -e "\n\n\n"; echo "add name=\"test\" source=\"$(sed -z 's/\n/\\n/g' "$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh" | sed 's/\\"/\\\\"/g;s/"/\\"/g')\"" | sed 's/"/\\"/g'
# add name="test" source="# Upgrade Mikrotik RouterOS and the firmware\n\n# author:  Tukusej's Sirs\n# date:    13 August 2020\n# version: 1.0\n\n# src: https://mivilisnet.wordpress.com/2020/03/31/updating-mikrotik-router-from-the-command-line/\n\n\n# Enter the specified location\n/system package update\n\n# Set the channel\nset channel=stable\n\n# Check for updates\ncheck-for-updates\n\n# Upgrade if current version is not the latest\n:if (installed-version != latest-version) do={\n  # Download the latest version\n  download\n\n  # Create a scheduler event that\n  # - upgrades RouterOS;\n  # - reboots the system;\n  # - upgrades the firmware;\n  # - reboots the system.\n  /system scheduler add name=REBOOT interval=5s on-event=\"/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\\\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\\\"; :execute {/system reboot;}\"\n}"






sshpass -p "$PASS" ssh "$USER@$IP" -p "$PORT" "add name=\"test\" source=\"$(sed -z 's/\n/\\n/g' "$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh" | sed 's/\\"/\\\\"/g;s/"/\\"/g')\"" | sed 's/"/\\"/g')"


sshpass -p "$PASS" ssh "$USER@$IP" -p "$PORT" "add name=\"test\" source=\"$(sed -z 's/\n/\\n/g' "$XDG_GIT_DIR/lnx_scripts/init_scripts/mikrotik_router/routeros_upgrade.sh" | sed 's/\\"/\\\\"/g;s/"/\\"/g')\""


sed 's/\\"/\\\\"/g;s/"/\\"/g;s/\\/\\\\/g


sshpass -p n45CjtYxWKXGJd ssh admin@192.168.1.1 -p 9713 add name="test" source="# Upgrade Mikrotik RouterOS and the firmware\n\n# author:  Tukusej's Sirs\n# date:    13 August 2020\n# version: 1.0\n\n# src: https://mivilisnet.wordpress.com/2020/03/31/updating-mikrotik-router-from-the-command-line/\n\n\n# Enter the specified location\n/system package update\n\n# Set the channel\nset channel=stable\n\n# Check for updates\ncheck-for-updates\n\n# Upgrade if current version is not the latest\n:if (installed-version != latest-version) do={\n  # Download the latest version\n  download\n\n  # Create a scheduler event that\n  # - upgrades RouterOS;\n  # - reboots the system;\n  # - upgrades the firmware;\n  # - reboots the system.\n  /system scheduler add name=REBOOT interval=5s on-event=\"/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\\\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\\\"; :execute {/system reboot;}\"\n}"
# Upgrade Mikrotik RouterOS and the firmware\n\n# author:  Tukusej's Sirs\n# date:    13 August 2020\n# version: 1.0\n\n# src: https://mivilisnet.wordpress.com/2020/03/31/updating-mikrotik-router-from-the-command-line/\n\n\n# Enter the specified location\n/system package update\n\n# Set the channel\nset channel=stable\n\n# Check for updates\ncheck-for-updates\n\n# Upgrade if current version is not the latest\n:if (installed-version != latest-version) do={\n  # Download the latest version\n  download\n\n  # Create a scheduler event that\n  # - upgrades RouterOS;\n  # - reboots the system;\n  # - upgrades the firmware;\n  # - reboots the system.\n  /system scheduler add name=REBOOT interval=5s on-event=\"/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\\\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\\\"; :execute {/system reboot;}\"\n}