#!/bin/bash

# Upgrade Mikrotik RouterOS and the firmware

# src: https://mivilisnet.wordpress.com/2020/03/31/updating-mikrotik-router-from-the-command-line/


# Enter the specified location
/system package update

# Set the channel
set channel=stable

# Check for updates
check-for-updates

# Upgrade if current version is not the latest
:if (installed-version != latest-version) do={
  # Download the latest version
  download

  # Create a scheduler event that
  # - upgrades RouterOS;
  # - reboots the system;
  # - upgrades the firmware;
  # - reboots the system.
  /system scheduler add name=REBOOT interval=5s on-event="/system scheduler remove REBOOT; /system scheduler add name=UPGRADE start-time=startup on-event=\"/system scheduler remove UPGRADE; :execute {/system routerboard upgrade;}; :execute {/system reboot;}\"; :execute {/system reboot;}"
}