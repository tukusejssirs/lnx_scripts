#!/bin/bash

# Exit codes:
# 1     No Internet connection
# 2     Failed to mount /mnt
# 3     Failed to mount /mnt/boot

# TODO: Before reinstallation, backup these:
# - psql DBs;
# - all local Git repos;
# - /ggl_drive;
# - /mega;
#
# - remove all duplicates and unnecessary files from my home folder;
#
# - Telegram configuration: https://github.com/telegramdesktop/tdesktop/wiki/Environment-variables;
# - add logging to file;
# - add `asciirama`;
# - on Intel CPUs, install Thermald;
#
# FIXME:
# - `git_init`: `gpg` generation still not working, although it works for Itens Git;
# - `gnome-terminal` colours are not set;
# - background in Gnome Desktop is not black, but blue;
# - in Gnome Desktop: dark UI theme is not used;
# - smerge crack does not work (it is not yet on the wabsite); try this: https://gist.github.com/rufoa/78e45d70f560f53678853c92dae2598a
# - Gnome Desktop: user account picture is not set yet;
# - no apps/programs installed in `/opt` (except for `smerge` and `subl`);
# - no `opera` broswer is installed;
# - `fonts.sh` are not installed;
#
# - `gnome_init`: no such keys: `title`, `use-transparent-background`, `display-overview-map`;
# - `gnome_init`: some settings requires `DISPLAY` variable to be set, others require the user to be logged into GNOME;
# - `yay` installation: `pacman` complains that `git` is a file and no `go` target;
#
# TESTS:
# - translate-shell: Check if my aliases still work;
# - check if the user has access to the optical drive;
# - if `npm` packages are instaled;

# TODO: Check optional dependencies of `vlc` on Arch Linux

# Note: `ttf-opensans` is needed by `telegram-desktop` (although optionally, but there might be some issues when it is not available).

# TODO: Create symlink for Todoist (check the path where `todoist-appimage` install Todoist)
# sudo ln -s /opt/todoist/todoist.appimage /opt/bin/todoist

# TODO: Add for Janka:
# - crotchet_charts;
# - jbead: $XDG_GIT_DIR/lnx_scripts/bash_progs/jbead.sh;
# - simple-scan;
# - kig;
# - firefox;
# - geogebra-5 [aur];
#
# FIXME: Change the trackpad speed to max for `janka` user (or on `e530c` notebook).
# - option 1:
# 	- gsettings get org.gnome.desktop.peripherals.touchpad speed
# - option 2:
# 	- xinput --list --short      # Get the ID
# 	- xinput --list-props "$ID"  # Get current settings
# 	- xinput --set-prop "$ID" "Device Accel Constant Deceleration" 1.5  # Set the speed

# TODO: add:
# - TODO: eid_client: add desktop file in order to add icon to the startup apps list in GNOME;
# - TODO: todoist: add desktop file in order to add icon to the startup apps list in GNOME;
# - TODO: transmission: add desktop file in order to add icon to the startup apps list in GNOME; currently, there is a `missing icon` icon;
# - [AUR] fragments-git [consider: bittorrent clinet];
# - girmshot;
# - samsung_printer_driver.sh:
	# # Add Samsung printer
	# sudo pacman -Sy --noconfirm cups cups-pdf
	# sudo systemctl enable --now cups
	# # Add a printer using http://localhost:631/admin CUPS web configuration (you need to login with the system user credentials)
	# # TODO: Add a way to do this programmatically
# - sane;
# - vbox: TODO: check if everything works as expected;
# - ??? cpanminus;
# - ??? jdk-openjdk;
# - ??? hdparm;
#
# - if the device is Lenovo Thinkpad, install `fwupd` (https://wiki.archlinux.org/title/Fwupd); don't forget to check for the updates and install them;
# - p15: configure fan control (https://wiki.archlinux.org/title/Lenovo_ThinkPad_P15_Gen_1#Fan_control);
# - p15: Enabling Turbo boost (https://wiki.archlinux.org/title/Lenovo_ThinkPad_P15_Gen_1#Enabling_Turbo_boost);
# - p15: check if the built-in smart card reader works with `pcscd`;
# - p15: console font is too small; use `setfont /usr/share/kbd/consolefonts/sun12x22.psfu.gz` (or find better one)



install_arch() {
	# Constants
	local LMAGENTA='\e[35m'
	local FDEFAULT='\e[39m'

	echo -e "${LMAGENTA}[INFO] Starting installation ...${FDEFAULT}"

	# Variables
	local BIOS_TYPE
	local HOST_NAME
	local BW_EMAIL
	local BW_PASSWORD
	local BW_PASSWORD_CONFIRMATION
	local BW_PASSWORD_MATCHES='false'
	local BW_SESSION_FILE='.bw/BW_SESSION'  # This path is relative to home folder
	local CMDLINE_LINUX_ROOT
	local COUNTRIES
	local CPU_VENDOR
	local DEVICE_LVM
	local DEVICE_ROOT
	local DISK_PARTITION_SEPARATOR
	local HOOKS
	local MODULES
	local OPTIONS
	local PACKAGES_DDX
	local PACKAGES_DRIVER
	local PACKAGES_DRIVER_MULTILIB
	local PACKAGES_HARDWARE_ACCELERATION
	local PACKAGES_HARDWARE_ACCELERATION_MULTILIB
	local PACKAGES_VULKAN
	local PACKAGES_VULKAN_MULTILIB
	local PARTITION_BOOT
	local PARTITION_PARTED_BIOS='mklabel msdos mkpart primary ext4 4MiB 512MiB mkpart primary ext4 512MiB 100% set 1 boot on'
	local PARTITION_PARTED_UEFI='mklabel gpt mkpart ESP fat32 1MiB 512MiB mkpart root ext4 512MiB 100% set 1 esp on'
	local PARTITION_ROOT
	local ROOT_PASSWORD
	local SYSTEMD_MICROCODE
	local SYSTEMD_OPTIONS
	local USER_PASSWORD
	local VG_LIST
	local VIRTUALBOX
	local MODEL
	local list_gpu
	local DISPLAY_DRIVER

	### PRE-CONFIGURATION

	# Load keyboard layout
	echo -e "${LMAGENTA}[INFO] Loading $KBLO keyboard layout ...${FDEFAULT}"
	loadkeys "$KBLO"

	# Ask the user for Bitwarden email
	echo -e "${LMAGENTA}[INFO] Asking for Bitwarden email ...${FDEFAULT}"
	read -rp 'Type Bitwarden email: ' BW_EMAIL

	# Ask the user for Bitwarden password
	echo -e "${LMAGENTA}[INFO] Asking for Bitwarden password ...${FDEFAULT}"

	while [ "$BW_PASSWORD_MATCHES" != 'true' ]; do
		read -srp 'Type Bitwarden password: ' BW_PASSWORD
		echo
		read -srp 'Confirm Bitwarden password: ' BW_PASSWORD_CONFIRMATION
		echo
		if [ "$BW_PASSWORD" == "$BW_PASSWORD_CONFIRMATION" ]; then
			BW_PASSWORD_MATCHES='true'
		else
			echo -e "${LMAGENTA}[WARNING] Bitwarden password does not match. Please, try again.${FDEFAULT}"
		fi
	done

	if [ ! "$PING_HOSTNAME" ]; then
		PING_HOSTNAME='mirrors.kernel.org'
	fi

	if [ ! "$LVM_LV_NAME" ]; then
		LVM_LV_NAME='root'
	fi

	if [ ! "$LVM_VG_NAME" ]; then
		LVM_VG_NAME='vg_arch'
	fi

	# Gather facts
	echo -e "${LMAGENTA}[INFO] Gathering facts ...${FDEFAULT}"

	if [ -d /sys/firmware/efi ]; then
		BIOS_TYPE='uefi'
	else
		BIOS_TYPE='bios'
	fi

	if grep -q '^/dev/\(nvme\|mmc\)' <<< "$DEVICE"; then
		DISK_PARTITION_SEPARATOR='p'
	fi

	if lscpu | grep -q 'GenuineIntel'; then
		CPU_VENDOR='intel'
	elif lscpu | grep -q 'AuthenticAMD'; then
		CPU_VENDOR='amd'
	fi

	if lspci | grep -qi 'virtualbox'; then
		VIRTUALBOX='true'
	fi

	# Enable NTP on Live OS
	echo -e "${LMAGENTA}[INFO] Enabling NTP on Live OS ...${FDEFAULT}"
	timedatectl set-ntp true

	# Make sure the disk is not mounted
	echo -e "${LMAGENTA}[INFO] Unmounting devices ...${FDEFAULT}"
	UMOUNT_DEVICES="$(mount | grep -o "^/mnt/boot \|^/mnt \|^${DEVICE}[^ ]*\|^/dev/mapper/$LVM_VG_NAME-[^ ]*" | tr '\n' ' ' | sed 's/  \+/ /;s/\s*$//')"
	umount $UMOUNT_DEVICES > /dev/null 2>&1 | grep -v "^umount: \(/mnt\|/mnt/boot\|${DEVICE}[^:]*\|/dev/mapper/$LVM_VG_NAME-$LVM_LV_NAME\):" || true

	# Make sure no LVM LV is active
	echo -e "${LMAGENTA}[INFO] Deactivating LVM LVs ...${FDEFAULT}"
	vgchange -an

	# Remove any VG on the DEVICE
	# FIXME: Use BTRFS without LVM instead.
	# FIXME: Check if Windows is installed; if yes, don't remove it and add it to GRUB options.
	echo -e "${LMAGENTA}[INFO] Removing LVM VGs ...${FDEFAULT}"
	VG_LIST=("$(pvs | grep -Po "^\s*${DEVICE}[^ ]*\s*\K[^ ]+" | sort | uniq)")

	for vg in "${VG_LIST[@]}"; do
		# Format LVs
		# Note: Otherwise, when you use previously formatted partition(s), there might occur some warnings.
		find "/dev/$vg"* -exec wipefs -af {} \; 2> /dev/null

		# Remove the previous VG(s)
		vgremove -ff "$vg"
	done

	# Connect to the WiFi network
	if [ "$SETUP_WIFI" == 'true' ]; then
		echo -e "${LMAGENTA}[INFO] Connecting to the WiFi network ...${FDEFAULT}"
		iwctl --passphrase "$WIFI_PSK" station "$WIFI_INTERFACE" connect "$WIFI_ESSID"
		sleep 10
	fi

	# Test if we are online
	echo -e "${LMAGENTA}[INFO] Testing Internet connection ...${FDEFAULT}"
	if ! ping -c 1 -i 2 -W 5 -w 30 "$PING_HOSTNAME" &> /dev/null; then
		echo "[ERROR] Network ping check failed. Cannot continue."
		exit 1
	fi

	### PARTITIONING

	# Probe partition table for changes
	echo -e "${LMAGENTA}[INFO] Probing the device partition table for changes ...${FDEFAULT}"
	partprobe "$DEVICE"

	# Partitioning
	PARTITION_BOOT="$DEVICE${DISK_PARTITION_SEPARATOR}1"
	PARTITION_ROOT="$DEVICE${DISK_PARTITION_SEPARATOR}2"
	DEVICE_ROOT="$DEVICE${DISK_PARTITION_SEPARATOR}2"

	PARTITION_ROOT_NUMBER="$(grep -o '[0-9]\+$' <<< "$PARTITION_ROOT")"

	# Wipe the disk
	echo -e "${LMAGENTA}[INFO] Wiping the disk ...${FDEFAULT}"
	sgdisk -Z "$DEVICE"
	wipefs -af "$DEVICE"
	partprobe "$DEVICE"

	# Create a boot partition
	echo -e "${LMAGENTA}[INFO] Creating a boot partition [$PARTITION_BOOT] ...${FDEFAULT}"
	case "$BIOS_TYPE" in
		'uefi')
			parted -s "$DEVICE" "$PARTITION_PARTED_UEFI"
			sgdisk -t "$PARTITION_ROOT_NUMBER:8e00" "$DEVICE"
		;;
		"bios")
			parted -s "$DEVICE" "$PARTITION_PARTED_BIOS"
		;;
	esac

	partprobe "$DEVICE"

	# Create LVM VG, LV and root partition
	echo -e "${LMAGENTA}[INFO] Creating LVM VG, LV and root partition [$PARTITION_ROOT; /dev/mapper/$LVM_VG_NAME-$LVM_LV_NAME] ...${FDEFAULT}"
	DEVICE_LVM="$DEVICE_ROOT"
	DEVICE_ROOT="/dev/mapper/$LVM_VG_NAME-$LVM_LV_NAME"

	pvcreate "$DEVICE_LVM"
	vgcreate "$LVM_VG_NAME" "$DEVICE_LVM"
	# Note: For some reasons `-Wy` does not work as intended at `lvcreate` still shows a warning question that a signature is detected.
	echo 'yes' | lvcreate -Wy -l 100%FREE -n "$LVM_LV_NAME" "$LVM_VG_NAME"

	# Wipe partitions
	echo -e "${LMAGENTA}[INFO] Wiping the partitions ...${FDEFAULT}"
	wipefs -af "$PARTITION_BOOT" || true
	wipefs -af "$DEVICE_ROOT" || true

	# Format boot partition
	echo -e "${LMAGENTA}[INFO] Formatting the partitions ...${FDEFAULT}"
	if [ "$BIOS_TYPE" == 'uefi' ]; then
		mkfs.fat -n ESP -F32 "$PARTITION_BOOT"
	fi

	if [ "$BIOS_TYPE" == 'bios' ]; then
		mkfs.ext4 -L boot "$PARTITION_BOOT"
	fi

	# Format root partition
	mkfs.ext4 -L root "$DEVICE_ROOT"

	echo -e "${LMAGENTA}[INFO] Mounting the partitions ...${FDEFAULT}"

	# Mount options
	PARTITION_OPTIONS_BOOT='defaults'
	PARTITION_OPTIONS_ROOT='defaults'

	if [ "$DEVICE_TRIM" == 'true' ]; then
		PARTITION_OPTIONS_BOOT="$PARTITION_OPTIONS_BOOT,noatime"
		PARTITION_OPTIONS_ROOT="$PARTITION_OPTIONS_ROOT,noatime"
	fi

	# Mount the partitions
	if ! mount -o "$PARTITION_OPTIONS_ROOT" "$DEVICE_ROOT" /mnt; then
		echo "[ERROR] command: mount -o \"$PARTITION_OPTIONS_ROOT\" \"$DEVICE_ROOT\" /mnt"
		echo '[ERROR] Failed to mount /mnt!' 1>&2
		exit 2
	fi

	mkdir /mnt/boot

	if ! mount -o "$PARTITION_OPTIONS_BOOT" "$PARTITION_BOOT" /mnt/boot; then
		echo "[ERROR] command: mount -o \"$PARTITION_OPTIONS_BOOT\" \"$PARTITION_BOOT\" /mnt/boot"
		echo '[ERROR] Failed to mount /mnt/boot!' 1>&2
		exit 3
	fi

	# Create a swap file
	echo -e "${LMAGENTA}[INFO] Creating a swap file ...${FDEFAULT}"

	if [ "$SWAP_SIZE" ]; then
		dd if=/dev/zero of="/mnt$SWAPFILE" bs=1M count="$SWAP_SIZE" status=progress
		chmod 600 "/mnt$SWAPFILE"
		mkswap "/mnt$SWAPFILE"
	fi

	### BASE OS INSTALLATION AND CONFIGURATION

	# Configure `reflector`	on Live OS
	# TODO: Configure it in the installed OS too. Or is it done automatically?
	echo -e "${LMAGENTA}[INFO] Configuring reflector on Live OS ...${FDEFAULT}"

	if [ "$REFLECTOR" == 'true' ]; then
		if [ "$REFLECTOR_COUNTRIES" ]; then
			COUNTRIES="--country $REFLECTOR_COUNTRIES"
		fi

		pacman -Sy --noconfirm reflector
		reflector "$COUNTRIES" --latest 25 --age 24 --protocol https --completion-percent 100 --sort rate --save /etc/pacman.d/mirrorlist
	fi

	# Configure `pacman` output in Live OS
	echo -e "${LMAGENTA}[INFO] Configuring pacman output in Live OS ...${FDEFAULT}"
	sed -i 's/#Color/Color/;s/#TotalDownload/TotalDownload/' /etc/pacman.conf

	# Pacstrap basic OS
	echo -e "${LMAGENTA}[INFO] Pacstrapping basic OS ...${FDEFAULT}"
	pacstrap /mnt base base-devel linux linux-firmware

	# Configure `pacman` output
	echo -e "${LMAGENTA}[INFO] Configuring pacman output ...${FDEFAULT}"
	sed -i 's/#Color/Color/;s/#TotalDownload/TotalDownload/' /mnt/etc/pacman.conf

	echo -e "${LMAGENTA}[INFO] Configuring multilib in pacman.conf ...${FDEFAULT}"
	if [ "$PACKAGES_MULTILIB" == 'true' ]; then
		cat << EOF >> /mnt/etc/pacman.conf

[multilib]
Include = /etc/pacman.d/mirrorlist

EOF
	fi

	# Generate fstab
	echo -e "${LMAGENTA}[INFO] Generating fstab ...${FDEFAULT}"
	genfstab -U /mnt >> /mnt/etc/fstab

	if [ "$SWAP_SIZE" ]; then
		echo "$SWAPFILE none swap defaults 0 0" >> /mnt/etc/fstab
	fi

	# Setup trimming
	if [ "$DEVICE_TRIM" == 'true' ]; then
		sed -i 's/relatime/noatime/' /mnt/etc/fstab
		arch-chroot /mnt systemctl enable fstrim.timer
	fi

	# Setup timezone and hardware clock
	echo -e "${LMAGENTA}[INFO] Setting up timezone and hardware clock ...${FDEFAULT}"
	arch-chroot /mnt ln -sf "/usr/share/zoneinfo/$TIMEZONE" /etc/localtime
	arch-chroot /mnt hwclock --systohc

	# Configure locales
	echo -e "${LMAGENTA}[INFO] Configuring locales ...${FDEFAULT}"
	sed -i "s/#\(en_GB.UTF-8 UTF-8\)/\1/" /mnt/etc/locale.gen

	if [ "$LOCALE_ADDITIONAL" ]; then
		sed -i "s/#$LOCALE_ADDITIONAL/$LOCALE_ADDITIONAL/" /mnt/etc/locale.gen
	fi

	for v in "${LOCALE_CONF[@]}"; do
		echo -e "$v" >> /mnt/etc/locale.conf
	done

	arch-chroot /mnt locale-gen

	arch-chroot /mnt bash -c 'source /etc/locale.conf && localectl set-locale LANG=en_GB.UTF-8'

	# Configure TTY console output
	echo -e "${LMAGENTA}[INFO] Configuring TTY console output ...${FDEFAULT}"
	echo -e "KEYMAP=$KEYMAP\n$FONT\n$FONT_MAP" > /mnt/etc/vconsole.conf

	# Xorg/X11 keyboard configuration
	echo -e "${LMAGENTA}[INFO] Configuring Xorg/X11 keyboard ...${FDEFAULT}"

	if [ -n "$KEYLAYOUT" ]; then
		OPTIONS="    Option \"XkbLayout\" \"$KEYLAYOUT\""
	fi

	if [ -n "$KEYMODEL" ]; then
		OPTIONS="$OPTIONS"$'\n'"    Option \"XkbModel\" \"$KEYMODEL\""
	fi

	if [ -n "$KEYVARIANT" ]; then
		OPTIONS="$OPTIONS"$'\n'"    Option \"XkbVariant\" \"$KEYVARIANT\""
	fi

	if [ -n "$KEYOPTIONS" ]; then
		OPTIONS="$OPTIONS"$'\n'"    Option \"XkbOptions\" \"$KEYOPTIONS\""
	fi

	mkdir -p /mnt/etc/X11/xorg.conf.d
	cat << EOF > /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
# Written by systemd-localed(8), read by systemd-localed and Xorg. It's
# probably wise not to edit this file manually. Use localectl(1) to
# instruct systemd-localed to update it.
Section "InputClass"
Identifier "system-keyboard"
MatchIsKeyboard "on"
$OPTIONS
EndSection
EOF

	# Set swappiness value
	echo -e "${LMAGENTA}[INFO] Configuring swappiness ...${FDEFAULT}"

	if [ "$SWAP_SIZE" ]; then
		echo 'vm.swappiness=10' > /mnt/etc/sysctl.d/99-sysctl.conf
	fi

	# Display driver / kernel modules configuration
	echo -e "${LMAGENTA}[INFO] Configuring display driver / kernel modules ...${FDEFAULT}"

	list_gpu="$(lspci -k | grep -EA2 '(VGA|3D)' | grep -Po 'Kernel driver in use: \K.*$')"

	if grep -q 'nouveau\|nvidia' <<< "$list_gpu"; then
		DISPLAY_DRIVER='nvidia'
	else
		DISPLAY_DRIVER="$(head -1 <<< "$list_gpu")"
	fi

	if [ "$KMS" == "true" ]; then
		MODULES=''
		case "$DISPLAY_DRIVER" in
			'intel')
				MODULES='i915'
			;;
			'nvidia' | 'nvidia-lts' | 'nvidia-dkms' | 'nvidia-390xx' | 'nvidia-390xx-lts' | 'nvidia-390xx-dkms')
				MODULES='nvidia nvidia_modeset nvidia_uvm nvidia_drm'
			;;
			'amdgpu')
				MODULES='amdgpu'
			;;
			'ati')
				MODULES='radeon'
			;;
			'nouveau')
				MODULES='nouveau'
			;;
		esac

		sed -i "s/^MODULES=()/MODULES=($MODULES)/" /mnt/etc/mkinitcpio.conf
	fi

	# Kernel boot parameters configuration for Intel CPUs
	if [ "$DISPLAY_DRIVER" == 'intel' ]; then
		OPTIONS=''

		if [ "$FASTBOOT" == 'true' ]; then
			OPTIONS="$OPTIONS fastboot=1"
		fi

		if [ "$FRAMEBUFFER_COMPRESSION" == 'true' ]; then
			OPTIONS="$OPTIONS enable_fbc=1"
		fi

		if [ "$OPTIONS" ]; then
			echo "options i915 $OPTIONS" > /mnt/etc/modprobe.d/i915.conf
		fi
	fi

	# Install LVM2 package
	echo -e "${LMAGENTA}[INFO] Installing lvm2 ...${FDEFAULT}"
	arch-chroot /mnt pacman -Syu --noconfirm --needed lvm2

	# Configure SystemD bootloader hooks
	echo -e "${LMAGENTA}[INFO] Configuring SystemD bootloader hooks ...${FDEFAULT}"
	HOOKS='base systemd keyboard autodetect modconf block sd-vconsole lvm2 fsck filesystems'
	sed -i "s/^HOOKS=(.*)$/HOOKS=($HOOKS)/" /mnt/etc/mkinitcpio.conf

	# Configure kernel compression
	echo -e "${LMAGENTA}[INFO] Configuring kernel compression ...${FDEFAULT}"

	if [ "$KERNELS_COMPRESSION" ]; then
		sed -i 's/^#COMPRESSION="'"$KERNELS_COMPRESSION"'"/COMPRESSION="'"$KERNELS_COMPRESSION"'"/' /mnt/etc/mkinitcpio.conf
	fi

	# Install the required package for the specified kernel compression
	if [ "$KERNELS_COMPRESSION" == 'bzip2' ]; then
		arch-chroot /mnt pacman -Syu --noconfirm --needed bzip2
	fi

	if [ "$KERNELS_COMPRESSION" == 'lzma' ] || [ "$KERNELS_COMPRESSION" == 'xz' ]; then
		arch-chroot /mnt pacman -Syu --noconfirm --needed xz
	fi

	if [ "$KERNELS_COMPRESSION" == 'lzop' ]; then
		arch-chroot /mnt pacman -Syu --noconfirm --needed lzop
	fi

	if [ "$KERNELS_COMPRESSION" == 'lz4' ]; then
		arch-chroot /mnt pacman -Syu --noconfirm --needed lz4
	fi

	if [ "$KERNELS_COMPRESSION" == 'zstd' ]; then
		arch-chroot /mnt pacman -Syu --noconfirm --needed zstd
	fi

	# Driver packages installation
	echo -e "${LMAGENTA}[INFO] Installing packages for the display driver ...${FDEFAULT}"

	case "$DISPLAY_DRIVER" in
		'intel')
			PACKAGES_DRIVER_MULTILIB='lib32-mesa'
		;;
		'amdgpu')
			PACKAGES_DRIVER_MULTILIB='lib32-mesa'
		;;
		'ati')
			PACKAGES_DRIVER_MULTILIB='lib32-mesa'
		;;
		'nvidia')
			PACKAGES_DRIVER='nvidia'
			PACKAGES_DRIVER_MULTILIB='lib32-nvidia-utils'
		;;
		'nvidia-lts')
			PACKAGES_DRIVER='nvidia-lts'
			PACKAGES_DRIVER_MULTILIB='lib32-nvidia-utils'
		;;
		'nvidia-dkms')
			PACKAGES_DRIVER='nvidia-dkms'
			PACKAGES_DRIVER_MULTILIB='lib32-nvidia-utils'
		;;
		'nvidia-390xx')
			PACKAGES_DRIVER='nvidia-390xx'
		;;
		'nvidia-390xx-lts')
			PACKAGES_DRIVER='nvidia-390xx-lts'
		;;
		'nvidia-390xx-dkms')
			PACKAGES_DRIVER='nvidia-390xx-dkms'
		;;
		'nouveau')
			PACKAGES_DRIVER_MULTILIB='lib32-mesa'
		;;
	esac

	if [ "$DISPLAY_DRIVER_DDX" == 'true' ]; then
		case "$DISPLAY_DRIVER" in
			'intel')
				PACKAGES_DDX='xf86-video-intel'
			;;
			'amdgpu')
				PACKAGES_DDX='xf86-video-amdgpu'
			;;
			'ati')
				PACKAGES_DDX='xf86-video-ati'
			;;
			'nouveau')
				PACKAGES_DDX='xf86-video-nouveau'
			;;
		esac
	fi

	if [ "$VULKAN" == 'true' ]; then
		case "$DISPLAY_DRIVER" in
			'intel')
				PACKAGES_VULKAN='vulkan-icd-loader vulkan-intel'
				PACKAGES_VULKAN_MULTILIB='lib32-vulkan-icd-loader lib32-vulkan-intel'
			;;
			'amdgpu')
				PACKAGES_VULKAN='vulkan-radeon'
				PACKAGES_VULKAN_MULTILIB='lib32-vulkan-radeon'
			;;
			'ati')
				PACKAGES_VULKAN='vulkan-radeon'
				PACKAGES_VULKAN_MULTILIB='lib32-vulkan-radeon'
			;;
			'nvidia')
				PACKAGES_VULKAN='nvidia-utils'
				PACKAGES_VULKAN_MULTILIB='lib32-nvidia-utils'
			;;
			'nvidia-lts')
				PACKAGES_VULKAN='nvidia-utils'
				PACKAGES_VULKAN_MULTILIB='lib32-nvidia-utils'
			;;
			'nvidia-dkms')
				PACKAGES_VULKAN='nvidia-utils'
				PACKAGES_VULKAN_MULTILIB='lib32-nvidia-utils'
			;;
			'nvidia-390xx')
				PACKAGES_VULKAN='nvidia-utils'
			;;
			'nvidia-390xx-lts')
				PACKAGES_VULKAN='nvidia-utils'
			;;
			'nvidia-390xx-dkms')
				PACKAGES_VULKAN='nvidia-utils'
			;;
		esac
	fi

	if [ "$DISPLAY_DRIVER_HARDWARE_ACCELERATION" == 'true' ]; then
		case "$DISPLAY_DRIVER" in
			'intel')
				if [ "$DISPLAY_DRIVER_HARDWARE_ACCELERATION_INTEL" ]; then
					PACKAGES_HARDWARE_ACCELERATION="$DISPLAY_DRIVER_HARDWARE_ACCELERATION_INTEL"
				fi
			;;
			'amdgpu')
				PACKAGES_HARDWARE_ACCELERATION='libva-mesa-driver'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-libva-mesa-driver'
			;;
			'ati')
				PACKAGES_HARDWARE_ACCELERATION='mesa-vdpau'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-mesa-vdpau'
			;;
			'nvidia')
				PACKAGES_HARDWARE_ACCELERATION='libva-mesa-driver'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-libva-mesa-driver'
			;;
			'nvidia-lts')
				PACKAGES_HARDWARE_ACCELERATION='libva-mesa-driver'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-libva-mesa-driver'
			;;
			'nvidia-dkms')
				PACKAGES_HARDWARE_ACCELERATION='libva-mesa-driver'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-libva-mesa-driver'
			;;
			'nvidia-390xx')
				PACKAGES_HARDWARE_ACCELERATION='libva-mesa-driver'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-libva-mesa-driver'
			;;
			'nvidia-390xx-lts')
				PACKAGES_HARDWARE_ACCELERATION='libva-mesa-driver'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-libva-mesa-driver'
			;;
			'nvidia-390xx-dkms')
				PACKAGES_HARDWARE_ACCELERATION='libva-mesa-driver'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-libva-mesa-driver'
			;;
			'nouveau')
				PACKAGES_HARDWARE_ACCELERATION='libva-mesa-driver'
				PACKAGES_HARDWARE_ACCELERATION_MULTILIB='lib32-libva-mesa-driver'
			;;
		esac
	fi

	arch-chroot /mnt pacman -Syu --noconfirm --needed mesa $PACKAGES_DRIVER $PACKAGES_DDX $PACKAGES_VULKAN $PACKAGES_HARDWARE_ACCELERATION

	if [ "$PACKAGES_MULTILIB" == "true" ]; then
		arch-chroot /mnt pacman -Syu --noconfirm --needed $PACKAGES_DRIVER_MULTILIB $PACKAGES_VULKAN_MULTILIB $PACKAGES_HARDWARE_ACCELERATION_MULTILIB
	fi

	# Install Linux kernel
	echo -e "${LMAGENTA}[INFO] Installing the Linux kernel ...${FDEFAULT}"
	arch-chroot /mnt pacman -Syu --noconfirm --needed linux-headers $KERNELS

	# Change some permissions
	# Note: Actually, the permissions are set the the same ones the folders already had, but if we don't do this, (sometimes?) `su` would fail to execute `/bin/bash`. See https://bbs.archlinux.org/viewtopic.php?pid=831408#p831408
	echo -e "${LMAGENTA}[INFO] Changing some folder permissions ...${FDEFAULT}"
	chmod 755 / /bin /usr/bin

	# Create an initial ramdisk environment
	echo -e "${LMAGENTA}[INFO] Creating an initial ramdisk environment ...${FDEFAULT}"
	arch-chroot /mnt mkinitcpio -P

	# Install NetworkManager
	echo -e "${LMAGENTA}[INFO] Installing NetworkManager ...${FDEFAULT}"
	arch-chroot /mnt pacman -Syu --noconfirm --needed networkmanager

	if [ "$VIRTUALBOX" == 'true' ]; then
		# Install Virtualbox guest utilities
		echo -e "${LMAGENTA}[INFO] Installing Virtualbox guest utilities ...${FDEFAULT}"

		if [ -z "$KERNELS" ]; then
			arch-chroot /mnt pacman -Syu --noconfirm --needed virtualbox-guest-utils
		else
			arch-chroot /mnt pacman -Syu --noconfirm --needed virtualbox-guest-utils virtualbox-guest-dkms
		fi
	else
		# Install ucode based on the particular CPU vendor
		echo -e "${LMAGENTA}[INFO] Installing ucode based on the particular CPU vendor ...${FDEFAULT}"

		if [ "$CPU_VENDOR" == 'intel' ]; then
			arch-chroot /mnt pacman -Syu --noconfirm --needed intel-ucode
		fi

		if [ "$CPU_VENDOR" == "amd" ]; then
			arch-chroot /mnt pacman -Syu --noconfirm --needed amd-ucode
		fi
	fi

	# Configure SystemD bootloader
	echo -e "${LMAGENTA}[INFO] Configuring SystemD bootloader ...${FDEFAULT}"

	CMDLINE_LINUX_ROOT="root=$DEVICE_ROOT"

	if [ "$KMS" == 'true' ]; then
		case "$DISPLAY_DRIVER" in
			'nvidia' | 'nvidia-390xx' | 'nvidia-390xx-lts')
				CMDLINE_LINUX="$CMDLINE_LINUX nvidia-drm.modeset=1"
			;;
		esac
	fi

	if [ "$KERNELS_PARAMETERS" ]; then
		CMDLINE_LINUX="$CMDLINE_LINUX $KERNELS_PARAMETERS"
	fi

	arch-chroot /mnt systemd-machine-id-setup
	arch-chroot /mnt bootctl --path='/boot' install
	mkdir -p /mnt/boot/loader/entries

	cat << EOF > "/mnt/boot/loader/loader.conf"
# alis
timeout 5
default archlinux
editor 1
EOF

	mkdir -p /mnt/etc/pacman.d/hooks

	cat << EOF > /mnt/etc/pacman.d/hooks/systemd-boot.hook
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot
When = PostTransaction
Exec = /usr/bin/bootctl update
EOF

	if [ "$VIRTUALBOX" != 'true' ]; then
		if [ "$CPU_VENDOR" == 'intel' ]; then
			SYSTEMD_MICROCODE='/intel-ucode.img'
		fi

		if [ "$CPU_VENDOR" == 'amd' ]; then
			SYSTEMD_MICROCODE='/amd-ucode.img'
		fi
	fi

	cat << EOF >> "/mnt/boot/loader/entries/archlinux.conf"
title Arch Linux
efi /vmlinuz-linux
EOF

	if [ "$SYSTEMD_MICROCODE" ]; then
		echo "initrd $SYSTEMD_MICROCODE" >> "/mnt/boot/loader/entries/archlinux.conf"
	fi

	cat << EOF >> "/mnt/boot/loader/entries/archlinux.conf"
initrd /initramfs-linux.img
options initrd=initramfs-linux.img $CMDLINE_LINUX_ROOT rw $CMDLINE_LINUX $SYSTEMD_OPTIONS
EOF

	cat << EOF >> "/mnt/boot/loader/entries/archlinux-terminal.conf"
title Arch Linux (terminal)
efi /vmlinuz-linux
EOF

	if [ "$SYSTEMD_MICROCODE" ]; then
		echo "initrd $SYSTEMD_MICROCODE" >> "/mnt/boot/loader/entries/archlinux-terminal.conf"
	fi

	cat << EOF >> "/mnt/boot/loader/entries/archlinux-terminal.conf"
initrd /initramfs-linux.img
options initrd=initramfs-linux.img $CMDLINE_LINUX_ROOT rw $CMDLINE_LINUX systemd.unit=multi-user.target $SYSTEMD_OPTIONS
EOF

	cat << EOF >> "/mnt/boot/loader/entries/archlinux-fallback.conf"
title Arch Linux (fallback)
efi /vmlinuz-linux
EOF

	if [ "$SYSTEMD_MICROCODE" ]; then
		echo "initrd $SYSTEMD_MICROCODE" >> "/mnt/boot/loader/entries/archlinux-fallback.conf"
	fi

	cat << EOF >> "/mnt/boot/loader/entries/archlinux-fallback.conf"
initrd /initramfs-linux-fallback.img
options initrd=initramfs-linux-fallback.img $CMDLINE_LINUX_ROOT rw $CMDLINE_LINUX $SYSTEMD_OPTIONS
EOF

	if [ "$VIRTUALBOX" == "true" ]; then
		echo -n "\EFI\systemd\systemd-bootx64.efi" > "/mnt/boot/startup.nsh"
	fi

	### INSTALLATION OF PACKAGES GLOBALLY

	# Change the default target to be graphical
	echo -e "${LMAGENTA}[INFO] Changing the default target to be graphical ...${FDEFAULT}"
	arch-chroot /mnt systemctl set-default graphical.target

	# Install packages via `pacman`
	if [ "$PACKAGES_PACMAN_INSTALL" = 'true' ] && [ "$PACKAGES_PACMAN" ]; then
		echo -e "${LMAGENTA}[INFO] Installing packages via pacman ...${FDEFAULT}"
		arch-chroot /mnt bash -c "LANG=en_GB.UTF-8 pacman -Syu --noconfirm --needed $PACKAGES_PACMAN"
	fi

	# Install packages via `flatpak`
	if [ "$PACKAGES_FLATPAK_INSTALL" = 'true' ]; then
		echo -e "${LMAGENTA}[INFO] Installing packages via flatpak ...${FDEFAULT}"
		arch-chroot /mnt pacman -Syu --noconfirm --needed flatpak
		arch-chroot /mnt flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

		if [ "$PACKAGES_FLATPAK" ]; then
			arch-chroot /mnt flatpak install --system -y flathub $PACKAGES_FLATPAK
		fi
	fi

	# Install packages via `npm`
	if [ "$PACKAGES_NPM_INSTALL" = 'true' ]; then
		echo -e "${LMAGENTA}[INFO] Installing packages via npm globally ...${FDEFAULT}"
		arch-chroot /mnt npm -g i npm $PACKAGES_NPM
	fi

	# TODO: Add installation via `pip` [python]
	# pip3 install bluetooth_battery

	# TODO: Add installation via `cpan` [perl]

	# TODO: Add installation via `go` [golang]

	# Build and install Git from source
	# TODO: How to build/install git-lfs?
	# TODO: Decide if this is better for `ts` user (in comparison with `git` installed from the repos) or not
	# arch-chroot /mnt "$XDG_GIT_DIR/lnx_scripts/bash_progs/git_src.sh"

	# TODO: Add shortcut for GenoPro (wine32) if it is (to be) installed

	# TODO: Add shortcut for MuseScore if it is (to be) installed

	# Install Lenovo Thinkpad E531 WiFi driver
	if [ "$INSTALL_WIFI_DRIVER" ] && [ "$MODEL" = 'e531' ]; then
		echo -e "${LMAGENTA}[INFO] Installing Lenovo Thinkpad E531 WiFi driver ...${FDEFAULT}"
		arch-chroot /mnt pacman -Syu --noconfirm --needed broadcom-wl-dkms
	fi

	# Install Sublime Text 3
	# TODO: Add this the the user-dependent options.
	echo -e "${LMAGENTA}[INFO] Installing Sublime Text 3 ...${FDEFAULT}"
	arch-chroot /mnt bash -c '
		curl -O https://download.sublimetext.com/sublimehq-pub.gpg
		pacman-key --add sublimehq-pub.gpg
		sudo pacman-key --lsign-key 8A8F901A
		rm sublimehq-pub.gpg
		echo -e "\n[sublime-text]\nServer = https://download.sublimetext.com/arch/stable/x86_64" > /etc/pacman.conf
		pacman -Syu --noconfirm --needed sublime-text

		# Variables
		subl_ver=$(subl --version | grep -o "[0-9]*$")
		subl_path="/opt/sublime_text/sublime_text"
		url="https://cynic.al/warez/$(curl -s https://cynic.al/warez/ | grep -o "sublime-text/${subl_ver}-linux/sublime_text_linux${subl_ver}cracked")"

		# Crack Sublime Text 3
		sudo mv $subl_path{,.bak}
		sudo curl -so "$subl_path" "$url"
		sudo chmod a+x "$subl_path"

		# Install the dummy licence
		curl -so ~/.config/sublime-text-3/Local/License.sublime_license https://cynic.al/warez/sublime-text/st_dummy_licence.txt
	'

	# Install Sublime Merge
	# TODO: Add this the the user-dependent options.
	echo -e "${LMAGENTA}[INFO] Installing Sublime Merge ...${FDEFAULT}"
	arch-chroot /mnt bash -c '
		pacman -Syu --noconfirm --needed sublime-merge

		# Variables
		smerge_ver=$(smerge --version | grep -o "[0-9]*$")
		smerge_path="/opt/sublime_merge/sublime_merge"
		url="https://cynic.al/warez/$(curl -s https://cynic.al/warez/ | grep -o "sublime-merge/${smerge_ver}-linux/sublime_merge_linux${smerge_ver}cracked")"

		# Crack Sublime Merge
		sudo mv ${smerge_path}{,.bak}
		sudo curl -so "$smerge_path" "$url"
		sudo chmod a+x "$smerge_path"

		# Install the dummy licence
		curl -so ~/.config/sublime-merge/Local/License.sublime_license https://cynic.al/warez/sublime-merge/sm_dummy_licence.txt
	'

	### ADDITIONAL GLOBAL CONFIGURATION

	# Set up hostname
	# TODO: Make this more general
	echo -e "${LMAGENTA}[INFO] Configuring hostname ...${FDEFAULT}"
	MODEL="$(arch-chroot /mnt dmidecode -t system | grep -Po 'Version: [ThinkPad Edge]* \K.*$' | tr '[:upper:]' '[:lower:]')"

	if [ ! "$MODEL" ]; then
		MODEL='test'
	fi

	HOST_NAME="$MODEL.domcek.lan"
	arch-chroot /mnt hostnamectl set-hostname "$HOST_NAME"

	# Disable suspending when the lid is closed
	# TODO: Add this to a separate settings BASH script.
	echo -e "${LMAGENTA}[INFO] Disabling suspending when the lid is closed ...${FDEFAULT}"
	sed -i 's/^#*\(HandlePowerKey\)=.*$/\1=ignore/;
		s/^#*\(HandleLidSwitch\)=.*$/\1=ignore/;
		s/^#*\(HandleLidSwitchExternalPower\)=.*$/\1=ignore/;
		s/^#*\(HandleLidSwitchDocked\)=.*$/\1=ignore/;
		s/^#*\(IdleAction\)=.*$/\1=ignore/' /mnt/etc/systemd/logind.conf

	# Remove htop.desktop
	# Note: I don't want it in the Gnome Overview; I access `htop` from terminal only.
	echo -e "${LMAGENTA}[INFO] Removing htop.desktop ...${FDEFAULT}"
	rm -rf /mnt/usr/share/applications/htop.desktop

	# Update `/var/lib/mlocate/mlocate.db`
	# Note: Otherwise `locate` would fail running.
	echo -e "${LMAGENTA}[INFO] Updating mlocate database ...${FDEFAULT}"
	arch-chroot /mnt updatedb

	# TODO: Restore `ssh` known hosts list
	# TODO: Use `git_init.sh` (and `ssh_key_init.sh` or similar) as much as possible
	# mkdir -p "$HOME/.ssh"
	# rm -rf "$HOME/.ssh/known_hosts"
	# cp "$XDG_GIT_DIR/lnx_data/ssh/known_hosts" "$HOME/.ssh/known_hosts"
	# chmod 600 "$HOME/.ssh/known_hosts"

	# Enable root login in GDM
	# Does not work on Fedora 30 (no such file)
	# TODO: Test if we can login as root on Manjaro by default
	# sudo sh -c "sed -i 's/auth required pam_succeed_if.so user != root quiet/# &/' /etc/pam.d/gdm"

	# Setup `/etc/hosts`
	# TODO: I have not done this on Janka's nb yet
	# mkdir -p $XDG_GIT_DIR/others/stevenblack_hosts
	# git clone git@github.com:StevenBlack/hosts.git $XDG_GIT_DIR/others/stevenblack_hosts
	# cat $XDG_GIT_DIR/lnx_data/hosts/hosts_local_domains $XDG_GIT_DIR/others/stevenblack_hosts/alternates/fakenews-gambling-porn/hosts | sudo tee /etc/hosts &>/dev/null

	# TODO
	# - setup networks:
	# 	- add `eth_dhcp`;
	# 	- remove `Wired connection 1`;
	# 	- add `domcek_wlan`: `nmcli dev wifi connect domcek password DobrovolneDarujDobro` (TODO: get the password from `bw`);
	# 	- add `eth_10.14.7.200` (only for ts user);
	# 	- add `vehovsky_wlan` (only for ts user);
	# 	- add `hotel_koruna_opava_wlan` (only for ts user);
	# 	- add `op3t_wlan` (only for ts user);
	# 	- add `op3t_usb_left_front` (only for ts user);
	# 	- add `op3t_usb_left_rear` (only for ts user);
	# 	- add `op3t_usb_right` (only for ts user);
	# 	- add `op6t_wlan` (only for ts user);
	# 	- add `op6t_usb_left_front` (only for ts user);
	# 	- add `op6t_usb_left_rear` (only for ts user);
	# 	- add `op6t_usb_right` (only for ts user);

	### ROOT USER CONFIGURATION

	# Clone `lnx_scripts` repo
	echo -e "${LMAGENTA}[INFO] Cloning lnx_scripts repo ...${FDEFAULT}"
	arch-chroot /mnt git clone https://gitlab.com/tukusejssirs/lnx_scripts.git /root/lnx_scripts

	# Log into Bitwarden for root user
	echo -e "${LMAGENTA}[INFO] Logging into Bitwarden for root user ...${FDEFAULT}"
	BW_SESSION=$(arch-chroot /mnt bash -c "\$(command -v bw) login '$BW_EMAIL' '$BW_PASSWORD' | tail -1 | awk '{print \$6}'")
	mkdir -p "$(dirname "/mnt/root/$BW_SESSION_FILE")"
	echo "$BW_SESSION" > "/mnt/root/$BW_SESSION_FILE"

	# Initialise Git for root user
	echo -e "${LMAGENTA}[INFO] Initialising Git for root user ...${FDEFAULT}"
	arch-chroot /mnt bash -c "gpg-agent --daemon; /root/lnx_scripts/init_scripts/git_init.sh"

	# Sync the Bitwarden vault
	echo -e "${LMAGENTA}[INFO] Syncing the Bitwarden vault ...${FDEFAULT}"
	arch-chroot /mnt bash -c "\$(command -v bw) sync --session '$BW_SESSION'"

	# Get Gitlab password from the Bitwarden vault
	echo -e "${LMAGENTA}[INFO] Getting Gitlab password from the Bitwarden vault ...${FDEFAULT}"
	gitlab_password="$(arch-chroot /mnt bash -c "\$(command -v bw) get item 0e6be610-32b0-44ec-9778-aa72007ef131 --session '$BW_SESSION' | jq -r .login.password")"

	# Clone `root_config_fedora_31` repo to `/root/.config`
	# TODO: Create `root_config_arch_linux` repo when I start using Arch Linux.
	echo -e "${LMAGENTA}[INFO] Cloning root_config_fedora_31 repo ...${FDEFAULT}"
	cp -r /mnt/root/.config/Bitwarden\ CLI /mnt/root/Bitwarden\ CLI
	rm -rf /mnt/root/.config
	# arch-chroot /mnt git clone https://gitlab.com/os_backups/e531/root_config_fedora_31.git /root/.config
	arch-chroot /mnt git clone "https://tukusejssirs:$gitlab_password@gitlab.com/os_backups/e531/root_config_fedora_31.git" /root/.config
	mv /mnt/root/Bitwarden\ CLI /mnt/root/.config/Bitwarden\ CLI

	# Change root user folder names
	echo -e "${LMAGENTA}[INFO] Changing root user folder names ...${FDEFAULT}"
	arch-chroot /mnt bash -c "while IFS= read -r line; do
		xdg_name=\"\$(cut -d= -f1 <<< \"\$line\")\"
		xdg_value=\"\${!xdg_name}\"
		new_value=\"\$(cut -d= -f2 <<< \"\$line\")\"

		if [ -d \"\$xdg_value\" ] && [ \"\$xdg_value\" != \"\$new_value\" ]; then
			mv \"\$xdg_value\" \"\$new_value\"
		else
			mkdir -p \"\$new_value\"
		fi
	done < <(grep -oP '^XDG[^=]*=\"[^\"]*' /root/.config/user-dirs.dirs | sed \"s#\\\$HOME#\$HOME#g;s/=\\\"/=/\")"

	# Move `lnx_scripts` to `$XDG_GIT_DIR`
	echo -e "${LMAGENTA}[INFO] Moving lnx_scripts to XDG_GIT_DIR ...${FDEFAULT}"
	# ShellCheck: Expressions don't expand in single quotes, use double quotes for that.
	# shellcheck disable=SC2016
	arch-chroot /mnt bash -c 'source /root/.config/user-dirs.dirs && mv /root/lnx_scripts "$XDG_GIT_DIR"'

	# Initialise BASH for root user
	echo -e "${LMAGENTA}[INFO] Initialising BASH for root user ...${FDEFAULT}"
	# ShellCheck: Expressions don't expand in single quotes, use double quotes for that.
	# shellcheck disable=SC2016
	arch-chroot /mnt bash -c 'source /root/.config/user-dirs.dirs && $XDG_GIT_DIR/lnx_scripts/init_scripts/bash_init.sh'

	# Configure root user password
	echo -e "${LMAGENTA}[INFO] Configuring root password ...${FDEFAULT}"
	ROOT_PASSWORD="$(arch-chroot /mnt bash -c "\$(command -v bw) get password 'root@$MODEL' --session '$BW_SESSION'")"

	printf '%s\n%s' "$ROOT_PASSWORD" "$ROOT_PASSWORD" | arch-chroot /mnt passwd

	# Change `lnx_scripts` remote URL (https to git protocol transition)
	echo -e "${LMAGENTA}[INFO] Changing lnx_scripts remote URL from HTTPS to Git protocol ...${FDEFAULT}"
	# ShellCheck: Expressions don't expand in single quotes, use double quotes for that.
	# shellcheck disable=SC2016
	arch-chroot /mnt bash -c '
		source /root/.config/user-dirs.dirs
		git -C "$XDG_GIT_DIR/lnx_scripts" remote rm origin
		git -C "$XDG_GIT_DIR/lnx_scripts" remote add origin git@gitlab.com:tukusejssirs/lnx_scripts.git &> /dev/null
		git -C "$XDG_GIT_DIR/lnx_scripts" branch --set-upstream-to=origin/master master &> /dev/null
		git -C "$XDG_GIT_DIR/lnx_scripts" push --set-upstream origin master &> /dev/null
		git -C "$XDG_GIT_DIR/lnx_scripts" remote set-head origin -a &> /dev/null
	'

	# Change `root_config_fedora_31` remote URL (https to git protocol transition)
	echo -e "${LMAGENTA}[INFO] Changing root_config_fedora_31 remote URL from HTTPS to Git protocol ...${FDEFAULT}"
	arch-chroot /mnt bash -c '
		git -C /root/.config remote rm origin
		git -C /root/.config remote add origin git@gitlab.com:os_backups/e531/root_config_fedora_31.git &> /dev/null
		git -C /root/.config branch --set-upstream-to=origin/master master &> /dev/null
		git -C /root/.config push --set-upstream origin master &> /dev/null
		git -C /root/.config remote set-head origin -a &> /dev/null
	'

	# If Gnome is installed, set some GNOME-specific settings
	# FIXME: Potentially fix the `dconf` issue using this: https://askubuntu.com/a/457023/279745q
	if arch-chroot /mnt bash -c 'command -v gnome-shell' &> /dev/null; then
		echo -e "${LMAGENTA}[INFO] Initialising GNOME ...${FDEFAULT}"
		# ShellCheck: Expressions don't expand in single quotes, use double quotes for that.
		# shellcheck disable=SC2016
		arch-chroot /mnt bash -c 'source /root/.config/user-dirs.dirs && $XDG_GIT_DIR/lnx_scripts/init_scripts/gnome_init.sh'
	fi

	# Clone some repos
	# TODO
	# mkdir -p $XDG_GIT_DIR/ofcl/rodokmen
	# # mkdir -p $XDG_GIT_DIR/ofcl/lnx_data
	# git clone -q git@gitlab.com:ofcl/rodokmen.git $XDG_GIT_DIR/ofcl/rodokmen
	# # git clone git@gitlab.com:tukusejssirs/lnx_data.git $XDG_GIT_DIR/lnx_data

	# TODO: Restore Gourmet if it is (to be) installed
	# rm -rf ${HOME}/.gourmet
	# ln -s $XDG_GIT_DIR/lnx_data/apps_data/gourmet ${HOME}/.gourmet

	### NON-ROOT USER CREATION AND CONFIGURATION

	# Create non-root user
	echo -e "${LMAGENTA}[INFO] Creating non-root user [$USER_NAME] ...${FDEFAULT}"
	USER_PASSWORD="$(arch-chroot /mnt bash -c "\$(command -v bw) get password '$USER_NAME@$MODEL' --session '$BW_SESSION'")"
	arch-chroot /mnt useradd -mG wheel,storage,optical -s /bin/bash "$USER_NAME"
	printf '%s\n%s' "$USER_PASSWORD" "$USER_PASSWORD" | arch-chroot /mnt passwd "$USER_NAME"
	arch-chroot /mnt sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL = (ALL) ALL/' /etc/sudoers

	# Clone `home_config_fedora_31` repo to `/home/$USER_NAME/.config`
	# TODO: Create `home_config_arch_linux` repo when I start using Arch Linux.
	# FIXME: Create a repo like this for `janka` user.
	# FIXME: Change global XDG_*_DIR variables (like XDG_GIT_DIR) for `janka` user.
	echo -e "${LMAGENTA}[INFO] Cloning home_config_fedora_31 repo ...${FDEFAULT}"
	rm -rf "/mnt/home/$USER_NAME/.config"
	arch-chroot /mnt git clone git@gitlab.com:os_backups/e531/home_config_fedora_31.git "/home/$USER_NAME/.config"
	arch-chroot /mnt chown -R "$USER_NAME:$USER_NAME" "/home/$USER_NAME/.config"

	# Create local XDG_*_DIR folders for non-root user
	echo -e "${LMAGENTA}[INFO] Creating local XDG_*_DIR folders for non-root user ...${FDEFAULT}"
	arch-chroot /mnt sudo -u "$USER_NAME" bash -c "while IFS= read -r line; do
		xdg_name=\"\$(cut -d= -f1 <<< \"\$line\")\"
		xdg_value=\"\${!xdg_name}\"
		new_value=\"\$(cut -d= -f2 <<< \"\$line\")\"

		if [ -d \"\$xdg_value\" ] && [ \"\$xdg_value\" != \"\$new_value\" ]; then
			mv \"\$xdg_value\" \"\$new_value\"
		else
			mkdir -p \"\$new_value\"
		fi
	done < <(grep -oP '^XDG[^=]*=\"[^\"]*' /home/$USER_NAME/.config/user-dirs.dirs | sed \"s#\\\$HOME#\$HOME#g;s/=\\\"/=/\")"

	# Change some permissions
	arch-chroot /mnt bash -c "
		source /home/$USER_NAME/.config/user-dirs.dirs
		setfacl -Rm \"u:$USER_NAME:rwx\" /opt \"\$XDG_BZR_DIR\" \"\$XDG_GGL_DIR\" \"\$XDG_GGL_DRIVE_DIR\" \"\$XDG_GIT_DIR\" \"\$XDG_MEGA_DIR\" \"\$XDG_SVN_DIR\"
	"

	# Log into Bitwarden for non-root user
	echo -e "${LMAGENTA}[INFO] Logging into Bitwarden for non-root user ...${FDEFAULT}"
	BW_SESSION=$(arch-chroot /mnt sudo -u "$USER_NAME" bash -c "\$(command -v bw) login '$BW_EMAIL' '$BW_PASSWORD' | tail -1 | awk '{print \$6}'")
	mkdir -p "$(dirname "/mnt/home/$USER_NAME/$BW_SESSION_FILE")"
	echo "$BW_SESSION" > "/mnt/home/$USER_NAME/$BW_SESSION_FILE"
	arch-chroot /mnt chown -R "$USER_NAME:$USER_NAME" "/home/$USER_NAME"

	# TODO: This is for debugging perposes, remove this when not needed
	echo -e "${LMAGENTA}[INFO] Stopped waiting for user input to continue ...${FDEFAULT}"
	read -r

	# Initialise BASH for non-root user
	echo -e "${LMAGENTA}[INFO] Initialising BASH for non-root user ...${FDEFAULT}"
	arch-chroot /mnt sudo -u "$USER_NAME" bash -c "source /home/$USER_NAME/.config/user-dirs.dirs && \$XDG_GIT_DIR/lnx_scripts/init_scripts/bash_init.sh"

	# Initialise Git for non-root user
	echo -e "${LMAGENTA}[INFO] Initialising Git for non-root user ...${FDEFAULT}"
	arch-chroot /mnt sudo -u "$USER_NAME" bash -c "source /home/$USER_NAME/.config/user-dirs.dirs && \$XDG_GIT_DIR/lnx_scripts/init_scripts/git_init.sh"

	# Initialise GNOME for non-root user
	if arch-chroot /mnt bash -c 'command -v gnome-shell' &> /dev/null; then
		echo -e "${LMAGENTA}[INFO] Initialising GNOME ...${FDEFAULT}"

		case "$USER_NAME" in
			'ts')
				arch-chroot /mnt sudo -u "$USER_NAME" bash -c "source /home/$USER_NAME/.config/user-dirs.dirs && \$XDG_GIT_DIR/lnx_scripts/init_scripts/gnome_init.sh"
			;;
			'janka')
				arch-chroot /mnt sudo -u "$USER_NAME" bash -c "source /home/$USER_NAME/.config/user-dirs.dirs && \$XDG_GIT_DIR/lnx_scripts/init_scripts/gnome_janka_init.sh"
			;;
		esac
	fi

	### INSTALLATION OF AUR AND SDKMAN PACKAGES, AND OTHER USER-DEPENDENT PACKAGES

	# Install packages from AUR
	if [ "$PACKAGES_AUR_INSTALL" = 'true' ]; then
		echo -e "${LMAGENTA}[INFO] Installing packages from AUR ...${FDEFAULT}"
		arch-chroot /mnt bash -c 'pacman -Syu --noconfirm --needed git go'

		case "$MODEL" in
			'e531')
				# Install some additional firmware on Lenovo Thinkpad E531
				echo -e "${LMAGENTA}[INFO] Adding some additional firmware on Lenovo Thinkpad E531 to the PACKAGES_AUR variable ...${FDEFAULT}"
				PACKAGES_AUR="$PACKAGES_AUR upd72020x-fw aic94xx-firmware wd719x-firmware"
			;;
		esac

		if [ "$PACKAGES_AUR" ]; then
			echo "$USER_NAME ALL = (ALL) NOPASSWD: ALL" > "/mnt/etc/sudoers.d/$USER_NAME"
			arch-chroot /mnt su "$USER_NAME" -c "git clone https://aur.archlinux.org/yay.git \"/home/$USER_NAME/yay\""
			arch-chroot /mnt gpg --recv-key 465022E743D71E39
			arch-chroot /mnt sudo -u "$USER_NAME" bash -c "cd \"/home/$USER_NAME/yay\" && echo -e \"$USER_PASSWORD\n$USER_PASSWORD\n$USER_PASSWORD\n$USER_PASSWORD\n\" | makepkg -si --noconfirm"
			rm -rf "/mnt/home/$USER_NAME/yay" "/mnt/etc/sudoers.d/$USER_NAME"
		fi
	fi

	# Install packages via `sdkman`
	if [ "$PACKAGES_SDKMAN_INSTALL" = 'true' ]; then
		echo -e "${LMAGENTA}[INFO] Installing packages via sdkman ...${FDEFAULT}"
		arch-chroot /mnt curl -s https://get.sdkman.io | bash

		if [ "$PACKAGES_SDKMAN" ]; then
			arch-chroot /mnt sed -i 's/sdkman_auto_answer=.*/sdkman_auto_answer=true/g' "/home/$USER_NAME/.sdkman/etc/config"
			arch-chroot /mnt sudo -u "$USER_NAME" bash -c "source /home/$USER_NAME/.sdkman/bin/sdkman-init.sh && sdk install $PACKAGES_SDKMAN"
			arch-chroot /mnt sed -i 's/sdkman_auto_answer=.*/sdkman_auto_answer=false/g' "/home/$USER_NAME/.sdkman/etc/config"
		fi
	fi

	# Install Firefox Nightly
	# TODO: Add this the the user-dependent options.
	echo -e "${LMAGENTA}[INFO] Installing Firefox Nightly ...${FDEFAULT}"
	arch-chroot /mnt sudo -u "$USER_NAME" bash -c "source /home/$USER_NAME/.config/user-dirs.dirs && \$XDG_GIT_DIR/lnx_scripts/bash_progs/ff_nightly.sh"

	# Restore Firefox (Mozilla) data from https://gitlab.com/os_backups/apps/mozilla repo
	# TODO: Add this the the user-dependent options.
	echo -e "${LMAGENTA}[INFO] Restoring .mozilla (and thus Firefox) data ...${FDEFAULT}"
	arch-chroot /mnt rm -rf "/home/$USER_NAME/.mozilla"
	arch-chroot /mnt sudo -u "$USER_NAME" git clone git@gitlab.com:os_backups/apps/mozilla.git "/home/$USER_NAME/.mozilla"

	# Install some fonts
	echo -e "${LMAGENTA}[INFO] Installing some fonts ...${FDEFAULT}"
	arch-chroot /mnt sudo -u "$USER_NAME" bash -c "source /home/$USER_NAME/.config/user-dirs.dirs && \$XDG_GIT_DIR/lnx_scripts/bash_progs/fonts.sh -a"

	# Install Mikrotik WinBox
	# TODO: Add this the the user-dependent options.
	echo -e "${LMAGENTA}[INFO] Installing Mikrotik WinBox ...${FDEFAULT}"
	arch-chroot /mnt sudo -u "$USER_NAME" bash -c "source /home/$USER_NAME/.config/user-dirs.dirs && \$XDG_GIT_DIR/lnx_scripts/bash_progs/winbox.sh"

	# Some settings specific to Janka
	if [ "$USER_NAME" = 'janka' ]; then
		# Add `Milovana` to `/etc/fstab`
		echo -e "${LMAGENTA}[INFO] Adding Milovana to fstab ...${FDEFAULT}"
		mkdir -p "/mnt/home/$USER_NAME/Milovana"

		if arch-chroot /mnt find /dev/disk | grep -q '7136323F6CD975F6'; then
			if grep -q '7136323F6CD975F6' /mnt/etc/fstab; then
				sed -i "s#^UUID=7136323F6CD975F6.*#UUID=7136323F6CD975F6   /home/$USER_NAME/Milovana    ntfs    default,rw,uid=$USER_NAME,gid=$USER_NAME      0 0#" /mnt/etc/fstab
			else
				echo "UUID=7136323F6CD975F6   /home/$USER_NAME/Milovana    ntfs    default,rw,uid=$USER_NAME,gid=$USER_NAME      0 0" >> /mnt/etc/fstab
			fi
		fi
	fi

	### INSTALLATION FINALISATION

	# Enable and disable required services
	echo -e "${LMAGENTA}[INFO] Enabling and disabling required services ...${FDEFAULT}"
	arch-chroot /mnt systemctl disable iptables
	arch-chroot /mnt systemctl enable chronyd gdm NetworkManager sshd teamviewerd ufw

	# Reboot
	# TODO: Uncomment this
	# echo -e "${LMAGENTA}[INFO] Rebooting ...${FDEFAULT}"
	# reboot
}

# [REQUIRED] User-dependent variables
DEVICE='/dev/sda'
DEVICE_TRIM='true'
# DISPLAY_DRIVER='intel !amdgpu !ati !nvidia !nvidia-lts !nvidia-dkms !nvidia-390xx !nvidia-390xx-lts !nvidia-390xx-dkms !nouveau'  # (single)
DISPLAY_DRIVER='intel'  # FIXME: use `intel` by default and `nvidia???` for `p15` (check it using `lspci -k | grep -EA2 '(VGA|3D)'`)
DISPLAY_DRIVER_DDX='false'
DISPLAY_DRIVER_HARDWARE_ACCELERATION='true'
# DISPLAY_DRIVER_HARDWARE_ACCELERATION_INTEL='intel-media-driver !libva-intel-driver'  # (single)
DISPLAY_DRIVER_HARDWARE_ACCELERATION_INTEL='intel-media-driver'  # (single)
FASTBOOT='true'
FONT=''
FONT_MAP=''  # FIXME: /usr/share/kbd/consolefonts/sun12x22.psfu.gz
FRAMEBUFFER_COMPRESSION='true'
KBLO='uk'  # Keyboard layout for inputting in Arch Linux Live OS
KERNELS_COMPRESSION='zstd'
KERNELS_PARAMETERS='ipv6.disable=1'
KEYLAYOUT='gb'  # In Xorg/X11
KEYMAP='uk'     # In TTY console
KEYMODEL=''
KEYOPTIONS=''
KEYVARIANT=''
KMS='true'
LOCALE_CONF=('LANG=en_GB.UTF-8' 'LC_CTYPE="en_GB.UTF-8"' 'LC_NUMERIC="en_GB.UTF-8"' 'LC_TIME="en_GB.UTF-8"' 'LC_COLLATE="en_GB.UTF-8"' 'LC_MONETARY="en_GB.UTF-8"' 'LC_MESSAGES="en_GB.UTF-8"' 'LC_PAPER="en_GB.UTF-8"' 'LC_NAME="en_GB.UTF-8"' 'LC_ADDRESS="en_GB.UTF-8"' 'LC_TELEPHONE="en_GB.UTF-8"' 'LC_MEASUREMENT="en_GB.UTF-8"' 'LC_IDENTIFICATION="en_GB.UTF-8"')

PACKAGES_MULTILIB='true'
REFLECTOR='true'
SETUP_WIFI='false'
SWAP_SIZE='4096'  # In MiB
SWAPFILE='/swapfile'  # Swap file path
TIMEZONE='Europe/Bratislava'
USER_NAME='ts'
VULKAN='true'
PACKAGES_PACMAN_INSTALL='true'
PACKAGES_FLATPAK_INSTALL='false'  # TODO: evaluate
PACKAGES_SDKMAN_INSTALL='false'   # TODO: evaluate
PACKAGES_AUR_INSTALL='true'
PACKAGES_NPM_INSTALL='true'
INSTALL_WIFI_DRIVER='true'  # TODO: Make this optional


# [OPTIONAL] User-dependent variables (set to default values)
# LVM_VG_NAME='vg_arch'
# LVM_LV_NAME='root'
# PING_HOSTNAME='mirrors.kernel.org'
# WIFI_ESSID=''
# WIFI_INTERFACE=''
# WIFI_PSK=''
REFLECTOR_COUNTRIES='Slovakia,Czechia,Hungary,Austria'
# TODO: Add `iotop` and `sysstat` (`iostat`).
PACKAGES_PACMAN='android-tools arp-scan audacious audacity bash-completion bc bind-tools breezy calibre caprine cheese chrome-gnome-shell chrony cifs-utils colordiff ddcutil dhclient dialog dkms dmidecode dnsmasq dosfstools easytag efibootmgr epiphany espeak etckeeper ethtool evince exfatprogs expect ffmpeg file-roller folks fping freetype2 gdm gedit gimp gimp-help-en_gb gimp-nufraw gimp-plugin-gmic git git-lfs gnome-calculator gnome-calendar gnome-characters gnome-clocks gnome-color-manager gnome-contacts gnome-control-center gnome-disk-utility gnome-font-viewer gnome-keyring gnome-logs gnome-screenshot gnome-session gnome-settings-daemon gnome-shell gnome-terminal gnome-themes-extra gnome-tweaks gnome-user-share gparted gthumb gvfs gvfs-afc gvfs-goa gvfs-google gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb htop imagemagick intel-ucode jq kdeconnect lftp libreoffice-fresh libreoffice-fresh-cs libreoffice-fresh-hu libreoffice-fresh-he libreoffice-fresh-el libreoffice-fresh-sk links lshw lynx man-db man-pages meson mkinitcpio-nfs-utils mlocate mtools musescore mutter nano nautilus networkmanager nfs-utils nodejs nomacs noto-fonts noto-fonts-emoji npm ntfs-3g openfortivpn openssh otf-font-awesome p7zip parted pavucontrol perl-rename php pv python python-html2text qpdf reflector remmina rsync rubygems rygel samba scantailor-advanced shellcheck smartmontools sshpass subversion sway swaylock syslinux systemd-resolvconf tcpdump telegram-desktop tesseract tesseract-data-ces tesseract-data-deu tesseract-data-heb tesseract-data-hun tesseract-data-lat tesseract-data-slk testdisk texlive-core texlive-fontsextra texlive-formatsextra texlive-langextra texlive-langgreek texlive-latexextra texlive-music texlive-pictures texlive-pstricks texlive-science texlive-humanities texlive-publishers tidy translate-shell transmission-gtk ttf-font-awesome ttf-opensans ufw unrar unzip usb_modeswitch usbutils virtualbox virtualbox-guest-iso virtualbox-host-dkms vlc w3m waybar wf-recorder wget wine wireless-regdb wireless_tools wl-clipboard xclip xdg-user-dirs-gtk xorg xorg-server xsane xsane-gimp youtube-dl zenity zip'
PACKAGES_AUR='contrast drive eidklient etherwake mailspring megatools megasync opera-developer pirate-get teamviewer todoist-appimage ttf-ms-fonts virtualbox-ext-oracle wlogout'
PACKAGES_NPM='@angular/cli @bitwarden/cli depcheck gnomon htmlhint js-yaml-cli @nestjs/cli npm npm-check npm-check-updates typescript'

# TODO: depreated npm packages:
# - request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142;
# - har-validator@5.1.5: this library is no longer supported;
# - graceful-fs@2.0.3: please upgrade to graceful-fs 4 for compatibility with current and future versions of Node.js;
# - minimatch@0.2.14: Please update to minimatch 3.0.2 or higher to avoid a RegExp DoS issue;
# - core-js@2.6.12: core-js@<3 is no longer maintained and not recommended for usage due to the number of issues. Please, upgrade your dependencies to the actual version of core-js@3;

# PACKAGES_FLATPAK=''
# PACKAGES_SDKMAN=''
# LOCALE_ADDITIONAL='en_GB.UTF-8 UTF-8'

# Start Arch Linux installation
install_arch