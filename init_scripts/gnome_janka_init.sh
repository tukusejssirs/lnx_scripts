#!/bin/bash

# This scripts sets my Gnome 3 preferences


# Configure GNOME Terminal
# src: https://askubuntu.com/a/733202
# src: https://superuser.com/questions/872397/how-to-add-a-pre-made-profile-to-gnome-terminal
if command -v gnome-terminal &> /dev/null; then
	profile="$(gsettings get org.gnome.Terminal.ProfilesList default | sed "s/'//g" -)"
	gsettings set org.gnome.Terminal.Legacy.Settings theme-variant 'dark'
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" audible-bell false
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" background-color '#000000'
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" bold-color-same-as-fg true
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" bold-is-bright true
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" encoding 'UTF-8'
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" font 'Ubuntu Mono 13'
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" foreground-color '#00ff00'
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" login-shell false
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" palette "['rgb(0,0,0)', 'rgb(170,0,0)', 'rgb(0,170,0)', 'rgb(170,85,0)', 'rgb(17,17,170)', 'rgb(170,0,170)', 'rgb(0,170,170)', 'rgb(170,170,170)', 'rgb(85,85,85)', 'rgb(255,85,85)', 'rgb(85,255,85)', 'rgb(255,255,85)', 'rgb(85,85,255)', 'rgb(255,85,255)', 'rgb(85,255,255)', 'rgb(255,255,255)']"
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" rewrap-on-resize true
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" scroll-on-keystroke true
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" scroll-on-output false
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" text-blink-mode never
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" use-system-font false
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" use-theme-colors false
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$profile/" visible-name 'janka'
fi

# `gedit` settings
if command -v gedit &> /dev/null; then
	gsettings set org.gnome.gedit.plugins active-plugins "['filebrowser', 'spell', 'time', 'docinfo', 'snippets', 'modelines', 'sort']"
	gsettings set org.gnome.gedit.plugins.externaltools font 'Ubuntu Mono 13'
	gsettings set org.gnome.gedit.plugins.pythonconsole font 'Ubuntu Mono 13'
	gsettings set org.gnome.gedit.plugins.pythonconsole use-system-font false
	gsettings set org.gnome.gedit.preferences.editor auto-indent true
	gsettings set org.gnome.gedit.preferences.editor auto-save true
	gsettings set org.gnome.gedit.preferences.editor auto-save-interval 'uint32 2'
	gsettings set org.gnome.gedit.preferences.editor bracket-matching true
	gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
	gsettings set org.gnome.gedit.preferences.editor editor-font 'Ubuntu Mono 13'
	gsettings set org.gnome.gedit.preferences.editor ensure-trailing-newline false
	gsettings set org.gnome.gedit.preferences.editor scheme 'cobalt'
	gsettings set org.gnome.gedit.preferences.editor tabs-size 'uint32 2'
	gsettings set org.gnome.gedit.preferences.editor use-default-font false
	gsettings set org.gnome.gedit.preferences.print print-font-body-pango 'Ubuntu Mono 13'
	gsettings set org.gnome.gedit.preferences.print print-font-header-pango 'Ubuntu Mono 13'
	gsettings set org.gnome.gedit.preferences.print print-font-numbers-pango 'Ubuntu Mono 13'
	gsettings set org.gnome.gedit.preferences.ui statusbar-visible true
fi

# Disable lock screen and screensaver
gsettings set org.gnome.desktop.lockdown disable-lock-screen true
gsettings set org.gnome.desktop.screensaver lock-enabled false
gsettings set org.gnome.desktop.screensaver idle-activation-enabled false

# Desktop and screensaver background
gsettings set org.gnome.desktop.background primary-color "#000000"
gsettings set org.gnome.desktop.background secondary-color "#000000"
gsettings set org.gnome.desktop.background color-shading-type "solid"
gsettings set org.gnome.desktop.background picture-uri ''   # Choose a picture
gsettings set org.gnome.desktop.screensaver picture-uri ''  # Choose a picture
gsettings set org.gnome.desktop.screensaver primary-color "#000000"
gsettings set org.gnome.desktop.screensaver secondary-color "#000000"
gsettings set org.gnome.desktop.screensaver color-shading-type "solid"

# This turns off auto-brightness
gsettings set org.gnome.settings-daemon.plugins.power ambient-enabled false

# Disable middle-button paste
gsettings set org.gnome.desktop.interface gtk-enable-primary-paste 'false'

# Disable attaching modal dialogues
gsettings set org.gnome.shell.overrides attach-modal-dialogs false
gsettings set org.gnome.mutter attach-modal-dialogs false

# Disable dimming screen and turning it off
gsettings set org.gnome.settings-daemon.plugins.power idle-dim false
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 0
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'nothing'       # blank screen
gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-type 'nothing'  # blank screen

# Disable location
gsettings set org.gnome.system.location enabled false

# Screenshot save directory
# TODO
# gsettings set org.gnome.gnome-screenshot auto-save-directory "file:///home/$USER/grcs/pics/screenshots/"
# gsettings set org.gnome.gnome-screenshot last-save-directory "file://$XDG_SCREENSHOTS_DIR"

# Configure keyboard shortcuts
source "$SCRIPT_PATH/../bash/functions/gshort.sh"
gshort 'Brightness up' 'gdbus call --session --dest org.gnome.SettingsDaemon.Power --object-path /org/gnome/SettingsDaemon/Power --method org.gnome.SettingsDaemon.Power.Screen.StepUp' '<Alt><Super>Up'
gshort 'Brightness down' 'gdbus call --session --dest org.gnome.SettingsDaemon.Power --object-path /org/gnome/SettingsDaemon/Power --method org.gnome.SettingsDaemon.Power.Screen.StepDown' '<Alt><Super>Down'
gshort 'gnome-terminal' 'gnome-terminal --geometry=1000x400' '<Super>t'
gshort 'Mikrotik WinBox' "wine /opt/winbox/winbox$arch.exe" '<Super>w'
gsettings set org.gnome.settings-daemon.plugins.media-keys home '<Super>e' 2> /dev/null || gsettings set org.gnome.settings-daemon.plugins.media-keys home "['<Super>e']"
gsettings set org.gnome.desktop.wm.keybindings show-desktop "['<Super>d']"
gsettings set org.gnome.desktop.wm.keybindings minimize "['<Super>Down']"
gsettings set org.gnome.desktop.wm.keybindings unmaximize "['<Shift><Super>Down']"

# Additional keyboard layouts and options
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:internal_nocancel', 'grp:shifts_toggle']"
gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'sk'), ('xkb', 'gb')]"

# Disable night light mode
gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled false

# Sound
gsettings set org.gnome.desktop.sound allow-volume-above-100-percent true

# Themes
# gsettings set org.gnome.desktop.interface cursor-theme Adwaita
# gsettings set org.gnome.desktop.interface gtk-theme Matcha-dark-azul
# gsettings set org.gnome.shell.extensions.user-theme name Matcha-dark-azul

# Configure Gnome 3 extensions
# TODO: update this
# sudo rm -rf /usr/share/gnome-shell/extensions && sudo ln -s ${XDG_GIT_DIR}/lnx_scripts/gnome/ext/global /usr/share/gnome-shell/extensions
# rm -rf ${HOME}/.local/share/gnome-shell/extensions && sudo ln -s ${XDG_GIT_DIR}/lnx_scripts/gnome/ext/local ${HOME}/.local/share/gnome-shell/extensions
gsettings set org.gnome.shell enabled-extensions "['pamac-updates@manjaro.org', 'dash-to-dock@micxgx.gmail.com', 'appindicatorsupport@rgcjonas.gmail.com', 'ding@rastersoft.com', 'drive-menu@gnome-shell-extensions.gcampax.github.com', 'openweather-extension@jenslody.de', 'extensions@abteil.org', 'status-area-horizontal-spacing@mathematical.coffee.gmail.com', 'noannoyance@sindex.com', 'disconnect-wifi@kgshank.net', 'advanced-settings-in-usermenu@nuware.ru', 'remove-dropdown-arrows@mpdeimos.com', 'user-theme@gnome-shell-extensions.gcampax.github.com', 'activities-config@nls1729', 'gsconnect@andyholmes.github.io']"

# Create `autostart` folder
mkdir -p "$HOME/.config/autostart"

# Disable suspension after laptop lid is closed
# Dependency: requires `gnome-tweaks` (or at least `gnome-tweak-tool-lid-inhibitor script)
# TODO: make this `gnome-tweaks`-independent
# Note: This tweak is required on CentOS 7 and 8 only; On Fedora, it works as expected
if [ "$(grep -Po 'ID="\K[^"]*' < "/etc/os-release")" = 'centos' ]; then
	echo -e "[Desktop Entry]\nType=Application\nName=ignore-lid-switch-tweak\nExec=/usr/libexec/gnome-tweak-tool-lid-inhibitor" > "$HOME/.config/autostart/ignore-lid-switch-tweak.desktop"
fi

# Disable touchpad (but keep the trackpoint enabled)
gsettings set org.gnome.desktop.peripherals.touchpad send-events 'disabled'

# Show week number in the panel calendar
gsettings set org.gnome.desktop.calendar show-weekdate 'true'
gsettings set org.gnome.desktop.interface clock-show-weekday true

# Hide hidden files in Nautilus
gsettings set org.gnome.nautilus.preferences show-hidden-files false

# org.gnome.online-accounts
# org.gnome.Contacts
# org.gnome.desktop.datetime
# org.gnome.desktop.default-applications
# org.gnome.desktop.default-applications.office
# org.gnome.desktop.default-applications.office.calendar
# org.gnome.desktop.default-applications.office.tasks
# org.gnome.desktop.default-applications.terminal
# org.gnome.desktop.interface
# org.gnome.desktop.lockdown
# org.gnome.desktop.media-handling
# org.gnome.desktop.notifications
# org.gnome.desktop.notifications.application:/
# org.gnome.GWeather
# org.gnome.Weather.Application
# org.gnome.packagekit
# org.gnome.seahorse
# org.gnome.seahorse.manager
# org.gnome.seahorse.window:/
# org.gnome.settings-daemon.plugins.account
# org.gnome.settings-daemon.plugins.color
# org.gnome.settings-daemon.plugins.housekeeping
# org.gnome.settings-daemon.plugins.media-keys
# org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/
# org.gnome.settings-daemon.plugins.xsettings
# org.gnome.shell
# org.gnome.shell.keybindings
# org.gnome.shell.keyboard
# org.gnome.shell.overrides
# org.gnome.eog[.*]  # set the background
# org.gnome.software