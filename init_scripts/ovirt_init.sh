#!/bin/bash

# Install/initialise ovirt on CentOS 8

# src: https://computingforgeeks.com/install-and-configure-ovirt-on-centos/
# src: https://www.ovirt.org/documentation/installing_ovirt_as_a_standalone_manager_with_local_databases/#Preparing_Local_Storage_SM_localDB_deploy


# TODO: Copy Kickstart file
# TODO: Add LV (an LVM2 partition) for /data/images
# TODO: Change hostname to `ovirt_host.domcek.lan`

# Add ovirt repo
# TODO: Get the latest ovirt version
dnf -y install https://resources.ovirt.org/pub/yum-repo/ovirt-release44.rpm

# Upgrade system
dnf -y upgrade

# Create a local storage directory:
# TODO: Move this to Kickstart file
lvcreate -l 100%FREE -n data vg_ovirt
mkfs.ext4 /dev/mapper/vg_ovirt-data
echo '/dev/mapper/vg_ovirt-data /data ext4 defaults,discard 1 2' >> /etc/fstab
mount /data

# Create the user vdsm in the group kvm
useradd vdsm -u 36 -g 36

# Create folder for local storage for VMs
mkdir -p /data/images

# Set permissions allowing read/write access to the vdsm user (UID 36) and kvm group (GID 36)
chown -R 36:36 /data /data/images
chmod 0755 /data /data/images

# Install Cockpit with Ovirt plugin and other tools
dnf -y install cockpit cockpit-ovirt-dashboard gluster-ansible-roles ovirt-engine-appliance ovirt-hosted-engine-setup

# Start and enable Cockpit service
systemctl enable --now cockpit.socket

# Enable cockpit service ports in the firewall
# Note: This is already enabled
# firewall-cmd --add-service=cockpit
# firewall-cmd --add-service=cockpit --permanent
# firewall-cmd --reload

# FQDNs prepared for your Engine and the deployment host. Forward and reverse lookup records must both be set in the DNS.
# Note: This is needed by ovirt Engine
# TODO: Check if this is correct
# TODO: How to get the IP (it should be new) from the router
cat << EOF > /etc/hosts
# Ovirt host
192.168.1.27 ovirt_host.domcek.lan
# Ovirt engine (virtual machine)
192.168.1.50 ovirt.domcek.lan
EOF
# 172.21.148.10 ovirt-node-01.computingforgeeks.com  # Deployment host
# 172.21.148.11 ovirt.computingforgeeks.com          # Ovirt Engine

# Set the DNS up (add the host IP to DNS address list)
# TODO: Get the NIC name
nmcli con mod enp6s0 ipv4.ignore-auto-dns yes
nmcli con mod enp6s0 ipv4.dns '1.1.1.1 8.8.8.8 192.168.1.27'
nmcli con up enp6s0

# Set up SELinux: Don't prevent firewalld from using the dac_override capability
# auditctl -w /etc/shadow -p w
# ausearch -m avc -ts recent
ausearch -c 'firewalld' --raw | audit2allow -M my-firewalld
semodule -X 300 -i my-firewalld.pp
rm my-firewalld.{pp,te}

# First host deployment
hosted-engine --deploy
# Answers:
# - `yes` to `The locally running engine will be used to configure a new storage domain and create a VM there. At the end the disk of the local VM will be moved to the shared storage. Are you sure you want to continue? (Yes, No)[Yes]`;
# - [enter] to `Please indicate the gateway IP address [192.168.1.1]`;
# - [enter] to `Please indicate a nic to set ovirtmgmt bridge on: (enp6s0) [enp6s0]`
# - [enter] to `Please specify which way the network connectivity should be checked (ping, dns, tcp, none)[dns]`;
# - [enter] to `Please enter the name of the datacenter where you want to deploy this hosted-engine host. [Default]`;
# - [enter] to `Please enter the name of the cluster where you want to deploy this hosted-engine host. [Default]`;
# - [enter] to `If you want to deploy with a custom engine appliance image, please specify the path to the OVA archive you would like to use (leave it empty to skip, the setup will use ovirt-engine-appliance rpm installing it if missing)`;
# - [enter] to `Please specify the number of virtual CPUs for the VM (Defaults to appliance OVF value): [4]`;
# - [enter] to `Please specify the memory size of the VM in MB (Defaults to appliance OVF value): [16384]:`;
# - `ovirt.domcek.lan` to `Please provide the FQDN you would like to use for the engine. Note: This will be the FQDN of the engine VM you are now going to launch, it should not point to the base host or to any other existing machine. Engine VM FQDN:  []`;
# - [enter] to `Engine VM domain: [domcek.lan]`;
# - choose root password (enter it + confirm it);
# - [enter] to `Enter ssh public key for the root user that will be used for the engine appliance (leave it empty to skip)`;
# - [enter] to `Do you want to enable ssh access for the root user (yes, no, without-password) [yes]`;
# - `no` to `Do you want to apply a default OpenSCAP security profile (Yes, No) [No]`;
# - [enter] to `You may specify a unicast MAC address for the VM or accept a randomly generated default [00:16:3e:76:87:6c]`;
# - `static` to `How should the engine VM network be configured (DHCP, Static)[DHCP]`;
# - `192.168.1.50` to `Please enter the IP address to be used for the engine VM`;
# - [enter] to `Please provide a comma-separated list (max 3) of IP addresses of domain name servers for the engine VM Engine VM DNS (leave it empty to skip) [1.1.1.1,8.8.8.8,192.168.1.27]:`;
# - `yes` to `Add lines for the appliance itself and for this host to /etc/hosts on the engine VM? Note: ensuring that this host could resolve the engine VM hostname is still up to you (Yes, No)[No]`;
# - [enter] to `Please provide the name of the SMTP server through which we will send notifications [localhost]`;
# - [enter] to `Please provide the TCP port number of the SMTP server [25]`;
# - [enter] to `Please provide the email address from which notifications will be sent [root@localhost]`;
# - [enter] to `Please provide a comma-separated list of email addresses which will get notifications [root@localhost]`;
# - choose engine admin password (enter it + confirm it);
# - [enter] to `Please provide the hostname of this host on the management network [ovirt_host.domcek.lan]:`;


##### In Cockpit #####

			# Virtualization → Hosted Engine → [Hosted Engine] Start

			# Engine VM FQDN : ovirt_engine.domcek.lan

# Virtualization → Hosted Engine → [Hyperconverged] Start