#!/bin/bash

# Stripped down Bash RC file


# Variables
if ! PATH_REPO_ROOT="$(git -C "$(dirname "$(realpath "$BASH_SOURCE")")" rev-parse --show-toplevel 2> /dev/null)"; then
	timed PATH_REPO_ROOT="$(dirname "$(dirname "$(dirname "$(realpath "$BASH_SOURCE")")")")"
fi

PATH_FN="${PATH_REPO_ROOT}/bash/functions"
PATH_STNGS="${PATH_REPO_ROOT}/bash/settings"
PATH_ALIASES="${PATH_REPO_ROOT}/bash/aliases"

# Source functions
# shellcheck disable=SC1091  # Not following: ./src.sh was not specified as input (see shellcheck -x).
source "${PATH_FN}/src.sh"

# Source settings
src "${PATH_STNGS}/source_user_dirs_and_get_xdg_git_dir.sh"
src "${PATH_STNGS}/lesspipe.sh"
src "${PATH_STNGS}/gcc_colours.sh"
src "${PATH_STNGS}/bash_completion.sh"
src "${PATH_STNGS}/bw_settings.sh"
src "${PATH_STNGS}/acroread.sh"
src "${PATH_STNGS}/kotlin_sdkman.sh"
src "${PATH_STNGS}/texlive_man_info_paths.sh"
src "${PATH_STNGS}/nvm.sh"
src "${PATH_STNGS}/dotnet.sh"

# Interactive shells
if [[ $- == *i* ]]; then
	# Bash colours
	src "${PATH_FN}/bash_colours.sh"

	# Bash aliases
	src "${PATH_ALIASES}/aliases.sh"

	# Git functions
	src "${PATH_FN}/git"

	# Bitwarden functions
	src "${PATH_FN}/bli.sh"
	src "${PATH_FN}/blo.sh"
	src "${PATH_FN}/bs.sh"
	src "${PATH_FN}/bsl.sh"

	# Bash functions
	src "${PATH_FN}/add_ssh_keys.sh"
	src "${PATH_FN}/add_to_path.sh"
	src "${PATH_FN}/brightness.sh"
	src "${PATH_FN}/bt.sh"
	src "${PATH_FN}/cconv.sh"
	src "${PATH_FN}/char_multiplier.sh"
	src "${PATH_FN}/create_ssh_keys.sh"
	src "${PATH_FN}/custom_progress.sh"
	src "${PATH_FN}/csp.sh"
	src "${PATH_FN}/fram.sh"
	src "${PATH_FN}/hexcol.sh"
	src "${PATH_FN}/mkcd.sh"
	src "${PATH_FN}/multiselect.sh"
	src "${PATH_FN}/prompt.sh"
	src "${PATH_FN}/rawurldecode.sh"
	src "${PATH_FN}/rawurlencode.sh"
	src "${PATH_FN}/release.sh"
	src "${PATH_FN}/round.sh"
	src "${PATH_FN}/scan.sh"
	src "${PATH_FN}/scan_batch_test.sh"  # TODO: Remove this by merging it into `scan.sh`
	src "${PATH_FN}/ssh_key_init.sh"
	src "${PATH_FN}/subnet_mask.sh"
	src "${PATH_FN}/t5500.sh"
	src "${PATH_FN}/termtitle.sh"
	src "${PATH_FN}/toggle_monitor.sh"
	src "${PATH_FN}/up.sh"
	src "${PATH_FN}/win10.sh"

	if [ -d ~ts ]; then
		src "${PATH_FN}/shutup.sh"
	fi
fi

# Define the PATH
src "${PATH_STNGS}/path_definition.sh"

# Add SSH keys
add_ssh_keys

# Unset variables used in .bashrc
unset PATH_ALIASES
unset PATH_FN
unset PATH_REPO_ROOT
unset PATH_STNGS

# Export environment variables
export GTK_THEME='Adwaita-dark'

# Export the list of secondary directories where ‘.pc’ files are looked up by `pkg-config`
export PKG_CONFIG_PATH='/usr/lib/pkgconfig:/usr/lib64/pkgconfig:/usr/share/pkgconfig'

# Make sure nothing appended to this file is sourced/read
return