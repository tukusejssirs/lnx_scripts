#!/bin/bash

# Validate `.gitlab-ci.yml` via GitLab API v4

# src: https://gist.github.com/Betree/56f9669c3adb2a1633429ff321198fff

# author:  Betree@GitHub, Tukusej's Sirs


if ! command -v yaml2json &> /dev/null; then
	echo 'ERROR: `yaml2json` is not installed. Please install it using `sudo npm -g i js-yaml-cli`.' 1>&2
	return
fi

if ! command -v python &> /dev/null; then
	echo 'ERROR: `python` is not installed. Please install it before trying again.' 1>&2
	return
fi

if command -v bw &> /dev/null; then
	BW="$(command -v bw)"
else
	echo 'ERROR: `bw` is not installed. Please install Bitwarden CLI utility before trying again.' 1>&2
	return
fi

TOKEN="$($BW get item 71a20424-7c68-4506-8ee1-aaf900d4d4d0 | jq -r .notes)"
GITLAB_CI=$(yaml2json < .gitlab-ci.yml | tr -d '\n' | python -c 'import json,sys; print(json.dumps(sys.stdin.read()))')
GITLAB_CI_VALIDITY_INFO="$(curl -s --header "Content-Type: application/json" "https://gitlab.com/api/v4/ci/lint?private_token=$TOKEN" --data "{\"content\": $GITLAB_CI}" | jq .)"
GITLAB_CI_VALIDITY="$(jq -r .status <<< "$GITLAB_CI_VALIDITY_INFO")"

if [ "$GITLAB_CI_VALIDITY" = 'valid' ]; then
	echo 'INFO: `.gitlab-ci.yml` is valid.' 1>&2
	exit 0
else
	echo 'ERROR: `.gitlab-ci.yml` is not valid.' 1>&2
	echo "$GITLAB_CI_VALIDITY_INFO" 1>&2
	exit 1
fi