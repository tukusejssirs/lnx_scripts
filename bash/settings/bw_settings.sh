#!/bin/bash

# Bitwarden startup settings



if [ -f "$HOME/.bw/BW_SESSION" ]; then
	# Set and export Bitwarden session ID
	BW_SESSION="$(cat "$HOME/.bw/BW_SESSION")"
	export BW_SESSION
fi