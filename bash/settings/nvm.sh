#!/usr/bin/env bash

# NVM configuration


export NVM_DIR="$XDG_GIT_DIR/others/nvm"

# Load `nvm`
if [ -s "$NVM_DIR/nvm.sh" ]; then
	# shellcheck disable=SC1091  # Not following: $filename was not specified as input (see shellcheck -x).
	source "$NVM_DIR/nvm.sh"
fi

# Load `nvm` BASH completion
if [ -s "$NVM_DIR/bash_completion" ]; then
	# shellcheck disable=SC1091  # Not following: $filename was not specified as input (see shellcheck -x).
	source "$NVM_DIR/bash_completion"
fi