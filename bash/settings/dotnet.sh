#!/usr/bin/env bash

# Out out from .NET telemetry

if command -v dotnet &> /dev/null; then
	export DOTNET_CLI_TELEMETRY_OPTOUT='true'
fi