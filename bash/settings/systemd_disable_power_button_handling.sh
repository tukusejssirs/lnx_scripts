#!/bin/bash

# Disable handling of power button my SystemD


sudo sed -i 's/^HandlePowerKey=.*$/HandlePowerKey=ignore/' /etc/systemd/logind.conf