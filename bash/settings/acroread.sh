#!/bin/bash

# This enables running Acrobat Reader as root
if [ $(which acroread &> /dev/null; echo $?) = 0 ]; then
	export ACRO_ALLOW_SUDO=true acroread
fi