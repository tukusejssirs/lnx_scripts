#!/bin/bash

# Source ${HOME}/.config/user-dirs.dirs to be able to use the variables in shell and get XDG_GIT_DIR variable


if [ -f "$HOME/.config/user-dirs.dirs" ]; then
	# shellcheck disable=SC1091  # Not following: ./src.sh was not specified as input (see shellcheck -x).
	source "$HOME/.config/user-dirs.dirs"
fi

# In case `user-dirs.dirs` does not define XDG_GIT_DIR variable
if [ ! "$XDG_GIT_DIR" ]; then
	if [ -d ~ts ]; then
		XDG_GIT_DIR='/git'
		export XDG_GIT_DIR
	fi
fi