#!/bin/bash

#TODO
# - getopt
# - add option to select which files are to be counted (glob);

# progress of anything
custom_progress() {
	local fdefault="\e[39m"
	local lred="\e[91m"
	local lgreen="\e[92m"
	local lyellow="\e[93m"
	local lblue="\e[94m"
	local lmagenta="\e[95m"



	######## prosby
	# smv_i=$(printf "%3s" `ls $HOME/git/prosby_ocr/txt/smv_i | wc -l`)
	# smv_ii=$(printf "%3s" `ls $HOME/git/prosby_ocr/txt/smv_ii | wc -l`)
	# chckd=$(printf "%3s" `ls $HOME/git/prosby_ocr/txt/chckd | wc -l`)
	# todo=$(printf "%3s" $(expr $smv_i + $smv_ii))
	# total=$(printf "%3s" $(expr $todo + $chckd))
	# smv_iPer=$(LC_ALL=C /usr/bin/printf "%04.1f\n" $(echo "$smv_i * 100 / $total" | bc -l))
	# smv_iiPer=$(LC_ALL=C /usr/bin/printf "%04.1f\n" $(echo "$smv_ii * 100 / $total" | bc -l))
	# chckdPer=$(LC_ALL=C /usr/bin/printf "%04.1f\n" $(echo "$chckd * 100 / $total" | bc -l))
	# todoPer=$(LC_ALL=C /usr/bin/printf "%04.1f\n" $(echo "$todo * 100 / $total" | bc -l))

	# echo -e "${lyellow}smv_i${fdefault}         ${lred}smv_ii${fdefault}        ${lgreen}chckd${fdefault}          ${lmagenta}todo${fdefault}           ${lblue}total${fdefault}"
	# echo -e "${lyellow}$smv_i ($smv_iPer %)${fdefault}, ${lred}$smv_ii ($smv_iiPer %)${fdefault}, ${lgreen}$chckd ($chckdPer %)${fdefault} + ${lmagenta}$todo ($todoPer %)${fdefault} = ${lblue}$total${fdefault}"



	####### romcal (locales)
	# grep -hPo "^\s+'*\K[^}': ]+(?='*:)" "src/locales/la.ts" | \
	# 	sed '/^advent$\|^season$\|^$\|^feria$\|^sunday$\|^christmastide$\|^day$\|^octave$\|^epiphany$\|^before$\|^after$\|^ordinaryTime$\|^lent$\|^dayAfterAshWed$\|^holyWeek$\|^eastertide$\|^liturgicalColors$\|^GOLD$\|^GREEN$\|^PURPLE$\|^RED$\|^ROSE$\|^WHITE$\|^celebrations$\|^sanctoral$/d' | \
	# 	sort > la_keys

	# # Get the list of `en.ts` keys
	# grep -hPo "^\s+'*\K[^}': ]+(?='*:)" "src/locales/en.ts" | \
	# 	sed '/^advent$\|^season$\|^$\|^feria$\|^sunday$\|^christmastide$\|^day$\|^octave$\|^epiphany$\|^before$\|^after$\|^ordinaryTime$\|^lent$\|^dayAfterAshWed$\|^holyWeek$\|^eastertide$\|^liturgicalColors$\|^GOLD$\|^GREEN$\|^PURPLE$\|^RED$\|^ROSE$\|^WHITE$\|^celebrations$\|^sanctoral$/d' | \
	# 	sort > en_keys

	# # Create some variables
	# missing=$(diff -y --suppress-common-lines la_keys en_keys | wc -l)
	# total=$(cat en_keys | wc -l)
	# done_percent=$((($total - $missing) * 100 / $total))
	#
	# # Output the info
	# echo -e "\n$missing missing of $total ($done_percent % done)"

	# # Remove the auxiliary files
	# rm en_keys la_keys


	# # Example output
	# 357 missing of 714 (50 % done)



	##### mr2004 scanning
	local path total done_count done_percent
	path="$1"
	total="$2"
	done_count="$(find "$path" -type f | wc -l)"
	done_percent=$((done_count * 100 / total))

	echo -e "\n$done_count done of $total ($done_percent %)"
}