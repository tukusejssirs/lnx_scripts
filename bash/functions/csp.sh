#!/bin/bash

# Send Wake-on-Lan magic packet to CS Promo (HP MicroServer Gen8)



csp() {
	local pass
	pass="$("$(command -v bw)" get item 961cfdee-3b24-4f4e-8d4c-aaf9015e95e8 | jq -r .login.password)"
	echo "$pass" | sudo -S ether-wake "$("$(command -v bw)" get item 08285ace-5600-4b7c-b615-ac6e009b4539 | jq -r .notes)"
}