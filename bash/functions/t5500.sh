#!/bin/bash

# Send Wake-on-Lan magic packet to Dell T5500


t5500() {
	local pass
	pass="$("$(command -v bw)" get item 961cfdee-3b24-4f4e-8d4c-aaf9015e95e8 | jq -r .login.password)"
	echo "$pass" | sudo -S ether-wake "$("$(command -v bw)" get item 736cf69c-c0b6-4ac3-81bd-ac3100849a01 | jq -r .notes)"
}