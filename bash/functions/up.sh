#!/bin/bash

# This script is used to move up the directories using `up n`, where `n` is an integer, how many dirs should be moved up


# dependencies: coreutils


up() {
	if [[ $1 = "" ]]; then d=1; else d=$1; fi
	if [[ $d -lt 1 ]]; then echo "Argument must be a positive integer."; fi

	for n in $(seq 1 $d); do
		dots+="../"
	done

	cd $dots
	unset dots
}