#!/bin/bash

# Output multiselect or single select options

# src: https://serverfault.com/a/949806/514866

# Changelog:
# - reformat;
# - change the function name to `multiselect` (I presume it was a typo in the original version of the script);
# - fix all [ShellCheck](https://github.com/koalaman/shellcheck) errors and warnings;
# - remove unnecessary variables (I used Perl extensions in `grep` in order to remove unused `COL` variable in `get_cursor_row()` function);
# - rename some variables in order to make them more understandable;
# - change some `if`s to `case`;
# - add ‘BASH Doc’ to all functions;
# - fix exiting terminal when pressing Ctrl+C (in `trap`, `exit` only in subshell; when the function is sourced, run only a `return`);
# - replace most `[[ ]]` with `[ ]`, leaving them only where necessary (i.e. when using `=~` operator);
# - option to show item ID (i.e. array index of that item);
# - use `getopt` (see either the `help_short()` function or run `multiselect -h`);
# - implement single select;
# - additional dependencies: GNU `grep`, GNU `sed`;
# - add help message (use `-h` or `--help` option to output it);
# - add an option override the prefixes (the marks for selected/deselected items);
# - make it possible to select items using 0-9 numbers (only the first 10 items; you can disable it;
# - add an option to change the pre-highlighted item;
# - merge `cursor_blink_{on,off}` into `toggle_cursor_blinking()`;

# Known issues I couldn’t solve by myself:
# - Sometimes (probably when pressing arrows and Space in fast succession), some ‘ghosts’ appear after items, e.g. `^[[A` or `^[[B` (mostly without `^` as it is overwritten by the item name). I presume those are keycodes. I have no idea how to suppress them from showing on STDOUT nor redirect them to somewhere. Maybe the solution is to refactor the `key_input()` function. Note that the original version of the script, however, it happened less. In spite of this issue, the script works as expected.


##
## @brief      Output multiselect interactive items and return the selections
##
## This function accepts `getopt` options. Use `-h` option to output all possible options.
##
## @return     Return/exit codes are also listed in the help (`-h`)
##
multiselect() {
	##
	## @brief      Toggle cursor blinking
	##
	## @param      $1    Action (on or off)
	##
	toggle_cursor_blinking() {
		action="$1"

		case "$action" in
			'on')
				printf '\033[?25h'
			;;
			'off')
				printf '\033[?25l'
			;;
		esac
	}

	##
	## @brief      Move cursor to the specified row
	##
	## @param      $1    Row number to which the cursor should move
	##
	cursor_to() {
		printf '\033[%s;%sH' "$1" "${2:-1}"
	}

	##
	## @brief      Print items
	##
	## @param      $1    Boolean if the item is highlighted
	## @param      $2    Boolean if item ID is to be shown
	## @param      $3    Item prefix (checked or unchecked)
	## @param      $4    Item value
	## @param      $5    Item ID
	## @param      $6    Width of array length number
	##
	print_item() {
		local highlighted="$1"
		local show_id="$2"
		local prefix="$3"
		local value="$4"
		local item_id="$5"
		local width="$6"

		if [ "$show_id" = 'true' ]; then
			if [ ! "$width" ]; then
				width="$(echo -n "$item_id" | wc -m)"
			fi

			width="$(printf "% ${width}d" "$item_id")"
			item_id+=' '
		else
			unset item_id
		fi

		if [ "$highlighted" = 'true' ]; then
			printf '%s %s\033[7m %s \033[27m ' "$prefix" "$item_id" "$value"
		else
			printf '%s %s %s ' "$prefix" "$item_id" "$value"
		fi
	}

	##
	## @brief      Get row number of current cursor position
	##
	get_cursor_row() {
		IFS=';' read -rsdR -p $'\E[6n' POS
		grep -Po '\[\K[0-9]+' <<< "$POS"
	}

	##
	## @brief      Get name of the pressed key
	##
	key_input() {
		local key
		IFS= read -rsn1 key &> /dev/null

		case "$key" in
			'')
				echo enter
			;;
			$'\x20')
				echo space
			;;
			$'\x1b')
				read -rsn2 key

				case "$key" in
					'[A')
						echo up
					;;
					'[B')
						echo down
					;;
				esac
			;;
			[0-9])
				echo "$key"
			;;
		esac
	}

	##
	## @brief      Toggle selection of an item
	##
	## @param      $1    Array of selected items
	## @param      $2    Element in the array of the item to be toggled
	## @param      $3    [Optional] Boolean if single select is permitted only
	##
	toggle_select_item() {
		local arr_sel item is_single_select
		read -ra arr_sel <<< "$1"
		item=$2
		is_single_select=$3

		if [ "$is_single_select" ] && [ "$is_single_select" = 'true' ]; then
			# In single select, replace the previously selected item by outputing the $item
			echo "$item"
		else
			# In multiselect
			if [[ ${arr_sel[*]} =~ $item ]]; then
				# When the item is already selected, deselect it, i.e. remove it from from the array
				# TODO: I wanted to use the following line instead of the `for` cycle below (because I use it more than once), but it did not work in here for some reason, but it does elsewhere
				# read -ra arr_sel <<< "$(remove_item_from_array "${arr_sel[*]}" "$i")"
				for i in "${!arr_sel[@]}"; do
					if [[ ${arr_sel[i]} = "$item" ]]; then
						unset 'arr_sel[i]'
					fi
				done
			else
				# When the item is not yet selected, select it
				arr_sel+=("$item")
			fi

			# Sort and trim the array and output it
			sort_trim_array "${arr_sel[*]}"
		fi
	}

	##
	## @brief      Output help message
	##
	short_help() {
		echo -e 'Usage'
		echo -e '\tmultiselect [--selected items | -s items] [--show-id | -i] [--single-select | -1] <space-separated items>'
		echo -e '\tmultiselect [--help | -h]'
		echo
		echo -e 'Description'
		echo -e '\tCreate multiselect or single select list of items.'
		echo
		echo -e '\tHighlighted item can be changed using arrows (Up and Down; endlessly circulated).'
		echo
		echo -e '\tAn item can be selected via Space and the selection confirmed by pressing Enter.'
		echo
		echo -e 'Options'
		echo -e '\tMandatory arguments to long options are mandatory for short options too.'
		echo
		echo -e '\t-a, --array'
		echo -e '\t\toutput variable (array) where the selected options are output'
		echo
		echo -e '\t-f, --file'
		echo -e '\t\toutput file where the selected options are output'
		echo
		echo -e '\t-h, --help'
		echo -e '\t\tdisplay this help and exit'
		echo
		echo -e '\t\tNote that either this option of `--array` is required. You may choose to use both, then both the file and the array will contain the selected item IDs.'
		echo
		echo -e '\t\tNote that either this option of `--file` is required. You may choose to use both, then both the file and the array will contain the selected item IDs.'
		echo
		echo -e '\t-s, --selected=ITEMS'
		echo -e '\t\tspace-separated list of selected item IDs'
		echo
		echo -e '\t\tNote that IDs are integers starting from 0. They are actually array indexes.'
		echo
		echo -e '\t\tAlso note that when more than one item should be selected, they should be quoted, as the option accepts one argument only.'
		echo
		echo -e '\t\tWhen non-existent IDs are selected, they are ignored and a warning is output.'
		echo
		echo -e '\t\tWhen --single-select is requested and more than one ID is selected, the last select item is selected.'
		echo
		echo -e '\t\tBy default, no item is selected in multiselect and the first item (0th index) in single select.'
		echo
		echo -e '\t-i, --show-id'
		echo -e '\t\tshow item IDs'
		echo
		echo -e '\t\tBy default, the IDs are not output.'
		echo
		echo -e '\t-1, --single-select'
		echo -e '\t\tpermit only one item to be selected'
		echo
		echo -e '\t\tBy default, multiselect is enabled.'
		echo
		echo -e '\t-e, --selected-prefix=PREFIX'
		echo -e '\t\toverride the prefix for selected items'
		echo
		echo -e '\t\tBy default, it is set to `[x]`.'
		echo
		echo -e '\t-d, --deselected-prefix=PREFIX'
		echo -e '\t\toverride the prefix for deselected items'
		echo
		echo -e '\t\tBy default, it is set to `[ ]`.'
		echo
		echo -e '\t-n, --no-number-input'
		echo -e '\t\tdisable number input'
		echo
		echo -e '\t\tWhen this option is not present, the user can use `0-9` to select items between 0 and 9.'
		echo
		echo -e '\t\tBy default, number input is enabled.'
		echo
		echo -e '\t-g, --highlighted=ITEM'
		echo -e '\t\titem ID of item which should be highlighted'
		echo
		echo -e '\t\tBy default, the first (0th) item is highlighted.'
		echo
		echo -e '\t-q, --quiet'
		echo -e "\t\tdon't output warning or error messages except for the error message when invalid option is provided"
		echo
		echo -e '\t\tBy default, warning and error messages are output'
		echo
		echo -e 'Examples'
		echo
		echo -e '\tOutput items:'
		echo -e '\t\tmultiselect apples pears grapes oranges'
		echo
		echo -e '\tOutput items with pre-selected items (apples and oranges):'
		echo -e "\t\tmultiselect -s '0 3' apples pears grapes oranges"
		echo
		echo -e '\tOutput items with pre-selected items (apples and oranges) and the item IDs are output:'
		echo -e "\t\tmultiselect -is '0 3' apples pears grapes oranges"
		echo
		echo -e '\tSingle select: output items with pre-selected item (oranges) and the item IDs are output:'
		echo -e '\t\tmultiselect -1is 3 apples pears grapes oranges'
		echo
		echo -e 'Exit/return codes'
		echo -e '\t\t0    Success'
		echo -e '\t\t1    Invalid options provided'
		echo -e '\t\t2    Exited using Ctrl+C (or equivalent)'
		echo -e '\t\t3    No option provided'
		echo -e '\t\t4    No output file nor output variable (array) provided'
	}

	##
	## @brief      Trapping some error codes
	##
	## @return     Return code 2 when this function is executed
	##
	trapped_exit() {
		toggle_cursor_blinking 'on'
		stty echo
		printf '\n'

		if [ "${BASH_SOURCE[0]}" = "$0" ]; then
			# The script is run as a subshell
			exit 2
		fi
	}

	##
	## @brief      Output messages
	##
	## @param      $1    Boolean if the messages should be output
	## @param      $2    Message type (e.g. `WARNING` or `ERROR`)
	## @param      $3    Message text
	## @param      $4    [Optional] Return/exit code
	##
	## @return     Return/exit code when provided
	##
	output_messages() {
		local quiet_warnings="$1"
		local type="$2"
		local msg="$3"
		local code="$4"

		if [ "$quiet_warnings" = 'false' ]; then
			echo -e "$type: $msg" 1>&2
		fi

		# if [ "$type" = 'ERROR' ]; then
			# FIXME: Create a function to check if running from a script or from a sourced file
			# TODO: It might be a good idea to use `trap()` function before actually exiting the script
			# return "$code"
		# fi
	}

	##
	## @brief      Remove an item from an array
	##
	## @param      $1    Array
	## @param      $2    Item
	##
	remove_item_from_array() {
		local arr item
		read -ra arr <<< "$1"
		# arr=($1)
		item="$2"

		for i in "${!arr[@]}"; do
			if [ "${arr[i]}" = "$item" ]; then
				unset 'arr[i]'
			fi
		done

		echo "${arr[*]}"
	}

	##
	## @brief      Sort and trim an array
	##
	## @param      $1    Array
	##
	sort_trim_array() {
		local arr
		read -ra arr <<< "$1"

		# Sort and trim the array and output it
		# Note: The `sed` command trims unnecessary whitespaces leaving a single space between the elements only
		printf '%s\n' "${arr[@]}" | sort -n | sed -z 's/\s\+/ /g;s/^\s*//g;s/\s*$//g'
	}

	# Variables
	local deselected_multi_prefix deselected_single_prefix getopt_options highlighted items last_row newlines no_number_input quiet_warnings selected selected_items selected_multi_prefix selected_single_prefix show_id single_select start_row selected_prefix deselected_prefix
	deselected_multi_prefix='[ ]'
	selected_multi_prefix='[x]'
	deselected_single_prefix='( )'
	selected_single_prefix='(o)'
	show_id='false'
	newlines=0
	selected=()
	highlighted=0
	single_select='false'
	quiet_warnings='false'
	no_number_input='false'

	# Call getopt to validate the provided input
	if ! getopt_options=$(getopt -o 1a:f:d:e:is:hg:nq --long show-id,single-select,selected:,selected-prefix:,deselected-prefix:,help,highlighted:,no-number-input,quiet,array:,file: -- "$@"); then
		output_messages "$quiet_warnings" 'ERROR' 'Invalid options provided.' 1
	fi

	eval set -- "$getopt_options"

	while true; do
		case "$1" in
			--single-select|-1)
				single_select='true'
			;;
			--show-id|-i)
				show_id='true'
			;;
			--selected|-s)
				shift
				read -ra selected_items <<< "$1"
			;;
			--selected-prefix|-e)
				shift
				selected_prefix="$1"
			;;
			--deselected-prefix|-d)
				shift
				deselected_prefix="$1"
			;;
			--no-number-input|-n)
				no_number_input='true'
			;;
			--highlighted|-g)
				shift
				highlighted="$1"
			;;
			--array|-a)
				shift
				output_array="$1"
			;;
			--file|-f)
				shift
				output_file="$1"
			;;
			--help|-h)
				short_help

				if [ "${BASH_SOURCE[0]}" != "${0}" ]; then
					# Return from sourced script
					return 0
				else
					# Exit from subshell (executed script)
					exit 0
				fi
			;;
			--quiet|-q)
				quiet_warnings='true'
			;;
			--)
				shift
				items=("$@")
				break
			;;
		esac
		shift
	done

	if [ "${#items[@]}" -lt 1 ]; then
		output_messages "$quiet_warnings" 'ERROR' 'There must be at least one option provided.'
		return 3
	fi

	if [ ! "$output_file" ] && [ ! "$output_array" ]; then
		output_messages "$quiet_warnings" 'ERROR' 'There must be either output file (`--file`) or output variable (`--array`) provided.'
		return 4
	fi

	for i in "${selected_items[@]}"; do
		local sel_removed_item

		if [ "$i" -ge ${#items[*]} ]; then
			output_messages "$quiet_warnings" 'WARNING' "Item $i can't be selected because you have entered ${#items[@]} items only."
			output_messages "$quiet_warnings" 'WARNING' "Removing $i from selected items."

			sel_removed_item="$(remove_item_from_array "${selected_items[*]}" "$i")"
			read -ra selected_items <<< "$(sort_trim_array "$sel_removed_item")"
		elif ! grep -q '^[0-9]\+$' <<< "$i"; then
			sel_removed_item="$(remove_item_from_array "${selected_items[*]}" "$i")"
			read -ra selected_items <<< "$(sort_trim_array "$sel_removed_item")"

			output_messages "$quiet_warnings" 'WARNING' "Item $i can't be selected because it is not an integer."
			output_messages "$quiet_warnings" 'WARNING' "Removing $i from selected items."
		fi
	done

	read -ra selected_items <<< "${selected_items[@]}"

	if [ "$single_select" = 'true' ]; then
		# Get the selected item
		local last_selected_item

		case "${#selected_items[@]}" in
			0|1)
				# When no item is selected in single select, the first item is selected by default
				last_selected_item=0
			;;
			*)
				last_selected_item="${selected_items[$((${#selected_items[@]} - 1))]}"

				output_messages "$quiet_warnings" 'WARNING' 'In single select, only one item can be selected; you have selected more.'
				output_messages "$quiet_warnings" 'WARNING' "Only the last item ($last_selected_item) will be selected."
			;;
		esac

		selected=("$last_selected_item")

		# Check if custom prefixes should be used
		if [ "$selected_prefix" ]; then
			selected_single_prefix="$selected_prefix"
		fi

		if [ "$deselected_prefix" ]; then
			deselected_single_prefix="$deselected_prefix"
		fi
	else
		# Get the selected items
		for ((i=0; i<${#items[@]}; i++)); do
			selected+=("${selected_items[$i]}")
		done

		# Check if custom prefixes should be used
		if [ "$selected_prefix" ]; then
			selected_multi_prefix="$selected_prefix"
		fi

		if [ "$deselected_prefix" ]; then
			deselected_multi_prefix="$deselected_prefix"
		fi
	fi

	# Test the highlighted item
	if [ ! "$highlighted" ] || ! grep -q '^[0-9]\+$' <<< "$highlighted" || [ "$highlighted" -ge "${#items[@]}" ] || [ "$highlighted" -lt 0 ]; then
		highlighted=0

		output_messages "$quiet_warnings" 'WARNING' "Item can't be highlighted, because it is not valid."
		output_messages "$quiet_warnings" 'WARNING' "Highlighing the first item (0)."
	fi

	# Make sure the items have enough space
	# Note: This is only required when there is one or more warnings output, however, it does not do any harm when there is no warning.
	newlines=$((${#items[@]}))

	for ((i=0; i < newlines; i++)); do
		printf '\n'
	done

	# Determine current screen position for overwriting the items
	last_row="$(get_cursor_row)"
	start_row=$((last_row - ${#items[@]}))

	# Ensure cursor and input echoing back on upon a ctrl+c during read -s
	trap 'trapped_exit; return 2 2> /dev/null' 2 SIGHUP SIGINT SIGTERM

	# Stop blinking the cursor
	toggle_cursor_blinking 'off'

	# TODO: Move this definition higher (above)
	# TODO: Get the length of the longest item
	array_length_width="$(echo -n "$((${#items[@]} - 1))" | wc -m)"

	# Print items by overwriting the previously printed lines
	while true; do
		local item_id=0

		for item in "${items[@]}"; do
			local prefix

			if [ "$single_select" = 'true' ]; then
				if [ "${selected[0]}" = "$item_id" ]; then
					prefix="$selected_single_prefix"
				else
					prefix="$deselected_single_prefix"
				fi
			else
				if [[ ${selected[*]} =~ $item_id ]]; then
					prefix="$selected_multi_prefix"
				else
					prefix="$deselected_multi_prefix"
				fi
			fi

			cursor_to $((start_row + item_id))

			if [ $item_id -eq $highlighted ]; then
				is_highlighted='true'
			else
				is_highlighted='false'
			fi

			print_item "$is_highlighted" "$show_id" "$prefix" "$item" "$item_id" "$array_length_width"

			((item_id++))
		done

		# Keyboard control (Up, Down, Enter)
		local key="$(key_input)"

		case "$key" in
			space)
				# Toggle selection of the highlighted item
				read -ra selected <<< "$(toggle_select_item "${selected[*]}" "$highlighted" "$single_select")"
			;;
			enter)
				# Break from the script and output to STDOUT the selected item indexes
				break
			;;
			up)
				# Highlight the item above or (when the first/0th item is highlighted), highlight the last item
				((highlighted--))

				if [ $highlighted -lt 0 ]; then
					highlighted=$((${#items[@]} - 1))
				fi
			;;
			down)
				# Highlight the item below or (when the last item is highlighted), highlight the first/0th item
				((highlighted++))

				if [ $highlighted -ge ${#items[@]} ]; then
					highlighted=0
				fi
			;;
			[0-9])
				# Toggle selection of the item uses its index number (only for 0-9 indexes)
				# Note: If an index (number) higher then the highest index is input, it is gracefully ignored (no warning is output)
				if [ "$no_number_input" = 'false' ]; then
					read -ra selected <<< "$(toggle_select_item "${selected[*]}" "$key" "$single_select")"
				fi
			;;
		esac
	done

	# cursor position back to normal
	cursor_to "$last_row"
	printf '\n'
	toggle_cursor_blinking 'on'

	# Output the selected values
	if [ "$output_array" ]; then
		eval "$output_array"='("${selected[@]}")'
	fi

	if [ "$output_file" ]; then
		echo "${selected[*]}" > "$output_file"
	fi

	if [ "${BASH_SOURCE[0]}" != "${0}" ]; then
		# Return from sourced script
		return 0
	else
		# Exit from subshell (executed script)
		exit 0
	fi
}