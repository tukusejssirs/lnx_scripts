#!/bin/bash

# This function OCR's a PDF file page-by-page. Each page will be in a separate TXT file.

# TODO: The following should be removed
# $1       [path/]pdf_file
# $2       [path/]output_file
# $3       [lang] (default: eng; multiple languages may be specified, separated by plus characters)
# $4       [start_page] (default: first page)
# $5       [last_page] (default: last page)
# $6       [prefix_width] (default: same as the width of the total page number)

# TODO: The following should be removed
# If you want to skip an argument, use a hyphen (-). This is only applicable to arguments $3 through $6.

# TODO: The following should be moved to the long help and the man page
# Return codes:
# 0        Success
# 1        Input file does not exist
# 2        Output path for images already exists, but it's not a directory
# 3        Output path for text files already exists, but it's not a directory
# 30+n     Tesseract command was unsuccessful n times

# TODO: The following should be removed
# pdfocr [path/]pdf_file [path/]output_file lang [leading_number_width] [start_page] [last_page]

# TODO
# - use `getopts` for options gathering;
# - option: add separate output path for images and for text file;
# - option to process images (at least TIFF files) besides PDF files;
# - create short and long help, incl the man page;
# - consider renaming the function as we won't process PDF file only anymore;
# - add output_messages() function;
# - list all dependencies;


 pdfocr() {
	 help_short() {
		cat << EOF
Usage: $FN_NAME -l lang [-w prefix_width] [-s start_page] [-e last_page] [-n name] [-i image_out_path] [-t text_out_path] [path/]pdf_file

-e, --end=end            end page number (used only for PDF files)
                         it can be used only for one PDF; if used with more than one PDF, the script will use the start-end page range on all PDF files
                         default: last page
-l, --lang=lang          language of the text to OCR (see \`tesseract --list-langs\` for possible options);
                         multiple languages may be specified, separated by plus characters;
                         default: eng
-n, --name=name          name of the output file without number prefix and extension in case the last argument without option is a folder (see below)
                         default: out
-h, --help               output this help
-i, --image-output=out   output path for images (used only for PDF files);
                         if the path does not exist, it will be created
                         if the path exists, but it is not a folder, the script is existed
                         default: same as the path of the input file
-p, --prefix-name-sep=c  separator between number prefix and the name in output filename
                         it can be any string of any length
                         default=_
-r, --start-prefix=num   first prefix number to be used
                         default: 1
-s, --start=start        start page number (used only for PDF files)
                         default: 1
-t, --text-output=out    output path for text files
                         if the path does not exist, it will be created
                         if the path exists, but it is not a folder, the script is existed
                         note that this path is used only for images exported from PDF files
                         default: same as the path of the input file
-w, --width=width        prefix number width;
                         default: width of total number of pages in input PDF(s) and/or total number of image(s)
                         - PDF: width of last processed page;
                         - images: width of the number of processed image files

All arguments without option are used as input files.

Supported image formats are those that are supported by Tesseract/Leptonica, including BMP, PNM, PNG, JFIF, JPEG, and TIFF, but mostly tested are PNG and TIFF.
EOF

		return 0
	}

	 help_long() {
		help_short
		# return 0
	}

	# Variables
	local FN_NAME="${FUNCNAME[0]}"
	local START_NUM=1
	local END_NUM=
	local PREFIX_WIDTH=
	local PREFIX_NAME_SEP='_'
	local START_PREFIX_NUM=1
	local TOTAL_NUM=0
	local NAME='out'
	local LANG='eng'
	local PATH_IMG=()
	local PATH_TXT=()
	local PATH_INPUT=()
	local FILE_INPUT=()
	local tess_success=30

	# Call getopt to validate the provided input
	local OPTIONS=$(getopt -o e:l:n:hi:p:r:s:t:w: --long end:,lang:,name:,help,image-output:,prefix-name-sep:,start-prefix:,start:,text-output:,width: -- "$@")

	[ $? -eq 0 ] || {
		echo 'ERROR: Invalid options provided.' 1>&2
		help_short
	}

	eval set -- "$OPTIONS"

	while true; do
		case "$1" in
			--end|-e)
				shift
				END_NUM=$1
			;;
			--lang|-l)
				shift
				LANG=$1
			;;
			--name|-n)
				shift
				NAME=$1
			;;
			--help|-h)
				help_long
			;;
			--image-output|-i)
				shift
				PATH_IMG="$(realpath "$1")"

				if [ -e "$PATH_IMG" ] && [ ! -d "$PATH_IMG" ]; then
					echo "ERROR: Output path for images $PATH_IMG already exists, but it's not a directory." 1>&2
					# TODO: Output help_short()
					return 2
				fi

				mkdir -p "$PATH_IMG"
			;;
			--prefix-name-sep|-p)
				shift
				PREFIX_NAME_SEP=$1
			;;
			--start-prefix|-r)
				shift
				START_PREFIX_NUM=$1
			;;
			--start|-s)
				shift
				START_NUM=$1
			;;
			--text-output|-t)
				shift
				PATH_TXT="$(realpath "$1")"

				if [ -e "$PATH_TXT" ] && [ ! -d "$PATH_TXT" ]; then
					echo "ERROR: Output path for text files $PATH_TXT already exists, but it's not a directory." 1>&2
					# TODO: Output help_short()
					return 3
				fi

				mkdir -p "$PATH_TXT"
			;;
			--width|-w)
				shift
				PREFIX_WIDTH=$1
			;;
			--)
				shift
				OPT_REST="$*"
				break
			;;
		esac
		shift
	done

	for i in $(seq 0 "$(("${#OPT_REST[@]}" - 1))"); do
		# Take care of the globs
		local TMP_FILE=(${OPT_REST[$i]})

		for j in $(seq 0 "$(("${#TMP_FILE[@]}" - 1))"); do
			if [ "${TMP_FILE[$j]}" != '' ] && [ -e "${TMP_FILE[$j]}" ]; then
				# Get the list of input files and their paths
				PATH_INPUT=("${PATH_INPUT[@]}" "$(dirname "$(realpath "${TMP_FILE[$j]}")")")
				FILE_INPUT=("${FILE_INPUT[@]}" "$(basename "$(realpath "${TMP_FILE[$j]}")")")

				# Set output paths to PATH_INPUT if not set
				if [ ! "$PATH_IMG" ]; then
					PATH_IMG=("${PATH_IMG[@]}" "${PATH_INPUT[-1]}")
				fi

				if [ ! "$PATH_TXT" ]; then
					PATH_TXT=("${PATH_TXT[@]}" "${PATH_INPUT[-1]}")
				fi

				# Last page
				# TODO: If not set, check the page number of the last page of the first file
				# TODO: If not set, count all the pages in all PDFs and all image files---that's the last page number
				local TEST_MIME_TYPE="$(file -b --mime-type "${PATH_INPUT[-1]}/${FILE_INPUT[-1]}")"

				case "$TEST_MIME_TYPE" in
					'application/pdf')
						if [ "$END_NUM" = '' ]; then
							(( TOTAL_NUM+=$(pdfinfo "${PATH_INPUT[-1]}/${FILE_INPUT[-1]}" | grep -Po 'Pages:\s*\K[0-9]+') ))
						else
							# Plus one at the end is because the start and end page numbers are inclusive
							(( TOTAL_NUM+=$(("$END_NUM" - "$START_NUM" + 1)) ))
						fi
					;;
					*)
						(( TOTAL_NUM++ ))
					;;
				esac
			else
				echo "ERROR: File ${TMP_FILE[$j]} does not exist." 1>&2
				return 1
			fi
		done

	done

	# Prefix width
	if [ ! "$PREFIX_WIDTH" ]; then
		PREFIX_WIDTH="${#TOTAL_NUM}"
	fi

	# echo "${TOTAL_NUM}"
	# echo "${PREFIX_WIDTH}"
	# return

	# Start file prefix number
	TMP_FILE_PREFIX="$START_PREFIX_NUM"

	for i in $(seq 0 "$(("${#FILE_INPUT[@]}" - 1))"); do
		local TMP_INPUT_FILE="${PATH_INPUT[$i]}/${FILE_INPUT[$i]}"

		if [ "${#PATH_IMG[@]}" = 1 ]; then
			local TMP_PATH_IMG="$PATH_IMG"
		else
			local TMP_PATH_IMG="${PATH_IMG[$i]}"
		fi

		if [ "${#TMP_PATH_TXT[@]}" = 1 ]; then
			local TMP_PATH_TXT="$PATH_TXT"
		else
			local TMP_PATH_TXT="${PATH_TXT[$i]}"
		fi

		local TMP_PATH_TXT="${PATH_TXT[$i]}"

		local TEST_MIME_TYPE="$(file -b --mime-type "$TMP_INPUT_FILE")"

		case "$TEST_MIME_TYPE" in
			'application/pdf')
				for j in $(seq "$START_NUM" "$END_NUM"); do
					local TMP_FILENAME_FORMATTED="$(printf "%0*d" "${PREFIX_WIDTH}" "${TMP_FILE_PREFIX}")${PREFIX_NAME_SEP}${NAME}"

					echo "input file:  ${PATH_INPUT[$i]}/${FILE_INPUT[$i]}"
					echo "output file: $TMP_FILENAME_FORMATTED"
					echo "PDF page:    $j of $END_NUM"
					echo "total page:  $TMP_FILE_PREFIX of $TOTAL_NUM"
					echo "file:        $i of ${#FILE_INPUT[@]}"

					pdfimages -png -f "${j}" -l "${j}" "$TMP_INPUT_FILE" "${TMP_PATH_IMG}/${TMP_FILENAME_FORMATTED}"
					mv "${TMP_PATH_IMG}/${TMP_FILENAME_FORMATTED}-"*.png "${TMP_PATH_IMG}/${TMP_FILENAME_FORMATTED}.png"
					tesseract "${TMP_PATH_IMG}/${TMP_FILENAME_FORMATTED}"* "${TMP_PATH_TXT}/${TMP_FILENAME_FORMATTED}" -l "${LANG}" 2> /dev/null
					local test=$?

					if [ $test -gt 0 ]; then
						(( tess_success+=1 ))
					fi

					echo '... done!'

					(( TMP_FILE_PREFIX++ ))
				done
			;;
			*)
				local TMP_FILENAME_FORMATTED="$(printf "%0*d" "${PREFIX_WIDTH}" "${TMP_FILE_PREFIX}")${PREFIX_NAME_SEP}${NAME}"

				echo "input file:  ${PATH_INPUT[$i]}/${FILE_INPUT[$i]}"
				echo "output file: $TMP_FILENAME_FORMATTED"
				echo "PDF page:    $j of $END_NUM"
				echo "total page:  $TMP_FILE_PREFIX of $TOTAL_NUM"
				echo "file:        $i of ${#FILE_INPUT[@]}"

				tesseract "$TMP_INPUT_FILE" "${TMP_PATH_TXT}/${TMP_FILENAME_FORMATTED}" -l "${LANG}" 2> /dev/null
				local test=$?

				if [ $test -gt 0 ]; then
					(( tess_success+=1 ))
				fi

				echo '... done!'

				(( TMP_FILE_PREFIX++ ))
			;;
		esac
	done

	return $tess_success
}