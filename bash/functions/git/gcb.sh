#!/bin/bash

# This function creates a new local branch, checks out to it and sets it to track origin remote branch of the same name (the remote branch will be created if it does not exist)

# $1       new branch name
# $2       (optional) commit/branch/tag; when not provided, the currently checked out branch is used


gcb() {
	new_branch="$1"
	based_on_branch="$2"

	if [ "$based_on_branch" ]; then
		git checkout -b "$new_branch" "$based_on_branch"
	else
		git checkout -b "$new_branch"
	fi

	git push --set-upstream origin "$new_branch"
}