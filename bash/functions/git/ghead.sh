#!/bin/bash

# Set HEAD branch (aka master/default branch) according to the remote settings


ghead() {
	git remote set-head origin -a
}