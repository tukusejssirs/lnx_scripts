#!/bin/bash

# This function deletes existing tags with the given names

# $@       tag name(s)


gtd() {
	git tag -d $@
}