#!/bin/bash

# Show the working tree status in the short-format


gss() {
	git status -s $@
}