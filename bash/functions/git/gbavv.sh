#!/bin/bash

# List all local and remote branches verbously show sha1 and commit subject line for each head, along with relationship to upstream branch (if any) and print the name of the upstream branch


gbavv() {
	git branch -avv $@
}