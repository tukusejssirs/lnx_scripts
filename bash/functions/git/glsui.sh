#!/bin/bash

# List all the untracked files, including the ignored files


glsui() {
	git ls-files --others "$@"
}