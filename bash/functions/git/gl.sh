#!/bin/bash

# Show git log (full commit hashes and messages with author and date)


gl() {
	git log $@
}