#!/bin/bash

# Remove changes (selected files/folders) from stage, i.e. the files become unstaged


grmc() {
	git rm --cached -- $@
}