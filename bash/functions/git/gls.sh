#!/bin/bash

# List all files, including untracked files, excluding ignored


gls() {
	git ls-files --exclude-standard "$@"
}