#!/bin/bash

# Show git log with short commit hashes, commit dates and only subject (first line) of commit messages


gl1sd() {
	git log --date=format-local:%c --format='%C(auto)%h  %<(29)%C(brightmagenta)%cd%Creset  %s%d' "$@"
}