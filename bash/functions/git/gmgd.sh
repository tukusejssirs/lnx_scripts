#!/bin/bash

# This function merges branch $1 and then deletes branch $1

# $1       existing branch name


gmgd() {
	git merge $1 && git branch --delete $1
}