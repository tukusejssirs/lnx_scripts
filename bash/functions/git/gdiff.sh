#!/bin/bash

# Show differences between HEAD files and files in CWD; if any arg specified, show only those files


gdiff() {
	git diff $@
}