#!/bin/bash

# This function outputs local branch name that tracks the remote HEAD (main) branch


gbm() {
	git branch -vv | grep -Po "^[\s\*]*\K[^\s]*(?=.*$(git branch -rl '*/HEAD' | grep -o '[^ ]\+$'))"
}