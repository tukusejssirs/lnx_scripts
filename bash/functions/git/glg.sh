#!/bin/bash

# Show git log with short commit hashes and only subject (first line) of commit messages with graphical representation of commit history


glg() {
	git log --pretty=format:"%h %s" --graph
}