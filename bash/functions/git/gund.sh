#!/bin/bash

# This script undoes the last N commit (by default, last 1 commit)


gund() {
	if [ "$1" -gt 1 ]; then
		git reset --soft HEAD~$1
	else
		git reset --soft HEAD~1
	fi
}