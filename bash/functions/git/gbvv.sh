#!/bin/bash

# List all local branches verbously show sha1 and commit subject line for each head, along with relationship to upstream branch (if any) and print the name of the upstream branch


gbvv() {
	git branch -vv $@
}