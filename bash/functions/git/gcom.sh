#!/bin/bash

# Check out to main branch


gcom() {
	git checkout "$(git branch -vv | grep -Po "^[\s\*]*\K[^\s]*(?=.*$(git branch -r | grep -Po 'HEAD -> \K.*$'))")"
}