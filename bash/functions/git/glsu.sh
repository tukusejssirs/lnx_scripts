#!/bin/bash

# List untracked files, excluding ignored files


glsu() {
	git ls-files --others --exclude-standard "$@"
}