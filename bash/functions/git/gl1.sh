#!/bin/bash

# Show git log with full commit hashes and messages


gl1() {
	git log --pretty=oneline
}