#!/bin/bash

# This function outputs remote branch name that is tracked by current branch


gbcr() {
	git for-each-ref --format='%(upstream:short)' $(git symbolic-ref -q HEAD)
}