#!/bin/bash

# Show git log with full commit hashes and only subject (first line) of commit messages


gl1s() {
	git log --pretty=oneline --abbrev-commit "$@"
}