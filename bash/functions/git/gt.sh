#!/bin/bash

# This function lists tags of current repo.

# $1       existing branch name ideally prefixed with remote’s name and a slashp


gt() {
	git tag -l
}