#!/bin/bash

# Without any additional arg/options, list all remote branches verbously


gbrv() {
	git branch -rv $@
}