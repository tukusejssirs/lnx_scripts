#!/bin/bash

# Remove the specified branch remotely

# $1       existing branch name with remote (e.g. `origin/branch`)


gbdr() {
	remote="${1%%/*}"
	branch="${1##*/}"
	git push "$remote" ":refs/heads/$remote/$branch"
}