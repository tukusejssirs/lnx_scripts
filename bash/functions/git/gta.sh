#!/bin/bash

# This function makes an unsigned, annotated tag object and use the given tag message.

# $1       tag name
# $2       tag message
# $3       commit hash (optional)


gta() {
	if [ $# <= 2 ]; then
		git tag -a "$1" -m "$(echo -e \"$2\" | sed 's/^"//;s/"$//' -)"
		git push origin --tags
	elif [ $# = 3 ]; then
		git checkout $3
		GIT_COMMITTER_DATE="$(git show --format=%aD | head -1)" git tag -a "$1" -m "$(echo -e \"$2\" | sed 's/^"//;s/"$//' -)"
		git checkout master
		git push origin --tags
	fi
}