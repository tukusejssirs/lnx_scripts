#!/bin/bash

# Output remote HEAD (main) branch name


gbmr() {
	git branch -r | grep -Po 'HEAD -> \K.*$'
}