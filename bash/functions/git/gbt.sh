#!/bin/bash

# This function sets the tracking branch of the currently checked out branch

# $1       remote branch to be tracked; if no remote is prefixed, then the first remote of `git remote` is used


gbt() {
	if [ -z "${1##*/*}" ]; then
		remote_branch="$1"
	else
		remote_branch="$(git remote | head -1)/$1"
	fi

	git branch --set-upstream-to "$remote_branch"
}