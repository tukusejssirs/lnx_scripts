#!/bin/bash

# Resets the file in the index but not in the working tree


grmr() {
	git reset HEAD -- $@
}