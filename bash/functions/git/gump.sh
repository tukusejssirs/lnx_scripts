#!/bin/bash

# This function commits all changes from current directory to $branch (locally and remotely), check outs to master, squashes (merges without commit) and commits all changes from $branch to $main (locally and remotely), deletes the $branch (locally and remotely) and finally pushes all the changes to the remote.

# Note that this function sources `gup` and `gbd` functions, which presumes that you have cloned the repo in `$HOME/git/lnx_scripts` folder.

# The function's name comes from the following words: Git Update Merge Push

# $1       commit message


gump() {
	# Variables
	branch=$(git rev-parse --abbrev-ref HEAD)
	main=$(git branch -vv | grep -Po "^\s*\K[^\s]*(?=.*$(git branch -r | grep -Po "HEAD -> \K.*$").*)")
	msg="$1"
	lnx_scripts_dir="$XDG_GIT_DIR/lnx_scripts"

	# Source `gup` and `gbd` functions
	source "$XDG_GIT_DIR/lnx_scripts/bash_functions/gup.sh"
	source "$XDG_GIT_DIR/lnx_scripts/bash_functions/gbd.sh"

	gup "$msg" && git checkout "$main" && git merge --squash "$branch" && gup "$msg" && git push && gbd $branch
}