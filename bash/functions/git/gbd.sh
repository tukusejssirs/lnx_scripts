#!/bin/bash

# Remove the specified branch locally and remotely

# $1       existing branch name


gbd() {
	git branch --delete "$1"
	git push origin --delete "$1" &>/dev/null || git fetch -p
}