#!/bin/bash

# Show git log with full commit hashes, commit dates and only subject (first line) of commit messages


gl1d() {
	git log --date=format-local:%c --format='%C(auto)%H  %<(29)%C(brightmagenta)%cd%Creset  %s%d' "$@"
}