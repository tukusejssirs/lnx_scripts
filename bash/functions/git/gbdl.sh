#!/bin/bash

# Remove the specified branch locally

# $1       existing branch name


gbdl() {
	git branch --delete "$1"
}