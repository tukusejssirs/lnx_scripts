#!/bin/bash

# This script checks if there is something to commit; if yes, commit the changes with commit message that is supplied as argument. Finally, it makes a GPG-signed tag, using the default e-mail address’s key and use the given tag message.

# $1       commit message
# $2       tag name
# $3       tag message

# TODO
# - when no msg is input as arg and ans is no, it won't ask me to input msg


gupt() {
	# Check if there are any changes in local git repo since last commit
	# src: https://stackoverflow.com/a/5143914/3408342
	if [[ ! $(git diff-index --quiet HEAD --) ]]; then
		# Local repo changed
		if [[ -n "$1" ]]; then
			git add --all
			git commit -m "$(echo -e \"$1\" | sed 's/^"//;s/"$//' -)"
			git tag -s $2 -m "$(echo -e \"$3\" | sed 's/^"//;s/"$//' -)"
			git push --follow-tags
		else
			echo "You must supply 3 arguments."
			exit 1
		fi
	else
		# Local repo did not changed
		echo "There were have been no changes made in the repo."
	fi
}