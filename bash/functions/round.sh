#!/bin/bash

# This function round a floating number accourding to IEEE 754 – Rounding to nearest, ties away from zero

# $1 is precision
# $2 is floating number to round


round() {
	LC_ALL=C $(which printf) "%.*f\n" $1 $2
}