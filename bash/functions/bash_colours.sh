#!/bin/bash

# Format and colours variables definition

# Note: Not every terminal emulator supports all of these definitions.

# terminal colour definitions
# ===========================
#
# name     : hex    : rrr ggg bbb
# ---------:--------:-------------
# black    : 000000 :   0   0   0
# dgrey    : 555555 :  85  85  85
# red      : aa0000 : 170   0   0
# green    : 00aa00 :   0 170   0
# yellow   : aa5500 : 170  85   0
# blue     : 030537 :   3   5  55
# magenta  : aa00aa : 170   0 170
# cyan     : 00aaaa :   0 170 170
# lgrey    : aaaaaa : 170 170 170
# lred     : ff5555 : 255  85  85
# lgreen   : 55ff55 :  85 255  85
# lyellow  : ffff55 : 255 255  85
# lblue    : 5555ff :  85  85 255
# lmagenta : ff55ff : 255  85 255
# lcyan    : 55ffff :  85 255 255
# white    : ffffff : 255 255 255


# Format
export bold='\e[1m'  # This is either bold (if supported) or bright in colour
export dim='\e[2m'
export italics='\e[3m'
export underline='\e[4m'
export blink='\e[5m'
export overline='\e[6m'
export invert='\e[7m'  # Invert the foreground and background colours
export hide='\e[8m'  # Change foreground to background colour
export strike='\e[9m'  # Strikethrough

# Format and colour reseting
export unbold='\e[21m'
export undim='\e[22m'
export unitalics='\e[23m'
export ununderline='\e[24m'
export unblink='\e[25m'
export unoverline='\e26m'
export uninvert='\e[27m'
export unhide='\e[28m'
export unstrike='\e[9m'
export normal='\e[0'  # Normal format (i.e reset all manually set format)
export fdefault='\e[39m'  # Default foreground colour
export bdefault='\e[49m'  # Default background colour

# Foreground colours (8/16)
export black='\e[30m'
export red='\e[31m'
export green='\e[32m'
export yellow='\e[33m'
export blue='\e[34m'
export magenta='\e[35m'
export cyan='\e[36m'
export lgrey='\e[37m'
export dgrey='\e[90m'
export lred='\e[91m'
export lgreen='\e[92m'
export lyellow='\e[93m'
export lblue='\e[94m'
export lmagenta='\e[95m'
export lcyan='\e[96m'
export white='\e[97m'

# Background colours (8/16)
export bblack='\e[40m'
export bred='\e[41m'
export bgreen='\e[42m'
export byellow='\e[43m'
export bblue='\e[44m'
export bmagenta='\e[45m'
export bcyan='\e[46m'
export blgrey='\e[47m'
export bdgrey='\e[100m'
export blred='\e[101m'
export blgreen='\e[102m'
export blyellow='\e[103m'
export blblue='\e[104m'
export blmagenta='\e[105m'
export blcyan='\e[106m'
export bwhite='\e[107m'