#!/bin/bash

# Add path(s) to PATH


# dependencies: (GNU) grep, realpath

# Usage: add_to_path [ absolute_path | relative_path | path glob ] [ ... ]

# Known bugs and limitations:
# - Currently, paths cannot contain a whitespace character.
# - Globs can be used, but at least the glob characters must be single-quoted.
# - Return code is for the last given path/argument.

# Return codes:
# 0        success
# 1        realpath error
# 2        one or more paths does not exist or is not a folder
# 3        one or more paths are already in the PATH


add_to_path() {
	# Get all paths (including those from globs)
	local PATHS_TO_ADD="$(echo $@)"

	# Loop through the paths
	for PATH_TO_ADD in $PATHS_TO_ADD; do
		# Get absolute path
		local PATH_TO_ADD_ABSOLUTE="$(/bin/realpath -q "$PATH_TO_ADD")"
		# Get `realpath` return code
		local PATH_TO_ADD_ABSOLUTE_RETVAL=$?

		# Check if `realpath` was executed successfully
		if [ $PATH_TO_ADD_ABSOLUTE_RETVAL = 0 ]; then
			# Check if the path exists
			if [ -d "$PATH_TO_ADD_ABSOLUTE" ]; then
				# Check if the path to add is already in PATH
				local TEST_PATH=$(/bin/grep -c "^$PATH_TO_ADD_ABSOLUTE:\|:$PATH_TO_ADD_ABSOLUTE:\|:$PATH_TO_ADD_ABSOLUTE$" <<< "$PATH")

				if [ "$TEST_PATH" = 0 ]; then  # If the path is not in the PATH
					# Check if PATH is already defined and not empty
					# Note: The PATH should be always defined, but I keep this here just in case
					if [ "$PATH" ]; then
						PATH="$PATH:$PATH_TO_ADD_ABSOLUTE"
					else
						PATH="$PATH_TO_ADD_ABSOLUTE"
					fi

					local RETVAL=0
				else
					local RETVAL=3
				fi
			else
				local RETVAL=2
			fi

		else
			local RETVAL=1
		fi
	done

	return $RETVAL
}