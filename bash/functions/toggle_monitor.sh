#!/bin/bash

# Toggle turn on/off the specified monitor in Sway VM

# $1 = monitor name in `swaymsg -t get_outputs`


toggle_monitor() {
	local MON="$1"
	local IS_ACTIVE
	IS_ACTIVE="$(swaymsg -t get_outputs | jq --arg MON "$MON" '.[] | select(.name == $MON) | .active')"

	if [ "$IS_ACTIVE" = 'true' ]; then
		swaymsg output "$MON" disable
	else
		swaymsg output "$MON" enable
	fi
}