#!/bin/bash

# Prepare Arch Linux USB

# Dependencies: aria2, GNU grep, CURL, transmission-cli, transmission-daemon


# Variables
TMP='/dev/shm/arch_iso'
URL='https://www.archlinux.org/download'

# Get latest Arch Linux release magnetic link
MAGNET_LINK="$(curl -s "${URL}/" | grep -o 'magnet:[^"]*')"

# Download the torrent
npm install webtorrent-cli --prefix "$TMP"
node "$TMP/node_modules/webtorrent-cli/bin/cmd.js" download "$MAGNET_LINK" -o "$TMP"
TEST_TORRENT=$?

if [ "$TEST_TORRENT" != 0 ]; then
	echo 'ERROR: Something went wrong during downloading the torrent.' 1>&2
fi

# Mofify the ISO to add a boot parameter and a script to run (install and configure Arch Linux)
# TODO