#!/bin/bash

# Multi-input using numbers


multiinput() {
	local quit='false'
	local options=(
		'ControlSERVER type'
		'Domain name'
		'Zabbix sender email address'
		'VMware network type'
		'VMware network IP address'
		'VMware network subnet mask'
		'VMware network gateway'
		'VMware network DNS 1'
		'VMware network DNS 2'
		'Internal network type'
		'Internal network IP address'
		'Internal network subnet mask'
		'Internal network gateway'
		'Internal network DNS 1'
		'Internal network DNS 2'
		'Machines network type'
		'Machines network IP address'
		'Machines network subnet mask'
		'Machines network gateway'
		'Machines network DNS 1'
		'Machines network DNS 2'
	)

	# Default values
	local list_cs_types=('prod' 'demo' 'dev' 'promo')
	local list_network_types=('dhcp' 'static')
	local cs_type='prod'
	local domain_name='controlserver.lan'
	local zabbix_admin_notif_email_send_from="controlserver@$domain_name"
	local network_vmware_type='dhcp'
	local network_vmware_ip=''
	local network_vmware_subnet_mask=''
	local network_vmware_gateway=''
	local network_vmware_dns_1=''
	local network_vmware_dns_2=''
	local network_internal_type='dhcp'
	local network_internal_ip=''
	local network_internal_subnet_mask=''
	local network_internal_gateway=''
	local network_internal_dns_1=''
	local network_internal_dns_2=''
	local network_machines_type='static'
	local network_machines_ip='10.14.7.1'
	local network_machines_subnet_mask='255.255.255.0'
	local network_machines_gateway=''
	local network_machines_dns_1=''
	local network_machines_dns_2=''

	while [ "$quit" = 'false' ]; do
		print_config "${options[@]}" '.' "$cs_type" "$domain_name" "$zabbix_admin_notif_email_send_from" "$network_vmware_type" "$network_vmware_ip" "$network_vmware_subnet_mask" "$network_vmware_gateway" "$network_vmware_dns_1" "$network_vmware_dns_2" "$network_internal_type" "$network_internal_ip" "$network_internal_subnet_mask" "$network_internal_gateway" "$network_internal_dns_1" "$network_internal_dns_2" "$network_machines_type" "$network_machines_ip" "$network_machines_subnet_mask" "$network_machines_gateway" "$network_machines_dns_1" "$network_machines_dns_2"

		# Read the input
		read -rn2 -p 'Choice: ' ans
		echo -e '\n'

		# Process the input
		case "$ans" in
			1)  # ControlSERVER type
				local value_set='false'
				print_values "${list_cs_types[@]}"

				while [ "$value_set" = 'false' ]; do
					read -rn1 -p "${options[((ans-1))]}: " value
					echo -e '\n'

					case "$value" in
						[1234])
							cs_type="${list_cs_types[((value-1))]}"
							value_set='true'
						;;
						c)
							value_set='true'
						;;
						*)
							echo '[WARNING] Invalid choice.' 1>&2
					esac
				done
			;;
			2)  # Domain name
				while
					read -rp "${options[((ans-1))]}: " domain_name
				do
					if ! validate 'domain' "$domain_name"; then
						echo '[WARNING] Invalid value.' 1>&2
					else
						zabbix_admin_notif_email_send_from="controlserver@$domain_name"
						break
					fi
				done
			;;
			3)  # Zabbix sender email address
				while
					read -rp "${options[((ans-1))]}: " zabbix_admin_notif_email_send_from
				do
					if ! validate 'email' "$zabbix_admin_notif_email_send_from"; then
						echo '[WARNING] Invalid value.' 1>&2
					else
						break
					fi
				done
			;;
			4)  # VMware network type
				local value_set='false'
				print_values "${list_network_types[@]}"

				while [ "$value_set" = 'false' ]; do
					read -rn1 -p "${options[((ans-1))]}: " value
					echo -e '\n'

					case "$value" in
						[12])
							network_vmware_type="${list_network_types[((value-1))]}"

							if [ "$network_vmware_type" = 'dhcp' ]; then
								network_vmware_ip=''
								network_vmware_subnet_mask=''
								network_vmware_gateway=''
								network_vmware_dns_1=''
								network_vmware_dns_2=''
							fi

							value_set='true'
						;;
						c)
							value_set='true'
						;;
						*)
							echo '[WARNING] Invalid choice.' 1>&2
					esac
				done
			;;
			5)  # VMware network IP address
				if [ "$network_vmware_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_vmware_ip
					do
						if ! validate 'ip' "$network_vmware_ip"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the VMware network type to static before configuring the IP address.\n' 1>&2
				fi
			;;
			6)  # VMware network subnet mask
				if [ "$network_vmware_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_vmware_subnet_mask
					do
						if ! validate 'ip' "$network_vmware_subnet_mask"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the VMware network type to static before configuring the subnet mask.\n' 1>&2
				fi
			;;
			7)  # VMware network gateway
				if [ "$network_vmware_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_vmware_gateway
					do
						if ! validate 'ip' "$network_vmware_gateway"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the VMware network type to static before configuring the gateway.\n' 1>&2
				fi
			;;
			8)  # VMware network DNS 1
				if [ "$network_vmware_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_vmware_dns_1
					do
						if ! validate 'ip' "$network_vmware_dns_1"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the VMware network type to static before configuring the DNS address.\n' 1>&2
				fi
			;;
			9)  # VMware network DNS 2
				if [ "$network_vmware_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_vmware_dns_2
					do
						if ! validate 'ip' "$network_vmware_dns_2"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the VMware network type to static before configuring the DNS address.\n' 1>&2
				fi
			;;
			10)  # Internal network type
				local value_set='false'
				print_values "${list_network_types[@]}"

				while [ "$value_set" = 'false' ]; do
					read -rn1 -p "${options[((ans-1))]}: " value
					echo -e '\n'

					case "$value" in
						[12])
							network_internal_type="${list_network_types[((value-1))]}"

							if [ "$network_internal_type" = 'dhcp' ]; then
								network_internal_ip=''
								network_internal_subnet_mask=''
								network_internal_gateway=''
								network_internal_dns_1=''
								network_internal_dns_2=''
							fi

							value_set='true'
						;;
						c)
							value_set='true'
						;;
						*)
							echo '[WARNING] Invalid choice.' 1>&2
					esac
				done
			;;
			11)  # Internal network IP address
				if [ "$network_internal_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_internal_ip
					do
						if ! validate 'ip' "$network_internal_ip"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the internal network type to static before configuring the IP address.\n' 1>&2
				fi
			;;
			12)  # Internal network subnet mask
				if [ "$network_internal_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_internal_subnet_mask
					do
						if ! validate 'ip' "$network_internal_subnet_mask"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the internal network type to static before configuring the subnet mask.\n' 1>&2
				fi
			;;
			13)  # Internal network gateway
				if [ "$network_internal_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_internal_gateway
					do
						if ! validate 'ip' "$network_internal_gateway"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the internal network type to static before configuring the gateway.\n' 1>&2
				fi
			;;
			14)  # Internal network DNS 1
				if [ "$network_internal_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_internal_dns_1
					do
						if ! validate 'ip' "$network_internal_dns_1"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the internal network type to static before configuring the DNS address.\n' 1>&2
				fi
			;;
			15)  # Internal network DNS 2
				if [ "$network_internal_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_internal_dns_2
					do
						if ! validate 'ip' "$network_internal_dns_2"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the internal network type to static before configuring the DNS address.\n' 1>&2
				fi
			;;
			16)  # Machines network type
				local value_set='false'
				print_values "${list_network_types[@]}"

				while [ "$value_set" = 'false' ]; do
					read -rn1 -p "${options[((ans-1))]}: " value
					echo -e '\n'

					case "$value" in
						[12])
							network_machines_type="${list_network_types[((value-1))]}"

							if [ "$network_machines_type" = 'dhcp' ]; then
								network_machines_ip=''
								network_machines_subnet_mask=''
								network_machines_gateway=''
								network_machines_dns_1=''
								network_machines_dns_2=''
							fi

							value_set='true'
						;;
						c)
							value_set='true'
						;;
						*)
							echo '[WARNING] Invalid choice.' 1>&2
					esac
				done
			;;
			17)  # Machines network IP address
				if [ "$network_machines_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_machines_ip
					do
						if ! validate 'ip' "$network_machines_ip"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the machines network type to static before configuring the IP address.\n' 1>&2
				fi
			;;
			18)  # Machines network subnet mask
				if [ "$network_machines_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_machines_subnet_mask
					do
						if ! validate 'ip' "$network_machines_subnet_mask"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the machines network type to static before configuring the subnet mask.\n' 1>&2
				fi
			;;
			19)  # Machines network gateway
				if [ "$network_machines_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_machines_gateway
					do
						if ! validate 'ip' "$network_machines_gateway"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the machines network type to static before configuring the gateway.\n' 1>&2
				fi
			;;
			20)  # Machines network DNS 1
				if [ "$network_machines_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_machines_dns_1
					do
						if ! validate 'ip' "$network_machines_dns_1"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the machines network type to static before configuring the DNS address.\n' 1>&2
				fi
			;;
			21)  # Machines network DNS 2
				if [ "$network_machines_type" = 'static' ]; then
					while
						read -rp "${options[((ans-1))]}: " network_machines_dns_2
					do
						if ! validate 'ip' "$network_machines_dns_2"; then
							echo '[WARNING] Invalid value.' 1>&2
						else
							break
						fi
					done
				else
					echo -e '[WARNING] You need to set the machines network type to static before configuring the DNS address.\n' 1>&2
				fi
			;;
			'l')
				local url
				read -rp "Enter URL: " url
				eval "$(load_config "$url")"
			;;
			'c')
				quit='true'

				printf "%s='%s'\\n" "cs_type" "$cs_type" \
					"domain_name" "$domain_name" \
					"zabbix_admin_notif_email_send_from" "$zabbix_admin_notif_email_send_from" \
					"network_vmware_type" "$network_vmware_type" \
					"network_vmware_ip" "$network_vmware_ip" \
					"network_vmware_subnet_mask" "$network_vmware_subnet_mask" \
					"network_vmware_gateway" "$network_vmware_gateway" \
					"network_vmware_dns_1" "$network_vmware_dns_1" \
					"network_vmware_dns_2" "$network_vmware_dns_2" \
					"network_internal_type" "$network_internal_type" \
					"network_internal_ip" "$network_internal_ip" \
					"network_internal_subnet_mask" "$network_internal_subnet_mask" \
					"network_internal_gateway" "$network_internal_gateway" \
					"network_internal_dns_1" "$network_internal_dns_1" \
					"network_internal_dns_2" "$network_internal_dns_2" \
					"network_machines_type" "$network_machines_type" \
					"network_machines_ip" "$network_machines_ip" \
					"network_machines_subnet_mask" "$network_machines_subnet_mask" \
					"network_machines_gateway" "$network_machines_gateway" \
					"network_machines_dns_1" "$network_machines_dns_1" \
					"network_machines_dns_2" "$network_machines_dns_2" > cs_config.ini
			;;
			*)
				echo '[WARNING] Invalid choice.' 1>&2
		esac
	done
}

print_config() {
	local intro_text='Choose one of the folowing options:'
	local load_text='Load configuration from URL'
	local exit_option='Confirm the configuration and continue with installation'
	local options=()
	local i=1
	local output=''

	# Get option names
	while [ "$1" != '.' ]; do
		options+=("$1")
		shift
	done

	# Remove '.' parameter
	shift

	# Get option values
	while [ "${1+x}" ]; do
		output="$(printf '%s\n[%2d]:%s:= "%s"\n' "$output" "$i" "${options[((i-1))]}" "$1")"
		shift
		((i++))
	done

	# Format the output
	output="$(printf '%s' "$output" | column -ts:)"

	# Prepend the intro text, load, and append exit option
	output="$(printf '%s\n\n%s\n\n[ l]  %s\n[ c]  %s\n' "$intro_text" "$output" "$load_text" "$exit_option")"

	# Output
	printf '%s\n\n' "$output"
}

print_values() {
	local exit_option='Go back to main menu without changing the value'
	local options=()
	local i=1
	local output=''

	# Get list of values
	while [ "${1+x}" ]; do
		output="$(printf '%s\n[%2d]:%s\n' "$output" "$i" "$1")"
		shift
		((i++))
	done

	# Format the output
	output="$(printf '%s' "$output" | column -ts:)"

	# Prepend the intro text and append exit option
	output="$(printf '%s\n\n[ c]  %s\n' "$output" "$exit_option")"

	# Output
	printf '%s\n\n' "$output"
}

validate() {
	local type="$1"
	local value="$2"

	case "$type" in
		'email')
			local email_regex='^(?:[a-z0-9!#$%&'\''*+/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&'\''*+/=?^_\`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$'

			if grep -vqP "$email_regex" <<< "$value"; then
				return 1
			fi
		;;
		'domain')
			local domain_regex='^(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$'

			if grep -vqP "$domain_regex" <<< "$value"; then
				return 1
			fi
		;;
		'ip')
			local ip_regex='^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\.(?!$)|$)){4}$'

			if grep -vqP "$ip_regex" <<< "$value"; then
				return 1
			fi
		;;
		*)
			return 2
	esac
}

load_config() {
	local url="$1"
	local config=''
	local output=''
	local count=0
	local list_cs_types=('prod' 'demo' 'dev' 'promo')  # TODO: This line is a duplicate to the definition in the multiinput() function.
	local list_network_types=('dhcp' 'static')  # TODO: This line is a duplicate to the definition in the multiinput() function.
	local cs_type=''
	local domain_name=''
	local zabbix_admin_notif_email_send_from=''
	local network_vmware_type=''
	local network_vmware_ip=''
	local network_vmware_subnet_mask=''
	local network_vmware_gateway=''
	local network_vmware_dns_1=''
	local network_vmware_dns_2=''
	local network_internal_type=''
	local network_internal_ip=''
	local network_internal_subnet_mask=''
	local network_internal_gateway=''
	local network_internal_dns_1=''
	local network_internal_dns_2=''
	local network_machines_type=''
	local network_machines_ip=''
	local network_machines_subnet_mask=''
	local network_machines_gateway=''
	local network_machines_dns_1=''
	local network_machines_dns_2=''

	if ! config="$(curl -Ls "$url")" || [ "$config" = '' ]; then
		echo '[WARNING] Could not download the configuration.' 1>&2
		return 1
	fi

	if cs_type="$(grep -Po "^cs_type=['\"]*\K[^'\"]+" <<< "$config")"; then
		if grep -qw "$cs_type" <<< "${list_cs_types[*]}"; then
			output="cs_type='$cs_type'"
			((count++))
		else
			echo "[WARNING] Invalid ControlSERVER type value: cs_type='$cs_type'" 1>&2
		fi
	fi

	if domain_name="$(grep -Po "^domain_name=['\"]*\K[^'\"]+" <<< "$config")"; then
		if validate 'domain' "$domain_name"; then
			output="$output domain_name='$domain_name'"
			((count++))
		else
			echo "[WARNING] Invalid domain name value: cs_type='$cs_type'" 1>&2
		fi
	fi

	if zabbix_admin_notif_email_send_from="$(grep -Po "^zabbix_admin_notif_email_send_from=['\"]*\K[^'\"]+" <<< "$config")"; then
		if validate 'email' "$zabbix_admin_notif_email_send_from"; then
			output="$output zabbix_admin_notif_email_send_from='$zabbix_admin_notif_email_send_from'"
			((count++))
		else
			echo "[WARNING] Invalid Zabbix sender email address value: zabbix_admin_notif_email_send_from='$zabbix_admin_notif_email_send_from'" 1>&2
		fi
	fi

	if network_vmware_type="$(grep -Po "^network_vmware_type=['\"]*\K[^'\"]+" <<< "$config")"; then
		if grep -qw "$network_vmware_type" <<< "${list_network_types[*]}"; then
			output="$output network_vmware_type='$network_vmware_type'"
			((count++))
		else
			if [ "$network_vmware_type" != '' ]; then
				echo "[WARNING] Invalid network type value: network_vmware_type='$network_vmware_type'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_vmware_type='dhcp'"
		fi
	fi

	if network_vmware_ip="$(grep -Po "^network_vmware_ip=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_vmware_type" = 'static' ]; then
		if validate 'ip' "$network_vmware_ip"; then
			output="$output network_vmware_ip='$network_vmware_ip'"
			((count++))
		else
			if [ "$network_vmware_ip" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_vmware_ip='$network_vmware_ip'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_vmware_ip=''"
		fi
	else
		if [ "$network_vmware_ip" != '' ] && [ "$network_vmware_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the VMware network type to static before configuring the IP address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_vmware_ip=''"
	fi

	if network_vmware_subnet_mask="$(grep -Po "^network_vmware_subnet_mask=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_vmware_type" = 'static' ]; then
		if validate 'ip' "$network_vmware_subnet_mask"; then
			output="$output network_vmware_subnet_mask='$network_vmware_subnet_mask'"
			((count++))
		else
			if [ "$network_vmware_subnet_mask" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_vmware_subnet_mask='$network_vmware_subnet_mask'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_vmware_subnet_mask=''"
		fi
	else
		if [ "$network_vmware_subnet_mask" != '' ] && [ "$network_vmware_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the VMware network type to static before configuring the subnet mask.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_vmware_subnet_mask=''"
	fi

	if network_vmware_gateway="$(grep -Po "^network_vmware_gateway=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_vmware_type" = 'static' ]; then
		if validate 'ip' "$network_vmware_gateway"; then
			output="$output network_vmware_gateway='$network_vmware_gateway'"
			((count++))
		else
			if [ "$network_vmware_gateway" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_vmware_gateway='$network_vmware_gateway'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_vmware_gateway=''"
		fi
	else
		if [ "$network_vmware_gateway" != '' ] && [ "$network_vmware_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the VMware network type to static before configuring the gateway.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_vmware_gateway=''"
	fi

	if network_vmware_dns_1="$(grep -Po "^network_vmware_dns_1=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_vmware_type" = 'static' ]; then
		if validate 'ip' "$network_vmware_dns_1"; then
			output="$output network_vmware_dns_1='$network_vmware_dns_1'"
			((count++))
		else
			if [ "$network_vmware_dns_1" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_vmware_dns_1='$network_vmware_dns_1'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_vmware_dns_1='$network_vmware_dns_1'"
		fi
	else
		if [ "$network_vmware_dns_1" != '' ] && [ "$network_vmware_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the VMware network type to static before configuring the DNS address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_vmware_dns_1=''"
	fi

	if network_vmware_dns_2="$(grep -Po "^network_vmware_dns_2=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_vmware_type" = 'static' ]; then
		if validate 'ip' "$network_vmware_dns_2"; then
			output="$output network_vmware_dns_2='$network_vmware_dns_2'"
			((count++))
		else
			if [ "$network_vmware_dns_2" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_vmware_dns_2='$network_vmware_dns_2'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_vmware_dns_2='$network_vmware_dns_2'"
		fi
	else
		if [ "$network_vmware_dns_2" != '' ] && [ "$network_vmware_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the VMware network type to static before configuring the DNS address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_vmware_dns_2=''"
	fi

	if network_internal_type="$(grep -Po "^network_internal_type=['\"]*\K[^'\"]+" <<< "$config")"; then
		if grep -qw "$network_internal_type" <<< "${list_network_types[*]}"; then
			output="$output network_internal_type='$network_internal_type'"
			((count++))
		else
			if [ "$network_internal_type" != '' ]; then
				echo "[WARNING] Invalid network type value: network_internal_type='$network_internal_type'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_internal_type='$network_internal_type'"
		fi
	fi

	if network_internal_ip="$(grep -Po "^network_internal_ip=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_internal_type" = 'static' ]; then
		if validate 'ip' "$network_internal_ip"; then
			output="$output network_internal_ip='$network_internal_ip'"
			((count++))
		else
			if [ "$network_internal_ip" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_internal_ip='$network_internal_ip'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_internal_ip=''"
		fi
	else
		if [ "$network_internal_ip" != '' ] && [ "$network_internal_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the internal network type to static before configuring the IP address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_internal_ip=''"
	fi

	if network_internal_subnet_mask="$(grep -Po "^network_internal_subnet_mask=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_internal_type" = 'static' ]; then
		if validate 'ip' "$network_internal_subnet_mask"; then
			output="$output network_internal_subnet_mask='$network_internal_subnet_mask'"
			((count++))
		else
			if [ "$network_internal_subnet_mask" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_internal_subnet_mask='$network_internal_subnet_mask'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_internal_subnet_mask=''"
		fi
	else
		if [ "$network_internal_subnet_mask" != '' ] && [ "$network_internal_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the internal network type to static before configuring the subnet mask.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_internal_subnet_mask=''"
	fi

	if network_internal_gateway="$(grep -Po "^network_internal_gateway=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_internal_type" = 'static' ]; then
		if validate 'ip' "$network_internal_gateway"; then
			output="$output network_internal_gateway='$network_internal_gateway'"
			((count++))
		else
			if [ "$network_internal_gateway" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_internal_gateway='$network_internal_gateway'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_internal_gateway=''"
		fi
	else
		if [ "$network_internal_gateway" != '' ] && [ "$network_internal_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the internal network type to static before configuring the gateway.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_internal_gateway=''"
	fi

	if network_internal_dns_1="$(grep -Po "^network_internal_dns_1=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_internal_type" = 'static' ]; then
		if validate 'ip' "$network_internal_dns_1"; then
			output="$output network_internal_dns_1='$network_internal_dns_1'"
			((count++))
		else
			if [ "$network_internal_dns_1" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_internal_dns_1='$network_internal_dns_1'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_internal_dns_1='$network_internal_dns_1'"
		fi
	else
		if [ "$network_internal_dns_1" != '' ] && [ "$network_internal_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the internal network type to static before configuring the DNS address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_internal_dns_1=''"
	fi

	if network_internal_dns_2="$(grep -Po "^network_internal_dns_2=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_internal_type" = 'static' ]; then
		if validate 'ip' "$network_internal_dns_2"; then
			output="$output network_internal_dns_2='$network_internal_dns_2'"
			((count++))
		else
			if [ "$network_internal_dns_2" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_internal_dns_2='$network_internal_dns_2'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_internal_dns_2='$network_internal_dns_2'"
		fi
	else
		if [ "$network_internal_dns_2" != '' ] && [ "$network_internal_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the internal network type to static before configuring the DNS address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_internal_dns_2=''"
	fi

	if network_machines_type="$(grep -Po "^network_machines_type=['\"]*\K[^'\"]+" <<< "$config")"; then
		if grep -qw "$network_machines_type" <<< "${list_network_types[*]}"; then
			output="$output network_machines_type='$network_machines_type'"
			((count++))
		else
			if [ "$network_machines_type" != '' ]; then
				echo "[WARNING] Invalid network type value: network_machines_type='$network_machines_type'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_machines_type='$network_machines_type'"
		fi
	fi

	if network_machines_ip="$(grep -Po "^network_machines_ip=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_machines_type" = 'static' ]; then
		if validate 'ip' "$network_machines_ip"; then
			output="$output network_machines_ip='$network_machines_ip'"
			((count++))
		else
			if [ "$network_machines_ip" = '' ] && [ "$network_machines_type" = 'dhcp' ]; then
				echo "[WARNING] Invalid IP address value: network_machines_ip='$network_machines_ip'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_machines_ip=''"
		fi
	else
		if [ "$network_machines_ip" != '' ] && [ "$network_machines_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the machines network type to static before configuring the IP address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_machines_ip=''"
	fi

	if network_machines_subnet_mask="$(grep -Po "^network_machines_subnet_mask=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_machines_type" = 'static' ]; then
		if validate 'ip' "$network_machines_subnet_mask"; then
			output="$output network_machines_subnet_mask='$network_machines_subnet_mask'"
			((count++))
		else
			if [ "$network_machines_subnet_mask" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_machines_subnet_mask='$network_machines_subnet_mask'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_machines_subnet_mask=''"
		fi
	else
		if [ "$network_machines_subnet_mask" != '' ] && [ "$network_machines_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the machines network type to static before configuring the subnet mask.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_machines_subnet_mask=''"
	fi

	if network_machines_gateway="$(grep -Po "^network_machines_gateway=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_machines_type" = 'static' ]; then
		if validate 'ip' "$network_machines_gateway"; then
			output="$output network_machines_gateway='$network_machines_gateway'"
			((count++))
		else
			if [ "$network_machines_gateway" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_machines_gateway='$network_machines_gateway'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_machines_gateway=''"
		fi
	else
		if [ "$network_machines_gateway" != '' ] && [ "$network_machines_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the machines network type to static before configuring the gateway.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_machines_gateway=''"
	fi

	if network_machines_dns_1="$(grep -Po "^network_machines_dns_1=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_machines_type" = 'static' ]; then
		if validate 'ip' "$network_machines_dns_1"; then
			output="$output network_machines_dns_1='$network_machines_dns_1'"
			((count++))
		else
			if [ "$network_machines_dns_1" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_machines_dns_1='$network_machines_dns_1'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_machines_dns_1='$network_machines_dns_1'"
		fi
	else
		if [ "$network_machines_dns_1" != '' ] && [ "$network_machines_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the machines network type to static before configuring the DNS address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_machines_dns_1=''"
	fi

	if network_machines_dns_2="$(grep -Po "^network_machines_dns_2=['\"]*\K[^'\"]+" <<< "$config")" && [ "$network_machines_type" = 'static' ]; then
		if validate 'ip' "$network_machines_dns_2"; then
			output="$output network_machines_dns_2='$network_machines_dns_2'"
			((count++))
		else
			if [ "$network_machines_dns_2" != '' ]; then
				echo "[WARNING] Invalid IP address value: network_machines_dns_2='$network_machines_dns_2'" 1>&2
				echo '[INFO] Assuming DHCP.' 1>&2
			fi

			output="$output network_machines_dns_2='$network_machines_dns_2'"
		fi
	else
		if [ "$network_machines_dns_2" != '' ] && [ "$network_machines_type" = 'dhcp' ]; then
			echo -e '[WARNING] You need to set the machines network type to static before configuring the DNS address.\n' 1>&2
			echo -e '[INFO] Assigning empty value.' 1>&2
		fi

		output="$output network_machines_dns_2=''"
	fi

	if [ "$count" = 0 ]; then
		echo '[WARNING] Could not load any variable from the configuration. It looks like it is in invalid format, see below.' 1>&2
		echo "$config" 1>&2
		return 2
	fi

	echo "$output"
	return 0
}

multiinput