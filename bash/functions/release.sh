#!/bin/bash

# Create a new release using data from changelog of the current repo in the currently checked out branch

# TODO:
# - raise an error (and don't make a new release, not even a dry run) if:
# 	- any data is staged to be committed;
# 	- there are no changes in the first row of the changelog table in `changelog.md`;
# - add custom path to Git folder (in order to use `-C` option of `git` command; currently, I need to be within the repository, although not necessarily in the repo root folder);
# - add custom major/minor/patch version number override;
# - add an option to provide previous version;
# - define `VER_LAST_*` only once unless we need a different definition (like when using user-defined VER_PREFIX);
# - add custom changelog path and name option;
# - add `-f` option to force push the commits to the remote;


##
## @brief      Create a new release using data from changelog of the current repo in the currently checked out branch
##
## @param      $1    Release type [major | minor | patch]
##
## @return     Return code number
##
release() {
	##
	## @brief      Output help message
	##
	short_help() {
		echo -e 'Usage'
		echo -e '\trelease [--staged | -g] [--type TYPE | -t TYPE] [--dev DEV_SUFFIX | -d DEV_SUFFIX] [--dry-run | -s] version_type>'
		echo -e '\trelease [--help | -h]'
		echo
		echo -e 'Description'
		echo -e '\tCreate a new release using data from changelog of the current repo in the currently checked out branch.'
		echo
		echo -e 'Note that no option is required to release a new version, however the version_type (the only parameter without option) is required when releasing a new semver version. It can be one of major, minor of patch. Note that it is ignored for ` releases.'
		echo
		echo -e 'Options'
		echo -e '\tMandatory arguments to long options are mandatory for short options too.'
		echo
		echo -e '\t-e, --dev=DEV_SUFFIX'
		echo -e '\t\trelease a dev version (appends -DEV_SUFFIX.NUMBER)'
		echo
		echo -e '\t\tSupported DEV_SUFFIX values: alpha, beta, canary, dev, pre, rc.'
		echo
		echo -e '\t-g, --staged'
		echo -e '\t\tcommit staged files only'
		echo
		echo -e '\t\tBy default, all files are committed.'
		echo
		echo -e '\t-m, --message=MESSAGE'
		echo -e '\t\tuse the provided commit message'
		echo
		# shellcheck disable=SC2016  # Expressions don't expand in single quotes, use double quotes for that.
		echo -e '\t\tNote that the provided message will be prepended with "Release $VERSION\\n\\n"'
		echo
		echo -e '\t-s, --dry-run'
		echo -e '\t\tgenerate and output the commit message to STDOUT and exit (nothing is done with files and no release is made'
		echo
		echo -e '\t-h, --help'
		echo -e '\t\tdisplay this help and exit'
		echo
		echo -e '\t-t, --type=TYPE'
		echo -e '\t\tversion type'
		echo
		echo -e '\t\tIt can be either semver or date. semver version is in the format major.minor.patch (no leading zeros) and date version in the format year.month.number, where year is last two digits of the current year, month is two-digit month (with a leading zero when needed) and number is the release number of the month (starting with 1).'
		echo
		echo -e '\t\tBy default, we use semantic versioning (semver).'
		echo
		echo -e 'Examples'
		echo
		echo -e '\tNew major semver release (both commands do the same thing):'
		echo -e '\t\trelease major'
		echo -e '\t\trelease -t semver major'
		echo
		echo -e '\tNew date release:'
		echo -e "\t\trelease -t date"
		echo
		echo -e '\tDry run to simulate a new patch semver version (it outputs the version and commit message):'
		echo -e "\t\trelease -s patch"
		echo -e "\t\trelease -st semver patch"
		echo
		echo -e 'Exit/return codes'
		echo -e '\t0    Success'
		echo -e '\t1    Invalid options provided'
		echo -e '\t2    Invalid versioning type provided'
		echo -e '\t3    Too many parameters provided for SemVer'
		echo -e '\t4    The folder is not in a Git repository'
		echo -e '\t5    Cannot find any changelog file in the repository root folder'
		echo -e '\t6    More than one changelog file is found in the repository root folder'
		echo -e '\t7    Last version type is lexically after RELEASE TYPE'
		echo -e '\t8    Invalid dev version suffix provided'
		echo -e '\t9    No release type provided for SemVer'
		echo -e '\t10   There are no staged changes in the changelog file'
		echo -e '\t11   There are no changes in the changelog file'
	}

	# Variables
	local ADD_NUM CHANGELOG COMMIT_CHANGELOG COMMIT_MESSAGE DEV_NUMBER DEV_SUFFIX DRY_RUN GEN_COMMIT_MSG GETOPT_OPTIONS OTHER_OPTIONS REPO_ROOT STAGED_FILES_ONLY TODAY TYPE VER_LAST VER_LAST_DEV_SUFFIX VER_LAST_DEV_NUM VER_LAST_MAJOR VER_LAST_MINOR VER_LAST_PATCH VER_NEXT_MAJOR VER_NEXT_MINOR VER_NEXT_PATCH VERSION
	DRY_RUN='false'
	STAGED_FILES_ONLY='false'
	TYPE='semver'
	GEN_COMMIT_MSG='true'

	# Call getopt to validate the provided input
	if ! GETOPT_OPTIONS="$(getopt -o e:ghm:st: --long dev:,dry-run,help,message:,staged,type: -- "$@")"; then
		echo 'ERROR: Invalid options provided.' 1>&2
		return 1
	fi

	eval set -- "$GETOPT_OPTIONS"

	while true; do
		case "$1" in
			--dev|-e)
				shift
				DEV_SUFFIX="$1"

				if [ "$DEV_SUFFIX" != 'alpha' ] && [ "$DEV_SUFFIX" != 'beta' ] && [ "$DEV_SUFFIX" != 'canary' ] && [ "$DEV_SUFFIX" != 'dev' ] && [ "$DEV_SUFFIX" != 'pre' ] && [ "$DEV_SUFFIX" != 'rc' ]; then
					echo "ERROR: Invalid dev version suffix provided." 1>&2
					echo "ERROR: Valid dev version suffices are: alpha, beta, canary, dev, pre, rc." 1>&2
					return 8
				fi
			;;
			--dry-run|-s)
				DRY_RUN='true'
			;;
			--help|-h)
				short_help
				return 0
			;;
			--message|-m)
				shift

				if [ -n "$1" ]; then
					GEN_COMMIT_MSG='false'
					COMMIT_CHANGELOG="$1"
				else
					echo 'WARNING: Provided commit message is empty, generating commit message.' 1>&2
				fi
			;;
			--staged|-g)
				STAGED_FILES_ONLY='true'
			;;
			--type|-t)
				shift

				case "$1" in
					'semver'|'date')
						TYPE="$1"
					;;
					*)
						echo 'ERROR: Invalid versioning type provided.' 1>&2
						return 2
				esac
			;;
			--)
				shift
				OTHER_OPTIONS=("$@")
				break
			;;
		esac
		shift
	done

	if [ "${#OTHER_OPTIONS}" = 0 ] && [ "$TYPE" = 'semver' ]; then
		echo "ERROR: No release type provided for SemVer." 1>&2
		echo "ERROR: You need to provide one of the following: major, minor, patch." 1>&2
		return 9
	fi

	if \
		{ \
			[ "${#OTHER_OPTIONS}" = 1 ]; \
		} && { \
			[ "${OTHER_OPTIONS[0]}" = 'major' ] \
			|| [ "${OTHER_OPTIONS[0]}" = 'minor' ] \
			|| [ "${OTHER_OPTIONS[0]}" = 'patch' ]; \
		} || { \
			[ ! "${OTHER_OPTIONS[1]}" ]; \
		} && { \
			[ "${OTHER_OPTIONS[*]}" = 'major' ] \
			|| [ "${OTHER_OPTIONS[*]}" = 'minor' ] \
			|| [ "${OTHER_OPTIONS[*]}" = 'patch' ]; \
		}; then
		RELEASE_TYPE="${OTHER_OPTIONS[0]}"
	elif [ "$TYPE" = 'semver' ]; then
		echo "ERROR: Too many parameters provided for SemVer: ${OTHER_OPTIONS[*]}" 1>&2
		return 3
	fi

	# Check we are in a repo
	# TODO: Change the path
	if ! git -C . rev-parse &> /dev/null; then
		echo 'ERROR: The folder is not in a Git repository.' 1>&2
		return 4
	fi

	REPO_ROOT="$(git rev-parse --show-toplevel)"

	# Check if there is any changelog file
	# TODO: Change the path
	#
	# if ! git -C . rev-parse &> /dev/null; then
	# 	echo 'ERROR: The folder is not in a Git repository.' 1>&2
	# 	return 4
	# fi
	read -rd'\n' -a CHANGELOG_NAME <<< "$(find "$REPO_ROOT" -type f \( -iname 'changelog.md' -o -iname 'changelog.txt' -o -iname 'history.md' -o -iname 'history.txt' \) -exec sh -c 'for f do git check-ignore -q "$f" || basename "$f"; done' find-sh {} +)"

	if [ "${#CHANGELOG_NAME[@]}" -eq 0 ]; then
		echo 'ERROR: Cannot find any changelog file in the repository root folder.' 1>&2
		echo '       Only the following filenames are matched (all are match case insensitively):' 1>&2
		echo '       - changelog.md;' 1>&2
		echo '       - changelog.txt;' 1>&2
		echo '       - history.md;' 1>&2
		echo '       - history.txt.' 1>&2
		return 5
	elif [ "${#CHANGELOG_NAME[@]}" -gt 1 ]; then
		echo 'ERROR: More than one changelog file is found in the repository root folder.' 1>&2
		# TODO: When the changelog file name/path getopt option is implemented, add a note here to use that option
		return 6
	fi

	if [ "$GEN_COMMIT_MSG" = 'true' ]; then
		# Check if changelog file has changed
		if [ "$STAGED_FILES_ONLY" = 'true' ]; then
			if git diff --cached --quiet "$REPO_ROOT/${CHANGELOG_NAME[0]}"; then
				echo "ERROR: There are no staged changes in the '$REPO_ROOT/${CHANGELOG_NAME[0]}' file." 1>&2
				return 10
			fi
		elif ! git status -s "$REPO_ROOT/${CHANGELOG_NAME[0]}" 2> /dev/null | grep -q ".. \(.*/\|\)${CHANGELOG_NAME[0]}"; then
			echo "ERROR: There are no changes in the '$REPO_ROOT/${CHANGELOG_NAME[0]}' file." 1>&2
			return 11
		fi

		CHANGELOG="$(sed -z 's/^[^|]*//' "$REPO_ROOT/${CHANGELOG_NAME[0]}" | sed -n '3p')"
		COMMIT_CHANGELOG="$(grep -Po '^\|[^\|]*\| \K.*(?= \|$)' <<< "$CHANGELOG" | sed 's/^/- /;s/\\n/\\\\n/g;s/ *<br> *<ul> *<li> */\n   - /g;s/ *<ul> *<li> */\n   - /g;s/ *<\/li> *<li> */;\n   - /g;s/ *<\/li> *<\/ul> *<br> *\([^|]\)/;\n- \1/g;s/ *<\/li> *<\/ul> *\([^|]\)/;\n- \1/g;s| *</li> *</ul> *$||g;s/ *<br> */;\n- /g')."
		TODAY="$(LC_ALL=C date '+%d %B %Y')"

		# Change the date in the changelog file if it is not set to today
		if [ "$(grep -Po '^\| \K[0-9]{2} [A-Za-z]+ [0-9]{4}' <<< "$CHANGELOG")" != "$TODAY" ]; then
			echo 'WARNING: The date in the changelog file is not set to today.' 1>&2

			if [ "$DRY_RUN" != 'true' ]; then
				echo 'WARNING: Changing the date in the changelog file to today.' 1>&2

				# Note: Number 17 is the maximum number of characters needed for date in format `DD MMMM YYYY` where `MMMM` is full month name in English.
				NUM_OF_SPACES=$((17 - $(wc -m <<< "$TODAY")+1))
				SPACES="$(printf "%0${NUM_OF_SPACES}s" ' ')"

				sed -zi "s/---|\n| [^|]*/---|\n| $TODAY$SPACES /" "$REPO_ROOT/${CHANGELOG_NAME[0]}"
				git add "$REPO_ROOT/${CHANGELOG_NAME[0]}"
			fi
		fi
	fi

	# Get last version
	VER_LAST="$(git tag | grep "$(git tag | sed 's/^v//' | LC_COLLATE=C sort -V | tail -1)")"

	if [ "$VER_LAST" ]; then
		VER_LAST_MAJOR="$(grep -Po '^[^0-9]*\K[^.]+' <<< "$VER_LAST")"
		VER_LAST_MINOR="$(grep -Po '^[^0-9]*[^.]+\.\K[^.]+' <<< "$VER_LAST")"
		VER_LAST_PATCH="$(grep -Po '^[^0-9]*[^.]+\.[^.]+\.\K[^.-]+' <<< "$VER_LAST")"

		if grep -q '-' <<< "$VER_LAST"; then
			# Get the previous dev version type and number
			VER_LAST_DEV_SUFFIX="$(grep -Po '\-\K[^.]+' <<< "$VER_LAST")"
			VER_LAST_DEV_NUM="$(grep -Po '\-[^.]+\.\K[0-9]+' <<< "$VER_LAST")"
		fi
	else
		VER_LAST_MAJOR=0
		VER_LAST_MINOR=0
		VER_LAST_PATCH=1
	fi

	# Bump a new version only when the previous release was not a dev version
	# Note: When the previous release was a dev version, we are about to release the same version as the dev version, but without suffix
	if [ "$VER_LAST_DEV_SUFFIX" ]; then
		ADD_NUM=0
	else
		ADD_NUM=1
	fi

	case "$TYPE" in
		'semver')
			case "$RELEASE_TYPE" in
			'major')
				VER_NEXT_MAJOR=$((VER_LAST_MAJOR + ADD_NUM))
				VERSION="$VER_NEXT_MAJOR.0.0"
			;;
			'minor')
				VER_NEXT_MINOR=$((VER_LAST_MINOR + ADD_NUM))
				VERSION="$VER_LAST_MAJOR.$VER_NEXT_MINOR.0"
			;;
			'patch')
				VER_NEXT_PATCH=$((VER_LAST_PATCH + ADD_NUM))
				VERSION="$VER_LAST_MAJOR.$VER_LAST_MINOR.$VER_NEXT_PATCH"
			;;
			esac
		;;
		'date')
			local YEAR MONTH
			YEAR="$(date '+%y')"
			MONTH="$(date '+%m')"

			if [ "$YEAR" = "$VER_LAST_MAJOR" ] && [ "$MONTH" = "$VER_LAST_MINOR" ]; then
				PATCH="$(printf '%02d' "$((VER_LAST_PATCH + ADD_NUM))")"
			else
				PATCH='01'
			fi

			VERSION="$YEAR.$MONTH.$PATCH"
		;;
	esac

	# Check if we should create a dev version
	if [ "$DEV_SUFFIX" ]; then
		# Check if $VER_LAST is a dev version
		if grep -q '-' <<< "$VER_LAST"; then
			# Check if $VER_LAST_DEV_SUFFIX lexically before or after $DEV_SUFFIX, or are they the same
			if [ "$VER_LAST_DEV_SUFFIX" = "$DEV_SUFFIX" ]; then
				DEV_NUMBER=$((VER_LAST_DEV_NUM + 1))
			elif [ "$(echo -e "$VER_LAST_DEV_SUFFIX\n$DEV_SUFFIX" | LC_COLLATE=C sort -V | tail -1)" = "$DEV_SUFFIX" ]; then
				DEV_NUMBER=1
			else
				echo "ERROR: Last version is $VER_LAST and $VER_LAST_DEV_SUFFIX is lexically after $DEV_SUFFIX, which should not happen." 1>&2
				return 7
			fi
		else
			DEV_NUMBER=1
		fi

		# Append the dev suffix and number to the version number
		VERSION="$VERSION-$DEV_SUFFIX.$DEV_NUMBER"
	fi

	# Finalise the commit message
	COMMIT_MESSAGE="$(printf 'Release %s\n\n%s' "$VERSION" "$COMMIT_CHANGELOG")"

	if [ "$DRY_RUN" = 'true' ]; then
		printf '\n%s' "$COMMIT_MESSAGE"
	else
		# Change the version in `.gitlab-ci.yml`
		if [ -f "$REPO_ROOT/.gitlab-ci.yml" ]; then
			sed -i "s/PACKAGE_VERSION: .*$/PACKAGE_VERSION: '$VERSION'/" "$REPO_ROOT/.gitlab-ci.yml"
			git add "$REPO_ROOT/.gitlab-ci.yml"
		fi

		# Change the version in `*.user.css` files in the repository root folder
		for user_style in "$REPO_ROOT/"*.user.css; do
			if ! grep -q '\*\.user\.css$' <<< "$user_style"; then
				sed -i "s/^@version        .*$/@version        $VERSION/" "$user_style"
				git add "$user_style"
			fi
		done

		# Change the version in `package.json`
		if [ -f "$REPO_ROOT/package.json" ]; then
			sed -i "s/^  \"version\": \"[^\"]\+/  \"version\": \"$VERSION/" "$REPO_ROOT/package.json"
			rm -rf "$REPO_ROOT/node_modules"
			npm i
			git add "$REPO_ROOT/package.json" "$REPO_ROOT/package-lock.json"
		fi

		# Create a new commit only where there are some changes
		if [ "$STAGED_FILES_ONLY" = 'true' ]; then
			if git status --porcelain=v1 2> /dev/null | grep -q '^[^ ?]' && git commit -m "$COMMIT_MESSAGE"; then
				git tag -a "$VERSION" -m "$COMMIT_MESSAGE"
			fi
		else
			if [ "$(git status --porcelain=v1 2> /dev/null)" != '' ] && gaa && git commit -m "$COMMIT_MESSAGE"; then
				git tag -a "$VERSION" -m "$COMMIT_MESSAGE"
			fi
		fi

		# Push only where there are unpushed commits or tags
		if [ "$(git log origin/master..master)" != '' ] || [ "$(comm -13 --nocheck-order <(git ls-remote --tags origin | grep -Po 'tags/\K.*$' | sed 's/\^{}//' | sort | uniq) <(git tag | sort))" != '' ]; then
			git push origin --follow-tags
		fi
	fi
}