#!/bin/bash

# create_ssh_key [name]


create_ssh_key() {
	name="$1"

	# Generating key
	ssh-keygen -t ed25519 -a 100 -f "$HOME/.ssh/$name" -P ''

	# Change permissions to 600
	chmod 600 "$HOME/.ssh/$name"*

	# Add key to ssh
	ssh-add "$HOME/.ssh/$name"
}