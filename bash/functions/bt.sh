#!/usr/bin/env bash

# Connect to different bluetooth devices

function bt() {
	local device="$1"
	local action="$2"
	local mac device_name port bat_level connected

	case "$device" in
		'freelace')
			mac='F4:BC:DA:24:89:71'
			device_name='Huawei FreeLace'
			port=1
		;;
		'note10')
			mac='C0:DC:DA:14:F7:FB'
			device_name='note10'
			port=1  # TODO: Check the port ([0-9] or [0-30])
		;;
		'sony')
			mac='04:5D:4B:83:EC:2B'
			device_name='Sony headphones'
			port=1  # TODO: Check the port ([0-9] or [0-30])
		;;
		*)
			echo 'ERROR: Unknown option. Supported options are: freelace, note10 and sony.' 1>&2
	esac

	if [ "$action" = 'bat' ]; then
		connected="$(bluetoothctl info "$mac" | grep -Po 'Connected: \K.*$')"

		if [ "$connected" = 'no' ]; then
			echo "ERROR: You haven't connected to '$device_name' yet." 1>&2
			return 2
		fi

		while [ -z "$bat_level" ]; do
			bat_level="$(bluetooth_battery "$mac.$port" | grep -Po '[0-9]+(?=%$)')"
		done

		echo "$bat_level %"
	else
		bluetoothctl connect "$mac"
	fi
}