#!/bin/bash

# Start Win 10 VM on xcp.domcek.lan


win10() {
	local uuid
	uuid="$("$(command -v bw)" get item d122d8e0-1f36-4274-be69-ac62015a5eaf | jq -r '.fields[] | select(.name == "uuid").value')"
	ssh xcp xe vm-start uuid="$uuid"
}