#!/bin/bash

# Prompt settings of Tukusej's Sirs

# Based upon multiple online sources, especially on:
# - demure's bashrc sub source prompt script (https://notabug.org/demure/dotfiles/raw/master/subbash/prompt)
# - `/etc/profile.d/vte.sh` (inspiration: https://unix.stackexchange.com/questions/93476/gnome-terminal-keep-track-of-directory-in-new-tab#comment219157_93477)

# This changes the PS1
# - the `prompt` sets the prompt (this must be the first command);
# - the `history -a` part is to add the run command to bash history immediately, not only after the shell quits;
# - the `term_title_prompt` sets the terminal title;
# - the `__vte_prompt_command` sets the CWD path to the previously open folder (defaults to user home directory).
export PROMPT_COMMAND='prompt; history -a; term_title_prompt; __vte_prompt_command'

term_title_prompt() {
	# Check if the shell is running locally or remotely
	# Note: This condition is based on this answer and its comments: https://serverfault.com/a/187719/514866
	if who am i | grep -q '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\|adsl\|t-com'; then
		# Remote host
		printf '\033]0;%s@%s\007' "$USER" "${HOSTNAME%%.*}"
	else
		# Local host
		if grep -q "$XDG_GIT_DIR/itens/controlserver/" <<< "$PWD"; then
			printf '\033]0;%s\007' "${PWD/#$XDG_GIT_DIR\/itens\/controlserver\//}"
		elif grep -q "$XDG_GIT_DIR/itens/" <<< "$PWD"; then
			printf '\033]0;%s\007' "${PWD/#$XDG_GIT_DIR\/itens\//}"
		elif grep -q "$XDG_GIT_DIR/" <<< "$PWD"; then
			printf '\033]0;%s\007' "${PWD/#$XDG_GIT_DIR\//}"
		else
			local TILDE='~'
			printf '\033]0;%s\007' "${PWD/#$HOME/$TILDE}"
		fi
	fi
}

__vte_urlencode() {
	# This is important to make sure string manipulation is handled byte-by-byte.
	local LC_ALL='C'
	local str="$1"

	while [ "$str" ]; do
		local safe="${str%%[!a-zA-Z0-9/:_\.\-\!\'\(\)~]*}"
		printf '%s' "$safe"
		str="${str#"$safe"}"

		if [ -n "$str" ]; then
			printf '%%%02X' "'$str"
			str="${str#?}"
		fi
	done
}

__vte_prompt_command() {
	local command cwd
	command="$(HISTTIMEFORMAT='' history 1 | sed 's/^ *[0-9]\+ *//')"
	command="${command//;/ }"
	cwd='~'
	printf '\033]7;file://%s%s\007' "${HOSTNAME:-}" "$(__vte_urlencode "$cwd")"
}

prompt() {
	local EXIT="$?"  # Note: This needs to be first in order to make it work
	PS1=''           # Note: This must not be a local variable
	local cwd
	local cyan='\e[36m'
	local fdefault='\e[39m'
	local lblue='\e[94m'
	local lred='\e[91m'
	local lyellow='\e[93m'
	local magenta='\e[35m'
	local red='\e[31m'
	local TILDE='~'
	local userHost=''
	local yellow='\e[33m'

	# Newline
	# Adds a newline after the previous command output (and before the PS1)
	PS1+='\n'

	## Date
	if [ "$LANG" = 'sk_SK.UTF-8' ]; then
		# Use 24-hour format
		PS1+="${fdefault}[$magenta\D{%k:%M}$fdefault]"
	else
		# Use 12-hour format
		PS1+="${fdefault}[$magenta\D{%l.%M%P}$fdefault]"
	fi

	# Exit code
	if [ "$EXIT" != 0 ]; then
		PS1+="$red$EXIT$fdefault"
	else
		PS1+="$fdefault$EXIT"
	fi

	### Host test
	# Note: This condition is based on this answer and its comments: https://serverfault.com/a/187719/514866
	local userHost

	if who am i | grep -q '[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\|adsl\|t-com'; then
		# Remote host
		userHost="$lyellow\u$red@$lyellow\h:$fdefault"
	else
		# Local host
		userHost="$lyellow\u@\h:$fdefault"
	fi

	PS1+=" $userHost"

	# Check background and stopped jobs
	# Background running jobs
	local BKGJBS STPJBS
	BKGJBS="$(jobs -r | wc -l | tr -d ' ')"

	if [ "$BKGJBS" -gt 0 ]; then
		PS1+="${yellow}[bg:$BKGJBS]$fdefault"
	fi

	## Stopped jobs
	STPJBS="$(jobs -s | wc -l | tr -d ' ')"

	if [ "$STPJBS" -gt 0 ]; then
		PS1+=" ${yellow}[stp:$STPJBS]$fdefault"
	fi

	# Git status
	local GStatus gss
	GStatus="$(git status --porcelain=2 -b 2> /dev/null | tr '\n' ':')"
	gss="$(git status --porcelain=1 2> /dev/null)"

	if [ "$GStatus" ]; then
		### Fetch Time Check ### {{{
		local LAST TIME GStatus gss
		LAST="$(stat -c %Y "$(git rev-parse --git-dir 2> /dev/null)"/FETCH_HEAD 2> /dev/null)"

		if [ "$LAST" ]; then
			TIME=$(($(date +"%s") - LAST))
			## Check if more than 60 minutes since last run of `${GStatus}`
			if [ "${TIME}" -gt "3600" ]; then
				git fetch 2> /dev/null
				PS1+=' +'
				## Refresh var
				GStatus="$(git status --porcelain=2 -b 2> /dev/null | tr '\n' ':')"
				gss="$(git status --porcelain=1 2> /dev/null)"
			fi
		fi
		### End Fetch Check ### }}}

		### Test For Changes ### {{{
		local GChanges GitColor
		GChanges="$(echo -n "$gss" | wc -m)"

		if [ "${GChanges}" != "0" ]; then
			GitColor=${lred}
		fi
		### End Test Changes ### }}}

		### Find Branch ### {{{
		local GBranch
		GBranch="$(awk 'match($0,/# branch.head [^ :]+/) {print substr($0,RSTART+14,RLENGTH-14)}' <<< "${GStatus}")"
		if [ -n "${GBranch}" ]; then
			GBranch="[${GBranch}]"			  ## Add brackets for final output. Will now test against brackets as well.
			if [ "${GBranch}" == "[master]" ]; then
				GBranch="[M]"			 ## Because why waste space
			fi
			## Test if in detached head state, and set output to first 8char of hash
			if [ "${GBranch}" == "[(detached)]" ]; then
				GBranch="($(awk 'match($0,/branch.oid [0-9a-fA-F]+/) {print substr($0,RSTART+11,RLENGTH-11)}' <<< "${GStatus}" | cut -c1-8))"
			fi
		else
			## Note: No braces applied to emphasis that there is an issue, and that you aren't in a branch named "ERROR".
			GBranch="ERROR"  # Could it happen?
		fi
		### End Branch ### }}}

		PS1+=" ${GitColor}${GBranch}${fdefault}"  # Add result to prompt

		### Find Commit Status ### {{{

		## Add 0 to knock off the '+'
		local GAhead
		GAhead="$(awk 'match($0,/# branch.ab \+[0-9]+ \-[0-9]+/) {split(substr($0,RSTART+12,RLENGTH-12),s," "); V=s[1]+0} END {if(V>0){print V}}' <<< "${GStatus}")"

		if [ -n "${GAhead}" ]; then
			PS1+="${lblue}↑${fdefault}${GAhead}"  ## Ahead
		fi

		## Needs a `git fetch`
		## Multiply by -1 to remove the '-'
		local GBehind
		GBehind="$(awk 'match($0,/# branch.ab \+[0-9]+ \-[0-9]+/) {split(substr($0,RSTART+12,RLENGTH-12),s," "); V=s[2]*-1} END {if(V>0){print V}}' <<< "${GStatus}")"
		if [ -n "${GBehind}" ]; then
			PS1+="${lred}↓${fdefault}${GBehind}"  ## Behind
		fi

		local current_branch_changes file_modified_staged_remodified file_modified_nonstaged
		local file_modified_staged file_added file_added_remodified file_deleted_nonstaged file_deleted_staged file_untracked
		current_branch_changes=$(echo -n "$gss" | wc -l)
		file_modified_staged_remodified=$(grep -c '^MM' <<< "$gss")
		file_modified_nonstaged=$(grep -c '^ M' <<< "$gss")
		file_modified_staged=$(grep -c '^M ' <<< "$gss")
		file_added=$(grep -c '^A ' <<< "$gss")
		file_added_remodified=$(grep -c '^AM' <<< "$gss")
		file_deleted_staged=$(grep -c '^D' <<< "$gss")
		file_deleted_nonstaged=$(grep -c '^ D' <<< "$gss")
		file_untracked=$(grep -c '^??' <<< "$gss")

		# Output a space between branch name and list of changes (only when there are some changes)
		if [ "${current_branch_changes}" -gt "0" ]; then
			PS1+=" "
		fi

		if [ "${file_modified_staged_remodified}" -gt "0" ]; then
			PS1+="${magenta}MM${fdefault}${file_modified_staged_remodified}"
		fi

		if [ "${file_modified_nonstaged}" -gt "0" ]; then
			PS1+="${lred}M-${fdefault}${file_modified_nonstaged}"
		fi

		if [ "${file_modified_staged}" -gt "0" ]; then
			PS1+="${lblue}M+${fdefault}${file_modified_staged}"
		fi

		if [ "${file_added}" -gt "0" ]; then
			PS1+="${yellow}A${fdefault}${file_added}"
		fi

		if [ "${file_added_remodified}" -gt "0" ]; then
			PS1+="${lyellow}AM${fdefault}${file_added_remodified}"
		fi

		if [ "${file_deleted_nonstaged}" -gt "0" ]; then
			PS1+="${lred}D${fdefault}${file_deleted_nonstaged}"
		fi

		if [ "${file_deleted_staged}" -gt "0" ]; then
			PS1+="${red}D${fdefault}${file_deleted_staged}"
		fi

		if [ "${file_untracked}" -gt "0" ]; then
			PS1+="${cyan}?${fdefault}${file_untracked}"
		fi

		### End Commit Status ### }}}
	fi

	# Current working Directory
	cwd="${PWD/#$HOME/$TILDE}"

	PS1+=" $cwd\n\$ "  # Currect working directory, newline, \$ and a space
}