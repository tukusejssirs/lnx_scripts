#!/usr/bin/env bash

# Install and update ElectronMail

# TODO: Compare the installed version with the available version prior to downloading and installing the latest version.


# Variables
url_api='https://api.github.com/repos/vladimiry/ElectronMail/releases/latest'
url_dl="$(curl -s "$url_api" | jq -r .assets[].browser_download_url | grep '\.rpm$')"

# Test if we got the download URL
if [ ! "$url_dl" ]; then
	echo "ERROR: I could not find the latest version. Check it at $url_api. And then it would be nice of you if you report this issue at https:/gitlab.com/tukusejssirs/lnx_scripts/issues (make sure there is no such report yet please)." 1>&2
	exit 1
fi

# Install ElectronMail
sudo dnf -y install "$url_dl"