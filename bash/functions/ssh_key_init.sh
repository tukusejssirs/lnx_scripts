#!/bin/bash

# This function creates a new SSH key for current local user and sets it up on the remote server

# TODO
# - create a function to delete host configuration from `~/.ssh/config` and the key used by it from `~/.ssh`;
# - check if a key with the same host, hostname, user and port is already added to the SSH config file;


ssh_key_init() {
	man_output() {
		cat << EOF
.TH $FN_NAME 1 \"\" \"\" "$FN_NAME man page"
.SH NAME
\fB$FN_NAME\fR \- SSH key initialisation
.SH SYNOPSIS
\fB$FN_NAME\fR [\fB\-a\fR | \fB\-\-add\fR] [\fB\-c\fR | \fB\-\-copy\fR] [\fB\-n\fR | \fB\-\-name \fIname\fR] [\fB\-q\fR | \fB\-\-quiet\fR] [\fBuser@\fR]\fBhost\fR[\fB:port\fR]
.br
\fB$FN_NAME\fR [\fB\-h\fR | \fB\-\-help\fR]
.SH DESCRIPTION
Initialise SSH key pairs, i.e. create one and optionally add it the SSH configuration file and optionally copy the public key to the server
.SH OPTIONS
Mandatory arguments to long options are mandatory for short options too.
.PP
.TP
\fB\-a\fR, \fB\-\-add\fR
add the key to the SSH configuration file
.TP
\fB\-c\fR, \fB\-\-copy\fR
copy the key to the host using \fBssh\-copy\-id\fR
.TP
\fB\-h\fR, \fB\-\-help\fR
display this help and exit; note that when this option is used, all other option is ignored
.TP
\fB\-k\fR, \fB\-\-key\-name\fR=\fIkey\-name\fR
use this key name prefix for the host in \fB~/.ssh\fR; default: \fIremote_user@remote_host_local_user@local_host\fR or \fIremote_host_local_user@local_host\fR when \fIuser\fR is not provided
.TP
\fB\-m\fR, \fB\-\-man\fR
create a man page for this script and save it to \fB/usr/share/man/man1/$FN_NAME.1\fR and exit
.br
Note that when this option is used, all other option is ignored.
.TP
\fB\-n\fR, \fB\-\-name\fR=\fIname\fR
use this name for the host in \fB~/.ssh/config\fR; default: \fIremote_user_remote_host\fR or \fIremote_host\fR when \fIuser\fR is not provided
.TP
\fB\-q\fR, \fB\-\-quiet\fR
be quiet and don't output anything
.SH EXIT STATUS
.TP
0
Success
.TP
1
Invalid options provided
.TP
2
No arguments provided
.TP
3
Could not create a man page for this script
.SH AUTHOR
Tukusej's Sirs
.br
tukusejssirs@protonmail.com
.br
t.me/tukusejssirs
.SH REPORTING BUGS
Bug reports should be opened at <https://gitlab.com/tukusejssirs/lnx_scripts/-/issues>
.SH COPYRIGHT
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
EOF
	}

	man_init() {
		local RETVAL
		sudo bash -c "echo \"$(man_output)\" > \"/usr/share/man/man1/$FN_NAME.1\"" && RETVAL=0 || RETVAL=3
		output_messages INFO "The man page was saved to \`/usr/share/man/man1/$FN_NAME.1\`."
		output_messages INFO "To open it, run \`man $FN_NAME\`."
		echo $RETVAL
	}

	help_long() {
		man_output | man -l -
	}

	help_short() {
		echo "Usage: $FN_NAME [-a | --add] [-c | --copy] [-h | --help] [-m | --man] [-n | --name NAME] [-k | --key-name KEY_NAME] [-q | --quiet] [user@]host[:port]"
	}

	output_messages() {
		local TYPE="$1"
		local MSG="$2"
		local CODE=$3

		if [ "$VERBOSE" = 'true' ]; then
			echo -e "$TYPE: $MSG" 1>&2
		fi

		if [ "$TYPE" = 'ERROR' ]; then
			return "$CODE"
		fi
	}

	# Variables
	local FN_NAME="${FUNCNAME[0]}"
	local ADD_SSH_KEYS='false'
	local SSH_COPY_ID='false'
	local VERBOSE='true'
	local remote_host KEY_NAME remote_name OPT_REST OPTIONS remote_port SSH_KEYGEN_OPT remote_user

	# Call getopt to validate the provided input
	if ! OPTIONS="$(getopt -o achk:mn:q --long add,copy,help,key-name:,man,name:,quiet -- "$@")"; then
		# Short help message
		output_messages ERROR "Invalid options provided.\n$(help_short)" 1
	fi

	eval set -- "$OPTIONS"

	while true; do
		case "$1" in
			--add|-a)
				ADD_SSH_KEYS='true'
			;;
			--copy|-c)
				ADD_SSH_KEYS='true'
				SSH_COPY_ID='true'
			;;
			--help|-h)
				help_long
				return 0
			;;
			--key-name|-k)
				shift
				KEY_NAME="$1"
			;;
			--man|-m)
				return "$(man_init)"
			;;
			--name|-n)
				shift
				remote_name="$1"
			;;
			--quiet|-q)
				VERBOSE='false'
				SSH_KEYGEN_OPT='-q'
			;;
			--)
				shift
				OPT_REST="$*"

				# Check if the user@host:port argument is provided
				if [ ! "$OPT_REST" ]; then
					output_messages ERROR "No arguments provided.\n$(help_short)" 2
				fi

				break
			;;
		esac
		shift
	done

	# Get the username, hostname, port number
	remote_user="$(grep -oP '^[^@]*(?=@)' <<< "$OPT_REST")"

	if [ "$remote_user" ]; then
		remote_host="$(grep -oP '@\K[^:]*' <<< "$OPT_REST")"
	else
		remote_host="$(grep -o '^[^:]*' <<< "$OPT_REST")"
	fi

	remote_port="$(grep -oP ':\K.*' <<< "$OPT_REST")"

	# `Host` config name
	if [ ! "$remote_name" ]; then
		if [ "$remote_user" ]; then
			remote_name="${remote_user}_$remote_host"
		else
			remote_name="$remote_host"
		fi
	fi

	# Key name
	if [ ! "$KEY_NAME" ]; then
		if [ "$remote_user" ]; then
			KEY_NAME="$remote_user@${remote_host}_$USER@$HOSTNAME"
		else
			KEY_NAME="${remote_host}_$USER@$HOSTNAME"
		fi
	fi

	# Generate a new SSH key pair
	ssh-keygen $SSH_KEYGEN_OPT -t ed25519 -a 100 -P '' -f "$HOME/.ssh/$KEY_NAME"
	chmod 600 "$HOME/.ssh/$KEY_NAME"
	output_messages INFO 'SSH key pair is generated.'

	if [ "$ADD_SSH_KEYS" = 'true' ]; then
		# Define XDG_GIT_DIR if not defined
		if [ ! "$XDG_GIT_DIR" ]; then
			source "$HOME/.config/user-dirs.dirs" &> /dev/null

			if [ ! "$XDG_GIT_DIR" ]; then
				XDG_GIT_DIR='/git'
			fi

			if [ ! -d "$XDG_GIT_DIR/lnx_scripts" ]; then
				# Get the path of this (ssh_key_init.sh) script path
				# src: https://unix.stackexchange.com/a/153061/267480
				THIS_SCRIPT_PATH="${BASH_SOURCE[${#BASH_SOURCE[@]} - 1]}"

				# Get the PATH_FN path
				test_git_repo_repo="$(git -C "$(dirname "$THIS_SCRIPT_PATH")" rev-parse --show-toplevel 2> /dev/null)"
				test_git_repo_repo_retval="$?"

				if [ $test_git_repo_repo_retval = 0 ] && [ -d "$test_git_repo_repo/bash/functions" ]; then
					path_fn="$test_git_repo_repo/bash/functions"
				else
					path_fn="$(dirname "$(dirname "$(realpath "$THIS_SCRIPT_PATH")")")/bash/functions"
				fi
			fi
		fi

		# Source add_ssh_keys.sh
		source "$path_fn/add_ssh_keys.sh"

		# Reload the ssh-agent
		add_ssh_keys

		# Update SSH config
		cat << EOF >> "$HOME/.ssh/config"

Host $remote_name
     Hostname      $remote_host
     IdentityFile  $HOME/.ssh/$KEY_NAME
EOF

		if [ "$remote_port" ]; then
			echo "     Port          $remote_port" >> "$HOME/.ssh/config"
		fi

		if [ "$remote_user" ]; then
			echo "     User          $remote_user" >> "$HOME/.ssh/config"
		fi

		output_messages INFO 'SSH key pair is added to the SSH configuration.'
	fi

	if [ "$SSH_COPY_ID" = 'true' ]; then
		if [ "$VERBOSE" = 'true' ]; then
			if [ "$remote_port" = '' ]; then
				ssh-copy-id "$remote_user@$remote_host"
			else
				ssh-copy-id -p "$remote_port" "$remote_user@$remote_host"
			fi
		else
			if [ "$remote_port" = '' ]; then
				ssh-copy-id "$remote_user@$remote_host" &> /dev/null
			else
				ssh-copy-id -p "$remote_port" "$remote_user@$remote_host" &> /dev/null
			fi
		fi
	fi

	# Unset local functions
	unset -f man_output man_init help help_short output_messages

	return 0
}