#!/bin/bash

# Batch scan wrapper around `scan.sh` function

#
# Usage:   batch_scan -s 248 -e 424 -w 1 -m c psv.png

# TODO
# - merge it into `scan.sh`, i.e. let's have one main function that would make it possible to scan one page (like the current implementation of `scan.sh`) or multiple pages in a batch (like in this script);
# - create long and short help


batch_scan() {
	exit_fn() {
		rm $TMP_FILE
		clear -x
	}

	help_short() {
		echo "Usage:   batch_scan -s <start_page> -e <end_page> -w <width> -m <scan_mode> base_filename.ext"
		echo
		echo "Options: -o s:e:w:m:h --long start:,end:,width:,mode:,help"
	}

	help_long() {
		help_short
	}

	# Variables
	local START_NUM=1
	local END_NUM=
	local NUM_WIDTH=
	local MODE='colour'
	local FILE_PATH='.'
	local FILENAME='out.png'
	local PROG_NAME='CLI Scanning Utility'
	local TMP_FILE='/dev/shm/scan.tmp'

	# Trap and delete temp files
	trap "exit_fn; return || exit" EXIT

	# Source the `scan.sh` function
	source "$XDG_GIT_DIR/lnx_scripts/bash/functions/scan.sh"

	# Call getopt to validate the provided input
	local OPTIONS=$(getopt -o s:e:w:m:h --long start:,end:,width:,mode:,help -- "$@")

	# Short help message
	[ $? -eq 0 ] || {
		# output_messages ERROR "Invalid options provided.\n$(short_help)" 1
		echo 'ERROR: Invalid options provided.' 1>&2
		help_short
	}

	eval set -- "$OPTIONS"

	while true; do
		case "$1" in
			--start|-s)
				shift
				START_NUM=$1
			;;
			--end|-e)
				shift
				END_NUM=$1
			;;
			--width|-w)
				shift
				NUM_WIDTH=$1
			;;
			--mode|-m)
				shift
				MODE=$1
			;;
			--help|-h)
				help_long
				return 0
			;;
			--)
				shift
				OPT_REST="$*"
				break
			;;
		esac
		shift
	done

	# Get the filename and its path
	if [ "$OPT_REST" ]; then
		FILE_PATH="$(dirname "$(realpath "$OPT_REST")")"
		FILENAME="$(basename "$OPT_REST")"
	fi

	# Get the end number width if not specified
	if [ ! "$END_NUM" ]; then
		END_NUM=$START_NUM
	fi

	# Get the number width if not specified
	if [ ! "$NUM_WIDTH" ]; then
		NUM_WIDTH=$(echo -n "$END_NUM" | wc -m)
	fi

	local n=$(( START_NUM - 1 ))
	local f f_n

	while [ "$n" -le "$END_NUM" ]; do
		f="$FILE_PATH/$(printf "%0${NUM_WIDTH}d" "$n")_$FILENAME"
		f_n="$FILE_PATH/$(printf "%0${NUM_WIDTH}d" "$(("$n" + 1))")_$FILENAME"

		# Get what menu items should be displayed
		case $n in
			$END_NUM)
				dialog --backtitle " $PROG_NAME " \
					--title 'Scanning dialogue' \
					--nocancel --clear --menu "Saving path: $(dirname "$f")\n\nChoose what to do:" 12 100 1 \
					rescan "rescan last scanned page ($(basename $f))" \
					exit 'exit the utility' 2>"${TMP_FILE}"
			;;
			$(($START_NUM - 1)))
				dialog --backtitle " $PROG_NAME " \
					--title 'Scanning dialogue' \
					--nocancel --clear --menu "Saving path: $(dirname "$f")\n\nChoose what to do:" 12 100 1 \
					scan "scan the following page  ($(basename $f_n))" \
					exit 'exit the utility' 2>"${TMP_FILE}"
			;;
			*)
				dialog --backtitle " $PROG_NAME " \
					--title 'Scanning dialogue' \
					--nocancel --clear --menu "Saving path: $(dirname "$f")\n\nChoose what to do:" 12 100 1 \
					scan "scan the following page  ($(basename $f_n))" \
					rescan "rescan last scanned page ($(basename $f))" \
					exit 'exit the utility' 2>"${TMP_FILE}"
			;;
		esac

		# Process the choice
		case "$(< "${TMP_FILE}")" in
			scan)
				# Scan the following page
				(( n++ ))
				scan $MODE $f_n 2>&1 | dialog --backtitle " $PROG_NAME " \
					--title "Scanning progress" \
					--clear --progressbox "Scanning into $f_n" 6 100
			;;
			rescan)
				# Rescan the current page
				scan $MODE $f 2>&1 | dialog --backtitle " $PROG_NAME " \
					--title "Scanning progress" \
					--clear --progressbox "Scanning into $f" 6 100
			;;
			exit)
				# Exit the utility
				exit_fn
				break
			;;
		esac
	done
}