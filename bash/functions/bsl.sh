#!/bin/bash

# This script gets the date of the last vault sync in local time



bsl() {
	date -d $(bw sync --last --session $BW_SESSION)
}