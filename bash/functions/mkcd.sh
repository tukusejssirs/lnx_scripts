#!/bin/bash

# This function creates a directory and then changes the current working director to it.


mkcd() {
	mkdir "$1"
	cd "$1"
}