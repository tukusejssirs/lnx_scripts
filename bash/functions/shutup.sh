#!/bin/bash

# Shutdown script



# TODO
##### - check if there is anything to stage/commit/push;
##### - check if the programs/apps are running (like ff, transmission, mailspring, etc);
##### - integrate getopt;
##### - consider creating a repo per app + gnome-related stuff (as separate repo);
# - /home/ts/.config/gsconnect
# - /home/ts/.config/gtk-4.0
# - /home/ts/.config/gtk-3.0
# - /home/ts/.config/libreoffice
# - /home/ts/.config/mimeapps.list [file]
# - /home/ts/.config/sublime-merge
# - /home/ts/.config/transmission
# - /home/ts/.config/user-dirs.dirs [file]
# - /home/ts/.config/user-dirs.locale [file]
# - /home/ts/.config/vlc
#
# - gimp
# - teamviewer
# - audacious

# Possible additions
# - /home/ts/.config/htop

# Janka
# - /home/ts/.config/GeoGebra

shutup() {
	local shutdown_check=$1
	local lmagenta='\e[95m'
	local fdefault='\e[39m'
	local lyellow='\e[93m'

	if [[ $(uname -r | grep -o "Microsoft$") == "Microsoft" ]]; then  # On WSL
		local paths='/mnt/c/Users/ts/git/os_backups/t5500/appdata/local/adobe /mnt/c/Users/ts/git/os_backups/t5500/appdata/local/mozilla /mnt/c/Users/ts/git/os_backups/t5500/appdata/locallow/adobe /mnt/c/Users/ts/git/os_backups/t5500/appdata/locallow/mozilla /mnt/c/Users/ts/git/os_backups/t5500/appdata/roaming/adobe /mnt/c/Users/ts/git/os_backups/t5500/appdata/roaming/mailspring /mnt/c/Users/ts/git/os_backups/t5500/appdata/roaming/mozilla /mnt/c/Users/ts/git/os_backups/t5500/indi_prtbl /mnt/c/Users/ts/git/os_backups/t5500/subl'
		local poweroff_cmd='cmd.exe /C shutdown -s -t 0'

	else  # On Linux
		local paths="$HOME/.mozilla $HOME/.config"
		local paths_root='/etc'
		local poweroff_cmd='sudo poweroff'
	fi

	for n in $paths; do
		if [ -d "$n" ]; then
			echo -e "\n$lmagenta$n$fdefault: $lyellow$(git -C "$n" rev-parse --abbrev-ref HEAD)$fdefault"
			git -C "$n" add --all

			if ! git -C "$n" commit -m 'Update'; then
				rm -rf "$n/.git/index.lock"
				git -C "$n" commit -m 'Update'
			fi

			git -C "$n" push
		fi
	done

	for n in $paths_root; do
		if [ -d "$n" ]; then
			echo -e "\n$lmagenta$n$fdefault: $lyellow$(git -C "$n" rev-parse --abbrev-ref HEAD)$fdefault"
			sudo git -C "$n" add --all

			if ! sudo git -C "$n" commit -m 'Update'; then
				sudo rm -rf "$n/.git/index.lock"
				sudo git -C "$n" commit -m 'Update'
			fi

			sudo git -C "$n" push
		fi
	done

	if [ "$shutdown_check" != 'no' ]; then
		$poweroff_cmd
	fi
}