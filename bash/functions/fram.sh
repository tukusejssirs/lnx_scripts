#!/bin/bash

# This script clears RAM
# You can use any option program `free` uses in order to change its output.

# dependencies; free sync sudo su echo


fram() {
	free -h "$1"
	sync
	sudo su -c 'echo 2 > /proc/sys/vm/drop_caches'
	echo
	free -h "$1"
}