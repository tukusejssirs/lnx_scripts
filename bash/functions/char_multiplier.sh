#!/bin/bash

# Output a character n times


# src: https://stackoverflow.com/a/5799353/3408342


char_multiplier() {
	str=$1
	num=$2
	v=$(printf "%0${num}s" "$str")
	echo -n "${v// /$str}"
}