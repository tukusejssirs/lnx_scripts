#!/bin/bash

# src: https://stackoverflow.com/a/17841619/3408342

##
## @brief      Join array (list of items) by a string
##
## @param      $1      Delimitor
## @param      $2-*    List of items
##
join_by() {
	local d=$1
	shift

	local f=$1
	shift

	printf %s "$f" "${@/#/$d}"
}