#!/bin/bash


# Variables
NIGHT_MAX_BAT_LEVEL=43       # In %
NIGHT_MIN_BAT_LEVEL=45       # In %
NIGHT_MAX_CURRENT=500        # In mA
NIGHT_MAX_VOLTAGE=3920       # In mV
NIGHT_TIME_UNTIL='7am'       # Whatever `date` accepts
DATE_CMD="$PREFIX/bin/date"  # $PREFIX is needed because of Termux
MAIN_MAX_BAT_LEVEL=80        # In %
MAIN_MIN_BAT_LEVEL=50        # In %
MAIN_MAX_CURRENT=            # In mA
MAIN_MAX_VOLTAGE=            # In mV
DATE_CMD="$PREFIX/bin/date"  # $PREFIX is needed because of Termux
MAIN_CONFIG_FILE='/sdcard/.config/acc/config.txt'
NIGHT_CONFIG_FILE='/sdcard/.config/acc/night_config.txt'

# Create main config if it does not exist
if [ ! -f "$MAIN_CONFIG_FILE" ]; then
	source ./acc_create_config.sh
	acc_create_config "$MAIN_CONFIG_FILE" $MAIN_MAX_BAT_LEVEL $MAIN_MIN_BAT_LEVEL $MAIN_MAX_CURRENT $MAIN_MAX_VOLTAGE
fi

# Create night config if it does not exist
if [ ! -f "$NIGHT_CONFIG_FILE" ]; then
	source ./acc_create_config.sh
	acc_create_config "$NIGHT_CONFIG_FILE" $NIGHT_MAX_BAT_LEVEL $NIGHT_MIN_BAT_LEVEL $NIGHT_MAX_CURRENT $NIGHT_MAX_VOLTAGE
fi