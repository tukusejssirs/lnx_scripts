#!/bin/bash


acc_create_config() {
	# Variables
	local CONFIG_FILE="$1"  # Absolute path of the night config file (incl the filename)
	local MAX_BAT_LEVEL=$2  # In %
	local MIN_BAT_LEVEL=$3  # In %
	local MAX_CURRENT=$4    # In mA
	local MAX_VOLTAGE=$5    # In mV

	# Check if $CONFIG_FILE path exists
	if [ ! -d "$(dirname "$CONFIG_FILE")" ]; then
		mkdir -p "$(dirname "$CONFIG_FILE")"
	fi

	# Create night config
	su -c "acc \"$CONFIG_FILE\" -s pc=$MAX_BAT_LEVEL rc=$MIN_BAT_LEVEL mcc=$MAX_CURRENT mcv=$MAX_VOLTAGE"
}