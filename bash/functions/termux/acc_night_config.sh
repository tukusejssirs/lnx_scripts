#!/bin/bash


NIGHT_CONFIG_FILE='/sdcard/.config/acc/night_config.txt'

# Create night config if it does not exist
if [ ! -f "$NIGHT_CONFIG_FILE" ]; then
	# Variables
	NIGHT_MAX_BAT_LEVEL=43       # In %
	NIGHT_MIN_BAT_LEVEL=45       # In %
	NIGHT_MAX_CURRENT=500        # In mA
	NIGHT_MAX_VOLTAGE=3920       # In mV

	source ./acc_create_config.sh

	acc_create_config "$NIGHT_CONFIG_FILE" $NIGHT_MAX_BAT_LEVEL $NIGHT_MIN_BAT_LEVEL $NIGHT_MAX_CURRENT $NIGHT_MAX_VOLTAGE
fi

su -c "accd \"$NIGHT_CONFIG_FILE\""