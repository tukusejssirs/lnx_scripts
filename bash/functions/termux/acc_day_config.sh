#!/bin/bash


MAIN_CONFIG_FILE='/sdcard/.config/acc/config.txt'

# Create main config if it does not exist
if [ ! -f "$MAIN_CONFIG_FILE" ]; then
	# Variables
	MAIN_MAX_BAT_LEVEL=80        # In %
	MAIN_MIN_BAT_LEVEL=50        # In %
	MAIN_MAX_CURRENT=            # In mA
	MAIN_MAX_VOLTAGE=            # In mV

	source ./acc_create_config.sh
	acc_create_config "$MAIN_CONFIG_FILE" $MAIN_MAX_BAT_LEVEL $MAIN_MIN_BAT_LEVEL $MAIN_MAX_CURRENT $MAIN_MAX_VOLTAGE
fi

su -c "accd \"$MAIN_CONFIG_FILE\""