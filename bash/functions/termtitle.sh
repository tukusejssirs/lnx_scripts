#!/bin/bash

# This function changes (gnome-)terminal title. Note that the title is static and won't change if you put there a variable and after that you change its value.

# author:  SebMa (https://askubuntu.com/users/426176/sebma)
# $*       terminal title

# src: https://askubuntu.com/a/143514/279745


termtitle() {
	printf "\033]0;$*\007"
}