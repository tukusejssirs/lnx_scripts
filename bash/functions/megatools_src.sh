#!/usr/bin/env bash

# Install megatools from source


# Install dependencies
sudo dnf -y install ninja meson
# apt-get -yq update
# apt-get -yq install ninja python asciidoc
# pip3 install --upgrade pip
# pip3 install meson

# Clone the repository
git clone https://megous.com/git/megatools "$XDG_GIT_DIR/others/megatools"
cd "$XDG_GIT_DIR/others/megatools"

# Build megatools
meson build~
ninja -C build~
sudo ninja -C build~ install