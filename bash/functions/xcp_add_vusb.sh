#!/bin/bash

# XCP-ng: Configure USB passthrough

# XCP-ng limitations:
# - USBs cannot be hot-plugged, therefore the VM must be turned off before configuring the USBs.
# - USB groups are currently restricted to contain no more than one vUSB.

# TODO:
# - check if a vUSB is already supplied;


xcp_add_vusb() {
	ssh -t xcp "$(cat << EOF
# Variables
VM_NAME="$1"

if [ ! "\$VM_NAME" ]; then
echo '[ERROR] You must supply a valid VM name.' 1>&2
else
# Get VM ID
# xe vm-list
# This is win10 VM ID
VM_UUID="\$(xe vm-list name-label="\$VM_NAME" params=uuid | grep -o '[^ ]*\$')"

# Check if the VM is already running
VM_STATE="\$(xe vm-list uuid=\$VM_UUID params=power-state | grep -o '[^ ]*\$')"

if [ "\$STATE" = 'running' ]; then
echo "[ERROR] \$VM_NAME is already running. The VM must be turned off before continuing." 1>&2
else
# Get USB ID
# Note: The USB must plugged in
# Let's presume there is only one USB
USB_UUID="\$(xe pusb-list params=uuid | grep -o '[^ ]*\$')"

# Get USB group ID
USB_GROUP_UUID="\$(xe usb-group-list PUSB-uuids="\$USB_UUID" params=uuid | grep -o '[^ ]*\$')" > /dev/null

# Enable passthrough on that USB
xe pusb-param-set uuid="\$USB_UUID" passthrough-enabled=true

# Attach the USB device to the VM
xe vusb-create usb-group-uuid="\$USB_GROUP_UUID" vm-uuid="\$VM_UUID"
fi
fi
EOF
)"
}