#!/bin/bash

# This file contains all functions/aliases that make it possible to run MS Windows apps from WSL Bash


if [ -e /mnt/c/Program\ Files/Adobe/Adobe\ InDesign\ CC\ 2018/InDesign.exe ]; then
	indesign() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/Adobe/Adobe\ InDesign\ CC\ 2018/InDesign.exe "$file" &
	}

	alias indi="indesign"
elif [ -e /mnt/c/Users/ts/git/os_backups/t5500/indi_prtbl/App/Id/InDesign.exe ]; then
	indesign() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Users/ts/git/os_backups/t5500/indi_prtbl/App/Id/InDesign.exe "$file" &
	}

	alias indi='indesign'
fi

if [ -e /mnt/c/Program\ Files/Adobe/Adobe\ Photoshop\ CC\ 2018/Photoshop.exe ]; then
	photoshop() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/Adobe/Adobe\ Photoshop\ CC\ 2018/Photoshop.exe "$file" &
	}
fi

if [ -e /mnt/c/Program\ Files/FileZilla\ FTP\ Client/filezilla.exe ]; then
	filezilla() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/FileZilla\ FTP\ Client/filezilla.exe "$file"
	}

	alias fz='filezilla'
fi

GIMP="$(find /mnt/c/Program\ Files/GIMP\ 2/bin/gimp-*[0-9.].exe 2> /dev/null | sort -V | tail -1)"

if [ "$GIMP" ] && [ -e "$GIMP" ]; then
	gimp() {
		local file GIMP
		GIMP="$(find /mnt/c/Program\ Files/GIMP\ 2/bin/gimp-*[0-9.].exe 2> /dev/null | sort -V | tail -1)"

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		"$GIMP" "$file"
	}
fi

unset GIMP

if [ -e /mnt/c/Program\ Files/Microsoft\ Office/root/Office16/WINWORD.EXE ]; then
	msword() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/Microsoft\ Office/root/Office16/WINWORD.EXE "$file"
	}

	alias word='msword'
fi

if [ -e /mnt/c/Program\ Files/Microsoft\ Office/root/Office16/EXCEL.EXE ]; then
	msexcel() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/Microsoft\ Office/root/Office16/EXCEL.EXE "$file"
	}

	alias excel='msexcel'
fi

if [ -e /mnt/c/Program\ Files/Microsoft\ Office/root/Office16/POWERPNT.EXE ]; then
	mspowerpoint() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/Microsoft\ Office/root/Office16/POWERPNT.EXE "$file"
	}

	alias powerpoint='mspowerpoint'
	alias pp='mspowerpoint'
	alias mspp='mspowerpoint'
fi

if [ -e /mnt/c/Program\ Files/Microsoft\ Office/root/Office16/SETLANG.EXE ]; then
	msolang() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/Microsoft\ Office/root/Office16/SETLANG.EXE "$file"
	}
fi

if [ -e /mnt/c/Program\ Files/Mozilla\ Firefox/firefox.exe ]; then
	firefox() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/Mozilla\ Firefox/firefox.exe "$file"
	}

	alias ff='firefox'
fi

if [ -e /mnt/c/Program\ Files/nomacs/bin/nomacs.exe ]; then
	nomacs() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/nomacs/bin/nomacs.exe "$file"
	}
fi

if [ -e /mnt/c/Program\ Files/Sublime\ Text\ 3/subl.exe ]; then
	subl() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/Sublime\ Text\ 3/subl.exe "$file"
	}
fi

transmission-qt() {
	local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

	/mnt/c/Program\ Files/Transmission/transmission-qt.exe "$file"
}

alias transmission='transmission-qt'

if [ -e /mnt/c/Program\ Files/VideoLAN/VLC/vlc.exe ]; then
	vlc() {
		local file

		if [ "$1" ]; then
			file="$(wslpath -aw "$1")"
		fi

		/mnt/c/Program\ Files/VideoLAN/VLC/vlc.exe "$file"
	}
fi

alias cmd='cmd.exe'