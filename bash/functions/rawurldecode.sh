#!/bin/bash

# This function decodes the supplied string ($1) in URL format (i.e. escapes special characters) to text


# src: Based on https://stackoverflow.com/a/10660730/3408342

# Usage: rawurldecode string to convert


# Returns a string in which the sequences with percent (%) signs followed by
# two hex digits have been replaced with literal characters.
rawurldecode() {
  # This is perhaps a risky gambit, but since all escape characters must be
  # encoded, we can replace %NN with \xNN and pass the lot to printf -b, which
  # will decode hex for us
  printf -v decoded '%b' "${1//%/\\x}"

  echo "${decoded}"
  return 0
}