#!/bin/bash

# Source a file, a folder (its content) or a symlink


# Usage:   src [file | folder | symlink]


src() {
	# Variable
	f="$1"

	if [ -f "$f" ]; then  # If file exists and is a regular file, source it
		source "$f"
	elif [ -d "$f" ]; then # If directory, source the files in it
		for n in "$f"/***; do
			source "$n"
		done
	elif [ -L "$f" ]; then
		path="$(file "$f" -b | sed 's/^symbolic link to //' -)"

		if [ -f "$path" ]; then
			source "$path"
		elif [ -d "$path" ]; then
			for n in "$path"/***; do
				source "$path/$n"
			done
		fi
	fi
}