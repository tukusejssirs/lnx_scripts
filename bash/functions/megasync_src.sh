#!/usr/bin/env bash

# Install megasync from source


# Install dependencies
sudo dnf -y install libtool gcc-c++ c-ares-devel cryptopp-devel openssl-devel qt-devel sqlite-devel zlib-devel LibRaw-devel

# Clone the repository
git clone --recursive git@github.com:meganz/MEGAsync.git "$XDG_GIT_DIR/others/megasync"
cd "$XDG_GIT_DIR/others/megasync/src"

# Build megatools
./configure  # -q
qmake MEGA.pro
lrelease MEGASync/MEGASync.pro
# FIXME: This does not work; see https://github.com/meganz/MEGAsync/issues/558.
make
make install