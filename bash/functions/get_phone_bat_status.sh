#!/bin/bash

# Get battery status of the first phone using KDE Connect


read -ra DEV_IDS -d';' <<< "$(kdeconnect-cli --id-only -l)"
PHONE_IS_REACHABLE=()
OUTPUT=''

if [ "${#DEV_IDS[@]}" = 1 ]; then
	SHOW_NAME='false'
else
	SHOW_NAME='true'
fi

for i in "${!DEV_IDS[@]}"; do
	PHONE_IS_REACHABLE[$i]="$(qdbus org.kde.kdeconnect "/modules/kdeconnect/devices/${DEV_IDS[$i]}" org.kde.kdeconnect.device.isReachable)"

	if [ "$OUTPUT" != '' ]; then
		OUTPUT="$OUTPUT   "
	fi

	BAT_LEVEL="$(qdbus org.kde.kdeconnect "/modules/kdeconnect/devices/${DEV_IDS[$i]}/battery" org.kde.kdeconnect.device.battery.charge 2> /dev/null)"
	BAT_IS_CHARGING="$(qdbus org.kde.kdeconnect "/modules/kdeconnect/devices/${DEV_IDS[$i]}/battery" org.kde.kdeconnect.device.battery.isCharging 2> /dev/null)"

	if [ "${PHONE_IS_REACHABLE[$i]}" = 'true' ] && ! grep -q '^Error' <<< "$BAT_LEVEL"; then

		if [ "$SHOW_NAME" = 'true' ]; then
			NAME="$(qdbus org.kde.kdeconnect "/modules/kdeconnect/devices/${DEV_IDS[$i]}" org.kde.kdeconnect.device.name | grep -o '^.') "
		fi

		if [ "$BAT_IS_CHARGING" = 'true' ]; then
			BAT_ICON='🗲'
		else
			BAT_ICON=''
		fi

		OUTPUT="$OUTPUT$NAME  $BAT_LEVEL % $BAT_ICON"
	else
		OUTPUT="$OUTPUT$NAME  n/a"
	fi
done

echo "$OUTPUT"

unset BAT_LEVEL BAT_IS_CHARGING BAT_ICON DEV_IDS NAME OUTPUT PHONE_IS_REACHABLE SHOW_NAME