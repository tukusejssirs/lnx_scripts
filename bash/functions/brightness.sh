#!/bin/bash

# This program sets brightness up or down by $step changing the int value in $brightness file

# $1       [ 'HP Z22i' | BenQG2222HDL | internal ] (required)
# $2       [ up | down ]                           (required)
# $3       [ step ]                                (default: 500 for internal/notebook screen; 3 % for external monitors)

# return codes:
# 0        success
# 1        no action input
# 2        no valid action input
# 3        return on user input (user did not have write access to $brightness)
# 4        unknown monitor was inputs


brightness() {
	get_int_current_brightness() {
		local brightness_file ret_val
		brightness_file="$1"
		ret_val=0

		if [ ! -w "$brightness_int_file" ]; then
			echo "WARNING: This scripts needs write permission to $brightness_int_file file." 2>&1
			echo
			echo 'You have two options:'
			echo '   [1] add write access to the file (you will be prompted for one-time sudo password);'
			echo '   [2] run the sudo command when necessery (you will be prompted for sudo password each time you run this script);'
			echo '   [3] quit the script.'
			echo
			echo 'What do you want to do? (default: [1])'
			while [ "$ans" != "1" ] && [ "$ans" != "2" ] && [ "$ans" != "3" ]; do
				local ans
				read -r ans
				case $ans in
					1|'')
						sudo chmod o+w "$brightness_file"
						cat "$brightness_file"
					;;
					2)
						sudo bash -c "cat '$brightness_file'"
						ret_val=1
					;;
					3)
						return 3
					;;
					*)
						echo "WARNING: $ans is not valid. Please enter valid option." 2>&1
				esac
			done
		fi

		return "$ret_val"
	}

	get_ext_current_brightness() {
		local monitor="$1"

		if ! sudo ddcutil -l "$monitor" getvcp 10 &> /dev/null; then
			echo "WARNING: This scripts needs write permission to run ddcutil. It can be run with root rights only." 2>&1
			echo
			echo 'You have two options:'
			echo '   [1] make current user run ddcutil without password (you will be prompted for one-time sudo password);'
			echo '   [2] run the sudo command when necessery (you will be prompted for sudo password each time you run this script);'
			echo '   [3] quit the script.'
			echo
			echo 'What do you want to do? (default: [1])'
			while [ "$ans" != "1" ] && [ "$ans" != "2" ] && [ "$ans" != "3" ]; do
				local ans
				read -r ans
				case $ans in
					1|'')
						sudo bash -c "echo '$USER ALL = (root) NOPASSWD: $(command -v ddcutil) *' >> /etc/sudoers.d/$USER"
					;;
					2)

					;;
					3)
						return 3
					;;
					*)
						echo "WARNING: $ans is not valid. Please enter valid option." 2>&1
				esac
			done
		fi

		if [ -f "/dev/shm/brightness_$monitor" ]; then
			brightness_ext_cur="$(< "/dev/shm/brightness_$monitor")"
		else
			brightness_ext_cur="$(sudo ddcutil -l "$monitor" getvcp 10 | grep -Po 'current value =\s*\K[0-9]+')"
			echo "$brightness_ext_cur" > "/dev/shm/brightness_$monitor"
		fi

		echo "$brightness_ext_cur"
	}

	# Variables
	local action brightness_int_file brightness_int_cur brightness_int_next brightness_ext_cur brightness_ext_next max_ext max_int step step_ext_dflt step_int_dflt su_user
	monitor="$1"
	action="${2,,}"
	step="$3"
	brightness_int_file='/sys/class/backlight/intel_backlight/brightness'
	max_ext=100
	max_int="$(< /sys/class/backlight/intel_backlight/max_brightness)"
	min_int=0  # Percentage
	min_ext=0
	step_ext_dflt=3  # Percentage
	step_int_dflt=500

	case "${monitor,,}" in
		'benqg2222hdl'|'benq'|'b')
			monitor='BenQG2222HDL'
		;;
		'philips'|'phil'|'p')
			monitor='Philips 221S'
		;;
		'hp z22i'|'hp'|'h')
			monitor='HP Z22i'
		;;
		'internal'|'i')
			monitor='internal'
		;;
	esac

	case "$action" in
		u|up)
			case "$monitor" in
				'internal')
					brightness_int_cur="$(get_int_current_brightness "$brightness_int_file")"

					if [ ! "$step" ]; then
						step="$step_int_dflt"
					fi

					brightness_int_next="$((brightness_int_cur + step))"

					if [ "$?" = 1 ]; then
						su_user='sudo'
					fi

					if [ "$brightness_int_next" -gt "$max_int" ]; then
						brightness_int_next="$max_int"
					fi

					if [ "$su_user" = 'sudo' ]; then
						"$su_user" bash -c "echo '$brightness_int_next' > '$brightness_int_file'"
					fi
				;;
				'HP Z22i'|'BenQG2222HDL'|'Philips 221S')
					brightness_ext_cur="$(get_ext_current_brightness "$monitor")"

					if [ ! "$step" ]; then
						step="$step_ext_dflt"
					fi

					brightness_ext_next="$((brightness_ext_cur + step))"

					if [ "$brightness_ext_next" -gt "$max_ext" ]; then
						brightness_ext_next="$max_ext"
					fi

					sudo ddcutil -l "$monitor" setvcp 10 "$brightness_ext_next"

					echo "$brightness_ext_next" > "/dev/shm/brightness_$monitor"
				;;
				*)
					echo 'ERROR: Unknown monitor was input.' 2>&1
					return 4
			esac
		;;
		d|down)
			case "$monitor" in
				'internal')
					brightness_int_cur="$(get_int_current_brightness "$brightness_int_file")"

					if [ ! "$step" ]; then
						step="$step_int_dflt"
					fi

					brightness_int_next="$((brightness_int_cur - step))"

					if [ "$?" = 1 ]; then
						su_user='sudo'
					fi

					if [ "$((brightness_int_cur - step))" -lt "$min_int" ]; then
						brightness_int_next="$min_int"
					fi

					if [ "$su_user" = 'sudo' ]; then
						"$su_user" bash -c "echo '$brightness_int_next' > '$brightness_int_file'"
					fi
				;;
				'HP Z22i'|'BenQG2222HDL'|'Philips 221S')
					brightness_ext_cur="$(get_ext_current_brightness "$monitor")"

					if [ ! "$step" ]; then
						step="$step_ext_dflt"
					fi

					brightness_ext_next="$((brightness_ext_cur - step))"

					if [ "$brightness_ext_next" -lt "$min_ext" ]; then
						brightness_ext_next="$min_ext"
					fi

					sudo ddcutil -l "$monitor" setvcp 10 "$brightness_ext_next"

					echo "$brightness_ext_next" > "/dev/shm/brightness_$monitor"
				;;
				*)
					echo 'ERROR: Unknown monitor was input.' 2>&1
					return 4
			esac
		;;
		*)
			if [ "$action" = '' ]; then
				echo 'ERROR: No action was input.' 2>&1
				return 1
			else
				echo "ERROR: Action $action is not valid." 2>&1
				return 2
			fi
	esac

	return 0
}