#!/bin/bash


if [ -n "$usr" ]; then
	usr='/usr'
fi

# ls, grep aliases
# TODO: Find my old `ls` aliases
if [ -x "$usr/bin/dircolors" ]; then
	if [ -r "$HOME/.dircolors" ]; then
		eval "$(dircolors -b "$HOME/.dircolors")"
	else
		eval "$(dircolors -b)"
	fi

	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
	alias ls='ls -v --color=auto'
	alias ll="ls -AlFv --color=auto --time-style='+ %a %e %b %Y, %l.%M%P '"
	alias la="ls -lhAv --color=auto --time-style='+ %a %e %b %Y, %l.%M%P '"
	alias l='ls -CFv --color=auto'
	alias lh="ls -Alhv --color=auto --time-style='+ %a %e %b %Y, %l.%M%P '"
fi

# Coloured GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Reload the $HOME/.bashrc file
alias srcb="source \$HOME/.bashrc"

# Move back into previous directory; useful when you change between two different dirs, but useless when going up and down in a directory tree
alias back="cd \$OLDPWD"

# Make bc quite (that it does not show the info part every time I run it) and use math library (without it e.g. 5/6 outputs 0; with it 5/6 outputs .83333333333333333333)
alias bc="bc -lq"

# trans (this takes some time to load)
if [ "$(trans -V 2> /dev/null | head -1 | grep -o 'Translate Shell')" = 'Translate Shell' ]; then
	for n in {af,am,ar,az,ba,be,bg,bn,bs,ca,ceb,co,cs,cy,da,de,el,emj,en,eo,es,et,eu,fa,fi,fj,fr,fy,ga,gd,gl,gu,ha,haw,he,hi,hmn,hr,ht,hu,hy,id,ig,is,it,ja,jv,ka,kk,km,kn,ko,ku,ky,la,lb,lo,lt,lv,mg,mhr,mi,mk,ml,mn,mr,mrj,ms,mt,mww,my,ne,nl,no,ny,otq,pa,pap,pl,ps,pt,ro,ru,sd,si,sk,sl,sm,sn,so,sq,sr-cyrl,sr-latn,st,su,sv,sw,ta,te,tg,th,tl,tlh,tlh-qaak,to,tr,tt,ty,udm,uk,ur,uz,vi,xh,yi,yo,yua,yue,zh-cn,zh-tw,zu}; do

		for m in {af,am,ar,az,ba,be,bg,bn,bs,ca,ceb,co,cs,cy,da,de,el,emj,en,eo,es,et,eu,fa,fi,fj,fr,fy,ga,gd,gl,gu,ha,haw,he,hi,hmn,hr,ht,hu,hy,id,ig,is,it,ja,jv,ka,kk,km,kn,ko,ku,ky,la,lb,lo,lt,lv,mg,mhr,mi,mk,ml,mn,mr,mrj,ms,mt,mww,my,ne,nl,no,ny,otq,pa,pap,pl,ps,pt,ro,ru,sd,si,sk,sl,sm,sn,so,sq,sr-cyrl,sr-latn,st,su,sv,sw,ta,te,tg,th,tl,tlh,tlh-qaak,to,tr,tt,ty,udm,uk,ur,uz,vi,xh,yi,yo,yua,yue,zh-cn,zh-tw,zu}; do

			if [[ $n != "$m" ]]; then
				aliasName="tr$n$m"

				if [[ $n = "tlh" ]] || [[ $m = "tlh" ]] || [[ $n = "tlh-qaak" ]] || [[ $m = "tlh-qaak" ]] || [[ $n = "otq" ]] || [[ $m = "otq" ]]; then
					# shellcheck disable=SC2139  # This expands when defined, not when used. Consider escaping.
					alias $aliasName="trans -e bing $n:$m"
				elif [[ $n = "emj" ]] || [[ $m = "emj" ]]; then
					# shellcheck disable=SC2139  # This expands when defined, not when used. Consider escaping.
					alias $aliasName="trans -e yandex $n:$m"
				else
					# shellcheck disable=SC2139  # This expands when defined, not when used. Consider escaping.
					alias $aliasName="trans $n:$m"
				fi
			fi
		done
	done
fi

# MM: FTP alias
if [[ -e "$HOME/.mm.cz/ftp" ]]; then
	alias mm="lftp -u mrtvamanzelkacz,\$(bw get item 48bf9816-5951-4eec-845c-aa72007ef132 | jq -r .login.password) ftpx.forpsi.com"
fi

# alias histuniq="tac ${HOME}/.bash_history_ts | cat -n | sort -uk2 | sort -nk1 | cut -f2- | tac > ${HOME}/.bash_history_ts"

# Colourise `tree`
alias tree="tree -C"

# Elementary Planner
if [ -e /usr/bin/com.github.alainm23.planner ]; then
	alias planner='/usr/bin/com.github.alainm23.planner'
fi

# Gnome Contrast
if command -v contrast &> /dev/null; then
	alias contrast="GSETTINGS_SCHEMA_DIR='/opt/share/glib-2.0/schemas/' contrast"
fi

# Shortcuts to use visudo with `nano` text editor
# Note: I could just change the EDITOR variable and use `visudo` directly, but using it like this always works (when `nano` is installed), like when using on a company's server.
alias nasudo='EDITOR="/bin/nano -u" visudo'
alias nasudoc='EDITOR="/bin/nano -u" visudo -f /etc/sudoers.d/controlserver'

# eID client app
if command -v EAC_MW_klient &> /dev/null; then
	alias eid='EAC_MW_klient'
fi

# Opera web browser (dev edition)
if command -v opera-developer &> /dev/null; then
	alias opera='opera-developer'
fi