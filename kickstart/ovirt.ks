#!/bin/bash

#version=RHEL8
ignoredisk --only-use=sdb
# Partition clearing information
clearpart --all --initlabel --drives=sdb
# Use graphical install
graphical
# Use CDROM installation media
cdrom
# Keyboard layouts
keyboard --vckeymap=gb --xlayouts='gb'
# System language
lang en_GB.UTF-8

# Network information
network  --bootproto=dhcp --device=enp6s0 --ipv6=auto --activate
network  --hostname=ovirt.domcek.lan
repo --name="AppStream" --baseurl=file:///run/install/repo/AppStream
# Root password
rootpw --iscrypted $6$iXW8EGu3w5mOvgBO$Cf1LeXBi5x/qD5RsyOIA8fAJmeIbIsbXl/vBqJcfA2Gl/LRzL1aFdNHJDPWM4CucGaB085k8.h2qVoF6.0j4.1
# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System services
services --enabled="chronyd"
# System timezone
timezone Europe/Bratislava --ntpservers=2.centos.pool.ntp.org,2.centos.pool.ntp.org,2.centos.pool.ntp.org,2.centos.pool.ntp.org
# Disk partitioning information
part /boot/efi --fstype="efi" --ondisk=sdb --size=512 --fsoptions="umask=0077,shortname=winnt" --label=boot_efi
part /boot --fstype="ext4" --ondisk=sdb --size=1024 --label=boot
part pv.786 --fstype="lvmpv" --ondisk=sdb --size=101384
volgroup vg_ovirt --pesize=4096 pv.786
logvol / --fstype="ext4" --size=97280 --label="root" --name=root --vgname=vg_ovirt
logvol swap --fstype="swap" --size=4096 --name=swap --vgname=vg_ovirt

%packages
@^virtualization-host-environment
@headless-management
@remote-system-management
@virtualization-platform
kexec-tools

%end

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end