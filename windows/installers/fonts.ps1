# This script installs select fonts (Ubuntu, Libertinus and Linux Libertine G). By default it installs all fonts.

# dependencies (Chocolatey): 7zip curl grep


if (! "$env:git") {
	$env:git = "$env:userprofile\git"
}

# Libertinus
$url_zip = curl.exe -s https://api.github.com/repos/alerque/libertinus/releases/latest | jq -r '.assets[] | select(.content_type == \"application/zip\").browser_download_url'
curl.exe -sLo "$env:temp\libertinus.zip" "$url_zip"
7z e -i!Libertinus-*\static\OTF\* -o"$env:windir\fonts" "$env:temp\libertinus.zip" > $null
rm "$env:temp\libertinus.zip"

# Linux Libertine G
curl.exe -sLo "$env:temp\linlibg.zip" http://www.numbertext.org/linux/e7a384790b13c29113e22e596ade9687-LinLibertineG-20120116.zip
7z e -i"!LinLibertineG\*.ttf" -o"$env:windir\fonts" "$env:temp\linlibg.zip" > $null
rm "$env:temp\linlibg.zip"

# Ubuntu
curl.exe -sLo "$env:temp\ubuntu.zip" https://assets.ubuntu.com/v1/0cef8205-ubuntu-font-family-0.83.zip
7z e -i"!ubuntu-font-family-0.83\*.ttf" -o"$env:windir\fonts" "$env:temp\ubuntu.zip" > $null
rm "$env:temp\ubuntu.zip"

# Ezra SIL
$ezra_url = curl.exe -s https://software.sil.org/ezra/ | grep -o 'https://software.sil.org/downloads/r/ezra/EzraSIL-[0-9.]*-source.zip'
curl.exe -so "$env:temp/ezra.zip" "$ezra_url"
7z e -i"!EzraSIL*\*.ttf" -o"$env:windir\fonts" "$env:temp\ezra.zip" > $null
rm "$env:temp\ezra.zip"

# SBL Hebrew
curl.exe -so "$env:windir\fonts\sbl_hebrew.ttf" http://www.sbl-site.org/Fonts/SBL_Hbrw.ttf

# Junicode
curl.exe -sLo "$env:temp/junicode.zip" https://sourceforge.net/projects/junicode/files/latest/download
7z e -i"!*.ttf" -o"$env:windir\fonts" "$env:temp\junicode.zip" > $null