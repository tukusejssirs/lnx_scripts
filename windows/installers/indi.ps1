# Adobe Indegisn CC initialisation

# dependencies: VcRedist


# TODO
# THE SCRIPT IS CURRENTLY WRITTEN IN BATCH, NOT POWERSHELL
# Export/import Adobe Indesign CC settings
# - dictionaries
#   - C:\Program Files\Adobe\Adobe InDesign CC 2018\Resources\Dictionaries\LILO\Linguistics\Providers\Plugins2\AdobeHunspellPlugin\Dictionaries
#   - textual files (mm/dicts repo)
# - preferences
# - print presets
# - scripts
# - indi ts_workspace
# - indi preferences


# Variables
$indi_path = "$env:userprofile\git\os_backups\t5500\indi_prtbl"

# Clone the repo
mkdir "$indi_path" -erroraction silentlycontinue > $null
mkdir "$ff_path\roaming\adobe" -erroraction silentlycontinue > $null
mkdir "$ff_path\local\adobe" -erroraction silentlycontinue > $null
mkdir "$ff_path\locallow\adobe" -erroraction silentlycontinue > $null
wsl git clone git@gitlab.com:os_backups/t5500/indi_prtbl.git "$indi_path"
wsl git clone git@gitlab.com:os_backups/t5500/appdata/roaming/adobe.git "$ff_path\roaming\adobe"
wsl git clone git@gitlab.com:os_backups/t5500/appdata/local/adobe.git "$ff_path\local\adobe"
wsl git clone git@gitlab.com:os_backups/t5500/appdata/locallow/adobe.git "$ff_path\locallow\adobe"

# Link a folder into AppData
mv "$env:userprofile\AppData\Roaming\Adobe" "$env:userprofile\AppData\Roaming\Adobe.bak" -erroraction silentlycontinue > $null
mv "$env:userprofile\AppData\Local\Adobe" "$env:userprofile\AppData\Local\Adobe.bak" -erroraction silentlycontinue > $null
mv "$env:userprofile\AppData\LocalLow\Adobe" "$env:userprofile\AppData\LocalLow\Adobe.bak" -erroraction silentlycontinue > $null
cmd /c mklink /d "$env:userprofile\AppData\Roaming\Adobe" "$ff_path\roaming\Adobe"
cmd /c mklink /d "$env:userprofile\AppData\Local\Adobe" "$ff_path\local\Adobe"
cmd /c mklink /d "$env:userprofile\AppData\LocalLow\Adobe" "$ff_path\locallow\Adobe"

# Pin the program to taskbar
# TODO: When running InDesignPortable, it won’t output the user scripts, but when running Indesign.exe directly, it does
pinutil taskbar "$indi_path\InDesignPortable.exe"  # Open InDesign using this program
# pinutil taskbar "$indi_path\App\Id\InDesign.exe"   # This is actual program when running

##############
##############
##############

# # Variables
# set "lang=en_GB"
# set "indi_year=2018"
# set "indi_ver=13.0"

# # Create startup scripts folder
# # TODO: Change the target path to be relative to the %repo_root% path
# cmd /c  /d "%systemdrive%\Program Files\Adobe\Adobe InDesign CC %indi_year%\Scripts\Startup Scripts" "%systemdrive%\Users\%username%\git\mm\adobe_indesign\scripts\startup_scripts"

# # Create user scripts folder
# # TODO: Change the target path to be relative to the %repo_root% path
# cmd /c  /d "%appdata%\Adobe\InDesign\Version %indi_ver%\%lang%\Scripts\Scripts Panel" "%systemdrive%\Users\%username%\git\mm\adobe_indesign\scripts\user_scripts"

# # Create Find-Change Queries folder
# # TODO: Change the target path to be relative to the %repo_root% path
# cmd /c  /d "%appdata%\Adobe\InDesign\Version %indi_ver%\%lang%\Find-Change Queries" "%systemdrive%\Users\%username%\git\mm\adobe_indesign\find-change_queries"

# # Create InDesign Shortcut Sets folder
# # TODO: Change the target path to be relative to the %repo_root% path
# cmd /c  /d "%appdata%\Adobe\InDesign\Version %indi_ver%\%lang%\InDesign Shortcut Sets" "%systemdrive%\Users\%username%\git\mm\adobe_indesign\indesign_shortcut_sets"

# # Create Autocorrect folder
# # TODO: Change the target path to be relative to the %repo_root% path
# cmd /c  /d "%appdata%\Adobe\InDesign\Version %indi_ver%\%lang%\Autocorrect" "%systemdrive%\Users\%username%\git\mm\adobe_indesign\autocorrect"

# # Create Page Sizes folder
# # TODO: Change the target path to be relative to the %repo_root% path
# cmd /c  /d "%appdata%\Adobe\InDesign\Version %indi_ver%\%lang%\Page Sizes" "%systemdrive%\Users\%username%\git\mm\adobe_indesign\page_sizes"

# # Create Workspaces folder
# # TODO: Change the target path to be relative to the %repo_root% path
# cmd /c  /d "%appdata%\Adobe\InDesign\Version %indi_ver%\%lang%\Workspaces" "%systemdrive%\Users\%username%\git\mm\adobe_indesign\workspaces"pinutil taskbar "$indi_path\InDesignPortable.exe"