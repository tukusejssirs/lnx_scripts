# Enable the 'Virtual Machine Platform' optional component and make sure WSL is enabled
dism /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart