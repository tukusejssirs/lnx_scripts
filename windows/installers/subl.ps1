# Clone the repo
$subl_path = "$env:userprofile\git\os_backups\t5500\subl"
mkdir "$subl_path" -erroraction silentlycontinue > $null
wsl git clone git@gitlab.com:os_backups/t5500/subl.git "$subl_path"

# Pin the program to taskbar
pinutil taskbar "$subl_path\sublime_text.exe"