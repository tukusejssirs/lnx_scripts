# Firefox Nightly initialisation

# Variables
$temp_path = "$env:userprofile\Downloads"
$ff_path = "$env:userprofile\git\os_backups\t5500\appdata"

# Download the program
curl -o "$temp_path\ff_nightly.msi" "https://download.mozilla.org/?product=firefox-nightly-msi-latest-l10n-ssl&os=win64&lang=en-GB"

# Install the program
msiexec.exe /i "$temp_path\ff_nightly.msi" TASKBAR_SHORTCUT=false DESKTOP_SHORTCUT=false INSTALL_MAINTENANCE_SERVICE=false /q

# Clone the repos
mkdir "$ff_path\roaming\mozilla" -erroraction silentlycontinue > $null
mkdir "$ff_path\local\mozilla" -erroraction silentlycontinue > $null
mkdir "$ff_path\locallow\mozilla" -erroraction silentlycontinue > $null
wsl git clone git@gitlab.com:os_backups/t5500/appdata/roaming/mozilla.git "$ff_path\roaming\mozilla"
wsl git clone git@gitlab.com:os_backups/t5500/appdata/local/mozilla.git "$ff_path\local\mozilla"
wsl git clone git@gitlab.com:os_backups/t5500/appdata/locallow/mozilla.git "$ff_path\locallow\mozilla"

# Link a folder into AppData
mv "$env:userprofile\AppData\Roaming\Mozilla" "$env:userprofile\AppData\Roaming\Mozilla.bak" -erroraction silentlycontinue > $null
mv "$env:userprofile\AppData\Local\Mozilla" "$env:userprofile\AppData\Local\Mozilla.bak" -erroraction silentlycontinue > $null
mv "$env:userprofile\AppData\LocalLow\Mozilla" "$env:userprofile\AppData\LocalLow\Mozilla.bak" -erroraction silentlycontinue > $null
cmd /c mklink /d "$env:userprofile\AppData\Roaming\Mozilla" "$ff_path\roaming\mozilla"
cmd /c mklink /d "$env:userprofile\AppData\Local\Mozilla" "$ff_path\local\mozilla"
cmd /c mklink /d "$env:userprofile\AppData\LocalLow\Mozilla" "$ff_path\locallow\mozilla"

# Pin the program to taskbar
pinutil taskbar "$env:programfiles\Firefox Nightly\firefox.exe"