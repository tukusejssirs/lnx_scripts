# Mailspring initialisation

# Variables
$temp_path = "$env:userprofile\Downloads"
$mailspring_path = "$env:userprofile\git\os_backups\t5500\appdata\roaming\mailspring"

# Download the program
curl -o "$temp_path\mailspring.exe" https://updates.getmailspring.com/download?platform=win32

# Install the program
start-process "$temp_path\mailspring.exe" -ArgumentList /silent -Wait -NoNewWindow

# Clone the repo
# TODO: This does not work yet (Mailspring cannot find Mailspring ID after reinstall)
# mkdir "$mailspring_path" -erroraction silentlycontinue > $null
# wsl git clone git@gitlab.com:os_backups/t5500/appdata/roaming/mailspring.git "$mailspring_path"

# Link a folder into AppData
mv "$env:appdata\Mailspring" "$env:appdata\Mailspring.bak" -erroraction silentlycontinue > $null
cmd /c mklink /d "$env:appdata\Mailspring" "$mailspring_path"

# Run the program
start-process $env:userprofile\AppData\Local\Mailspring\mailspring.exe