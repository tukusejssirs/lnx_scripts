#!/bin/bash

# Get Office Deployment Tool to download and install MS Office


# Download ODT
curl -so odt.exe https://download.microsoft.com/download/2/7/A/27AF1BE6-DD20-4CB4-B154-EBAB8A7D4A7E/officedeploymenttool_13929-20296.exe

# Extract the ODT
wine odt.exe /extract:. /quiet