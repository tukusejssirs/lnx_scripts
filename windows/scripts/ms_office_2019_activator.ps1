# Automatic MS Office 2019 licence renewal


# Variables
$act_path = "$env:systemdrive\ms_office_activator"
$activator = "$act_path\Microsoft-Activation-Scripts-master\MAS_1.4\Separate-Files-Version\Activators\Online_KMS_Activation\Activate.cmd"

# Remove the folder
rmdir -force -recurse "$act_path" > $null

# Create the folder
mkdir "$act_path" -erroraction silentlycontinue > $null

# Add a Defender exclusion
# TODO: Check if Defender is enabled (e.g. when Avast is installed, Defender does not act as antivirus and thus even when it is enabled, we can't create an exception)
Add-MpPreference -ExclusionPath "$act_path"

# Download the activator
curl -o "$act_path\act.zip" https://github.com/massgravel/Microsoft-Activation-Scripts/archive/master.zip

# Extract the activator
Expand-Archive -Path "$act_path\act.zip" -DestinationPath "$act_path"

# Create a job in Task Scheduler
$action = New-ScheduledTaskAction -Execute "powershell.exe" -Argument "-WindowStyle Hidden Start-Process -FilePath $env:systemdrive\ms_office_activator\Microsoft-Activation-Scripts-master\MAS_1.4\Separate-Files-Version\Activators\Online_KMS_Activation\Activate.cmd -NoNewWindow"
$trigger = New-ScheduledTaskTrigger -AtLogon
$principal = New-ScheduledTaskPrincipal -GroupId 'BUILTIN\Administrators' -RunLevel Highest
$task = New-ScheduledTask -Action $action -Principal $principal -Trigger $trigger
$task | Register-ScheduledTask -TaskName 'ms_office_2019_re-activation'

# Activate MS Office 2019
# TODO: Are both commands necessary? Don't they do the same thing? If yes, I think we should keep the second command.
#  - If running from powershell/pwsh, use the first command, elif from cmd, use the second one.
#  - TODO: Check if we are running powershell/pwsh.
Start-Process -FilePath "$activator" -NoNewWindow
powershell.exe -WindowStyle Hidden Start-Process -FilePath "$activator" -NoNewWindow