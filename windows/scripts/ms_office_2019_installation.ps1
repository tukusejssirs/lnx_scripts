# TODO: make the path of `setup.exe` variable
# TODO: make the config.xml path and filename variable

# Create configuration [example name: `config.xml`]: https://config.office.com

# Download the installation files [this needs to be run as administration on Windows]
./setup.exe /download config.xml

# Install MS Office [this needs to be run as administration on Windows]
./setup.exe /configure config.xml