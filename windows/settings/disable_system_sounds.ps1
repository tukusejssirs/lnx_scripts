# Disable system sounds by setting all sound schemes to use no sound
# src: https://stackoverflow.com/a/63313425/3408342
$Path = 'HKCU:\AppEvents\Schemes'
$Keyname = '(Default)'
$SetValue = '.None'
$TestPath = Test-Path $Path

# Create $Path if it does not exist
if (-Not ($TestPath -eq $True)) {
	New-item $path -force
}

if (Get-ItemProperty -path $Path -name $KeyName -EA SilentlyContinue) {
	$Keyvalue = (Get-ItemProperty -path $Path).$keyname

	# Change the key value if it is different from the required value
	if ($KeyValue -ne $setValue) {
		# Set 'No Sound' Schemes
		New-itemProperty -path $Path -Name $keyname -value $SetValue -force
		# Apply 'No Sound' Schemes
		Get-ChildItem -Path 'HKCU:\AppEvents\Schemes\Apps' | Get-ChildItem | Get-ChildItem | Where-Object { $_.PSChildName -eq '.Current' } | Set-ItemProperty -Name '(Default)' -Value ''
	}
} else {
	# Create key
	New-itemProperty -path $Path -Name $keyname -value $SetValue -force
	Get-ChildItem -Path 'HKCU:\AppEvents\Schemes\Apps' | Get-ChildItem | Get-ChildItem | Where-Object { $_.PSChildName -eq '.Current' } | Set-ItemProperty -Name '(Default)' -Value ''
}