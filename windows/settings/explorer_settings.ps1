# Enable `Show hidden files, folders, and drives`
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced Hidden 1

# Disable `Hide extensions for known file types`
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced HideFileExt 0

# Disable `Hide protected operating system files (Recommended)`
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced ShowSuperHidden 1

# Hide Cortana button from the panel
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced ShowCortanaButton 0

# Disable panel animations
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced TaskbarAnimations 0

# Disable panel animations
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Ribbon MinimizedStateTabletModeOff 0

# Set `Open File Explorer to` to `This PC`
Set-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced LaunchTo 1

# Hide `Documents` folder from This PC
Set-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{f42ee2d3-909f-4907-8871-4c22fc0bf756}\PropertyBag' ThisPCPolicy 'Hide'

# Hide `Pictures` folder from This PC
Set-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{0ddd015d-b06c-45d5-8c4c-f59713854639}\PropertyBag' ThisPCPolicy 'Hide'

# Hide `Videos` folder from This PC
Set-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{35286a68-3c57-41a1-bbb1-0eae73d76c95}\PropertyBag' ThisPCPolicy 'Hide'

# Hide `Downloads` folder from This PC
Set-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{7d83ee9b-2244-4e70-b1f5-5393042af1e4}\PropertyBag' ThisPCPolicy 'Hide'

# Hide `Music` folder from This PC
Set-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{a0c69a99-21c8-4671-8703-7934162fcf1d}\PropertyBag' ThisPCPolicy 'Hide'

# Hide `Desktop` folder from This PC
Set-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FolderDescriptions\{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}\PropertyBag' ThisPCPolicy 'Hide'



# Restart Explorer
Stop-Process -processname explorer