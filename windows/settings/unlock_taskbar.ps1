# Unlock the taskbar (panel)
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name TaskbarSizeMove -Value 1 -Type DWord -Force

# Restart Explorer
Stop-Process -processname explorer