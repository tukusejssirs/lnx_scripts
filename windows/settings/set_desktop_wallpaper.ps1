# Remove wallpaper
Set-ItemProperty -Path HKCU:\Control Panel\Desktop -Name WallPaper -Value '' -Force

# Use solid colour as wallpaper
Set-ItemProperty -Path HKCU:\Control Panel\Colors -Name Background -Value '0 0 0' -Force

# Restart Explorer
Stop-Process -processname explorer