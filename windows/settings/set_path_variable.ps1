# Set path environment variable
$prgs_path = "$env:systemdrive\progs"
mkdir "$prgs_path" -erroraction silentlycontinue > $null
$old_path = [System.Environment]::GetEnvironmentVariable('path') -replace ';$', ''

if ($old_path -notlike "*$env:systemdrive\progs*") {
	$new_path = "$old_path;$prgs_path"
	[Environment]::SetEnvironmentVariable('path', "$new_path")
}