#!/bin/bash

# Instal girmshot


git clone git@github.com:swaywm/sway.git "$XDG_GIT_DIR/others/sway"
sudo ln -s "$XDG_GIT_DIR/others/sway/contrib/grimshot" /opt/bin/grimshot
sudo ln -s "$XDG_GIT_DIR/others/sway/contrib/grimshot.1" /usr/local/share/man/man1/grimshot.1
sudo mandb


# On Arch Linux (however, it might be better to use the instructions above)
yay -Sy grimshot-git