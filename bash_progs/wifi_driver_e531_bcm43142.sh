#!/bin/bash

# Install WiFi driver for Brodcomm BCM43142


# src: https://tanmaync.wordpress.com/2017/12/02/install-broadcom-bcm43142-wifi-drivers-fedora/


# Check if the chip is present; if yes, install the driver (module)
test_wifi=$(lspci | grep -o BCM43142)
if [ "$test_wifi" = "BCM43142" ]; then
	sudo dnf -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %centos).noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-$(rpm -E %centos).noarch.rpm
	sudo dnf -y install kmod-wl akmod-wl kernel-devel
fi
