#!/bin/bash

# Install Waterfox

# dependencies: jq curl

# src: https://github.com/hawkeye116477/install-waterfox-linux/blob/master/install_waterfox_current.sh


# Variables
TMP='/dev/shm'
INSTALLATION_PATH='/opt'
WF_TYPE='current'  # current | classic

# Download Waterfox
URL="$(curl -Ls https://www.waterfox.net/download | grep -o "https://cdn.waterfox.net/releases/linux64/installer/waterfox-${WF_TYPE}[^\"]*")"
curl -sLo "$TMP/waterfox.tar.bz2" "$URL"

# Remove old Waterfox installation if any
sudo rm -rf "$INSTALLATION_PATH/waterfox"

# Extract Waterfox
sudo tar xf "$TMP/waterfox.tar.bz2" -C "$INSTALLATION_PATH"

# Create a symlink to Waterfox into /opt/bin/waterfox if it is extracted into /opt
if [ "$INSTALLATION_PATH" = '/opt' ]; then
	sudo ln -s "$INSTALLATION_PATH/waterfox/waterfox" "$INSTALLATION_PATH/bin/waterfox"
fi

# Clean up
rm -rf "$TMP/waterfox.tar"

# Create symlinks to icons
sudo ln -sf "$INSTALLATION_PATH/waterfox/browser/chrome/icons/default/default16.png /usr/share/icons/hicolor/16x16/apps/waterfox.png"
sudo ln -sf "$INSTALLATION_PATH/waterfox/browser/chrome/icons/default/default32.png /usr/share/icons/hicolor/32x32/apps/waterfox.png"
sudo ln -sf "$INSTALLATION_PATH/waterfox/browser/chrome/icons/default/default48.png /usr/share/icons/hicolor/48x48/apps/waterfox.png"
sudo ln -sf "$INSTALLATION_PATH/waterfox/browser/chrome/icons/default/default64.png /usr/share/icons/hicolor/64x64/apps/waterfox.png"
sudo ln -sf "$INSTALLATION_PATH/waterfox/browser/chrome/icons/default/default128.png /usr/share/icons/hicolor/128x128/apps/waterfox.png"

# Create a desktop file in /usr/share/applications
sudo install -Dm644 /dev/stdin /usr/share/applications/waterfox.desktop << EOF
[Desktop Entry]
Version=1.0
Name=Waterfox
GenericName=Web Browser
Comment=Browse the World Wide Web
Exec=$INSTALLATION_PATH/waterfox/waterfox %u
Icon=/usr/share/icons/hicolor/128x128/apps/waterfox.png
Terminal=false
Type=Application
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;x-scheme-handler/chrome;video/webm;application/x-xpinstall;
StartupNotify=false
Categories=Network;WebBrowser;
Keywords=Internet;WWW;Browser;Web;Explorer;
; X-MuiltpleArgs=false
Actions=NewWindow;NewPrivateWindow;

[Desktop Action new-window]
Name=New Window
Exec=$INSTALLATION_PATH/waterfox/waterfox --new-window %u

[Desktop Action new-private-window]
Name=New Private Window
Exec=$INSTALLATION_PATH/waterfox/waterfox --private-window %u
EOF



###################
###################
###################

# # Install dependencies
# # TODO: Add all the dependencies
# sudo dnf -y install clang yasm autoconf213 libXt-devel

# # Clone Waterfox repo
# # Note: Packed data (objects) size is about 4 GiB, therefore downloading, unpacking and resolving daltas takes quite long; it took me 75 minutes
# git clone git@github.com:MrAlex94/Waterfox.git "$XDG_GIT_DIR/others/waterfox"
# cd "$XDG_GIT_DIR/others/waterfox"

# ./mach bootstrap  # Choose option 2 (Firefox Destkop) and accept everything else (press `y`) except for telemetry
# # Note: This took about 3 hours
# ./mach build



# To take your build for a test drive, run: |mach run|
# For more information on what to do now, see https://developer.mozilla.org/docs/Developer_Guide/So_You_Just_Built_Firefox
#
# profile folder: /git/others/waterfox/objdir-current/tmp/profile-default