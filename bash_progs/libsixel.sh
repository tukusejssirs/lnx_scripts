#!/bin/bash

# Install libsixel


INSTALL_PATH="$XDG_GIT_DIR/others/libsixel"

git clone git@github.com:saitoha/libsixel.git "$INSTALL_PATH"
cd "$INSTALL_PATH" && ./configure --with-libcurl --with-gdk-pixbuf2 --with-jpeg --with-png && make && sudo make install