#!/bin/bash

# This script installs xclip from source



sudo dnf -y install libXmu
old_pwd="$PWD"
mkdir -p "${XDG_GIT_DIR}/others/xclip"

if [ $(ssh -T git@gitlab.com) = 'Permission denied (publickey).' ]; then
	url='https://github.com/astrand/xclip.git'
else
	url='git@github.com:astrand/xclip.git'
fi

git clone $url "${XDG_GIT_DIR}/others/xclip"
cd "${XDG_GIT_DIR}/others/xclip"
autoreconf
./configure
cd "$old_pwd"