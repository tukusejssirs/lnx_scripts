#!/bin/bash

# Install rkflashkit on Fedora


# Install dependencies
# alien : to convert deb package to rpm
sudo dnf -y install alien

git clone git@github.com:linuxerwang/rkflashkit.git "$XDG_GIT_DIR/others/rkflashkit"
cd "$XDG_GIT_DIR/others/rkflashkit" && ./waf debian
sudo apt-get install python-gtk2
sudo alien -r rkflashkit_0.1.1_all.deb
sudo dnf -y install ./rkflashkit_0.1.1_all.deb

# `./waf debian` errors:
'debian' finished successfully (0.000s)
Setting top to                           : /git/others/rkflashkit
Setting out to                           : /git/others/rkflashkit/build
Checking for program python              : /usr/bin/python
Checking for python version              : (3, 8, 5, 'final', 0)
'configure' finished successfully (0.101s)
Traceback (most recent call last):
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Node.py", line 282, in ant_iter
		raise StopIteration
StopIteration

The above exception was the direct cause of the following exception:

Traceback (most recent call last):
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Scripting.py", line 97, in waf_entry_point
		run_commands()
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Scripting.py", line 153, in run_commands
		ctx=run_command(cmd_name)
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Scripting.py", line 146, in run_command
		ctx.execute()
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Scripting.py", line 351, in execute
		return execute_method(self)
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Build.py", line 105, in execute
		self.load_envs()
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Build.py", line 81, in load_envs
		lst=node.ant_glob('**/*%s'%CACHE_SUFFIX,quiet=True)
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Node.py", line 331, in ant_glob
		ret=[x for x in self.ant_iter(accept=accept,pats=[to_pat(incl),to_pat(excl)],maxdepth=25,dir=dir,src=src,remove=kw.get('remove',True))]
	File "/git/others/rkflashkit/.waf3-1.7.11-edc6ccb516c5e3f9b892efc9f53a610f/waflib/Node.py", line 331, in <listcomp>
		ret=[x for x in self.ant_iter(accept=accept,pats=[to_pat(incl),to_pat(excl)],maxdepth=25,dir=dir,src=src,remove=kw.get('remove',True))]
RuntimeError: generator raised StopIteration