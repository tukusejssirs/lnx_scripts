#!/bin/bash

# Install Thunderbird Beta


# Variables
TEMP='/dev/shm'
BASH_SHEBANG='#!/bin/bash'

# Download
mkdir -p $TEMP /opt/bin
sudo rm -rf /opt/thunderbird
curl -sLo "$TEMP/thunderbird.tar.bz2" 'https://download.mozilla.org/?product=thunderbird-beta-latest&os=linux64&lang=en-GB'
sudo tar xf "$TEMP/thunderbird.tar.bz2" -C /opt
rm "$TEMP/thunderbird.tar.bz2"

# Create a script to run FF Nightly in `/opt/bin`
# Note: The `TZ=UTC` sets the timezone to forbid potential browser fingerprinting (src: https://wiki.archlinux.org/index.php/Firefox/Privacy#Change_browser_time_zone)
# echo -e "$BASH_SHEBANG\nTZ=UTC /opt/thunderbird/thunderbird \$@" | sudo tee /opt/bin/thunderbird > /dev/null
sudo bash -c "echo -e '$BASH_SHEBANG\nTZ=UTC /opt/thunderbird/thunderbird \$@' > /opt/bin/thunderbird"
sudo chmod a+x /opt/bin/thunderbird
sudo chown -R root:root /opt/bin/thunderbird /opt/thunderbird