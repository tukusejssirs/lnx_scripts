#!/bin/bash

# This script installs MuseScore


sudo mkdir -p /opt/mscore /opt/bin
sudo curl -Lso /opt/mscore/musescore-x86_64.appimage "$(curl -s "https://musescore.org/en/download/musescore-x86_64.AppImage" | grep -Po "Please use this <a href=\"\K[^\"]*")"
sudo chmod a+x /opt/mscore/musescore-x86_64.appimage
sudo ln -s /opt/mscore/musescore-x86_64.appimage /opt/bin/mscore