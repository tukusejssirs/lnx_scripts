#!/bin/bash

# Install Geary systray icon GNOME extension


# TODO
# - after I have create the `gext.sh` bash function, use it instead of manually download and enable this extension


git clone git@github.com:TheBigFatTony/gnome-shell-extension-geary-tray-icon.git ~/.local/share/gnome-shell/extensions/geary-tray-icon@TheBigFatTony.github.com
# Or using HTTPS instead of SSH
# git clone https://github.com/TheBigFatTony/gnome-shell-extension-geary-tray-icon.git ~/.local/share/gnome-shell/extensions/geary-tray-icon@TheBigFatTony.github.com

# Get list of enabled extensions
SHELL_ENABLED_EXTS="$(gsettings get org.gnome.shell enabled-extensions | sed '/^@as/d;s/[][]//g')"

# Enable the extension
if [ "$SHELL_ENABLED_EXTS" ]; then  # When there are some extensions already enabled
	# Check if extension is already enabled
	TMP_ENABLE_TEST="$(grep -o 'geary-tray-icon@TheBigFatTony.github.com' <<< "${SHELL_ENABLED_EXTS}")"

	# If the extension is not enabled already, enable it
	if [ ! "$TMP_ENABLE_TEST" ]; then
		gsettings set org.gnome.shell enabled-extensions "[${SHELL_ENABLED_EXTS},'geary-tray-icon@TheBigFatTony.github.com']"
	fi
else  # When there are no extensions enabled yet
	# Enable the extension
	gsettings set org.gnome.shell enabled-extensions "['geary-tray-icon@TheBigFatTony.github.com']"
fi