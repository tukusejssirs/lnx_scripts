#!/bin/bash

# Install eID client (Slovakia) and D.Launcher

# src: https://www.develart.sk/blog/eid-klient-fedora-opensuse-linux-repozitar/235.htm
# src: https://www.ditec.sk/produkty/informacie_pre_pouzivatelov_aplikacii_pre_kep


# Install eID client
sudo dnf config-manager --add-repo https://www.salstar.sk/pub/fedora/salstar.repo
sudo dnf -y install eidklient

# eID app name: EAC_MW_klient
# eID app location: /usr/bin/EAC_MW_klient


# Enable `pcscd` service that communicates with the smart card reader
sudo systemctl enable --now pcscd


#### D.LAUNCHER INSTALLATION IS NOT IN A WORKING STATE

# Install D.Launcher
# sudo dnf -y install desktop-file-utils  # icedtea-web
# java-latest-openjdk

URL="https://www.ditec.sk/$(curl -s https://www.ditec.sk/produkty/informacie_pre_pouzivatelov_aplikacii_pre_kep | grep -Po '[.]{2}/\K[^"]+DLauncher.linux.x86_64.run')"
curl -sLo /tmp/d.launcher.run "$URL"
chmod a+x /tmp/d.launcher.run
/tmp/d.launcher.run  # Follow the GUI instructions
rm -rf /tmp/d.launcher.run
_JAVA_AWT_WM_NONREPARENTING=1 ~/.ditec/dlauncher-bin/dLauncher


# Install
# /tmp/d.launcher.run
# TODO
# - select lang: sk | en;
# - sign EULA;
# - [TODO] make it work using global/custom installation path;
# - run D.Launcher and perform initial setup:
# 	- TODO: check if Firefox is running; if yes, ask to close it with an option (a button) to quit it;
# 	- install certificates to NSS Store and to all profiles of Firefox

# app location: /home/ts/.ditec/dlauncher-bin/dLauncher
# cert location: /home/ts/.ditec/dlauncher/certificates/ca.crt


# rm -rf /tmp/d.launcher.run


# curl -Lo oracle_java.rpm https://javadl.oracle.com/webapps/download/AutoDL?BundleId=244057_89d678f2be164786b292527658ca1605
# sudo rpm -i ./oracle_java.rpm




# # Download Qt 4
# git clone --depth 1 git@github.com:rochus-keller/Qt-4.8.7.git qt
# wget http://git.yoctoproject.org/cgit/cgit.cgi/meta-qt4/plain/recipes-qt4/qt4/qt4-4.8.7/0036-qt-everywhere-opensource-src-4.8.7-gcc6.patch?h=b37d8b93924b314df3591b4a61e194ff3feb5517 -O 0036-qt-everywhere-opensource-src-4.8.7-gcc6.patch
# wget https://raw.githubusercontent.com/gobolinux/Recipes/b7501a9f0ba0681f51050ffefb6f1f1fe8b93353/revisions/Qt/4.8.7-r2/01-gcc6_build_fix.patch

# # Rozbalíme a opatchujeme zdrojáky.
# tar xf qt-everywhere-opensource-src-4.8.7.tar.gz
# cd qt-everywhere-opensource-src-4.8.7
# patch -p1 < ../01-gcc6_build_fix.patch
# patch -p1 < ../0036-qt-everywhere-opensource-src-4.8.7-gcc6.patch

# Skompilujeme

# CFLAGS="-pipe -O1 -fPIC -fpermissive" CXXFLAGS="-pipe -O1 -fPIC -fpermissive -std=gnu++11" ./configure -prefix /usr/local/eid -accessibility -no-webkit -qt-zlib -qt-libtiff -qt-libpng -qt-libmng -qt-libjpeg -no-gtkstyle -no-sse3 -no-sse4.1 -no-sse4.2 -fast -opensource -no-javascript-jit -no-scripttools -script -declarative -no-declarative-debug -confirm-license -nomake "examples demos docs"
# make

# a nainštalueme (pod rootom).

# make install