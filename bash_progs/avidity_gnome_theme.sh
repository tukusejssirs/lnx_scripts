#!/bin/bash

# Install Avidity GNOME 3 theme


sudo dnf -y install gtk-murrine-engine
git clone -b Avidity git@github.com:rtlewis88/rtl88-Themes.git "$XDG_GIT_DIR/others/avidity_gnome_theme"
sudo ln -s "$XDG_GIT_DIR/others/avidity_gnome_theme/Avidity-Grape-3.36" /usr/share/themes/Avidity
gsettings set org.gnome.shell.extensions.user-theme name 'Avidity'