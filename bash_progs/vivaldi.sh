#!/bin/bash

# Download and install Vivaldi Snapshot on RHEL-like systems


sudo dnf -y config-manager --add-repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo
sudo dnf -y install vivaldi-snapshot