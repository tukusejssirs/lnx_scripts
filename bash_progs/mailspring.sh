#!/bin/bash

# This script installs Mailspring


temp='/dev/shm'
sudo dnf -y install redhat-lsb-core libXScrnSaver
curl -sLo "$temp/mailspring.rpm" 'https://updates.getmailspring.com/download?platform=linuxRpm'
sudo dnf -y install "$temp/mailspring.rpm"
# CentOS 8: If nothing provides `libappindicator`, you can force install is using the following command
# sudo rpm -i --nodeps  ${temp}/mailspring.rpm
rm "$temp/mailspring.rpm"