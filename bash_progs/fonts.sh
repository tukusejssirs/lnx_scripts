#!/bin/bash

# This script installs select fonts (Ubuntu, Libertinus and Linux Libertine G). By default it installs all fonts.

# dependencies: git p7zip wget curl


# Default options
TEMP='/dev/shm'
UBUNTU=1
LIBERTINUS=1
LINLIBG=1
EZRA=1
SBL_HEBREW=1
JUNICODE=1

# Source $HOME/.config/user-dirs.dirs to be able to use the variables in shell
source "$HOME/.config/user-dirs.dirs"

# In case `user-dirs.dirs` does not define XDG_GIT_DIR variable
if [ ! "$XDG_GIT_DIR" ]; then
	XDG_GIT_DIR='/git'
fi

# If no option is provided, install all fonts
if [ ! "$@" ]; then
	set -- '-a'
fi

# Call getopt to validate the provided input
if ! OPTIONS="$(getopt -o a,e,h,j,l,ll,s,u --long all,ezra,help,junicode,libertinus,linlibg,sbl_hebrew,ubuntu -- "$@")"; then
	# Short help message
	echo 'ERROR: Incorrect options provided.'
	exit 1
fi
eval set -- "$OPTIONS"

while true; do
	case "$1" in
		--all|-a)
			UBUNTU=1
			LIBERTINUS=1
			LINLIBG=1
			EZRA=1
			SBL_HEBREW=1
		;;
		--ubuntu|-u)
			UBUNTU=1
			LIBERTINUS=0
			LINLIBG=0
			EZRA=0
			SBL_HEBREW=0
		;;
		--libertinus|-l)
			UBUNTU=0
			LIBERTINUS=1
			LINLIBG=0
			EZRA=0
			SBL_HEBREW=0
		;;
		--linlibg|-ll)
			UBUNTU=0
			LIBERTINUS=0
			LINLIBG=1
			EZRA=0
			SBL_HEBREW=0
		;;
		--ezra|-e)
			UBUNTU=0
			LIBERTINUS=0
			LINLIBG=0
			EZRA=1
			SBL_HEBREW=0
		;;
		--sbl_hebrew|-s)
			UBUNTU=0
			LIBERTINUS=0
			LINLIBG=0
			EZRA=1
			SBL_HEBREW=1
		;;
		--junicode|-j)
			UBUNTU=0
			LIBERTINUS=0
			LINLIBG=0
			EZRA=1
			SBL_HEBREW=1
		;;
		--help|-h)
			# Show long help message
			# TODO
			echo "Usage: ${FUNCNAME[0]} [-a | --all] [-u | --ubuntu] [-l | --libertinus] [-ll | --linlibg] [-e | --ezra] [-h | --help]"
			exit 0
		;;
		--)
			shift
			OPT_REST="$*"
			break
		;;
	esac

	shift
done

if [ "$OPT_REST" ]; then
	echo "WARNING: $OPT_REST are ignored." 1>&2
fi

# Libertinus
if [ "$LIBERTINUS" = 1 ]; then
	echo -n 'Installing Libertinus font ... '

	# Get URL of the ZIP of the latest Libertinus release
	url_zip="$(curl -s https://api.github.com/repos/alerque/libertinus/releases/latest | jq -r '.assets[] | select(.content_type == "application/zip").browser_download_url')"

	# Download the ZIP
	curl -sLo "$TEMP/libertinus.zip" "$url_zip"

	# Create the font folder
	sudo rm -rf /usr/share/fonts/truetype/libertinus
	sudo mkdir -p /usr/share/fonts/truetype/libertinus

	# Extract the OTF files only
	sudo 7za e -i\!Libertinus-*/static/OTF/*.otf -o/usr/share/fonts/truetype/libertinus "$TEMP/libertinus.zip" > /dev/null

	# Change the permissions
	sudo chmod -R 755 /usr/share/fonts/truetype/libertinus

	# Remove the ZIP
	rm -rf "$TEMP/libertinus.zip"

	echo 'done!'
fi

# Linux Libertine G
if [ "$LINLIBG" = 1 ]; then
	echo -n 'Installing Linux Libertine G font ... '
	sudo mkdir -p /usr/share/fonts/truetype/linlibg
	wget -qO "$TEMP/linlibg.zip" http://www.numbertext.org/linux/e7a384790b13c29113e22e596ade9687-LinLibertineG-20120116.zip
	sudo 7za e -i\!LinLibertineG/*.ttf -o/usr/share/fonts/truetype/linlibg "$TEMP/linlibg.zip" > /dev/null
	rm "$TEMP/linlibg.zip"
	sudo chmod -R 755 /usr/share/fonts/truetype/linlibg
	echo 'done!'
fi

# Ubuntu
if [ "$UBUNTU" = 1 ]; then
	echo -n 'Installing Ubuntu font ... '
	sudo mkdir -p /usr/share/fonts/truetype/ubuntu
	wget -O "$TEMP/ubuntu.zip" https://assets.ubuntu.com/v1/0cef8205-ubuntu-font-family-0.83.zip
	sudo 7za e -i\!ubuntu-font-family-0.83/*.ttf -o/usr/share/fonts/truetype/ubuntu $TEMP/ubuntu.zip
	rm "$TEMP/ubuntu.zip"
	sudo chmod -R 755 /usr/share/fonts/truetype/ubuntu
	echo 'done!'
fi

# Ezra SIL
if [ "$EZRA" = 1 ]; then
	echo -n 'Installing Ezra SIL font ... '
	EZRA_URL="$(curl -s https://software.sil.org/ezra/ | grep -o 'https://software.sil.org/downloads/r/ezra/EzraSIL-[0-9.]*-source.zip')"
	curl -so "$TEMP/ezra.zip" "$EZRA_URL"
	sudo 7za e -i\!EzraSIL*/*.ttf -o/usr/share/fonts/truetype/ezra_sil "$TEMP/ezra.zip"
	rm "$TEMP/ezra.zip"
	sudo chmod -R 755 /usr/share/fonts/truetype/ezra_sil
	echo 'done!'
fi

# SBL Hebrew
if [ "$SBL_HEBREW" = 1 ]; then
	echo -n 'Installing SBL Hebrew font ... '
	sudo mkdir /usr/share/fonts/truetype/sbl_hebrew
	sudo curl -so /usr/share/fonts/truetype/sbl_hebrew/sbl_hebrew.ttf http://www.sbl-site.org/Fonts/SBL_Hbrw.ttf
	sudo chmod -R 755 /usr/share/fonts/truetype/sbl_hebrew
	echo 'done!'
fi

# Junicode
if [ "$JUNICODE" = 1 ]; then
	echo -n 'Installing Junicode font ... '
	sudo mkdir /usr/share/fonts/truetype/junicode
	curl -sLo "$TEMP/junicode.zip" https://sourceforge.net/projects/junicode/files/latest/download
	sudo 7za e -i\!*.ttf -o/usr/share/fonts/truetype/junicode "$TEMP/junicode.zip" > /dev/null
	sudo chmod -R 755 /usr/share/fonts/truetype/junicode
	echo 'done!'
fi

# Force re-build font information cache files
sudo fc-cache -f