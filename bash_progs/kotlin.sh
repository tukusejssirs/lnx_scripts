#!/bin/bash

# Kotlin installation



# Download and install the SDK
curl -s https://get.sdkman.io | bash

# Source the SDKman init
source "${HOME}/.sdkman/bin/sdkman-init.sh"

# Install Kotlin
sdk install kotlin