#!/bin/bash

# This script installs libtool from source



sudo rpm -e --nodeps libtool
mkdir -p ${XDG_GIT_DIR}/libtool
git clone git://git.savannah.gnu.org/libtool.git ${XDG_GIT_DIR}/libtool
cd ${XDG_GIT_DIR}/libtool
./bootstrap
./configure
make && sudo make install