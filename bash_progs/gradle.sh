#!/bin/bash

# Install Gradle

# Dependencies: SDKMAN!


# Install OpenJDK Java 8
sudo dnf -y install java-1.8.0-openjdk-devel

# Install Gradle
sdk install gradle

# Don't forget to add the following path to PATH
# $USER/.sdkman/candidates/gradle/current/bin