#!/bin/bash

# Install Sublime Text 3 and crack it on RHEL-like systems
# Note: Tested on CentOS 8 and Fedora 31 and multiple builds of ST3

# Valid licence (as of 21 May 2021 for ST4):
# ----- BEGIN LICENSE -----
# Niels Joubert
# Single User License
# EA7E-1240964-450435
# 4B7D8A67 B8FA7B80 FB442BC5 758F0D9D
# 2095B776 23092E23 201DC0A2 A24557EB
# 59790847 5AB3310D 52C58EEE 38C85685
# 515109BC AAFA47F6 C877EC57 C32BCD0D
# 2DDC69AC DA1FF1A3 7FAD0DBF A6DEBE45
# A530043B DE3A9E22 3AE1F065 6BD0901F
# 3CF6D11E 2F0C277E 7D8087B9 B576231D
# 2732AC98 BC39E891 50561580 D004EED7
# ------ END LICENSE ------


# Add the ST repo
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

# Install Sublime Text 3
sudo dnf -y install sublime-text

# Variables
subl_ver=$(subl --version | grep -o '[0-9]*$')
subl_path='/opt/sublime_text/sublime_text'
url="https://cynic.al/warez/$(curl -s https://cynic.al/warez/ | grep -o "sublime-text/${subl_ver}-linux/sublime_text_linux${subl_ver}cracked")"

# Crack Sublime Text 3
sudo mv ${subl_path}{,.bak}
sudo curl -so "$subl_path" "$url"
sudo chmod a+x "$subl_path"

# Install the dummy licence
curl -so ~/.config/sublime-text-3/Local/License.sublime_license https://cynic.al/warez/sublime-text/st_dummy_licence.txt