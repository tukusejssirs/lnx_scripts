#!/bin/bash





# Install Flathub repo
sudo systemctl restart flatpak-system-helper
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Install Sublime Text 3 and Visual Studio Code
flatpak --system -y install flathub com.sublimetext.three com.visualstudio.code


# Run a Flatpak app
# flatpak run com.sublimetext.threedrop