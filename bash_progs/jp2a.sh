#!/bin/bash

# jp2a installation (JPEG images to ASCII converter)



git clone git@github.com:Talinx/jp2a.git "${XDG_GIT_DIR}/others/jp2a"
cd "${XDG_GIT_DIR}/others/jp2a"
autoreconf -vi
./configure --with-jpeg-prefix=/usr/local --with-curl-config=$(which curl-config) --enable-curl --prefix=/opt
make -j4
sudo make -j4 install