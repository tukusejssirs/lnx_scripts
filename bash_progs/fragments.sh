#!/bin/bash

# Build and install Fragments app (torrent client)
# Note: This guide does not work yet (mainly because of `libtransmission`)


# Install dependencies
# Note: What provides `libtransmission` on Fedora 32 x86_64? `transmission` package itself?
# Note: What provdes `dht`, `libutp`?
#
# Note: First row are Fragments dependencies, second one those for Transmission
sudo dnf -y install vala meson ninja-build transmission libnatpmp-devel libevent-devel libb64-devel miniupnpc-devel libhandy1-devel \
automake autoconf libtool pkgconf intltool libcurl-devel glib2-devel libevent-devel miniupnpc-devel libappindicator-devel

# Clone the Fragments repository
git clone git@gitlab.gnome.org:World/Fragments.git "$XDG_GIT_DIR/others/fragments"

# (lib)transmission
mkdir -p "$XDG_GIT_DIR/others/fragments/submodules/transmission"
git clone --recurse-submodules git@github.com:transmission/transmission.git "$XDG_GIT_DIR/others/fragments/submodules/transmission"
# sudo apt-get install build-essential automake autoconf libtool pkg-config intltool libcurl4-openssl-dev libglib2.0-dev libevent-dev libminiupnpc-dev libappindicator-dev

cd "$XDG_GIT_DIR/others/fragments/submodules/transmission" && ./autogen.sh &&make
# Do I need to install Transmission?
# sudo make install

# Build the program
PREFIX='/opt'
cd "$XDG_GIT_DIR/others/fragments" && meson --prefix "$PREFIX" build --buildtype release --strip -Db_lto=true && ninja -C build

# Install the program
sudo ninja -C build install