#!/usr/bin/env bash

# MongoDB 5.x installation on RHEL-like systems


temp="$(mktemp -d /dev/shm/mongo.XXX)"

# Add a repo
# Note: Fedora is not officially supported by MongoDB, therefore we need to use `8Server` (as in Red Hat / CentOS 8). We might also be able to use `8` instead, but I have no idea what is the difference between `8` and `8Server` (one might be a symlink of the other).
sudo bash -c "cat << 'EOF' > /etc/yum.repos.d/mongodb-org-5.0.repo
[mongodb-org-5.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/8Server/mongodb-org/5.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-5.0.asc
EOF"

# Install MongoDB
# Note: `checkpolicy` is later used to permit access to `cgroup`.
sudo dnf -y install checkpolicy mongodb-org-server mongodb-org-shell mongodb-database-tools mongodb-org-mongos mongodb-mongosh

# Permit access to `cgroup`
# Note: This is required to determine the available memory on your system by MongoDB.
cat > "$temp/mongodb_cgroup_memory.te" <<EOF
module mongodb_cgroup_memory 1.0;
require {
  type cgroup_t;
  type mongod_t;
  class dir search;
  class file { getattr open read };
}
#============= mongod_t ==============
allow mongod_t cgroup_t:dir search;
allow mongod_t cgroup_t:file { getattr open read };
EOF

checkmodule -M -m -o "$temp/mongodb_cgroup_memory.mod" "$temp/mongodb_cgroup_memory.te"
semodule_package -o "$temp/mongodb_cgroup_memory.pp" -m "$temp/mongodb_cgroup_memory.mod"
sudo semodule -i "$temp/mongodb_cgroup_memory.pp"

rm -rf "$temp"