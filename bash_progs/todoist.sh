#!/bin/bash

# Install Todoist AppImage


sudo rm -rf /opt/todoist
sudo mkdir -p /opt/todoist
sudo setfact -Rm "u:$USER:rwx" /opt/todoist
curl -sLo /opt/todoist/todoist.appimage https://todoist.com/linux_app/appimage
chmod a+x /opt/todoist/todoist.appimage

cat << 'EOF' > /opt/bin/todoist
#!/bin/bash
DESKTOPINTEGRATION=1 /opt/todoist/todoist.appimage
EOF

chmod a+x /opt/bin/todoist