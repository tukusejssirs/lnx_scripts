#!/bin/bash

# This script installs driver for Samsung M332x-382x-402x printer



temp='/dev/shm'
curl -sLo "$temp/samsung_driver.tar.gz" https://ftp.hp.com/pub/softlib/software13/printers/SS/SL-C4010ND/uld_V1.00.39_01.17.tar.gz
tar xf "$temp/samsung_driver.tar.gz" -C ${temp}

# Modify the scripts to perform unattended installation
sed -i -e '/show_nls_message_no_nl.*$/,+8d;s#show_nls_message.*"#make_hole_in_firewall "${DIST_DIR}/noarch" 2>\&1 | log_redirected_output\n\ttouch_p "$FIREWALL_FILE"#' "${temp}/uld/noarch/security.pkg"
sed -i '/if ! \[ "$UNINSTALLMODE" \]; then/,+2d;/show_nls_message_no_nl "\*\*\*\* Press.*/,+7d' "${temp}/uld/noarch/pre_install.sh"

# Run the installation script
sudo "${temp}/uld/install.sh"

# Remove the installation files
chmod -R 777 "$temp/uld"
rm -rf "$temp/samsung_driver.tar.gz" "$temp/uld"