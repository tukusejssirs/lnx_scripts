#!/bin/bash

# Install Brave Nightly browser


sudo dnf -y install dnf-plugins-core
sudo dnf -y config-manager --add-repo https://brave-browser-rpm-nightly.s3.brave.com/x86_64
sudo rpm --import https://brave-browser-rpm-nightly.s3.brave.com/brave-core-nightly.asc
sudo dnf -y install brave-browser-nightly