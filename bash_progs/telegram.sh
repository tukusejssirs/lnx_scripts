#!/bin/bash

# Install Telegram client



TEMP='/dev/shm'
curl -sLo "${TEMP}/telegram.tar.xz" https://telegram.org/dl/desktop/linux
tar xf "${TEMP}/telegram.tar.xz" -C /opt
mv /opt/{T,t}elegram
ln -s /opt/telegram/Telegram /opt/bin/telegram