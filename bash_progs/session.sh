#!/bin/bash

# This script installs latest version of Session app



# User-dependent variables
path='/opt/session'

# Create $path if it does not exist
mkdir -p "$path"

# Remove the $path folder content if any
rm -rf $path/*

# Download the files
curl -sLo "$path/session.appimage" https://getsession.org/linux

# Make it executable
chmod a+x "$path/session.appimage"

# Create a symlink in /opt/bin
sudo ln -s "$path/session.appimage" /opt/bin/session