#!/usr/bin/env bash

# Download MQTT Explorer


# Variables
shebang='#!/bin/bash'
app_name='mqttex'
app_comment='MQTT Explorer'
url_api='https://api.github.com/repos/thomasnordquist/MQTT-Explorer/releases/latest'
url_dl="$(curl -s "$url_api" | jq -r .assets[].browser_download_url | grep AppImage | sed '/\(armv7l\|i386\)\.AppImage/d')"
prefix='/opt'
path="$prefix/$app_name"
filename_app="$app_name.appimage"
filename_script="$app_name"
app_logo_url='https://raw.githubusercontent.com/thomasnordquist/MQTT-Explorer/master/icon.png'
app_logo_ext='png'

# Test if we got the download URL
if [ ! "$url_dl" ]; then
	echo "ERROR: I could not find the latest version. Check it at $url_api. And then it would be nice of you if you report this issue at https:/gitlab.com/tukusejssirs/lnx_scripts/issues (make sure there is no such report yet please)." 1>&2
	exit 1
fi

# Create folder
sudo rm -rf "$path"
sudo mkdir -p "$path"

# Download the app
sudo curl -sLo "$path/$filename_app" "$url_dl"
sudo chmod a+x "$path/$filename_app"

# Create a script to execute the app
sudo mkdir -p "$prefix/bin"
echo -e "$shebang\n$path/$filename_app &> /dev/null &" | sudo tee "$prefix/bin/$filename_script" > /dev/null
sudo chmod a+x "$prefix/bin/$filename_script"

# Download the app icon image
sudo curl -so "$path/$app_name.$app_logo_ext" "$app_logo_url"

# Create .desktop file
# Note: This makes the app accessible from Gnome Applications menu
echo -e "[Desktop Entry]\nVersion=1.0\nName=$app_name\nComment=$app_comment\nExec=$prefix/bin/$filename_script\nIcon=$path/$app_name.$app_logo_ext\nTerminal=false\nType=Application\nStartupNotify=false\n# Categories=;\n# Keywords=;\n\nX-Desktop-File-Install-Version=0.23" | sudo tee /usr/share/applications/$app_name.desktop > /dev/null

# Change file ownership (just in case)
sudo chown -R root:root "$path" "$prefix/bin/$filename_script"