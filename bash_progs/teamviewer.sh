#!/bin/bash

# This script installs TeamViewer on CentOS and Fedora



temp="/dev/shm"
distro_name="$(grep -Po '^ID=\K.*$' /etc/os-release)"

sudo rpm --import https://dl.tvcdn.de/download/linux/signature/TeamViewer2017.asc

if [ "$distro_name" = 'centos' ]; then
	sudo yum -y install epel-release
fi

curl -sLo ${temp}/teamviewer.x86_64.rpm https://download.teamviewer.com/download/linux/teamviewer.x86_64.rpm
sudo yum -y install ${temp}/teamviewer.x86_64.rpm
rm ${temp}/teamviewer.x86_64.rpm