#!/bin/bash

# This script downloads latest stable version of Mikrotik Routeros (main package for ARM devices)



path="$XDG_DOWNLOAD_DIR/mikrotik_routeros"
mkdir -p "$path"
url=$(curl -s https://mikrotik.com/download |grep -io "https://download.mikrotik.com/routeros/[0-9.]*/routeros-arm-[0-9.]*npk" | sort -V | tail -1)
version=$(echo $url | grep -oP "/\K[0-9.]*")
filename="routeros-arm-$version.npk"
curl -s $url > "$path/$filename"