#!/bin/bash

# This script installs ShadowFox theme for Firefox



mkdir -p "$XDG_PROG_BIN_DIR/shadowfox"
version=$(curl -s "https://api.github.com/repos/SrKomodo/shadowfox-updater/releases/latest" | grep -Po "^[\t ]*\"tag_name\": \"\K[v0-9.]*(?=\",$)")
case "$(uname -m)" in
	x86_64)   arch="x64" ;;
	i[0-9]86) arch="x32" ;;
esac

curl -sLo "$XDG_PROG_BIN_DIR/shadowfox/shadowfox_linux_$arch" https://github.com/SrKomodo/shadowfox-updater/releases/download/$version/shadowfox_linux_$arch
chmod a+x "$XDG_PROG_BIN_DIR/shadowfox/shadowfox_linux_$arch"
./shadowfox_linux_x64 -generate-uuids -profile-name 6w05dah0.default-nightly -set-dark-theme