#!/bin/bash

# Install `ddcutil` to allow configuration of brightness of external monitors


# Installation
sudo dnf -y install ddcutil
# Let the current user run `ddcutil` without password as root
sudo bash -c "echo '$USER ALL = (root) NOPASSWD: $(command -v ddcutil) *' >> /etc/sudoers.d/$USER"