#!/bin/bash

# Install Sublime Merge and crack it on RHEL-like systems
# Note: Tested on CentOS 8 and Fedora 31 and multiple builds of ST3
#
# crack Build 2050: https://gist.github.com/rufoa/78e45d70f560f53678853c92dae2598a
#
# FIXME: Update this script to patch it using smerge_patch.sh. src: https://gist.github.com/rufoa/78e45d70f560f53678853c92dae2598a



# Add the ST repo
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

# Install Sublime Merge
sudo dnf -y install sublime-merge

# Variables
smerge_ver=$(smerge --version | grep -o '[0-9]*$')
smerge_path='/opt/sublime_merge/sublime_merge'
url="https://cynic.al/warez/$(curl -s https://cynic.al/warez/ | grep -o "sublime-merge/${smerge_ver}-linux/sublime_merge_linux${smerge_ver}cracked")"

# Crack Sublime Merge
sudo mv ${smerge_path}o{,.bak}
sudo curl -so "$smerge_path" "$url"
sudo chmod a+x "$smerge_path"

# Install the dummy licence
curl -so ~/.config/sublime-merge/Local/License.sublime_license https://cynic.al/warez/sublime-merge/sm_dummy_licence.txt