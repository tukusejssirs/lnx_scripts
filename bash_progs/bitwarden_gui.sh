#!/bin/bash

# This script installs Bitwarden GUI


# TODO
# - create desktop file for `${XDG_PROG_BIN_DIR}/bitwarden/bitwarden-x86_64.appimage --no-sandbox` (add test if it is already included in `.bashrc` or `.bash_aliases` or any other file source by `.bashrc`)


# Password
if [ "$1" != '' ]; then
	password="$1"
	sudo_cmd="echo \"$password\" | sudo -Sp ''"

	sudo_cmd() {
		args=$@

		if [ "$1" = 'bash' ]; then
			args=$(echo "$args" | sed 's/^bash -c //')
			echo "$password" | sudo -Sp '' bash -c "$args"
		else
			echo "$password" | sudo -Sp '' "$@"
		fi
	}
else
	sudo_cmd() {
		sudo $@
	}
fi

# Set `sudo` timeout to zero
echo "Defaults:ts timestamp_timeout=0" | sudo_cmd tee -a /etc/sudoers.d/bitwarden_gui &>/dev/null

# GUI app
sudo_cmd mkdir -p /opt/bitwarden
sudo_cmd curl -sLo /opt/bitwarden/bitwarden-x86_64.appimage "https://vault.bitwarden.com/download/?app=desktop&platform=linux&variant=appimage"
sudo_cmd chmod a+x /opt/bitwarden/bitwarden-x86_64.appimage

# Set `sudo` timeout back to the original
sudo rm -rf /etc/sudoers.d/bitwarden_gui
