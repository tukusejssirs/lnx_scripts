#!/bin/bash

# Installation of guiscrcpy (a GUI wrapper of scrcpy)


# dependencies: Android SDK Tools


# Enable required repos
sudo dnf -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %centos).noarch.rpm  # ffms2-devel
sudo dnf config-manager --set-enabled PowerTools  # meson SDL2-devel
sudo rpm -ivh http://repo.okay.com.mx/centos/8/x86_64/release/okay-release-1-3.el8.noarch.rpm # libav libav-devel

# Install dependencies
# java-devel : presumably same version as Android SDK Tools require
sudo dnf -y install SDL2-devel ffms2-devel meson ninja-build gcc make ffmpeg-devel

# Clone scrcpy
git clone git@github.com:Genymobile/scrcpy.git "${XDG_GIT_DIR}/others/scrcpy"
cd "${XDG_GIT_DIR}/others/scrcpy"

# Get Android SDK Tools location
SDK_TOOLS_BIN_PATH="$(which sdkmanager)"

if [ -h $SDK_TOOLS_BIN_PATH ]; then
	# If $SDK_TOOLS_BIN_PATH is a symlink
	export ANDROID_HOME=$(ls -l "$SDK_TOOLS_BIN_PATH" | grep -Po -- '-> \K.*$' | sed 's|/tools/bin/sdkmanager||')
else
	export ANDROID_HOME=$(sed 's|/tools/bin/sdkmanager||' <<< "$SDK_TOOLS_BIN_PATH")
fi

meson --prefix /opt build --buildtype release --strip -Db_lto=true
ninja -C build
sudo ninja -C build install

# Add /opt/share/man to MANPATH
TEST_MANPATH_SCRCPY=$(grep -c '/opt/share/man' <<< "$MANPATH")

if [ -e /opt/share/man/man1 ]; then
	if [ "$TEST_MANPATH_SCRCPY" = "0" ]; then
		if [ "$MANPATH" ]; then
			export MANPATH="$MANPATH:/opt/share/man"
		else
			export MANPATH='/opt/share/man'
		fi
	fi
fi

########################################
#                                      #
# Here ends the installation of scrcpy #
#                                      #
########################################

# Install dependencies
sudo dnf -y install python36 python3-qt5 wmctrl xdotool python3-pyqt5-sip
sudo dnf -y install --enablerepo=epel-testing python3-xlib

# Clone guiscrcpy
git clone git@github.com:srevinsaju/guiscrcpy.git "${XDG_GIT_DIR}/others/guiscrcpy"

# Enter the directory
cd "${XDG_GIT_DIR}/others/guiscrcpy"

# Install the dependencies
pip install setuptools
python3 -m pip install -r requirements.txt --user  # This did not work for me at all
# pip install --upgrade setuptools gitpyphon psutil pynput pyshortcuts PyQt5.sip  # This did not work without sudo

# Build guiscrcpy
sudo python3 setup.py install --prefix=/opt

# Export
TEST_PYTHONPATH=$(grep -c '/opt/lib/python3.8/site-packages' <<< "$PYTHONPATH")

if [ -e /opt/lib/python3.8/site-packages ]; then
	if [ "$TEST_PYTHONPATH" = "0" ]; then
		if [ "$PYTHONPATH" ]; then
			export PYTHONPATH="$PYTHONPATH:/opt/lib/python3.8/site-packages"
		else
			export PYTHONPATH='/opt/lib/python3.8/site-packages'
		fi
	fi
fi

# Copy the .desktop file
# sudo cp /git/others/guiscrcpy/guiscrcpy.desktop ?????/path

cat << EOF
[Desktop Entry]
Name=guiscrcpy
Exec=python bin/guiscrcpy
Type=Application
Icon=guiscrcpy_logo
Terminal=false
Categories=Application;Development;
Keywords=Python;Screen;Mirroring;Android
StartupWMClass=guiscrcpy
EOF

# Install the fonts
sudo cp -r "${XDG_GIT_DIR}/others/guiscrcpy/guiscrcpy/ui/fonts" /usr/share/fonts/trebuchet
sudo fc-cache -f

# This is how it can be run without installation
# python ${XDG_GIT_DIR}/others/guiscrcpy/guiscrcpy

# After installation, you need to run it like this
# python guiscrcpy