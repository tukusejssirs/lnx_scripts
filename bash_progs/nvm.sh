#!/usr/bin/env bash

# Install NVM


path_settings="$(dirname "$(dirname "$(dirname "$(realpath "$0")")")")/bash/settings"

# Get XDG_GIT_DIR variable definition
if [ -n "$XDG_GIT_DIR" ]; then
	file_to_src="$path_settings/source_user_dirs_and_get_xdg_git_dir.sh"

	if [ -f "$file_to_src" ]; then
		# shellcheck disable=SC1090  # ShellCheck can't follow non-constant source. Use a directive to specify location.
		source "$file_to_src"
	fi
fi

path_installation="$XDG_GIT_DIR/others/nvm"

if [ -d "$path_installation/.git" ]; then
	# Update `nvm`
	# TODO
	# git pull
	# read -ra latest_version_tag -d '\t' <<< "$(git ls-remote -t git@github.com:nvm-sh/nvm.git | grep -v '\^{}$' | sort -V -k2 | tail -1)"  # FIXME: Using `git tag`.
	# git checkout "${latest_version_tag[1]}"
	:
else
	# Get latest version tag
	read -ra latest_version_tag -d '\t' <<< "$(git ls-remote -t git@github.com:nvm-sh/nvm.git | grep -v '\^{}$' | sort -V -k2 | tail -1)"

	# Clone `nvm`
	# shellcheck disable=SC2154  # $variable_name is referenced but not assigned.
	git clone git@github.com:nvm-sh/nvm.git -b "${latest_version_tag[1]//refs\/tags\//}" "$path_installation"
fi

# Source the files right away
# shellcheck disable=SC1091  # Not following: ./nvm.sh was not specified as input (see shellcheck -x).
# Note: I set up my `nvm` loading and BASH completion in this file.
source "$path_settings/nvm.sh"

# Install latest LTS Node.js version
nvm install --lts

# Install latest Node.js version
nvm install node

# Set the latest LTS Node.js version as default
nvm alias default lts/*