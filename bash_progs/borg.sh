#!/bin/bash

# Install Borg and BorgMatic
#
# TODO: Create a separate bash installer file from this

# sudo mkdir /git
sudo dnf -y install python3 python3-devel python3-pip python3-virtualenv openssl-devel openssl libacl-devel libacl gcc gcc-c++
sudo pip install borgbackup
sudo pip install --user --upgrade borgmatic
sudo mkdir /backup
cat << EOF > /etc/borgmatic/config.yaml
location:
	# List of source directories to backup (required). Globs and tildes are expanded.
	source_directories:
		- /home
		- /root
		- /git
		- /etc
		- /var/log

	# Paths to local or remote repositories (required). Tildes are expanded. Multiple
	# repositories are backed up to in sequence. See ssh_command for SSH options like
	# identity file or port.
	repositories:
		- /backup/e531_fedora_31.borg

# Repository storage options. See
# https://borgbackup.readthedocs.io/en/stable/usage.html#borg-create and
# https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables for
# details.
storage:
	# The standard output of this command is used to unlock the encryption key. Only
	# use on repositories that were initialized with passcommand/repokey encryption.
	# Note that if both encryption_passcommand and encryption_passphrase are set,
	# then encryption_passphrase takes precedence. Defaults to not set.
	# encryption_passcommand: secret-tool lookup borg-repository repo-name

	# Passphrase to unlock the encryption key with. Only use on repositories that were
	# initialized with passphrase/repokey encryption. Quote the value if it contains
	# punctuation, so it parses correctly. And backslash any quote or backslash
	# literals as well. Defaults to not set.
	encryption_passphrase: "2e5bf2li"

	# Number of seconds between each checkpoint during a long-running backup. See
	# https://borgbackup.readthedocs.io/en/stable/faq.html#if-a-backup-stops-mid-way-does-the-already-backed-up-data-stay-there
	# for details. Defaults to checkpoints every 1800 seconds (30 minutes).
	# checkpoint_interval: 1800

	# Specify the parameters passed to then chunker (CHUNK_MIN_EXP, CHUNK_MAX_EXP,
	# HASH_MASK_BITS, HASH_WINDOW_SIZE). See https://borgbackup.readthedocs.io/en/stable/internals.html
	# for details. Defaults to "19,23,21,4095".
	# chunker_params: 19,23,21,4095

	# Type of compression to use when creating archives. See
	# https://borgbackup.readthedocs.org/en/stable/usage.html#borg-create for details.
	# Defaults to "lz4".
	# compression: lz4

	# Remote network upload rate limit in kiBytes/second. Defaults to unlimited.
	# remote_rate_limit: 100

	# Command to use instead of "ssh". This can be used to specify ssh options.
	# Defaults to not set.
	# ssh_command: ssh -i /path/to/private/key

	# Base path used for various Borg directories. Defaults to $HOME, ~$USER, or ~.
	# See https://borgbackup.readthedocs.io/en/stable/usage/general.html#environment-variables for details.
	# borg_base_directory: /path/to/base

	# Path for Borg configuration files. Defaults to $borg_base_directory/.config/borg
	# borg_config_directory: /path/to/base/config

	# Path for Borg cache files. Defaults to $borg_base_directory/.cache/borg
	# borg_cache_directory: /path/to/base/cache

	# Path for Borg security and encryption nonce files. Defaults to $borg_base_directory/.config/borg/security
	# borg_security_directory: /path/to/base/config/security

	# Path for Borg encryption key files. Defaults to $borg_base_directory/.config/borg/keys
	# borg_keys_directory: /path/to/base/config/keys

	# Umask to be used for borg create. Defaults to 0077.
	# umask: 0077

	# Maximum seconds to wait for acquiring a repository/cache lock. Defaults to 1.
	# lock_wait: 5

	# Name of the archive. Borg placeholders can be used. See the output of
	# "borg help placeholders" for details. Defaults to
	# "{hostname}-{now:%Y-%m-%dT%H:%M:%S.%f}". If you specify this option, you must
	# also specify a prefix in the retention section to avoid accidental pruning of
	# archives with a different archive name format. And you should also specify a
	# prefix in the consistency section as well.
	archive_name_format: '{hostname}-fedora_31-{now}'

	# Bypass Borg error about a repository that has been moved. Defaults to false.
	# relocated_repo_access_is_ok: true

	# Bypass Borg error about a previously unknown unencrypted repository. Defaults to
	# false.
	# unknown_unencrypted_repo_access_is_ok: true

	# Additional options to pass directly to particular Borg commands, handy for Borg
	# options that borgmatic does not yet support natively. Note that borgmatic does
	# not perform any validation on these options. Running borgmatic with
	# "--verbosity 2" shows the exact Borg command-line invocation.
	# extra_borg_options:
		# Extra command-line options to pass to "borg init".
		# init: --make-parent-dirs

		# Extra command-line options to pass to "borg prune".
		# prune: --save-space

		# Extra command-line options to pass to "borg create".
		# create: --no-files-cache

		# Extra command-line options to pass to "borg check".
		# check: --save-space

# Retention policy for how many backups to keep in each category. See
# https://borgbackup.readthedocs.org/en/stable/usage.html#borg-prune for details.
# At least one of the "keep" options is required for pruning to work. See
# https://torsion.org/borgmatic/docs/how-to/deal-with-very-large-backups/
# if you'd like to skip pruning entirely.
retention:
	# Number of daily archives to keep.
	keep_daily: 7

	# When pruning, only consider archive names starting with this prefix.
	# Borg placeholders can be used. See the output of "borg help placeholders" for
	# details. Defaults to "{hostname}-". Use an empty value to disable the default.
	prefix: "{hostname}-"
EOF
sudo borgmatic init --encryption repokey
# Export the borg key
mkdir /root/.borg
sudo borg key export /backup/e531_fedora_31.borg/ /root/.borg/e531_fedora_31_key
# Initial backup
sudo borgmatic --verbosity 1