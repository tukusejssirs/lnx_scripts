#!/bin/bash

# Android SDK Tools installation



# Variables
TEMP='/dev/shm'
INST_DIR='/opt/android_sdk_tools'

# Install dependencies
sudo dnf -y install java-1.8.0-openjdk zlib.i686 ncurses-libs.i686 bzip2-libs.i686 glibc-devel.i686

# Get the URL
URL="$(curl -s https://developer.android.com/studio/index.html | grep -o 'https[^"]*sdk-tools-linux.*zip')"

# Download the archive
curl -sLo "${TEMP}/sdk-tools-linux.zip" "$URL"

# Create installation folder
sudo mkdir -p "${INST_DIR}"

# Remove the old installation
sudo rm -rf "${INST_DIR}"/*

# Decompress the archive
# unzip "${TEMP}/platform-tools-latest-linux.zip" -d /opt
sudo unzip -qq "${TEMP}/sdk-tools-linux.zip" -d "${INST_DIR}"

# Install the latest version of the platforms tools
# Note: For options, see https://developer.android.com/studio/command-line/sdkmanager
echo y | "${INST_DIR}/tools/bin/sdkmanager" platform-tools --channel=0 &>/dev/null

# Create symblinks of all executables to /opt/bin
BIN_SDK="$(ls -l "${INST_DIR}/tools" | grep '^[^d][^ ]*x' | grep -o '[^ ]*$' | tr '\n' ' ')"
BIN_SDK_BIN="$(ls -l "${INST_DIR}/tools/bin" | grep '^[^d][^ ]*x' | grep -o '[^ ]*$' | tr '\n' ' ')"
BIN_PT="$(ls -l "${INST_DIR}/platform-tools" | grep '^[^d][^ ]*x' | grep -o '[^ ]*$' | sed '/\.conf$\|sqlite3/d' | tr '\n' ' ')"

for file in $BIN_SDK; do
	sudo rm -rf /opt/bin/$file
	sudo ln -s "${INST_DIR}/tools/$file" /opt/bin/$file
done

for file in $BIN_SDK_BIN; do
	sudo rm -rf /opt/bin/$file
	sudo ln -s "${INST_DIR}/tools/bin/$file" /opt/bin/$file
done

for file in $BIN_PT; do
	sudo rm -rf /opt/bin/$file
	sudo ln -s "${INST_DIR}/platform-tools/$file" /opt/bin/$file
done

rm -rf "${TEMP}/sdk-tools-linux.zip"