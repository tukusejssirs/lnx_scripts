#!/bin/bash

# This script install Remmina


# src: https://gitlab.com/Remmina/Remmina/-/wikis/Compilation/Compilation-guide-for-RHEL


# Variables
distro_name="$(grep -Po '^ID=\K.*$' /etc/os-release)"
distro_ver=$(rpm -E %$distro_name)

# Make sure not `remmina` or `freerdp` is installed
sudo yum -y remove freerdp remmina

# Add required repos
# rpmfusion-free-updates: required for `ffmpeg-devel`
if [ "$distro_name" = 'centos' ]; then
	# epel: required for `openjpeg2-devel`, `openssl-devel`
	# PowerTools repo is needed on CentOS 8+
	# getpagespeed repo is needed on CentOS 8+: required for `libgnome-keyring-devel`, `libappindicator-devel`
	# okay repo is needed on CentOS 8+; required for `avahi-ui-devel`
	sudo yum -y install epel-release https://download1.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %centos).noarch.rpm
	if [ $(rpm -E %centos) = 8 ]; then
		sudo dnf config-manager --enable PowerTools
		sudo rpm -ivh http://repo.okay.com.mx/centos/8/x86_64/release/okay-release-1-3.el8.noarch.rpm
		sudo dnf config-manager --disable okay
	fi
elif [ "$distro_name" = 'fedora' ]; then
	sudo rpm --import https://raw.githubusercontent.com/UnitedRPMs/unitedrpms/master/URPMS-GPG-PUBLICKEY-Fedora
	sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-${distro_ver}.noarch.rpm  # https://github.com/UnitedRPMs/unitedrpms/releases/download/15/unitedrpms-${distro_ver}-15.fc${distro_ver}.noarch.rpm
	# sudo dnf config-manager --disable unitedrpms

	# sudo dnf config-manager --enable fedora-cisco-openh264
fi

# Update yum cache and packages
sudo yum -y upgrade

# FreeRDP dependencies
# TODO: official docs: gcc cmake ninja-build openssl-devel libX11-devel libXext-devel libXinerama-devel libXcursor-devel libXi-devel libXdamage-devel libXv-devel libxkbfile-devel alsa-lib-devel cups-devel ffmpeg-devel glib2-devel libusb-devel
# TODO: add (at least on CentOS 8, probably on CentOS 7 too; otherwise build fails): make gcc-c++ pulseaudio-libs-devel libXrandr-devel
# TODO: recommended (for man pages): libxslt libxslt-devel docbook-style-xsl
# TODO: recommended (for multimedia redirection, audio and video playback): gstreamer1-devel gstreamer1-plugins-base-devel
# TODO: recommended (): xorg-x11-server-utils
# TODO: recommended (required by virtual:world): cairo-devel
# Remmina dependencies (without FreeRDP and its dependencies) (c8)
# cmake3 gtk3-devel libgcrypt-devel libssh-devel libxkbfile-devel openjpeg2-devel gnutls-devel libgnome-keyring-devel avahi-ui-devel avahi-ui-gtk3 libvncserver-devel $vte libappindicator-devel libappindicator-gtk3 libappindicator-gtk3-devel libSM-devel webkitgtk4-devel json-glib-devel libsoup-devel libsodium libsodium-devel libXtst-devel xmlto harfbuzz-devel pango-devel atk-devel libsecret-devel
if [ "$distro_name" = 'fedora' ]; then
	pkgs='unitedrpms avahi-ui-devel openh264 openh264-devel'  # libopenh264-devel libx264-devel
fi

sudo yum -y install alsa-lib-devel atk-devel avahi-ui-gtk3 cairo-devel cmake3 cups-devel docbook-style-xsl ffmpeg-devel gcc gcc-c++ git glib2-devel gnutls-devel gstreamer1-devel gstreamer1-plugins-base-devel gtk3-devel harfbuzz-devel json-glib-devel libappindicator-gtk3 libappindicator-gtk3-devel libgcrypt-devel libsecret-devel libSM-devel libsodium libsodium-devel libsoup-devel libssh-devel libusb-devel libvncserver-devel libX11-devel libXcursor-devel libXdamage-devel libXext-devel libXi-devel libXinerama-devel libxkbfile-devel libXrandr-devel libxslt libxslt-devel libXtst-devel libXv-devel make ninja-build openjpeg2-devel openssl-devel pango-devel pulseaudio-libs-devel vte291 vte291-devel webkitgtk4-devel xmlto xorg-x11-server-utils $pkgs

if [ "$distro_name" = 'centos' ] && [ "$distro_ver" = 8 ]; then  # On CentOS 8
  sudo dnf -y install --enablerepo=okay avahi-ui-devel openh264 libopenh264-devel libx264-devel
fi

# Remove freerdp-x11 package and all packages containing the string remmina in the package name
# Note: no such packages are available
# sudo rpm -e remmina remmina-devel remmina-plugins-gnome remmina-plugins-nx remmina-plugins-rdp remmina-plugins-telepathy remmina-plugins-vnc remmina-plugins-xdmcp

# If you install FreeRDP and Remmina to `/opt`, you need to add `/opt/bin` to PATH
TEST_PATH_SCRCPY=$(grep -c '/opt/bin' <<< "$PATH")

if [ -e /opt/bin ]; then
	if [ "$TEST_MANPATH_SCRCPY" = "0" ]; then
		if [ "$PATH" ]; then
			export PATH="$PATH:/opt/bin"
		else
			export PATH='/opt/bin'
		fi
	fi
fi

# It might be a good idea to export PATH when Bash starts
# echo 'export PATH="$PATH:/opt/bin"' >> ${HOME}/.bashrc

# Clone FreeRDP and Remmina repos
mkdir -p ${XDG_GIT_DIR}/others/{freerdp,remmina}
git clone https://github.com/FreeRDP/FreeRDP.git ${XDG_GIT_DIR}/others/freerdp && git checkout $(git tag | sort -V | grep -v 'beta\|rc' | tail -1)
git clone https://gitlab.com/Remmina/Remmina.git ${XDG_GIT_DIR}/others/remmina

# Build FreeRDP
mkdir ${XDG_GIT_DIR}/others/freerdp/build
cd ${XDG_GIT_DIR}/others/freerdp/build
# Note: In the following line, the `DWITH_PULSE=ON` option needs to be included
cmake3 -DCMAKE_BUILD_TYPE=Debug -DWITH_SSE2=ON -DWITH_PULSE=ON -DWITH_CUPS=on -DWITH_WAYLAND=off -DCMAKE_INSTALL_PREFIX:PATH=/usr -DWITH_OPENH264=ON -DWITH_X264=OFF -Wno-dev ..
make && sudo make install && cd .. && rm -rf ${XDG_GIT_DIR}/others/freerdp/build

# Make your system dynamic loader aware of the new libraries added by FreeRDP installation
sudo ldconfig

# You can test FreeRDP by connecting to an RDP host
# xfreerdp +clipboard /sound:rate:44100,channel:2 /v:hostname /u:username

# Build Remmina
mkdir ../../remmina/build
cd ../../remmina/build
# Note: `-DCMAKE_INSTALL_LIBDIR=/usr/lib64` is not required to successfully run Remmina, but `/usr/lib64` is the proper location for the libraries; again, it is not required at all when Remmina is installed to `/usr`
cmake3 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=/opt -DCMAKE_INSTALL_LIBDIR=/usr/lib64 \
  -DCMAKE_PREFIX_PATH=/opt --build=build ..




# cmake3 -DCMAKE_BUILD_TYPE=Debug -DFREERDP_LIBRARY=/usr/lib64/freerdp2 -DFREERDP_INCLUDE_DIR=/opt/include -DFREERDP_CLIENT_LIBRARY=/usr/lib64/libfreerdp-client2.so.2 -DFREERDP_WINPR_LIBRARY=/usr/lib64/libwinpr2.so.2 -DWINPR_INCLUDE_DIR=/opt/include/winpr3/winpr -DWITH_SSE2=ON -DWITH_PULSE=ON -DWITH_CUPS=on -DWITH_WAYLAND=off -DCMAKE_INSTALL_LIBDIR=/usr/lib64 -DCMAKE_INSTALL_PREFIX:PATH=/opt -DWITH_OPENH264=ON -DWITH_X264=OFF ..

# cmake3 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=/opt -DCMAKE_INSTALL_LIBDIR=/usr/lib64 -DCMAKE_PREFIX_PATH=/opt --build=build ..

# cmake3 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=/opt -DCMAKE_INSTALL_LIBDIR=/usr/lib64 -DCMAKE_PREFIX_PATH=/usr --build=build ..
# FREERDP_INCLUDE_DIR=/usr/lib64
# cmake3 -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=/opt -DCMAKE_INSTALL_LIBDIR=/usr/lib64 -DFREERDP_LIBRARY=/usr/lib64/freerdp2 -DFREERDP_INCLUDE_DIR=/opt/include -DCMAKE_PREFIX_PATH=/usr -Wno-dev --build=build ..


make && sudo make install && cd .. && rm -rf ${XDG_GIT_DIR}/others/remmina/build

# If Remmina is installed to `/opt`, we need to create some symlinks
sudo mkdir -p /usr/share/icons/hicolor/scalable/panel /usr/share/icons/hicolor/apps

for old in /opt/share/applications/*desktop $(find /opt/share/icons -type f | grep 'svg$\|png$'); do
	new="${old/opt/usr}"
	sudo ln -s $old $new 2> /dev/null
done

# Unistall
# sudo rm /etc/ld.so.conf.d/freerdp_devel.conf /usr/local/bin/remmina /usr/local/bin/xfreerdp
# sudo ldconfig
# rm /usr/bin/remmina /usr/lib64/remmina /usr/include/remmina /usr/share/remmina /usr/share/man/man1/remmina.1 /usr/bin/xfreerdp /usr/share/man/man1/xfreerdp.1 /usr/share/man/man1/xfreerdp.1.gz