#!/bin/bash

# Patch Sublime Merge Build 2056 on Linux

# src: https://gist.github.com/rufoa/78e45d70f560f53678853c92dae2598a

# Note: For future reference: change `patch_offset` parameters from @Rufio's version to my version:
# find    : patch_offset ([^ ]+) ([^ ]+) ([^ \n]+)$
# replace : patch_offset "\$smerge_file" $1 $2 $3

# TODO: Add support for 2059 build (src: https://gist.github.com/rufoa/78e45d70f560f53678853c92dae2598a).


set -o errexit
set -o nounset
set -o pipefail

patch_offset() {
	local -r filename="$1"
	local -r offset="$2"
	local -r byte_count="$3"
	local -r hex_value="$4"
	printf '%s' "$hex_value" | sudo dd of="$filename" bs=1 seek=$((offset)) count="$byte_count" conv=notrunc status=none
}

revert() {
	err=$?
	local -r filename="$1"
	local -r backup="$2"
	trap - TERM INT EXIT

	if mv "$backup" "$filename"; then
		echo "[WARN] We reverted the modifications to $smerge_file because of an error." 1>&2
	else
		echo "[ERROR] We failed to revert the modifications to $smerge_file because of an error." 1>&2
	fi

	exit $err
}

trap revert TERM INT EXIT

# FIXME: Add help
short_help() {
	echo -e 'Usage'
	echo -e '\trelease [--staged | -g] [--type TYPE | -t TYPE] [--dev DEV_SUFFIX | -d DEV_SUFFIX] [--dry-run | -s] version_type>'
	echo -e '\trelease [--help | -h]'
	echo
	echo -e 'Description'
	echo -e '\tCreate a new release using data from changelog of the current repo in the currently checked out branch.'
	echo
	echo -e 'Note that no option is required to release a new version, however the version_type (the only parameter without option) is required when releasing a new semver version. It can be one of major, minor of patch. Note that it is ignored for ` releases.'
	echo
	echo -e 'Options'
	echo -e '\tMandatory arguments to long options are mandatory for short options too.'
	echo
	echo -e '\t-e, --dev=DEV_SUFFIX'
	echo -e '\t\trelease a dev version (appends -DEV_SUFFIX.NUMBER)'
	echo
	echo -e '\t\tSupported DEV_SUFFIX values: alpha, beta, canary, dev, pre, rc.'
	echo
	echo -e '\t-g, --staged'
	echo -e '\t\tcommit staged files only'
	echo
	echo -e '\t\tBy default, all files are committed.'
	echo
	echo -e '\t-s, --dry-run'
	echo -e '\t\tgenerate and output the commit message to STDOUT and exit (nothing is done with files and no release is made'
	echo
	echo -e '\t-h, --help'
	echo -e '\t\tdisplay this help and exit'
	echo
	echo -e '\t-t, --type=TYPE'
	echo -e '\t\tversion type'
	echo
	echo -e '\t\tIt can be either semver or date. semver version is in the format major.minor.patch (no leading zeros) and date version in the format year.month.number, where year is last two digits of the current year, month is two-digit month (with a leading zero when needed) and number is the release number of the month (starting with 1).'
	echo
	echo -e '\t\tBy default, we use semantic versioning (semver).'
	echo
	echo -e 'Examples'
	echo
	echo -e '\tNew major semver release (both commands do the same thing):'
	echo -e '\t\trelease major'
	echo -e '\t\trelease -t semver major'
	echo
	echo -e '\tNew date release:'
	echo -e "\t\trelease -t date"
	echo
	echo -e '\tDry run to simulate a new patch semver version (it outputs the version and commit message):'
	echo -e "\t\trelease -s patch"
	echo -e "\t\trelease -st semver patch"
	echo
	echo -e 'Exit/return codes'
	echo -e '\t0    Success'
	echo -e '\t1    Invalid options provided'
	echo -e '\t2    Invalid versioning type provided'
	echo -e '\t3    Too many parameters provided for SemVer'
	echo -e '\t4    The folder is not in a Git repository'
	echo -e '\t5    Cannot find any changelog file in the repository root folder'
	echo -e '\t6    More than one changelog file is found in the repository root folder'
	echo -e '\t7    Last version type is lexically after RELEASE TYPE'
	echo -e '\t8    Invalid dev version suffix provided'
	echo -e '\t9    No release type provided for SemVer'
	echo -e '\t10   There are no staged changes in the changelog file'
	echo -e '\t11   There are no changes in the changelog file'
}

patch_smerge() {
	# Variables
	local smerge_file="${1:-/opt/sublime_merge/sublime_merge}"
	local -r sha_valid=('44c6587efd8dd8faf84a47504dccf4ab36d00ca47e90f60879f749669fbdc897' '296872fe3bcb5c35908918c59fc78f35b8325547824dc773b1a52cc8cfd17061')
	local sha_matches='false'
	local sha_current
	local licence='----- BEGIN LICENSE -----
	TEAM RUFIO
	Unlimited User License
	E52D-666666
	487EE6F0309908F702DDD52AFCD99A6A
	6EE14CF8A2D42271B4FC0991BBF93ADC
	FAA9075C436B3796669194A2F36CAAEF
	B251155329EC2E434FD28B4A21BE68CC
	955D306EE9ED843C5E98B1577D02DEAA
	1F4E872AE6495CD5E3B1DA55D5ACD2B2
	2EA4110FB800F21AA3EC2E3902589BCF
	7281A19C2DFF0CEE4AEA5DDD1E6DF893
	----- END LICENSE -----'

	# Check if the SHA 256 checksum matches
	sha_current="$(sha256sum "$smerge_file" | cut -d' ' -f1)"

	for s in "${sha_valid[@]}"; do
		if [ "$sha_current" = "$s" ]; then
			echo '[INFO] SHA 256 checksum matches.' 1>&2
			sha_matches='true'
			break
		fi
	done

	if [ "$sha_matches" = 'true' ]; then
		echo '[INFO] Patching Sublime Merge started ...' 1>&2

		# Backup the original file
		sudo cp "$smerge_file"{,.bak}

		# Swap public key
		patch_offset "$smerge_file" 345216 160 '\x30\x81\x9d\x30\x0d\x06\x09\x2a\x86\x48\x86\xf7\x0d\x01\x01\x01\x05\x00\x03\x81\x8b\x00\x30\x81\x87\x02\x81\x81\x00\xc9\xda\x03\xe0\xc6\x33\xce\x4e\x55\xf5\xbf\x60\xf9\xb1\xb0\xda\xd6\x64\xc0\x5d\x03\xca\x7e\x21\xa6\x57\xd2\x17\xa9\x58\x9d\x51\x73\x30\x0d\xb5\x34\x13\x08\xab\x55\x5c\x22\x26\x6c\x03\x0d\xbe\x3c\x80\xb4\x59\xe9\xee\xad\x45\x8f\xa1\x38\x37\x69\xcd\x51\xa2\x19\xa4\x41\x4b\x8c\x0a\x1e\x51\x7f\x58\xc8\x33\xa5\x3c\x15\xc8\x24\xcd\xcc\x94\xb8\x5a\xfe\x44\x12\xa0\x18\x34\x63\x87\x72\x11\x11\x0b\x0c\x12\x44\x76\xec\x60\x13\xc0\x0d\x7e\xf1\x48\xbf\x8a\xce\x10\x02\x79\x45\x31\xf5\x3a\x34\xf2\x56\x6e\x71\xc7\xf4\x45\x02\x01\x11'

		# Swap xor key
		patch_offset "$smerge_file" 3804370 1 '\x00'

		# Fix SHA-2 check
		patch_offset "$smerge_file" 3806511 1 '\xf9'
		patch_offset "$smerge_file" 3806514 1 '\x0e'

		# Disable online licence check
		patch_offset "$smerge_file" 3809430 1 '\xc3'

		# Insert licence
		rm -rf "$HOME/.config/sublime-merge/Local/License.sublime_license"
		echo "$licence" > "$HOME/.config/sublime-merge/Local/License.sublime_license"

		echo '[INFO] Sublime Merge was successfully patched.' 1>&2
	else
		echo '[ERROR] SHA 256 checksum does not match, quitting.' 1>&2
		exit 1
	fi
}

# Default option values
smerge_file='/opt/sublime_merge/sublime_merge'

# Call getopt to validate the provided input
if ! GETOPT_OPTIONS="$(getopt -o b:f:h --long backup:,file:,help -- "$@")"; then
	echo '[ERROR] Invalid options provided.' 1>&2
	return 1
fi

eval set -- "$GETOPT_OPTIONS"

while true; do
	case "$1" in
		--backup|-b)
			shift
			smerge_file_backup="$1"
		;;
		--file|-f)
			shift
			smerge_file="$1"

			# Check if the $smerge_file exists and is writable
			if [ ! -w "$smerge_file" ]; then
				echo "$smerge_file does not exist or is not writable. Ensure the path is correct and that you have write permission." 1>&2
				exit 2
			fi
		;;
		--help|-h)
			short_help
			return 0
		;;
		--)
			shift
			# Note: The rest of the options are ignored unless we find a good way to use them.
			# OTHER_OPTIONS=("$@")
			break
		;;
	esac
	shift
done

if [ ! "$smerge_file_backup" ] || [ "$smerge_file_backup" = "$smerge_file" ]; then
	smerge_file_backup="$smerge_file.bak"
fi

patch_smerge "$1"

trap - TERM INT EXIT