#!/bin/python

"""Display Sublime Text 3 project name in status bar
"""

# src: https://forum.sublimetext.com/t/displaying-project-name-on-the-rite-side-of-the-status-bar/24721/2

# We need to disable `import-error` pylint option because it cannot find `sublime` `sublime_plugin` plugins
# pylint: disable=import-error
import os
import re
import sublime
import sublime_plugin


def plugin_loaded ():
	"""Show project in all views of all windows

	Args:
		param1 (int): The first parameter.
	"""

	for window in sublime.windows ():
		for view in window.views ():
			show_project (view)

def show_project(view):
	"""Show project in specified view of window

	Args:
		view (class): View of window
	"""

	# Check if there is a project file defined
	project_file = view.window ().project_file_name ()
	if project_file is not None:
		# Get the project filename without path or extension, however with any subdirectories
		regex = re.compile(r'^.*Packages/User/Projects/(.*)\.sublime-project$')
		project_name = regex.match(project_file).groups()[0]

		view.set_status ("00ProjectName", "[" + project_name + "]")

class ProjectInStatusbar(sublime_plugin.EventListener):
	"""Display the current project name in the status bar

	Args:
		sublime_plugin.EventListener (eventListerner): VEvent listener.
	"""

	def on_new(self, view):
		"""Display project name when a new file is created

		Args:
		    view (class): View of window
		"""

		show_project (view)

	def on_load(self, view):
		"""Display project name when an existing file is loaded

		Args:
		    view (class): Description
		"""
		show_project (view)

	# When you use File > New view into file on an existing file
	def on_clone(self, view):
		"""Display project name when a new view into file is created

		Args:
		    view (class): Description
		"""

		show_project (view)